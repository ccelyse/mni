<?php

namespace App\Http\Controllers;

use AfricasTalking\SDK\AfricasTalking;
use App\Archives;
use App\archivessongs;
use App\ArtistAccounts;
use App\Attractions;
use App\BroadCast;
use App\BroadCastComment;
use App\BroadcastTopic;
use App\CommentStatus;
use App\CompanyCategory;
use App\CompetitionCardTransactions;
use App\Competitions;
use App\CompetitionSlider;
use App\CompetitionTransactions;
use App\CompVotes;
use App\Contestants;
use App\Countries;
use App\Funds;
use App\FundsSliders;
use App\FundTransaction;
use App\Groups;
use App\Hireaguide;
use App\HomeSlider;
use App\JoinMember;
use App\LevelsTrack;
use App\MomoTransaction;
use App\MusicStakeHolders;
use App\Nominee;
use App\PlaylistCategory;
use App\PlaylistInfo;
use App\Post;
use App\PromotionTransactions;
use App\Property;
use App\PropertyGallery;
use App\SongInfo;
use App\SongNominations;
use App\User;
use App\Votes;
use App\voting_comp_status;
use App\VotingCardTransactions;
use App\VotingStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
require_once('./Class_MR_SMS_API.php');

class BackendController extends Controller
{
    public function SignIn_(Request $request){
        $all = $request->all();
        if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {

            $user_get_role = User::where('email', $request['email'])->value('role');
//            dd($user_get_role);

            switch ($user_get_role) {
                case "Admin":
                    return redirect()->intended('/Dashboard');
                    break;
                case "Artist":
                    return redirect()->intended('/ArtistDashboard');
                    break;

                case "Editor":
                    return redirect()->intended('/DashboardUser');
                    break;

                case "Competition":
                    return redirect()->intended('/CompetitionDashboard');
                    break;

                default:
                    return back();
                    break;
            }

        }
        else{
            return back()->with('success','Your email or your password is not matching');
        }
    }
    public function AccountList(){
        $listaccount = User::where('role','Admin')->orWhere('role','Editor')->orWhere('role','Competition')->get();
        return view('backend.AccountList')->with(['listaccount'=>$listaccount]);
    }
    public function Dashboard(){
        $firstday = new Carbon('first day of this month');
        $endofmonth = new Carbon('last day of this month');

        $firstdaydate = $firstday->toDateString();
        $endmontdate = $endofmonth->toDateString();

//        $getvotesM = Votes::where('voter_status','SUCCESSFUL')
//            ->whereBetween('created_at', [$firstdaydate,$endmontdate])
//            ->count();
        $getvotesM = Votes::where('voter_status','SUCCESSFUL')
            ->whereBetween('created_at', [$firstdaydate,$endmontdate])
            ->sum('vote');

        $songs = SongInfo::count();
//        $votes = Votes::where('vote','1')->count();
        $votes = Votes::where('vote','1')->sum('vote');
//        $votespending = Votes::where('voter_status','PENDING')->count();
        $votespending = Votes::where('voter_status','PENDING')->sum('vote');
        $votessuccessful = Votes::where('voter_status','SUCCESSFUL')->sum('vote');
//        $votessuccessful = Votes::where('voter_status','SUCCESSFUL')->count();
        $countSongs = SongInfo::count();

        $approvedaccounts = ArtistAccounts::where('artist_account_status','Approved')->count();
        $rejectaccounts = ArtistAccounts::where('artist_account_status','Rejected')->count();
        $pendingaccounts = ArtistAccounts::where('artist_account_status','Pending')->count();
        $check_status = VotingStatus::value('voting_status');
        $comment_status = CommentStatus::value('comment_status');
        $Competitions = Competitions::count();
        $Contestants = Contestants::count();
        $competitionpending = CompetitionTransactions::where('payment_status','PENDING')->count();
        $competitionsuccessful = CompetitionTransactions::where('payment_status','SUCCESSFUL')->count();
        $votes = CompVotes::where('status','SUCCESSFUL')->sum('vote');
        $votes_monthly = CompVotes::where('status','SUCCESSFUL')
            ->whereBetween('created_at', [$firstdaydate,$endmontdate])
            ->sum('vote');

        return view('backend.Dashboard')->with(['votes_monthly'=>$votes_monthly,'votes'=>$votes,'competitionsuccessful'=>$competitionsuccessful,'competitionpending'=>$competitionpending,'Competitions'=>$Competitions,'Contestants'=>$Contestants,'comment_status'=>$comment_status,'check_status'=>$check_status,'getvotesM'=>$getvotesM,'countSongs'=>$countSongs,'pendingaccounts'=>$pendingaccounts,'votessuccessful'=>$votessuccessful,'votespending'=>$votespending,'songs'=>$songs,'votes'=>$votes,'approvedaccounts'=>$approvedaccounts,'rejectaccounts'=>$rejectaccounts]);
    }
    public function DashboardUser(){
        $firstday = new Carbon('first day of this month');
        $endofmonth = new Carbon('last day of this month');

        $firstdaydate = $firstday->toDateString();
        $endmontdate = $endofmonth->toDateString();

//        $getvotesM = Votes::where('voter_status','SUCCESSFUL')
//            ->whereBetween('created_at', [$firstdaydate,$endmontdate])
//            ->count();
        $getvotesM = Votes::where('voter_status','SUCCESSFUL')
            ->whereBetween('created_at', [$firstdaydate,$endmontdate])
            ->sum('vote');

        $songs = SongInfo::count();
//        $votes = Votes::where('vote','1')->count();
        $votes = Votes::where('vote','1')->sum('vote');
//        $votespending = Votes::where('voter_status','PENDING')->count();
        $votespending = Votes::where('voter_status','PENDING')->sum('vote');
        $votessuccessful = Votes::where('voter_status','SUCCESSFUL')->sum('vote');
//        $votessuccessful = Votes::where('voter_status','SUCCESSFUL')->count();
        $countSongs = SongInfo::count();

        $approvedaccounts = ArtistAccounts::where('artist_account_status','Approved')->count();
        $rejectaccounts = ArtistAccounts::where('artist_account_status','Rejected')->count();
        $pendingaccounts = ArtistAccounts::where('artist_account_status','Pending')->count();
        $check_status = VotingStatus::value('voting_status');
        $comment_status = CommentStatus::value('comment_status');

        return view('backend.DashboardUser')->with(['comment_status'=>$comment_status,'check_status'=>$check_status,'getvotesM'=>$getvotesM,'countSongs'=>$countSongs,'pendingaccounts'=>$pendingaccounts,'votessuccessful'=>$votessuccessful,'votespending'=>$votespending,'songs'=>$songs,'votes'=>$votes,'approvedaccounts'=>$approvedaccounts,'rejectaccounts'=>$rejectaccounts]);
    }

    public function VotingStatus(Request $request){
        $voting = $request['voting_status'];
        $check_status = VotingStatus::all();

        if(0 == count($check_status)){
            $add = new VotingStatus();
            $add ->voting_status = $voting;
            $add->save();
            return response()->json([
                'response_message' => "successfully changed voting status",
                'response_status' =>true
            ]);
        }else{

            $update = DB::table('voting_statuses')->update(['voting_status' => $voting]);
            return response()->json([
                'response_message' => "successfully updated voting status",
                'response_status' =>true
            ]);
        }
    }

    public function VotingStatusComp(Request $request){
        $voting = $request['voting_status'];
        $check_status = voting_comp_status::all();
        if(0 == count($check_status)){
            $add = new voting_comp_status();
            $add ->user_id = \Auth::user()->id;
            $add ->voting_status = $voting;
            $add->save();
            return response()->json([
                'response_message' => "successfully changed voting status",
                'response_status' =>true
            ]);
        }else{
            $update = DB::table('voting_comp_statuses')->update(['voting_status' => $voting]);
            return response()->json([
                'response_message' => "successfully updated voting status",
                'response_status' =>true
            ]);
        }
    }

    public function CommentStatus(Request $request){
        $voting = $request['comment_status'];
        $check_status = CommentStatus::all();

        if(0 == count($check_status)){
            $add = new CommentStatus();
            $add ->comment_status = $voting;
            $add->save();
            return response()->json([
                'response_message' => "successfully changed comment status",
                'response_status' =>true
            ]);
        }else{

            $update = DB::table('comment_statuses')->update(['comment_status' => $voting]);
            return response()->json([
                'response_message' => "successfully updated comment status",
                'response_status' =>true
            ]);
        }
    }

    public function MobileMoney(){
        $list = MomoTransaction::orderBy('id', 'DESC')->paginate(100);
        return view('backend.MobileMoney')->with(['list'=>$list]);
    }
    public function CardsPayment(){
        $list = VotingCardTransactions::orderBy('id', 'DESC')
            ->join('country', 'country.id', '=', 'voting_card_transactions.country')
            ->select('voting_card_transactions.*', 'country.name')
            ->paginate(100);
        return view('backend.CardsPayment')->with(['list'=>$list]);
    }
    public function VoteCardPaymentFilter(Request $request){
        $startdate= $request['startdate'];
        $enddate= $request['enddate'];
        $list = CompetitionCardTransactions::orderBy('id', 'DESC')
            ->whereBetween('competition_card_transactions.created_at',[$startdate,$enddate])
            ->join('country', 'country.id', '=', 'voting_card_transactions.country')
            ->select('voting_card_transactions.*', 'country.name')
            ->paginate(100);
        return view('backend.CardsPayment')->with(['list'=>$list]);
    }
    public function CompetitionCardsPayment(){
        $competition = Competitions::all();
        $list = CompetitionCardTransactions::orderBy('id', 'DESC')
            ->join('country', 'country.id', '=', 'competition_card_transactions.country')
            ->join('competitions', 'competitions.id', '=', 'competition_card_transactions.competition_id')
            ->join('contestants', 'contestants.id', '=', 'competition_card_transactions.contestant_id')
            ->select('competition_card_transactions.*', 'country.name','competitions.competition_title','contestants.names')
            ->paginate(100);
        return view('backend.CompetitionCardsPayment')->with(['list'=>$list,'competition'=>$competition]);
    }
    public function CompetitionCardFilter(Request $request){
        $startdate= $request['startdate'];
        $enddate= $request['enddate'];
        $competition_id= $request['competition_id'];
        $competition = Competitions::all();
//        $list = CompetitionCardTransactions::orderBy('id', 'DESC')
//            ->whereBetween('competition_card_transactions.created_at',[$startdate,$enddate])
//            ->join('country', 'country.id', '=', 'competition_card_transactions.country')
//            ->join('competitions', 'competitions.id', '=', 'competition_card_transactions.competition_id')
//            ->join('contestants', 'contestants.id', '=', 'competition_card_transactions.contestant_id')
//            ->select('competition_card_transactions.*', 'country.name','competitions.competition_title','contestants.names')
//            ->paginate(100);

        if($startdate & $enddate and $competition_id == null){
            $list = DB::table('competition_card_transactions')
                ->whereBetween('competition_card_transactions.created_at',[$startdate,$enddate])
                ->join('country', 'country.id', '=', 'competition_card_transactions.country')
                ->join('competitions', 'competitions.id', '=', 'competition_card_transactions.competition_id')
                ->join('contestants', 'contestants.id', '=', 'competition_card_transactions.contestant_id')
                ->select('competition_card_transactions.*', 'country.name','competitions.competition_title','contestants.names')
                ->paginate(100);
            return view('backend.CompetitionCardsPayment')->with(['competition'=>$competition,'list'=>$list]);

        }elseif($startdate & $enddate and $competition_id){
            $list = DB::table('competition_card_transactions')
                ->whereBetween('competition_card_transactions.created_at',[$startdate,$enddate])
                ->where('competition_card_transactions.competition_id',$competition_id)
                ->join('country', 'country.id', '=', 'competition_card_transactions.country')
                ->join('competitions', 'competitions.id', '=', 'competition_card_transactions.competition_id')
                ->join('contestants', 'contestants.id', '=', 'competition_card_transactions.contestant_id')
                ->select('competition_card_transactions.*', 'country.name','competitions.competition_title','contestants.names')
                ->paginate(100);
            return view('backend.CompetitionCardsPayment')->with(['competition'=>$competition,'list'=>$list]);

        }elseif($startdate == null & $enddate == null and $competition_id){
            $list = DB::table('competition_card_transactions')
                ->where('competition_card_transactions.competition_id',$competition_id)
                ->join('country', 'country.id', '=', 'competition_card_transactions.country')
                ->join('competitions', 'competitions.id', '=', 'competition_card_transactions.competition_id')
                ->join('contestants', 'contestants.id', '=', 'competition_card_transactions.contestant_id')
                ->select('competition_card_transactions.*', 'country.name','competitions.competition_title','contestants.names')
                ->paginate(100);
            return view('backend.CompetitionCardsPayment')->with(['competition'=>$competition,'list'=>$list]);
        }
//        return view('backend.CompetitionCardsPayment')->with(['list'=>$list]);
    }
    public function CompetitionMobileMoney(){
        $competition = Competitions::all();
//        dd($competition);
        $list = CompetitionTransactions::join('competitions', 'competitions.id', '=', 'competition_transactions.competition_id')
            ->select('competition_transactions.*', 'competitions.competition_title')
            ->orderBy('id', 'DESC')
            ->paginate(100);
        return view('backend.CompetitionMobileMoney')->with(['competition'=>$competition,'list'=>$list]);
    }
    public function CompetitionMobileMoneyFilter(Request $request){
        $startdate = $request['startdate'];
        $enddate = $request['enddate'];
        $competition_id = $request['competition_id'];
        $competition = Competitions::all();
        $list = CompetitionTransactions::where('competition_transactions.competition_id',$competition_id)
            ->whereBetween('competition_transactions.created_at',[$startdate,$enddate])
            ->join('competitions', 'competitions.id', '=', 'competition_transactions.competition_id')
            ->select('competition_transactions.*', 'competitions.competition_title')
            ->orderBy('id', 'DESC')
            ->get();

        return view('backend.CompetitionMobileMoney')->with(['competition'=>$competition,'list'=>$list]);
    }
    public function MobileMoneyFilter(Request $request){
        $startdate = $request['startdate'];
        $enddate = $request['enddate'];
        $list = MomoTransaction::whereBetween('created_at',[$startdate,$enddate])->orderBy('id', 'DESC')->get();
        return view('backend.MobileMoneyFilter')->with(['list'=>$list]);
    }
    public function FundMobileMoney(){
        $list = FundTransaction::orderBy('id', 'DESC')->paginate(100);
        return view('backend.FundMobileMoney')->with(['list'=>$list]);
    }
    public function FundMobileMoneyFilter(Request $request){
        $startdate = $request['startdate'];
        $enddate = $request['enddate'];
        $list = FundTransaction::whereBetween('created_at',[$startdate,$enddate])->orderBy('id', 'DESC')->get();
        return view('backend.FundMobileMoneyFilter')->with(['list'=>$list]);
    }


    public function CreateAccount(){
        return view('backend.CreateAccount');
    }

    public function CreateAccount_(Request $request){
        $all  = $request->all();

        $request->validate([
            'name' => 'required|string:posts|max:255',
            'email' => 'required|string|email|max:255|unique:users','exists:connection.user,email',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $register = new User();
        $register->name = $request['name'];
        $register->email = $request['email'];
        $register->role =  $request['role'];
        $register->password = bcrypt($request['password']);
        $register->save();

        return redirect()->route('backend.AccountList')->with('Success','You have successfully Created Your Account');
    }
    public function DeleteAccount(Request $request){
        $id = $request['id'];
        $getuseremail = User::where('id',$id)->value('email');
        $checkartist = ArtistAccounts::where('artist_email',$getuseremail)->get();

        if(0 == count($checkartist)){
            $deleteaccount = User::find($id);
            $deleteaccount->delete();
            return back()->with('success','you have successfully deleted user account');
        }else{
            $deleteaccount = User::find($id);
            $deleteaccount->delete();
            $deleteartist = ArtistAccounts::where('artist_email',$getuseremail)->delete();
            return back()->with('success','you have successfully deleted artist account');
        }

    }

    public function ListNominee(){
        $listcate = PlaylistCategory::all();
        $listnominee = Nominee::join('playlistcategory', 'playlistcategory.id', '=', 'nominee.nominee_category')
            ->select('nominee.*','playlistcategory.category')
            ->get();
        return view('backend.ListNominee')->with(['listnominee'=>$listnominee,'listcate'=>$listcate]);
    }
    public function EditListNominee(Request $request){
        $id = $request['id'];
        $editnominee = Nominee::find($id);
        $editnominee->artist_name = $request['artist_name'];
        $editnominee->artist_song = $request['artist_song'];
        $editnominee->nominee_category = $request['nominee_category'];
        $editnominee->save();
        return back()->with('success','you have successfully edited nominee data');
    }
    public function DeleteListNominee(Request $request){
        $id = $request['id'];
        $deletenominee = Nominee::find($id);
        $deletenominee->delete();
        return back()->with('success','you have successfully deleted nominee data');
    }


    public function PlaylistCategory(){
        $listplaylist = PlaylistCategory::all();
        return view('backend.PlaylistCategory')->with(['listplaylist'=>$listplaylist]);
    }
    public function AddPlaylistCategory(Request $request){
        $addcat = new PlaylistCategory();
        $addcat->category = $request['playlistcate'];
        $addcat->save();
        return back()->with('success','you have successfully added a new category');
    }
    public function EditPlaylistCategory(Request $request){
        $id = $request['id'];
        $editcate = PlaylistCategory::find($id);
        $editcate->category = $request['playlistcate'];
        $editcate->save();
        return back()->with('success','you have successfully updated category');

    }
    public function DeletePlaylistCategory(Request $request){
        $id = $request['id'];
        $delete = PlaylistCategory::find($id);
        $delete->delete();
        return back()->with('success','you have successfully deleted category');
    }
    public function ArtistsAccounts(){
        $listaccounts = ArtistAccounts::where('artist_account_status','Approved')->get();
        return view ('backend.ArtistsAccounts')->with(['listaccounts'=>$listaccounts]);
    }
    public function ArtistsAccountsPending(){
        $listaccounts = ArtistAccounts::where('artist_account_status','Pending')->get();
        return view ('backend.ArtistsAccountsPending')->with(['listaccounts'=>$listaccounts]);
    }
    public function ArtistsAccountsRejected(){
        $listaccounts = ArtistAccounts::where('artist_account_status','Rejected')->get();
        return view ('backend.ArtistsAccountsRejected')->with(['listaccounts'=>$listaccounts]);
    }
    public function DeleteArtistAccount(Request $request){
        $id = $request['id'];
        $getuseremail = ArtistAccounts::where('id',$id)->value('artist_email');
        $checkartist = ArtistAccounts::where('id',$id)->get();
        $user_id = User::where('email',$getuseremail)->value('id');

        $deleteaccount = User::find($user_id);
        $deleteaccount->delete();

        $deleteartist = ArtistAccounts::where('id',$id)->delete();
        return back()->with('success','you have successfully deleted artist account');

    }
    public function ArtistRegistrationUpdate(Request $request){
        $id = $request['id'];
        $user_id = \Auth::user()->id;
        $getuseremail = User::where('id',$user_id)->value('email');
        $accountdetails = ArtistAccounts::where('artist_email',$getuseremail)->get();
//        $user_id = \Auth::user()->id;
        return view('backend.ArtistRegistrationUpdate')->with(['accountdetails'=>$accountdetails]);

    }
    public function UpdateArtistInfo(Request $request){
        $id = $request['id'];
        $user_id = \Auth::user()->id;
        $user_mail = User::where('id',$user_id)->update(['email'=>$request['artist_email']]);

        if($request['artist_password']){
            $password = bcrypt(($request['artist_password']));
            $updatepass = User::where('id',$user_id)->update(['password'=>$password]);

            $updateinfo = ArtistAccounts::where('id',$id)->update(['names'=>$request['names'],'artistic_names'=>$request['artistic_names'],
                'artist_number'=>$request['artist_number'],'artist_email'=>$request['artist_email'],'artist_ID'=>$request['artist_ID'],
                'artist_modeofpayment'=>$request['artist_modeofpayment'],'artist_mobilemoney_number'=>$request['artist_mobilemoney_number'],
                'artist_mobilemoney_names'=>$request['artist_mobilemoney_names'],'artist_bankaccount_number'=>$request['artist_bankaccount_number'],
                'artist_bankaccount_names'=>$request['artist_bankaccount_names'],'artist_bank_name'=>$request['artist_bank_name'],'artist_facebook'=>$request['artist_facebook'],
                'artist_instagram'=>$request['artist_instagram'],'artist_twitter'=>$request['artist_twitter'],'artist_youtube'=>$request['artist_youtube'],'artist_description'=>$request['artist_description']]);
            return back()->with('success','you have successfully updated artist account');
        }else{
            $updateinfo = ArtistAccounts::where('id',$id)->update(['names'=>$request['names'],'artistic_names'=>$request['artistic_names'],
                'artist_number'=>$request['artist_number'],'artist_email'=>$request['artist_email'],'artist_ID'=>$request['artist_ID'],
                'artist_modeofpayment'=>$request['artist_modeofpayment'],'artist_mobilemoney_number'=>$request['artist_mobilemoney_number'],
                'artist_mobilemoney_names'=>$request['artist_mobilemoney_names'],'artist_bankaccount_number'=>$request['artist_bankaccount_number'],
                'artist_bankaccount_names'=>$request['artist_bankaccount_names'],'artist_bank_name'=>$request['artist_bank_name'],'artist_facebook'=>$request['artist_facebook'],
                'artist_instagram'=>$request['artist_instagram'],'artist_twitter'=>$request['artist_twitter'],'artist_youtube'=>$request['artist_youtube'],'artist_description'=>$request['artist_description']]);
            return back()->with('success','you have successfully updated artist account');
        }
    }
    public function ApproveSendSMS(Request $request){
        $id = $request['id'];
        $getnames = ArtistAccounts::where('id',$id)->value('names');
        $getemail = ArtistAccounts::where('id',$id)->value('artist_email');
        $getnumber = ArtistAccounts::where('id',$id)->value('artist_number');

        $updatecredit = ArtistAccounts::where('id',$id)->update(['artist_account_status' => 'Approved']);
        $updateuser = User::where('email',$getemail)->update(['role' => 'Artist']);

        $rsgamessage = "Murakoze $getnames\n,turabamenyesha ko ubusabe bwo gukoresha MNI Selection bwemewe. Sura urubuga rwacu ushyireho indirimbo";
        $phonenumber = '0782384772';

        $intnumber = '25'.$getnumber;
        $api_key = 'c3dheUpQaWp2QWdQb2JPZ0dsaWo=';
        $from = 'MNI';
        $destination = $intnumber;
        $action='send-sms';
        $url = 'https://mistasms.com/sms/api';
        $sms = $rsgamessage;
        $unicode = 1; //For Plain Message
        $unicode = 0; //For Unicode Message
        $sms_body = array(
            'api_key' => $api_key,
            'to' => $destination,
            'from' => $from,
            'sms' => $sms,
            'unicode' => $unicode,
        );
        $client = new \MRSMSAPI();
        $response = $client->send_sms($sms_body, $url);
        $response =json_decode($response);
        $name = $getnames;
        $email = "infomni611@gmail.com";
        $toemail = $getemail;
        $messageMail = $rsgamessage;
        $company_name = "MNI SELECTION";
        $subjectmail = "MNI ACCOUNT APPROVED";

        Mail::send('backend.mail',
            array(
                'namemail'=>$company_name,
                'name' => $name,
                'email' => $email,
                'messageMail' => $messageMail
            ), function($message) use ($email,$messageMail, $name,$subjectmail,$company_name,$toemail)
            {
                $message->from($email, $company_name);
                $message->to($toemail, $name)->subject($subjectmail);
            });

        return back()->with('success','you have successfully sent your message');

    }

    public function RejectSendSMS(Request $request){
        $id = $request['id'];
        $getnames = ArtistAccounts::where('id',$id)->value('names');
        $getnumber = ArtistAccounts::where('id',$id)->value('artist_number');
        $getemail = ArtistAccounts::where('id',$id)->value('artist_email');
        $updatecredit = ArtistAccounts::where('id',$id)->update(['artist_account_status' => 'Rejected']);
        $rsgamessage = "Murakoze $getnames\ngukoresha MNI Selection. Turabamenyesha ko ubusabe bwanyu butemewe. Hamagara 0780008883 kubindi bisobanuro";
        $phonenumber = '0782384772';

        $intnumber = '25'.$getnumber;
        $api_key = 'c3dheUpQaWp2QWdQb2JPZ0dsaWo=';
        $from = 'MNI';
        $destination = $intnumber;
        $action='send-sms';
        $url = 'https://mistasms.com/sms/api';
        $sms = $rsgamessage;
        $unicode = 1; //For Plain Message
        $unicode = 0; //For Unicode Message
        $sms_body = array(
            'api_key' => $api_key,
            'to' => $destination,
            'from' => $from,
            'sms' => $sms,
            'unicode' => $unicode,
        );
        $client = new \MRSMSAPI();
        $response = $client->send_sms($sms_body, $url);
        $response =json_decode($response);
        $link = "";
        $name = $getnames;
        $email = "infomni611@gmail.com";
        $toemail = $getemail;
        $messageMail = $rsgamessage;
        $company_name = "MNI SELECTION";
        $subjectmail = "MNI ACCOUNT REJECTED";

        Mail::send('backend.rmail',
            array(
                'namemail'=>$company_name,
                'name' => $name,
                'email' => $email,
                'messageMail' => $messageMail
            ), function($message) use ($email,$messageMail, $name,$subjectmail,$company_name,$toemail)
            {
                $message->from($email, $company_name);
                $message->to($toemail, $name)->subject($subjectmail);
            });

        return back()->with('success','you have successfully sent your message');

    }
    public function TestEmail(){
        $name = "Elysee CONFIANCE";
        $email = "infomni611@gmail.com";
        $toemail = "ccelyse1@gmail.com";
        $messageMail = "Thank you for applying for affordable mortgage your application have been sent to review you will be communicated once it is approved or rejected";
        $company_name = "MNI SELECTION";
        $subjectmail = "MNI ACCOUNT APPROVED";

        Mail::send('backend.mail',
            array(
                'namemail'=>$company_name,
                'name' => $name,
                'email' => $email,
                'messageMail' => $messageMail
            ), function($message) use ($email,$messageMail, $name,$subjectmail,$company_name,$toemail)
            {
                $message->from($email, $company_name);
                $message->to($toemail, $name)->subject($subjectmail);
            });
    }
    public function ArtistAddSong(){
        $listcate = PlaylistCategory::all();
        $user_id = \Auth::user()->id;
        $user_email = User::where('id',$user_id)->value('email');
        $get_artistname = ArtistAccounts::where('artist_email',$user_email)->value('artistic_names');
        $listplaylist = SongInfo::where('user_id',$user_id)->get();
        return view('backend.ArtistAddSong')->with(['listplaylist'=>$listplaylist,'listcate'=>$listcate,'get_artistname'=>$get_artistname]);
    }
    public function ArtistLogin(){
        return view ('ArtistLogin');
    }
    public function ArtistDashboard(){
        $firstday = new Carbon('first day of this month');
        $lastday = new Carbon('last day of this month');
        $lastday_ = $lastday->subDays(7);

        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();
        $endofmonth = new Carbon('last day of this month');
        $endmontdate = $endofmonth->toDateString();

        $user_id = \Auth::user()->id;
        $checksong = SongInfo::where('user_id',$user_id)->value('song_name');
        $checkname = SongInfo::where('user_id',$user_id)->value('song_artist');
        $checkvotes = Votes::select(DB::raw('count(id) as votes'))
            ->where('voter_artist_song',$checksong)
            ->where('voter_artist',$checkname)
            ->where('vote','1')
            ->value('votes');
        $amountpayment = ($checkvotes * 25);

        $getmusicmodern = SongInfo::where('voter_status','SUCCESSFUL')->where('user_id',$user_id)->leftJoin('votes', 'songinfo.id', '=', 'votes.song_id')
            ->select('songinfo.id','songinfo.song_artist','songinfo.song_name','songinfo.song_youtube_link',
                'songinfo.song_cover_picture','songinfo.playlist_code','songinfo.starting_date',
                'songinfo.ending_date', DB::raw("count(votes.id) as votesnumber"))
//            ->whereBetween(DB::raw('votes.created_at'), [DB::raw($firstdaydate), DB::raw($endmontdate)])
            ->groupBy('votes.song_id')
            ->orderBy('votesNumber','DESC')
            ->get();

        foreach ($getmusicmodern as $datas){
            $this->CheckVotes($datas);
            $this->CheckVotesMonthly($datas);
//            $datas['votesNumber'] = (double) $this->CheckVotes($datas);
            $datas['votesNumberMon'] = (double) $this->CheckVotesMonthly($datas);
        }
        return view('backend.ArtistDashboard')->with(['getmusicmodern'=>$getmusicmodern,'checksong'=>$checksong,'checkvotes'=>$checkvotes,'amountpayment'=>$amountpayment]);
    }
    public function CompetitionDashboard(){
        $get_user = Competitions::where('user_id',\Auth::user()->id)->get();
        foreach ($get_user as $data){
            $ids = $data->id;
        }
        $ids= array();
        foreach($get_user as $data){
            array_push($ids, $data->id);
        }
        $firstday = new Carbon('first day of this month');
        $endofmonth = new Carbon('last day of this month');

        $firstdaydate = $firstday->toDateString();
        $endmontdate = $endofmonth->toDateString();
        $votes = CompVotes::whereIn('competition_id',$ids)->where('status','SUCCESSFUL')->sum('vote');
        $Competitions = Competitions::where('user_id',\Auth::user()->id)->count();
        $Contestants = Contestants::where('user_id',\Auth::user()->id)->count();
        $competitionpending = CompVotes::where('status','PENDING')->whereIn('competition_id',$ids)->count();
        $competitionsuccessful = CompVotes::where('status','SUCCESSFUL')->whereIn('competition_id',$ids)->count();
        $votes_monthly = CompVotes::whereIn('competition_id',$ids)->where('status','SUCCESSFUL')
            ->whereBetween('created_at', [$firstdaydate,$endmontdate])
            ->sum('vote');
        $check_status = voting_comp_status::where('user_id',\Auth::user()->id)->value('voting_status');

        return view('backend.CompetitionDashboard')->with(['check_status'=>$check_status,'votes_monthly'=>$votes_monthly,'competitionsuccessful'=>$competitionsuccessful,'competitionpending'=>$competitionpending,'votes'=>$votes,'Competitions'=>$Competitions,'Contestants'=>$Contestants]);
    }
    public function AddCompetition(){
        $checkuser = User::where('id',\Auth::user()->id)->value('role');
        if($checkuser == "Admin"){
            $competition = Competitions::all();
            return view('backend.AddCompetition')->with(['competition'=>$competition]);
        }else{
            $competition = Competitions::where('user_id',\Auth::user()->id)->get();
            return view('backend.AddCompetition')->with(['competition'=>$competition]);
        }

    }
    public function CompetitionUserCard(){
        $competition = Competitions::where('user_id',\Auth::user()->id)->get();
        foreach ($competition as $data){
            $ids = $data->id;
        }
        $ids= array();
        foreach($competition as $data){
            array_push($ids, $data->id);
        }
        $list = DB::table('competition_card_transactions')
            ->whereIn('competition_card_transactions.competition_id',$ids)
            ->join('country', 'country.id', '=', 'competition_card_transactions.country')
            ->join('competitions', 'competitions.id', '=', 'competition_card_transactions.competition_id')
            ->join('contestants', 'contestants.id', '=', 'competition_card_transactions.contestant_id')
            ->select('competition_card_transactions.*', 'country.name','competitions.competition_title','contestants.names')
            ->paginate(100);
        return view('backend.CompetitionCardsPaymentUser')->with(['competition'=>$competition,'list'=>$list]);
    }
    public function CompetitionUserCardFilter(Request $request){
        $all = $request->all();
//        dd($all);
        $startdate = $request['startdate'];
        $enddate = $request['enddate'];
        $competition_id = $request['competition_id'];
        $competition = Competitions::where('user_id',\Auth::user()->id)->get();

        if($startdate & $enddate and $competition_id == null){
            $list = DB::table('competition_card_transactions')
                ->whereBetween('competition_card_transactions.created_at',[$startdate,$enddate])
                ->join('country', 'country.id', '=', 'competition_card_transactions.country')
                ->join('competitions', 'competitions.id', '=', 'competition_card_transactions.competition_id')
                ->join('contestants', 'contestants.id', '=', 'competition_card_transactions.contestant_id')
                ->select('competition_card_transactions.*', 'country.name','competitions.competition_title','contestants.names')
                ->paginate(100);
            return view('backend.CompetitionCardsPaymentUser')->with(['competition'=>$competition,'list'=>$list]);

        }elseif($startdate & $enddate and $competition_id){
            $list = DB::table('competition_card_transactions')
                ->whereBetween('competition_card_transactions.created_at',[$startdate,$enddate])
                ->where('competition_card_transactions.competition_id',$competition_id)
                ->join('country', 'country.id', '=', 'competition_card_transactions.country')
                ->join('competitions', 'competitions.id', '=', 'competition_card_transactions.competition_id')
                ->join('contestants', 'contestants.id', '=', 'competition_card_transactions.contestant_id')
                ->select('competition_card_transactions.*', 'country.name','competitions.competition_title','contestants.names')
                ->paginate(100);
            return view('backend.CompetitionCardsPaymentUser')->with(['competition'=>$competition,'list'=>$list]);

        }elseif($startdate == null & $enddate == null and $competition_id){
            $list = DB::table('competition_card_transactions')
                ->where('competition_card_transactions.competition_id',$competition_id)
                ->join('country', 'country.id', '=', 'competition_card_transactions.country')
                ->join('competitions', 'competitions.id', '=', 'competition_card_transactions.competition_id')
                ->join('contestants', 'contestants.id', '=', 'competition_card_transactions.contestant_id')
                ->select('competition_card_transactions.*', 'country.name','competitions.competition_title','contestants.names')
                ->paginate(100);
            return view('backend.CompetitionCardsPaymentUser')->with(['competition'=>$competition,'list'=>$list]);
        }
    }
    public function CompetitionMobileMoneyUser(){
        $competition = Competitions::where('user_id',\Auth::user()->id)->get();
        foreach ($competition as $data){
            $ids = $data->id;
        }
        $ids= array();
        foreach($competition as $data){
            array_push($ids, $data->id);
        }
//        dd($competition);
        $list = CompetitionTransactions::whereIn('competition_transactions.competition_id',$ids)
            ->join('competitions', 'competitions.id', '=', 'competition_transactions.competition_id')
            ->select('competition_transactions.*', 'competitions.competition_title')
            ->orderBy('id', 'DESC')
            ->paginate(100);
        return view('backend.CompetitionMobileMoneyUser')->with(['competition'=>$competition,'list'=>$list]);
    }
    public function CompetitionMobileMoneyFilterUser(Request $request){
        $all = $request->all();
//        dd($all);
        $startdate = $request['startdate'];
        $enddate = $request['enddate'];
        $competition_id = $request['competition_id'];
        $competition = Competitions::where('user_id',\Auth::user()->id)->get();


        if($startdate & $enddate and $competition_id == null){
            $list = CompetitionTransactions::whereBetween('competition_transactions.created_at',[$startdate,$enddate])
                ->join('competitions', 'competitions.id', '=', 'competition_transactions.competition_id')
                ->select('competition_transactions.*', 'competitions.competition_title')
                ->orderBy('id', 'DESC')
                ->get();
            return view('backend.CompetitionMobileMoneyUser')->with(['competition'=>$competition,'list'=>$list]);

        }elseif($startdate & $enddate and $competition_id){
            $list = CompetitionTransactions::where('competition_transactions.competition_id',$competition_id)
                ->whereBetween('competition_transactions.created_at',[$startdate,$enddate])
                ->join('competitions', 'competitions.id', '=', 'competition_transactions.competition_id')
                ->select('competition_transactions.*', 'competitions.competition_title')
                ->orderBy('id', 'DESC')
                ->get();
            return view('backend.CompetitionMobileMoneyUser')->with(['competition'=>$competition,'list'=>$list]);

        }elseif($startdate == null & $enddate == null and $competition_id){
            $list = CompetitionTransactions::where('competition_transactions.competition_id',$competition_id)
                ->join('competitions', 'competitions.id', '=', 'competition_transactions.competition_id')
                ->select('competition_transactions.*', 'competitions.competition_title')
                ->orderBy('id', 'DESC')
                ->get();
            return view('backend.CompetitionMobileMoneyUser')->with(['competition'=>$competition,'list'=>$list]);
        }

    }
    public function Groups(Request $request){
        $id = $request['id'];
        $competition = Competitions::where('id',$id)->get();
//        $groups = Groups::where('competition_id',$id)->get();
        $groups = Groups::where('competition_id',$request['id'])
            ->join('competitions', 'competitions.id', '=', 'groups.competition_id')
            ->select('groups.*', 'competitions.competition_title')
            ->get();
        return view('backend.Groups')->with(['groups'=>$groups,'competition'=>$competition]);
    }
    public function AddGroup(Request $request){

        $all = $request->all();
        $add = new Groups();
        $add->competition_id = $request['competition_id'];
        $add->group_name = $request['group_names'];
        $add->group_status = "Active";
        $add->user_id = Auth::user()->id;
        $add->save();

        return back()->with('success','successfully added group');
//        dd($all);
    }
    public function EditGroup(Request $request){
        $all = $request->all();
//        dd($all);
        $id = $request['id'];
        $update = Groups::find($id);
        $update->competition_id = $request['competition_id'];
        $update->group_name = $request['group_name'];
        $update->group_status = $request['group_status'];
        $update->save();
        return back()->with('success','successfully updated group');
    }
    public function UpdateVotingStatus(Request $request){
        $update = Competitions::where('id',$request['id'])->update(['voting_status'=>$request['status']]);
        return response()->json([
            'response_message' => "successfully updated status",
            'response_status' =>true
        ]);
    }
    public function UploadCompetition(Request $request){

        $all = $request->all();
        $currentMonth = Carbon::now()->format('m');
        $MNI = "MNI";
        $code = $MNI . mt_rand(10, 9999). $currentMonth;
        $add_new = new Competitions();
        $add_new->competition_title = $request['competition_title'];
        $add_new->competition_youtube_link = $request['competition_youtube_link'];
        $add_new->starting_date = $request['starting_date'];
        $add_new->end_date = $request['end_date'];
//        $add_new->voting_price = $request['voting_price'];
        $add_new->competition_description = $request['competition_description'];
        $add_new->voting_status = "On";
        $add_new->competition_code = $code;
        $add_new->user_id = \Auth::user()->id;

        $image = $request->file('competition_image');
        $new_name_image = rand() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('/Competition'), $new_name_image);
        $add_new->competition_image = $new_name_image;
        $add_new->save();
        return back()->with('success','You have successfully added a new competition');

    }
    public function EditCompetition(Request $request){
        $id = $request['id'];

        if($request->file('competition_image')){

            $edit = Competitions::find($id);
            $edit->competition_title = $request['competition_title'];
            $edit->competition_youtube_link = $request['competition_youtube_link'];
            $edit->starting_date = $request['starting_date'];
            $edit->end_date = $request['end_date'];
//            $edit->voting_price = $request['voting_price'];
            $edit->competition_description = $request['competition_description'];

            $image = $request->file('competition_image');
            $new_name_image = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('/Competition'), $new_name_image);
            $edit->competition_image = $new_name_image;
            $edit->save();

            return back()->with('success','You have successfully added a new competition');
        }else{

            $edit = Competitions::find($id);
            $edit->competition_title = $request['competition_title'];
            $edit->competition_youtube_link = $request['competition_youtube_link'];
            $edit->starting_date = $request['starting_date'];
            $edit->end_date = $request['end_date'];
//            $edit->voting_price = $request['voting_price'];
            $edit->competition_description = $request['competition_description'];
            $edit->save();
            return back()->with('success','You have successfully added a new competition');

        }
    }
    public function DeleteCompetition(Request $request){
        $id = $request['id'];
        $delete = Competitions::find($id);
        $delete_cont = Contestants::where('competition_id',$id)->delete();
        $delete->delete();
        return back()->with('success','You have successfully deleted competition');
    }
    public function CompetitionSlider(){

        $list = Competitions::where('user_id',\Auth::user()->id)->get();
        $competition = CompetitionSlider::where('competition_sliders.user_id',\Auth::user()->id)
            ->join('competitions', 'competitions.id', '=', 'competition_sliders.competition_id')
            ->select('competition_sliders.*', 'competitions.competition_title')
            ->get();

        return view('backend.CompetitionSlider')->with(['competition'=>$competition,'list'=>$list]);

    }
    public function AddCompetition_Slider(Request $request){
        $all = $request->all();
        $addslider = new CompetitionSlider();

        $addslider->slider_title = $request['slider_title'];
        $addslider->slider_description = $request['slider_description'];
        $addslider->competition_id = $request['competition_id'];
        $addslider->user_id = \Auth::user()->id;

        $image = $request->file('slider_photo');
        $addslider->slider_photo = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/HomeSlider');
        $image->move($destinationPath, $imagename);
        $addslider->save();
        return back()->with('success','You have successfully added a new competition slider');


    }
    public function EditCompetition_Slider(Request $request){

        if($request->file('slider_photo')){
            $edit = CompetitionSlider::find($request['id']);
            $edit->slider_title = $request['slider_title'];
            $edit->slider_description = $request['slider_description'];
            $edit->competition_id = $request['competition_id'];
            $edit->slider_status = "Pending";
            $edit->user_id = \Auth::user()->id;

            $image = $request->file('slider_photo');
            $edit->slider_photo = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/HomeSlider');
            $image->move($destinationPath, $imagename);
            $edit->save();
            return back()->with('success','You have successfully edited competition slider');
        }else{
            $edit = CompetitionSlider::find($request['id']);
            $edit->slider_title = $request['slider_title'];
            $edit->slider_description = $request['slider_description'];
            $edit->competition_id = $request['competition_id'];
            $edit->slider_status = "Pending";
            $edit->user_id = \Auth::user()->id;
            $edit->save();
            return back()->with('success','You have successfully edited competition slider');
        }
    }
    public function DeleteCompetition_Slider(Request $request){
        $delete = CompetitionSlider::find($request['id']);
        $delete->delete();
        return back()->with('success','You have successfully deleted competition slider');
    }
    public function Contestants(Request $request){
        $checkuser = User::where('id',\Auth::user()->id)->value('role');
        if($checkuser == "Admin"){
//            $group = Groups::where('id',$request['id'])->get();
            $group_contestant = Groups::where('id',$request['id'])->get();
            foreach ($group_contestant as $groups){
                $competition_id = $groups->competition_id;
            }
//            $competition = Competitions::all();
            $competition = Competitions::where('id',$competition_id)->get();
            $group = Groups::where('competition_id',$competition_id)->get();
            $contestant = Contestants::where('contestants.competition_id',$request['id'])
                ->join('competitions', 'competitions.id', '=', 'contestants.competition_id')
                ->select('contestants.*', 'competitions.competition_title')
                ->get();

//            $contestant = Contestants::where('contestants.competition_id',$competition_id)
//                ->where('contestants.group_id',$request['id'])
//                ->join('competitions', 'competitions.id', '=', 'contestants.competition_id')
//                ->join('groups', 'groups.id', '=', 'contestants.group_id')
//                ->select('contestants.*', 'competitions.competition_title','groups.group_name')
//                ->get();

//            $contestant = DB::table('contestants')->where('contestants.competition_id',$request['id'])
////
//                ->leftJoin('comp_votes', 'contestants.id', '=', 'comp_votes.contestant_id')
////            ->select('contestants.*', DB::raw("sum(vote) as votesNumber"))
//                ->select('contestants.*','comp_votes.contestant_id','comp_votes.vote',DB::raw('coalesce(sum(vote), 0) as votesNumber_'))
////            ->where('comp_votes.status','SUCCESSFUL')
//                ->groupBy('contestants.id')
//                ->orderBy('votesNumber_','DESC')
//                ->get();

            foreach ($contestant as $datas){
                $this->CheckCompetitionVotes($datas);
                $datas['votesNumber'] = (double) $this->CheckCompetitionVotes($datas);
            }
            return view('backend.Contestants')->with(['group'=>$group,'competition'=>$competition,'contestant'=>$contestant]);
        }else{
            $group_contestant = Groups::where('id',$request['id'])->get();
            foreach ($group_contestant as $groups){
                $competition_id = $groups->competition_id;
            }
            $competition = Competitions::where('id',$competition_id)->get();
            $group = Groups::where('competition_id',$competition_id)->get();
            $contestant = Contestants::where('contestants.competition_id',$competition_id)
                ->where('contestants.group_id',$request['id'])
                ->join('competitions', 'competitions.id', '=', 'contestants.competition_id')
                ->join('groups', 'groups.id', '=', 'contestants.group_id')
                ->select('contestants.*', 'competitions.competition_title','groups.group_name')
                ->get();
            foreach ($contestant as $datas){
                $this->CheckCompetitionVotes($datas);
                $datas['votesNumber'] = (double) $this->CheckCompetitionVotes($datas);
            }
            return view('backend.Contestants')->with(['group'=>$group,'competition'=>$competition,'contestant'=>$contestant]);
        }

    }
    public function TransferContestant(Request $request){
        $id = $request['id'];
        $transfer = Contestants::where('id',$id)->update(['group_id'=>$request['group_id']]);
        return back()->with('success','You have successfully transfered contestant');
    }
    public function CheckCompetitionVotes($datas){
//        $votes = CompetitionTransactions::where('competition_id',$datas->competition_id)
//            ->where('contestant_id',$datas->id)
//            ->where('payment_status','SUCCESSFUL')
//            ->sum('vote');
        $votes = CompVotes::where('competition_id',$datas->competition_id)
            ->where('contestant_id',$datas->id)
            ->where('status','SUCCESSFUL')
            ->sum('vote');
        return json_encode($votes);
    }
    public function AddContestants(Request $request){
        $all = $request->all();
        $add = new Contestants();
        $add->names = $request['names'];
        $add->youtube = $request['youtube'];
        $add->competition_id = $request['competition_id'];
        $add->group_id = $request['group_id'];
        $add->user_id = \Auth::user()->id;
        $image = $request->file('photo');
        $add->photo = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/Contestant');
        $image->move($destinationPath, $imagename);
        $add->save();
        return back()->with('success','You have successfully added new contestant');
    }
    public function EditContestants(Request $request){
        if($request->file('photo')){

            $edit = Contestants::find($request['id']);
            $edit->names = $request['names'];
            $edit->youtube = $request['youtube'];
            $edit->competition_id = $request['competition_id'];
            $edit->group_id = $request['group_id'];
            $edit->user_id = \Auth::user()->id;
            $image = $request->file('photo');
            $edit->photo = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/Contestant');
            $image->move($destinationPath, $imagename);
            $edit->save();
            return back()->with('success','You have successfully added new contestant');

        }else{

            $edit = Contestants::find($request['id']);
            $edit->names = $request['names'];
            $edit->youtube = $request['youtube'];
            $edit->competition_id = $request['competition_id'];
            $edit->group_id = $request['group_id'];
            $edit->user_id = \Auth::user()->id;
            $edit->save();
            return back()->with('success','You have successfully updated contestant');

        }
    }
    public function DeleteContestant(Request $request){
        $delete = Contestants::find($request['id']);
        $delete->delete();
        return back()->with('success','You have successfully deleted contestant');
    }
    public function ArtistsAccountsInfo(){
        $user_id = \Auth::user()->id;
        $getuseremail = User::where('id',$user_id)->value('email');
        $listaccounts = ArtistAccounts::where('artist_email',$getuseremail)->get();
        return view ('backend.ArtistsAccountsInfo')->with(['listaccounts'=>$listaccounts]);
    }

    public function Playlist(){
//        $listplaylist = PlaylistInfo::all();
        $listcate = PlaylistCategory::all();
//        $listplaylist = PlaylistInfo::join('playlistcategory', 'playlistcategory.id', '=', 'playlistinfo.playlist_cate')
//            ->select('playlistinfo.*','playlistcategory.category')
//            ->get();
        $listplaylist = SongInfo::where('playlist_payment_status','Claimed Payment')->get();

        return view('backend.Playlist')->with(['listplaylist'=>$listplaylist,'listcate'=>$listcate]);
    }
    public function PlaylistRejected(){
        $listplaylist = SongInfo::where('playlist_payment_status','Payment Rejected')->get();
        return view('backend.PlaylistRejected')->with(['listplaylist'=>$listplaylist]);
    }
    public function PlaylistCompleted(){
        $listplaylist = SongInfo::where('playlist_payment_status','Payment completed')->get();
        return view('backend.PlaylistCompleted')->with(['listplaylist'=>$listplaylist]);
    }
    public function AllSongs(){
        $archive = Archives::all();
        $listplaylist = SongInfo::where('archives_status',null)->get();

//        foreach ($listplaylist as $datas){
//            $this->CheckVotes($datas);
//            $this->CheckVotesMonthly($datas);
//            $this->ArtistModePayment($datas);
//            $datas['votesNumber'] = (double) $this->CheckVotes($datas);
//            $datas['votesNumberMon'] = (string) $this->CheckVotesMonthly($datas);
//            $datas['payment'] = $this->ArtistModePayment($datas);
//            $datas['paymentNumber'] = $this->ArtistModePaymentNumber($datas);
//        }

        foreach ($listplaylist as $datas){
            $this->CheckVotesUnlimited($datas);
            $this->CheckVotesMonthlyUnlimited($datas);

            $this->ArtistModePayment($datas);
            $datas['votesNumber'] = (double) $this->CheckVotesUnlimited($datas);
            $datas['votesNumberMon'] = (string) $this->CheckVotesMonthlyUnlimited($datas);
            $datas['payment'] = $this->ArtistModePayment($datas);
            $datas['paymentNumber'] = $this->ArtistModePaymentNumber($datas);
        }
        return view('backend.AllSongs')->with(['listplaylist'=>$listplaylist,'archive'=>$archive]);
    }

    public function AllSongsAjax(Request $request){
        $archive = Archives::all();
        $song_artist = $request['inputValM'];
        $listplaylist = SongInfo::where('song_name', 'like', '%' . $song_artist. '%')
            ->where('archives_status',null)->get();

//        foreach ($listplaylist as $datas){
//            $this->CheckVotes($datas);
//            $this->CheckVotesMonthly($datas);
//            $this->ArtistModePayment($datas);
//            $datas['votesNumber'] = (double) $this->CheckVotes($datas);
//            $datas['votesNumberMon'] = (string) $this->CheckVotesMonthly($datas);
//            $datas['payment'] = $this->ArtistModePayment($datas);
//            $datas['paymentNumber'] = $this->ArtistModePaymentNumber($datas);
//        }
        foreach ($listplaylist as $datas){
            $this->CheckVotesUnlimited($datas);
            $this->CheckVotesMonthlyUnlimited($datas);

            $this->ArtistModePayment($datas);
            $datas['votesNumber'] = (double) $this->CheckVotesUnlimited($datas);
            $datas['votesNumberMon'] = (string) $this->CheckVotesMonthlyUnlimited($datas);
            $datas['payment'] = $this->ArtistModePayment($datas);
            $datas['paymentNumber'] = $this->ArtistModePaymentNumber($datas);
        }
        return $listplaylist->toArray();
//        return view('backend.AllSongs')->with(['listplaylist'=>$listplaylist,'archive'=>$archive]);
    }

    public function AllSongsFilter(Request $request){
        $archive = Archives::all();
        $all = $request->all();
        $startdate = $request['startdate'];
        $enddate = $request['enddate'];
//        $listplaylist = SongInfo::whereBetween('created_at',[$startdate,$enddate])->get();
        $listplaylist = SongInfo::where('archives_status',null)->get();
//        foreach ($listplaylist as $datas){
//            $this->CheckVotesFilter($datas,$startdate,$enddate);
//            $this->CheckVotesFilter($datas,$startdate,$enddate);
//            $this->CheckVotesMonthly($datas);
//            $datas['votesNumber'] = (double) $this->CheckVotesFilter($datas,$startdate,$enddate);
//            $datas['votesNumberMon'] = (double) $this->CheckVotesMonthly($datas);
//        }
        foreach ($listplaylist as $datas){
            $this->CheckVotesUnlimitedFilter($datas,$startdate,$enddate);
            $this->CheckVotesMonthlyUnlimited($datas);
            $datas['votesNumber'] = (double) $this->CheckVotesUnlimitedFilter($datas,$startdate,$enddate);
            $datas['votesNumberMon'] = (double) $this->CheckVotesMonthlyUnlimited($datas);
        }
        return view('backend.AllSongsFilter')->with(['listplaylist'=>$listplaylist,'archive'=>$archive]);
    }

    public function HomeSlider(){
        $list = HomeSlider::all();
        return view('backend.HomeSlider')->with(['list'=>$list]);
    }
    public function AddHomeSlider(Request $request){
       $all = $request->all();
       $addslider = new HomeSlider();
       $addslider->slider_title = $request['slider_title'];
       $addslider->slider_description = $request['slider_description'];

        $image = $request->file('slider_photo');
        $addslider->slider_photo = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/HomeSlider');
        $image->move($destinationPath, $imagename);
        $addslider->save();
        return back()->with('success','You have successfully added a new slider');
    }
    public function EditHomeSlider(Request $request){
        $all = $request->all();
        if($request->file('slider_photo')){
            $id = $request['id'];
            $editslider = HomeSlider::find($id);
            $editslider->slider_title = $request['slider_title'];
            $editslider->slider_description = $request['slider_description'];
            $image = $request->file('slider_photo');
            $editslider->slider_photo = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/HomeSlider');
            $image->move($destinationPath, $imagename);
            $editslider->save();
            return back()->with('success','You have successfully edited a slider');
        }else{
            $id = $request['id'];
            $editslider = HomeSlider::find($id);
            $editslider->slider_title = $request['slider_title'];
            $editslider->slider_description = $request['slider_description'];
            $editslider->save();
            return back()->with('success','You have successfully edited a slider');
        }
    }
    public function DeleteHomeSlider(Request $request){
        $id = $request['id'];
        $delete_slider = HomeSlider::find($id);
        $delete_slider->delete();
        return back()->with('success','You have successfully deleted a slider');
    }

    public function FundsSlider(){
        $list = FundsSliders::all();
        return view('backend.FundsSlider')->with(['list'=>$list]);
    }
    public function AddFundsSlider(Request $request){
        $all = $request->all();
        $addslider = new FundsSliders();
        $addslider->slider_title = $request['slider_title'];
        $addslider->slider_description = $request['slider_description'];

        $image = $request->file('slider_photo');
        $addslider->slider_photo = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/HomeSlider');
        $image->move($destinationPath, $imagename);
        $addslider->save();
        return back()->with('success','You have successfully added a new slider');
    }

    public function EditFundsSlider(Request $request){
        $all = $request->all();
        if($request->file('slider_photo')){
            $id = $request['id'];
            $editslider = FundsSliders::find($id);
            $editslider->slider_title = $request['slider_title'];
            $editslider->slider_description = $request['slider_description'];
            $image = $request->file('slider_photo');
            $editslider->slider_photo = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/HomeSlider');
            $image->move($destinationPath, $imagename);
            $editslider->save();
            return back()->with('success','You have successfully edited a slider');
        }else{
            $id = $request['id'];
            $editslider = FundsSliders::find($id);
            $editslider->slider_title = $request['slider_title'];
            $editslider->slider_description = $request['slider_description'];
            $editslider->save();
            return back()->with('success','You have successfully edited a slider');
        }
    }
    public function DeleteFundsSlider(Request $request){
        $id = $request['id'];
        $delete_slider = FundsSliders::find($id);
        $delete_slider->delete();
        return back()->with('success','You have successfully deleted a slider');
    }

    public function BroadCast(){
        $list = BroadCast::all();
        return view('backend.BroadCast')->with(['list'=>$list]);
    }

    public function AddBroadCast(Request $request){
        $all = $request->all();
        $addslider = new BroadCast();
        $addslider->slider_title = $request['slider_title'];
        $addslider->slider_description = $request['slider_description'];
//        $addslider->slider_status = $request['slider_status'];

        $image = $request->file('slider_photo');
        $addslider->slider_photo = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/HomeSlider');
        $image->move($destinationPath, $imagename);
        $addslider->save();
        return back()->with('success','You have successfully added a new broadcast slider');

    }

    public function EditBroadCast(Request $request){
        $all = $request->all();
        if($request->file('slider_photo')){
            $id = $request['id'];
            $editslider = BroadCast::find($id);
            $editslider->slider_title = $request['slider_title'];
            $editslider->slider_description = $request['slider_description'];
            $editslider->slider_status = $request['slider_status'];
            $image = $request->file('slider_photo');
            $editslider->slider_photo = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/HomeSlider');
            $image->move($destinationPath, $imagename);
            $editslider->save();
            return back()->with('success','You have successfully edited a slider');
        }else{
            $id = $request['id'];
            $editslider = BroadCast::find($id);
            $editslider->slider_title = $request['slider_title'];
            $editslider->slider_description = $request['slider_description'];
            $editslider->slider_status = $request['slider_status'];
            $editslider->save();
            return back()->with('success','You have successfully edited a slider');
        }
    }

    public function DeleteBroadCast(Request $request){
        $id = $request['id'];
        $delete = BroadCast::find($id);
        $delete->delete();
        return back()->with('success','You have successfully deleted a slider');
    }
    public function BroadCastTopic(){
        $list = BroadcastTopic::all();
        return view('backend.BroadCastTopic')->with(['list'=>$list]);
    }
    public function AddBroadCastTopic(Request $request){
        $all = $request->all();
        $add = new BroadcastTopic();
        $add->topic_name = $request['topic_name'];
        $add->save();
        return back()->with('success','You have successfully added  a broadcast topic');
    }
    public function UpdatedBroadCastTopic(Request $request){
        $id = $request['id'];
        $update = BroadcastTopic::find($id);
        $update->topic_name = $request['topic_name'];
        $update->save();
        return back()->with('success','You have successfully updated broadcast topic');
    }
    public function DeleteBroadCastTopic(Request $request){
        $id = $request['id'];
        $delete = BroadcastTopic::find($id);
        $delete->delete();
        return back()->with('success','You have successfully deleted broadcast topic');
    }
    public function BroadCastTopicComment(Request $request){
        $id = $request['id'];
        $list_comments = BroadCastComment::where('broadcast_id',$id)->get();
        return view('backend.BroadCastTopicComment')->with(['list_comments'=>$list_comments]);
    }
    public function MusicStakeHolders(){
        $list = MusicStakeHolders::all();
        return view('backend.MusicStakeHolders')->with(['list'=>$list]);
    }
    public function AddMusicStakeHolders(Request $request){
        $all = $request->all();
        $addslider = new MusicStakeHolders();
        $addslider->slider_title = $request['slider_title'];
        $addslider->slider_description = $request['slider_description'];
        $addslider->slider_moto = $request['slider_moto'];

        $image = $request->file('slider_photo');
        $addslider->slider_photo = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/HomeSlider');
        $image->move($destinationPath, $imagename);
        $addslider->save();
        return back()->with('success','You have successfully added a new music stakeholder slider');

    }
    public function EditMusicStakeHolders(Request $request){
        $all = $request->all();
        if($request->file('slider_photo')){
            $id = $request['id'];
            $editslider = MusicStakeHolders::find($id);
            $editslider->slider_title = $request['slider_title'];
            $editslider->slider_description = $request['slider_description'];
            $editslider->slider_moto = $request['slider_moto'];
            $image = $request->file('slider_photo');
            $editslider->slider_photo = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/HomeSlider');
            $image->move($destinationPath, $imagename);
            $editslider->save();
            return back()->with('success','You have successfully edited a slider');
        }else{
            $id = $request['id'];
            $editslider = MusicStakeHolders::find($id);
            $editslider->slider_title = $request['slider_title'];
            $editslider->slider_description = $request['slider_description'];
            $editslider->slider_moto = $request['slider_moto'];
            $editslider->save();
            return back()->with('success','You have successfully edited a slider');
        }
    }

    public function DeleteMusicStakeHolders(Request $request){
        $id = $request['id'];
        $delete = MusicStakeHolders::find($id);
        $delete->delete();
        return back()->with('success','You have successfully deleted a slider');
    }
    public function RefreshMomo(Request $request){
        $all = $request->all();
        $stat='PENDING';

        $tomorrow = $request['startdate'];
        $yesterday = $request['enddate'];

        $transactions = MomoTransaction::get()->count();
        $transacs = MomoTransaction::where('status','like','%'.$stat.'%')
            ->whereBetween('created_at', [$yesterday,$tomorrow])
            ->get();

        $num = count((array)$transactions);

        $nums = array(count((array)$transactions)=>$num);

        //dd($transacs);
        //return view('backend.dashboard')->with(['num'=>$num]);

        foreach($transacs as $transac){

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$transac->transactionid",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "Accept: */*",
                    "Cache-Control: no-cache",
                    "Connection: keep-alive",
                    "Host: akokanya.com",
                    "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                    "User-Agent: PostmanRuntime/7.11.0",
                    "accept-encoding: gzip, deflate",
                    "cache-control: no-cache",
                    "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                    "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
//                echo "cURL Error #:" . $err;
                return response()->json([
                    'message' =>"Oooops something went wrong",
                    'status' =>false,
                ]);
            } else {
                //echo $response;

                $responsedata=json_decode($response);
                //Update to database Momo payments
                //echo $responsedata[0]->payment_status;

                $deletedat=0;

                $Momopayment = MomoTransaction::where('transactionid',$responsedata[0]->external_payment_code)
                    ->update([
                        'transactionid'=> $responsedata[0]->external_payment_code,
                        'status'=> $responsedata[0]->payment_status,
                        'assignedid'=> $responsedata[0]->id,
                        'company_name'=> $responsedata[0]->company_name,
                        'code'=> $responsedata[0]->code,
                        // 'amount'=> $responsedata[0]->amount,
                        'payment_code'=> $responsedata[0]->payment_code,
                        'external_payment_code'=> $responsedata[0]->external_payment_code,
                        'payment_status'=> $responsedata[0]->payment_status,
                        'payment_type'=> $responsedata[0]->payment_type,
                        'callback_url'=> $responsedata[0]->callback_url,
                        'momodeleted_at'=> $deletedat,
                        'momocreated_at'=> $responsedata[0]->created_at,
                        'momoupdated_at'=> $responsedata[0]->updated_at,

                    ]);
                if($responsedata[0]->payment_status ==  "SUCCESSFUL"){
                    $updateverstatus = Votes::where('id',$transac->voter_id)->update(['voter_status'=> $responsedata[0]->payment_status]);
//                    return response()->json([
//                        'message' =>"You have successfully refreshed transactions",
//                        'status' =>true,
//                    ]);
                }
            }
        }
        return response()->json([
            'message' =>"You have successfully refreshed transactions",
            'status' =>true,
        ]);
    }
    public function SearchSong(Request $request){
//
        $firstday = new Carbon('first day of this month');
        $dt = Carbon::now();
        $currentdate = $firstday->subMonth();
        $testindate = $dt->diffForHumans($firstday);

        $lastday = new Carbon('last day of this month');
        $endofmonth = new Carbon('last day of this month');
        $lastday_ = $lastday->subDays(7);

        $testdate = Carbon::parse($firstday);
        $testdateL = Carbon::parse($lastday_);
        $newfirst = $testdate->format('jS F Y');
        $newlast = $testdateL->format('jS F Y');

        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();
        $endmontdate = $endofmonth->toDateString();
//        $name = "idsvvds";
        $name = $request['modernSearch'];

        $getmusicmodern = SongInfo::where('song_name', 'like', '%' . $name . '%')->get();

//        if(0 == count($getmusicmodern)){
//            return response()->json([
//                'response_message' => "",
//                'response_status' =>400
//            ]);
//        }else{
//            return response()->json($getmusicmodern);
//        }

        if (0 == count($getmusicmodern)) {
//            echo "<div class=\"director-speech clearfix\" style=\"padding-top: 30px\"> <img src=\"front/images/logo/logojpeg.jpeg\" class=\"d-img\"> <div class=\"bio-block\"> <h6 class=\"name\" style=\"padding: 15px 0;\">Oops no results found</h6> </div></div>";
        }else {
            foreach ($getmusicmodern as $index => $getmusicsmodern) {
                $votesbe = $getmusicsmodern->votesNumber;
                $newvotes = $votesbe - 1;
                $created_at = $getmusicsmodern->created_at;
                $lastTimeLoggedOut = \Illuminate\Support\Carbon::parse($created_at)->diffForHumans();
                $voteroute = route('NewVoteNow', ['id' => $getmusicsmodern->id]);
                $song_id = $getmusicsmodern->id;
                $votes = Votes::where('voter_status', 'SUCCESSFUL')->select(DB::raw('count(id) as votes'))->where('song_id', $song_id)
                    ->where('vote', '1')
                    ->value('votes');
                $newvotestotal = $votes - 1;
                $counts = $index + 1;
                $route = route('backend.PromoteSong',['id'=> $getmusicsmodern->id]);
//                echo "<div class='row' style='border: 1px solid #1c706f;margin-bottom: 5px;'><div class='col-md-12 songinfo'> <div class='director-speech clearfix'> <img src='SongImages/$getmusicsmodern->song_cover_picture' alt='' class='d-img'> <div class='bio-block'> <h6 class='name' >$getmusicsmodern->song_name</h6> <span style='color: #848484 !important;'>$getmusicsmodern->song_artist</span><br><span><a href='$getmusicsmodern->song_youtube_link' target='_blank'><i class='fa fa-youtube' aria-hidden='true' style='font-size: 30px;color:#1c706d;'></i></a></span> <h6 class='name'> <span> Amajwi y'ukwezi: $newvotes </span><br><span style='font-size: 12px; !important;'>$lastTimeLoggedOut</span> </h6> </div><div class='bio-block' id='votesmobile' style='position: absolute; right: 20%; float: none; top: 14%;'> </div><h6 class='sign'> <a class='votebutton' href='$voteroute'>Tora</a><br><span style='top: 5px;position: relative;'> Total: $newvotestotal </span> </h6></div></div></div>";
//                echo "<div class=\"director-speech clearfix\" style=\"padding-top: 30px\"> <img src=\"SongImages/$getmusics->song_cover_picture\" class=\"d-img\"> <div class=\"bio-block\"> <h6 class=\"name\">$getmusics->song_artist</h6> <span>$getmusics->song_name</span><br><span><a href='$getmusics->song_youtube_link' target='_blank'><i class='fa fa-youtube' aria-hidden='true' style='font-size: 30px;color:#fff;'></i></a></span></div><h6 class=\"sign\"> <a class='votebutton' href='$voteroute'>Tora</a><br> </h6> <div id=\"$getmusics->id-m\" class=\"modal\" data-backdrop=\"true\"> <div class=\"modal-dialog\"> <div class=\"modal-content\"> <div class=\"modal-header\"> <h5 class=\"modal-title\">$getmusics->song_name Music Video</h5> </div><div class=\"modal-body text-center p-lg\"> $getmusics->song_youtube_link</div><div class=\"modal-footer\"> <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Close</button> </div></div></div></div></div>";
                echo "<table class=\"table table-striped table-bordered dataex-html5-export table-responsive\">
                    <thead>
                        <tr>
                           <th>Song Artist</th>
                            <th>Song Name</th>
                            <th>Song Cover Picture</th>
                            <th>Total Votes</th>
                            <th>Song Code</th>
                            <th>Promotion Status</th>
                            <th>Date Created</th>
                        </tr>
                    </thead>
                    <tbody>
                    <td><a href='$route'>$getmusicsmodern->song_artist</a></td>
                    <td>$getmusicsmodern->song_name</td>
                    <td><img src=\"SongImages/$getmusicsmodern->song_cover_picture\" style=\"width:100px\"></td>
                    <td>$newvotestotal</td>
                    <td>$getmusicsmodern->playlist_code</td>
                    <td>$getmusicsmodern->promotion_status</td>
                    <td>$getmusicsmodern->created_at</td>
                    </tbody>";

            }
        }

    }

    public function PromoteSong(Request $request){
        $id = $request['id'];
        $getmusicmodern = SongInfo::where('id',$id)->get();
        return view('backend.PromoteSong')->with(['getmusicmodern'=>$getmusicmodern]);
    }

    public function PromoteStatus(Request $request){
        $all = $request->all();
        $check_promoted_song = SongInfo::where('promotion_status','Promoted')->count();
//        dd($check_promoted_song);
        if($request['promotion_status'] == "Promoted"){
            if($check_promoted_song < 6){
                $phonenumbers = $request['phonenumber'];
                $unlimitedamount = ($request['promotion_period'] * 10000);

                $firstday = new Carbon('first day of last month');
                $lastday = new Carbon('last day of last month');
                $lastday_ = $lastday->subDays(7);
                $firstdaydate = $firstday->toDateString();
                $lastdaydate = $lastday_->toDateString();

                $str_number = substr($phonenumbers, 3);
                $phonenumber = $phonenumbers;
//                    $phonenumber = $phoneNumber;
                $amount = $unlimitedamount;
                $idt = mt_rand(10, 99);
                //Generating Payment Gateway
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://akokanya.com/mtn-pay?amount=$amount&phone=$phonenumber&company_name=MNI&payment_code=$idt",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => "",
                    CURLOPT_HTTPHEADER => array(
                        "Accept: */*",
                        "Cache-Control: no-cache",
                        "Connection: keep-alive",
                        "Host: akokanya.com",
                        "Postman-Token: caddbf9d-3cf9-4fc3-b051-bc0ee25a2561,de8e362a-f9dd-4a20-a3bf-099dba0f6b26",
                        "User-Agent: PostmanRuntime/7.11.0",
                        "accept-encoding: gzip, deflate",
                        "cache-control: no-cache",
                        "content-length: "
                    ),
                ));

                $responseapi = curl_exec($curl);
//            dd($response);
                $err = curl_error($curl);

                curl_close($curl);

                if ($err) {
                    //echo "cURL Error #:" . $err;
                    $title="Not Connected";
                    $message="Sorry you do not have no internet connection";
                    //return redirect()->route('balance')->with($title,$message);
//            echo "$err";
                    return response()->json([
                        'message' =>$message,
                    ]);

                } else {
                    //echo $response;

                    //Showing Status
                    //second part Status checking

                    $responseid=json_decode($responseapi);
//                        dd($responseid);

                    $curl = curl_init();

                    if($responseapi=='{"@attributes":{"errorcode":"TARGET_AUTHORIZATION_ERROR"}}'){
                        //checking balance
                        $maintitle="Balance";
                        $title="Not Enough Balance";
                        $response ="Sorry you do not have enough money on your account to make this transaction, please try again after topping up your account";
                        //return redirect()->route('balance')->with($title,$message);
                        return response()->json([
                            'message' =>$response,
                        ]);
//                return view('VotePayError')->with(['response'=>$response]);
                    }
                    elseif($responseapi=='{"@attributes":{"errorcode":"ACCOUNTHOLDER_WITH_FRI_NOT_FOUND"},"arguments":{"@attributes":{"name":"fri","value":"FRI:25'.$phonenumber.'\/MSISDN"}}}'){
                        //User not registered in Momo
                        $maintitle="Not Registered";
                        $title="Not Registered";
                        $response="Sorry you're not registered with MTN Mobile Rwanda";
//                return view('VotePayError')->with(['response'=>$response]);
                        return response()->json([
                            'message' =>$response,
                        ]);
                    }
                    elseif($responseapi=='{"error":"the minimum amount is 100"}'){
                        //User not registered in Momo
                        $maintitle="Minimum amount";
                        $title="Minimum amount";
                        $response="Sorry the minimum amount to send is 100Frw";
//                return view('VotePayError')->with(['response'=>$response]);
                        return response()->json([
                            'message' =>$response,
                        ]);
                    }
                    elseif($responseapi=='{"@attributes":{"errorcode":"AUTHORIZATION_MAXIMUM_AMOUNT_ALLOWED_TO_SEND"}}'){
                        //User not registered in Momo
                        $maintitle="Maximum amount";
                        $title="Maximum amount";
                        $response="Sorry the maximum amount to send is 2,000,000Frw";
                        //return redirect()->route('balance')->with($title,$message);
//                return view('VotePayError')->with(['response'=>$response]);
                        return response()->json([
                            'message' =>$response,
                        ]);
                    }
                    else{
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$responseid->transactionid",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "GET",
                            CURLOPT_POSTFIELDS => "",
                            CURLOPT_HTTPHEADER => array(
                                "Accept: */*",
                                "Cache-Control: no-cache",
                                "Connection: keep-alive",
                                "Host: akokanya.com",
                                "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                                "User-Agent: PostmanRuntime/7.11.0",
                                "accept-encoding: gzip, deflate",
                                "cache-control: no-cache",
                                "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                                "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
                            ),
                        ));

                        $responseapis = curl_exec($curl);
                        $err = curl_error($curl);

                        curl_close($curl);

                        if ($err) {
                            echo "cURL Error #:" . $err;
                        } else {
                            //echo $response;

                            $responsedata=json_decode($responseapis);
//                        dd($responsedata);
                            //Save to database Momo payments
                            //dd( $amount);
                            $deletedat=0;
                            $Momopayment = PromotionTransactions::create([
                                'phonenumber'=> $phonenumber,
                                'song_id'=> $request['song_id'],
                                'promotion_period'=> $request['promotion_period'],
                                'transactionid'=> $responsedata[0]->external_payment_code,
                                'status'=> $responsedata[0]->payment_status,
                                'assignedid'=> $responsedata[0]->id,
                                'company_name'=> $responsedata[0]->company_name,
                                'code'=> $responsedata[0]->code,
                                'amount'=> $amount,
                                'payment_code'=> $responsedata[0]->payment_code,
                                'external_payment_code'=> $responsedata[0]->external_payment_code,
                                'payment_status'=> $responsedata[0]->payment_status,
                                'payment_type'=> $responsedata[0]->payment_type,
                                'callback_url'=> $responsedata[0]->callback_url,
                                'momodeleted_at'=> $deletedat,
                                'momocreated_at'=> $responsedata[0]->created_at,
                                'momoupdated_at'=> $responsedata[0]->updated_at,
                            ]);
                            if($Momopayment){
//                        event(new Event($Momopayment));
                                $response="Kanda *182*7# Ukurikize amabwiriza, amafaranga ni $amount RWF";
                                return response()->json([
                                    'message' =>$response,
                                    'transactionid'=>$responsedata[0]->external_payment_code,
                                    'payment_status'=>$responsedata[0]->payment_status,
                                    'status_promoted' =>'promoted',
                                ]);
//                        return view('Thankyou')->with(['song_name'=>$song_name,'song_artist'=>$song_artist]);
                            }
                            //end saving to database Momo payments
                        }
                    }
                }

            }else{
                return response()->json([
                    'message' =>"We have reached maximum songs to promote",
                    'status_promoted' =>'reached maximum',
                ]);
            }

        }
        else{
            $update_song = SongInfo::where('id',$request['song_id'])
                ->update(['promotion_status'=>$request['promotion_status'],'promotion_period'=>null]);
            return response()->json([
                'message' =>"You have successfully changed promotion status",
                'status_promoted' =>'not promoted',
            ]);
        }

    }

    public function CallBackPromotedMomo(Request $request){
        $transactionid = $request['transactionid'];
//        $transactionid = "2367841156";
        $getVoter_id = PromotionTransactions::where('transactionid',$transactionid)->value('song_id');
        $getpromo = PromotionTransactions::where('transactionid',$transactionid)->value('promotion_period');

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$transactionid",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Host: akokanya.com",
                "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                "User-Agent: PostmanRuntime/7.11.0",
                "accept-encoding: gzip, deflate",
                "cache-control: no-cache",
                "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            //echo $response;

            $responsedata=json_decode($response);
            //Update to database Momo payments
            //echo $responsedata[0]->payment_status;

            $deletedat=0;

            $Momopayment = PromotionTransactions::where('transactionid',$responsedata[0]->external_payment_code)
                ->update([
                    'transactionid'=> $responsedata[0]->external_payment_code,
                    'status'=> $responsedata[0]->payment_status,
                    'assignedid'=> $responsedata[0]->id,
                    'company_name'=> $responsedata[0]->company_name,
                    'code'=> $responsedata[0]->code,
                    // 'amount'=> $responsedata[0]->amount,
                    'payment_code'=> $responsedata[0]->payment_code,
                    'external_payment_code'=> $responsedata[0]->external_payment_code,
                    'payment_status'=> $responsedata[0]->payment_status,
                    'payment_type'=> $responsedata[0]->payment_type,
                    'callback_url'=> $responsedata[0]->callback_url,
                    'momodeleted_at'=> $deletedat,
                    'momocreated_at'=> $responsedata[0]->created_at,
                    'momoupdated_at'=> $responsedata[0]->updated_at,

                ]);
            if($responsedata[0]->payment_status == "SUCCESSFUL"){
                $update_song = SongInfo::where('id',$getVoter_id)
                    ->update(['promotion_status'=>"Promoted",'promotion_period'=>$getpromo]);
                return response()->json([
                    'message' =>"Transaction successfully processed",
                    'status' =>"SUCCESSFUL",
                ]);
            }else{
                $status = $responsedata[0]->payment_status;
                return response()->json([
                    'message' =>"Transaction is $status ",
                    'status' =>"PENDING",
                ]);
            }

            //end Update to database Momo payments
        }
    }

    public function Fundings(){
        $list = Funds::join('users', 'users.id', '=', 'funds.fund_user_id')
            ->select('funds.*','users.name')->get();
        foreach ($list as $datas){
            $this->CheckRaisedAmount($datas);
//            $datas['votesNumber'] = (double) $this->CheckVotes($datas);
            $datas['CheckRaisedAmount'] = (double) $this->CheckRaisedAmount($datas);
        }
        return view('backend.Fundings')->with(['list'=>$list]);
    }

    public function FundingsUser(){
        $user_id = \Auth::user()->id;
        $list = Funds::where('fund_user_id',$user_id)
            ->join('users', 'users.id', '=', 'funds.fund_user_id')
            ->select('funds.*','users.name')->get();
        foreach ($list as $datas){
            $this->CheckRaisedAmount($datas);
//            $datas['votesNumber'] = (double) $this->CheckVotes($datas);
            $datas['CheckRaisedAmount'] = (double) $this->CheckRaisedAmount($datas);
        }
        return view('backend.FundingsUser')->with(['list'=>$list]);
    }
    public function CheckRaisedAmount($datas){
        $donation = FundTransaction::where('payment_status','SUCCESSFUL')->where('fund_id',$datas->id)->sum('amount');
        return json_encode($donation);
    }
    public function AddFund(Request $request){
        $all = $request->all();
//        dd($all);
        $user_id = \Auth::user()->id;
        $add_fund = new Funds();
        $add_fund->fund_title = $request['fund_title'];
        $add_fund->fund_amount = $request['fund_amount'];
        $add_fund->fund_description = $request['fund_description'];
        $add_fund->fund_user_id = $user_id;
        $add_fund->fund_status = "Pending";

        $image = $request->file('fund_image');
        $add_fund->fund_image = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/Funds');
        $image->move($destinationPath, $imagename);
        $add_fund->save();
        return back()->with('success','You have successfully added new fund');
    }

    public function UpdateFundStatus(Request $request){
        $id = $request['id'];
        $update_fund = Funds::where('id', $id)->update(['fund_status'=>$request['fund_status']]);
        return back()->with('success','You have successfully udpated fund status');
    }
    public function UpdateFund(Request $request){
        $id = $request['id'];
        $update_fund = Funds::find($id);
        if($image = $request->file('fund_image')){
            $update_fund->fund_title = $request['fund_title'];
            $update_fund->fund_amount = $request['fund_amount'];
            $update_fund->fund_description = $request['fund_description'];

            $image = $request->file('fund_image');
            $update_fund->fund_image = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/Funds');
            $image->move($destinationPath, $imagename);
            $update_fund->save();
            return back()->with('success','You have successfully added new fund');
        }else{
            $update_fund->fund_title = $request['fund_title'];
            $update_fund->fund_amount = $request['fund_amount'];
            $update_fund->fund_description = $request['fund_description'];
            $update_fund->save();
            return back()->with('success','You have successfully updated new fund');
        }
    }
    public function DeleteFund(Request $request){
        $id = $request['id'];
        $delete = Funds::find($id);
        $delete->delete();
        return back()->with('success','You have successfully deleted fund');
    }
    public function AllArchived(){
        $archive = Archives::all();
        $listplaylist = SongInfo::where('archives_status','archived')->get();
        foreach ($listplaylist as $datas){
//            $this->CheckVotes($datas);
//            $this->CheckVotesMonthly($datas);
            $this->CheckVotesUnlimited($datas);
            $this->CheckVotesMonthlyUnlimited($datas);

            $this->ArtistModePayment($datas);
//            $datas['votesNumber'] = (double) $this->CheckVotes($datas);
//            $datas['votesNumberMon'] = (string) $this->CheckVotesMonthly($datas);

            $datas['votesNumber'] = (double) $this->CheckVotesUnlimited($datas);
            $datas['votesNumberMon'] = (string) $this->CheckVotesMonthlyUnlimited($datas);

            $datas['payment'] = $this->ArtistModePayment($datas);
            $datas['paymentNumber'] = $this->ArtistModePaymentNumber($datas);
        }
        return view('backend.AllArchived')->with(['listplaylist'=>$listplaylist,'archive'=>$archive]);
    }
    public function FilterArchived(Request $request){
        $archive = Archives::all();
        $archives = archivessongs::where('id',$request['archive_name'])
            ->whereBetween('created_at', [$request['archive_starting_date'], $request['archive_ending_date']])
            ->get();
        $new = json_decode($archives);
        foreach ($new as $data){
            $ids = $data->song_id;
        }
        $ids= array();
        foreach($new as $data){
            array_push($ids, $data->song_id);
        }
        $listplaylist = SongInfo::whereIn('id',$ids)->where('archives_status','archived')
            ->get();

        foreach ($listplaylist as $datas){
            $this->CheckVotes($datas);
            $this->CheckVotesMonthly($datas);
            $this->ArtistModePayment($datas);
            $datas['votesNumber'] = (double) $this->CheckVotes($datas);
            $datas['votesNumberMon'] = (string) $this->CheckVotesMonthly($datas);
            $datas['payment'] = $this->ArtistModePayment($datas);
            $datas['paymentNumber'] = $this->ArtistModePaymentNumber($datas);
        }
        return view('backend.AllArchived')->with(['listplaylist'=>$listplaylist,'archive'=>$archive]);
    }
    public function CheckVotesUnlimited($datas){
//        $getvotes = Votes::where('song_id',$datas->id)
//            ->where('voter_status','SUCCESSFUL')
//            ->count();
        $getvotes = Votes::where('song_id',$datas->id)
            ->where('voter_status','SUCCESSFUL')
            ->sum('vote');

        return json_encode($getvotes);
    }

    public function CheckVotesMonthlyUnlimited($datas){
        $firstday = new Carbon('first day of this month');
        $endofmonth = new Carbon('last day of this month');

        $firstdaydate = $firstday->toDateString();
        $endmontdate = $endofmonth->toDateString();

//        $getvotesM = Votes::where('song_id',$datas->id)->where('voter_status','SUCCESSFUL')
//            ->whereBetween('created_at', [$firstdaydate,$endmontdate])
//            ->count();
        $getvotesM = Votes::where('song_id',$datas->id)->where('voter_status','SUCCESSFUL')
            ->whereBetween('created_at', [$firstdaydate,$endmontdate])
            ->sum('vote');
        return json_encode($getvotesM);
    }

    public function CheckVotesUnlimitedFilter($datas,$startdate,$enddate){
        $getvotes = Votes::whereBetween('created_at', [$startdate,$enddate])->where('song_id',$datas->id)
            ->where('voter_status','SUCCESSFUL')
            ->sum('vote');
        return json_encode($getvotes);
    }
    public function CheckVotesMonthlyUnlimitedFilter($datas){

        $firstday = new Carbon('first day of this month');
        $endofmonth = new Carbon('last day of this month');

        $firstdaydate = $firstday->toDateString();
        $endmontdate = $endofmonth->toDateString();

        $getvotesM = Votes::where('song_id',$datas->id)->where('voter_status','SUCCESSFUL')
            ->whereBetween('created_at', [$firstdaydate,$endmontdate])
            ->sum('vote');

        return json_encode($getvotesM);
    }

    public function CheckVotesLastMonthUnlimited($datas){
        $firstday = new Carbon('first day of last month');
        $endofmonth = new Carbon('last day of last month');
        $firstdaynexm = new Carbon('first day of this month');

        $firstdaydate = $firstday->toDateString();
        $endmontdate = $endofmonth->toDateString();
        $getvotesM = Votes::where('song_id',$datas->id)->where('voter_status','SUCCESSFUL')
            ->whereBetween('created_at', [$firstdaydate,$firstdaynexm])
            ->sum('vote');

        return json_encode($getvotesM);
    }
    public function AddArchivesSong(Request $request){
        $all = $request->all();
        $add = archivessongs::create($all);
        $song_info = SongInfo::where('id',$request['song_id'])->update(['archives_status'=>'archived']);
        return back()->with('success','You have successfully archived your song');
    }
    public function AddArchives(Request $request){
        $all = $request->all();
        $add = Archives::create($all);
        return back()->with('success','You have successfully created archives');
    }
    public function CheckVotes($datas){
        $getvotes = Votes::where('song_id',$datas->id)
            ->where('voter_status','SUCCESSFUL')
            ->count();
        return json_encode($getvotes);
    }
    public function CheckVotesFilter($datas,$startdate,$enddate){
        $getvotes = Votes::whereBetween('created_at', [$startdate,$enddate])->where('song_id',$datas->id)
            ->where('voter_status','SUCCESSFUL')
            ->count();
        return json_encode($getvotes);
    }
    public function CheckVotesMonthly($datas){
        $firstday = new Carbon('first day of this month');
        $endofmonth = new Carbon('last day of this month');

        $firstdaydate = $firstday->toDateString();
        $endmontdate = $endofmonth->toDateString();

        $getvotesM = Votes::where('song_id',$datas->id)->where('voter_status','SUCCESSFUL')
            ->whereBetween('created_at', [$firstdaydate,$endmontdate])
            ->count();

    }
    public function ArtistModePayment($datas){
        $payment = ArtistAccounts::where('id',$datas->user_id)->value('artist_modeofpayment');
        return json_encode($payment);
    }
    public function ArtistModePaymentNumber($datas){
        $payment = ArtistAccounts::where('id',$datas->user_id)->value('artist_mobilemoney_number');
        return json_encode($payment);
    }
    public function SongPlaylist(Request $request){
        $id = $request['id'];
        $listsongs = SongInfo::where('playlist_id',$id)->get();

        return view('backend.SongPlaylist')->with(['listsongs'=>$listsongs]);
    }
    public function DeletePlaylist(Request $request){
        $id = $request['id'];
        $deleteplaylist = SongInfo::find($id);
        $deleteplaylist->delete();
        return back();
    }
    public function DeleteSongs(Request $request){
        $id = $request['id'];
        $deleteplaylist = SongInfo::find($id);
        $deleteplaylist->delete();
        return back();
    }
    public function AddPlaylist(Request $request){
        $all = $request->all();
//        dd($all);
        $firstday = new Carbon('first day of this month');
        $lastday = new Carbon('last day of this month');

        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday->toDateString();

        $user_id = \Auth::user()->id;
        $getemail = User::where('id',$user_id)->value('email');
        $monthname = $firstday->format('F');

        $checksong = SongInfo::where('song_name', 'like', '%' . $request['song_name'] . '%')->where('song_artist', 'like', '%' . $request['song_artist'] . '%')->get();

        if(0 == count($checksong)){
            $checkdates = SongInfo::where('user_id',$user_id)->whereBetween('starting_date',[$firstdaydate,$lastdaydate])->value('playlist_name');
            $currentMonth = Carbon::now()->format('m');
            $currentyear = Carbon::now()->format('Y');
            $code = mt_rand(10, 99). $currentMonth;

            $messages = [
                'song_cover_picture' => 'The file to upload must be less than or equal to 500kb and  in JPG, PNG or JPEG format',
            ];
            $request->validate([
                'song_cover_picture'   => 'mimes:jpeg,jpg,png|required|max:500',
            ],$messages);


            $image = $request->file('song_cover_picture');

            $addsong = new SongInfo();
            $addsong ->starting_date = $firstdaydate;
            $addsong ->ending_date = $lastdaydate;
            $addsong ->user_id = $user_id;
            $addsong ->playlist_name = $monthname ." $currentyear ". 'Selection';
            $addsong ->playlist_cate = "";
            $addsong ->playlist_payment_status = "Not Payed";
            $addsong ->playlist_code =$code;
            $addsong->song_artist = $request['song_artist'];
            $addsong->song_name = $request['song_name'];
            $addsong->song_youtube_link = $request['song_youtube_link'];

            $image = $request->file('song_cover_picture');
            $addsong->song_cover_picture = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/SongImages');
            $image->move($destinationPath, $imagename);
            $addsong->save();
            $last_id = $addsong->id;


            $vote = new Votes();
            $vote->starting_date = $firstdaydate;
            $vote->ending_date = $lastdaydate;
            $vote->voterphonenumber = "";
            $vote->vote = "1";
            $vote->voter_status = "SUCCESSFUL";
            $vote->voter_artist = $request['song_artist'];
            $vote->voter_artist_song = $request['song_name'];
            $vote->song_id = $last_id;
            $vote->save();


            return back()->with('success','you have successfully added a new song on MNI Selection');
        }else{
            return back()->with('success','you already have a this song on MNI Selection');
        }


//        if(0 == count($checkdates)){
//            $user_id = \Auth::user()->id;
//            $currentMonth = Carbon::now()->format('m');
//            $code = mt_rand(10, 99). $currentMonth;
//
//            $messages = [
//                'song_cover_picture' => 'The file to upload must be less than or equal to 500kb and  in JPG, PNG or JPEG format',
//            ];
//            $request->validate([
//                'song_cover_picture'   => 'mimes:jpeg,jpg,png|required|max:500',
//            ],$messages);
//
//
//            $image = $request->file('song_cover_picture');
//
//            $addsong = new SongInfo();
//            $addsong ->starting_date = $firstdaydate;
//            $addsong ->ending_date = $lastdaydate;
//            $addsong ->user_id = $user_id;
//            $addsong ->playlist_name = $monthname . 'Selection';
//            $addsong ->playlist_cate = $request['playlist_cate'];
//            $addsong ->playlist_payment_status = "Not Payed";
//            $addsong ->playlist_code =$code;
//            $addsong->song_artist = $request['song_artist'];
//            $addsong->song_name = $request['song_name'];
//            $addsong->song_youtube_link = $request['song_youtube_link'];
//
//            $image = $request->file('song_cover_picture');
//            $addsong->song_cover_picture = $image->getClientOriginalName();
//            $imagename = $image->getClientOriginalName();
//            $destinationPath = public_path('/SongImages');
//            $image->move($destinationPath, $imagename);
//            $addsong->save();
//
//            return back()->with('success','you have successfully added a new song on MNI Selection');
//        }else{
//            return back()->with('success','you already have a selection for this month, kindly wait for next month');
//        }

    }
    public function EditPlaylist(Request $request){
        $all= $request->all();
        $id = $request['id'];
        $user_id = \Auth::user()->id;
        if($image = $request->file('song_cover_picture')){
            $image = $request->file('song_cover_picture');

            $addsong = SongInfo::find($id);
            $addsong ->starting_date = $request['starting_date'];
            $addsong ->ending_date = $request['ending_date'];
            $addsong ->user_id = $user_id;
            $addsong ->playlist_name = $request['playlist_name'];
            $addsong ->playlist_cate = $request['playlist_cate'];
            $addsong ->playlist_payment_status = $request['playlist_payment_status'];
            $addsong ->playlist_code = $request['playlist_code'];
            $addsong->song_artist = $request['song_artist'];
            $addsong->song_name = $request['song_name'];
            $addsong->song_youtube_link = $request['song_youtube_link'];

            $image = $request->file('song_cover_picture');
            $addsong->song_cover_picture = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/SongImages');
            $image->move($destinationPath, $imagename);
            $addsong->save();
            return back()->with('success', 'You have successfully edited playlist');
        }else{
            $addsong = SongInfo::find($id);
            $addsong ->starting_date = $request['starting_date'];
            $addsong ->ending_date = $request['ending_date'];
            $addsong ->user_id = $user_id;
            $addsong ->playlist_name = $request['playlist_name'];
            $addsong ->playlist_cate = $request['playlist_cate'];
            $addsong ->playlist_payment_status = $request['playlist_payment_status'];
            $addsong ->playlist_code = $request['playlist_code'];
            $addsong->song_artist = $request['song_artist'];
            $addsong->song_name = $request['song_name'];
            $addsong->song_youtube_link = $request['song_youtube_link'];
            $addsong->save();
            return back()->with('success', 'You have successfully edited playlist');
        }

    }
    public function ClaimPayment(Request $request){
        $id = $request['id'];
//        $updateclaim = SongInfo::where('id', $id)->update(['playlist_payment_status'=>'Claimed Payment']);
        $updateclaim = SongInfo::where('id', $id)->update(['playlist_payment_status'=>'Claimed Payment']);
        $getnames = SongInfo::where('id',$id)->value('song_artist');
        $getnumber = SongInfo::where('id',$id)->value('song_number');
        $getsong = SongInfo::where('id',$id)->value('song_name');

        $rsgamessage = "$getnames\n has claimed his payment for this song $getsong, kindly get back to him/her";
        $phonenumber = '0780008883';

        $intnumber = '25'.$phonenumber;
        $api_key = 'c3dheUpQaWp2QWdQb2JPZ0dsaWo=';
        $from = 'MNI';
        $destination = $intnumber;
        $action='send-sms';
        $url = 'https://mistasms.com/sms/api';
        $sms = $rsgamessage;
        $unicode = 1; //For Plain Message
        $unicode = 0; //For Unicode Message
        $sms_body = array(
            'api_key' => $api_key,
            'to' => $destination,
            'from' => $from,
            'sms' => $sms,
            'unicode' => $unicode,
        );
        $client = new \MRSMSAPI();
        $response = $client->send_sms($sms_body, $url);
        $response =json_decode($response);
        return back()->with('success', 'You have successfully claimed your payment, we shall get back to you within 24 hours');
    }
    public function ClaimPaymentAdmin(Request $request){
        $id = $request['id'];
        $payment_status = $request['payment_status'];
        $updateclaim = SongInfo::where('id', $id)->update(['playlist_payment_status'=>$payment_status]);
        $getnames = SongInfo::where('id',$id)->value('song_artist');
        $getid = SongInfo::where('id',$id)->value('user_id');
        $getsong = SongInfo::where('id',$id)->value('song_name');

        $getuseremail= User::where('id',$getid)->value('email');
        $getnumber = ArtistAccounts::where('artist_email',$getuseremail)->value('artist_number');

        $rsgamessage = "$getnames\n your $payment_status";
//        $phonenumber = '0780008883';

        $intnumber = '25'.$getnumber;
        $api_key = 'c3dheUpQaWp2QWdQb2JPZ0dsaWo=';
        $from = 'MNI';
        $destination = $intnumber;
        $action='send-sms';
        $url = 'https://mistasms.com/sms/api';
        $sms = $rsgamessage;
        $unicode = 1; //For Plain Message
        $unicode = 0; //For Unicode Message
        $sms_body = array(
            'api_key' => $api_key,
            'to' => $destination,
            'from' => $from,
            'sms' => $sms,
            'unicode' => $unicode,
        );
        $client = new \MRSMSAPI();
        $response = $client->send_sms($sms_body, $url);
        $response =json_decode($response);
        return back()->with('success', 'You have successfully updated payment status');
    }
    public function SendSMS(){
        $listnumber = JoinMember::where('approval','Approved')->get();
        return view ('backend.SendSMS')->with(['listnumber'=>$listnumber]);
    }
    public function SendSMS_(Request $request){
//        $all = $request->all();
//        dd($all);


//        $username   = "rha";
//        $apiKey     = "55fa2436c4624ed856c4910111fde3ecd425a6bc738ed2fc20d6c85c438258b8";
        $message = $request['limitedtextarea'];
        $phonenumber = $request['phonenumber'];
        $pluscode = '+25';
        $recipients = $pluscode . $phonenumber;

        $username = 'sandbox'; // use 'sandbox' for development in the test environment
        $apiKey 	= 'fa1c39e0943a7802136e51e9b04acc43367003b517b09e6954463a688c27315f'; // use your sandbox app API key for development in the test environment
        $AT = new AfricasTalking($username, $apiKey,"sandbox");
        $SMS = $AT->sms();
        $options = array(
            "to" => '+250782384772',
            "message" => "$message",
        );

        $SMS->send($options);
        return redirect()->route('backend.SendSMS')->with('success','you have successfully sent your message');
    }
    public function SendEmail(){
        $listnumber = JoinMember::where('approval','Approved')->get();
        return view ('backend.SendEmail')->with(['listnumber'=>$listnumber]);
    }
    public function TestApp(){

        $MSISDN = $_POST['MSISDN'];
        $input = base64_decode($_POST['input']);
        $sessionID = $_POST['sessionID'];

        $sessionId   = $sessionID;
        $phoneNumber = $MSISDN;
        $text        = $input;


        $te = substr($text, strrpos($text, '*' )+1);

        //////////////////// Nominate ////////////////////////////////////
////////////////////////////////////////////////////////////////
        if (isset($text)) {

            $checksession = LevelsTrack::where('phonenumber',$phoneNumber)->value('user_session');

            if($checksession == $sessionId){

                $checklevels = LevelsTrack::where('phonenumber',$phoneNumber)->value('user_level');

                if($checklevels == 1 and $text == "747*77"){

                    $response = "Urakaza Neza Kuri MNI Playlist, Kanda \n";
                    $response .= "1. Gutora \n";
                    $response .= "2. Gutanga Indirimbo";

                    header('Content-type: text/json');
                    $response2 = [
                        "RESPONSE_DESCRIPTION" => 'Describe',
                        "RESPONSE_STRING" => $response,
                        "SESSION_STATE" => "CONTINUE",
                        "SESSION_ID" => $sessionId,
                        "MSISDN" => $phoneNumber,
                    ];
                    echo json_encode($response2);

                    //echo $response;

                }elseif ($checklevels == 1 and $text == 1 ){

                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '2','user_choice'=>'1']);
                    $getplaylist = PlaylistCategory::all();

                    $response = "CON MNI Playlist Category \n";

//                    foreach ($getplaylist as $getplaylists){
//                        $category = $getplaylists->category;
//                        $cate_id = $getplaylists->id;
//                        $sepa = "\n";
//                        $response .= $cate_id . ". " . $category . "$sepa";
//                    }

                    header('Content-type: text/json');
                    $response2 = [
                        "RESPONSE_DESCRIPTION" => 'Describe',
                        "RESPONSE_STRING" => $response,
                        "SESSION_STATE" => "CONTINUE",
                        "SESSION_ID" => $sessionId,
                        "MSISDN" => $phoneNumber,
                    ];
                    echo json_encode($response2);
                }

                elseif ($checklevels == 2 and $te == $te ){

                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '3','user_choice'=>$te,'category_track'=>$te]);
                    $getplaylist = PlaylistCategory::all();

                    $firstday = new Carbon('first day of this month');
                    $lastday = new Carbon('last day of this month');
                    $lastday_ = $lastday->subDays(7);

                    $firstdaydate = $firstday->toDateString();
                    $lastdaydate = $lastday_->toDateString();

                    $getplaylist = PlaylistInfo::where('starting_date', $firstdaydate)->where('ending_date', $lastdaydate)->where('playlist_cate', $te)->value('id');
                    $getcategory = PlaylistCategory::where('id', $te)->value('category');
                    $getmusic = SongInfo::where('playlist_id', $getplaylist)->get();

                    $response = "CON MNI $getcategory Playlist \n";

                    foreach ($getmusic as $getmusics) {
                        $artistname = $getmusics->song_artist;
                        $artistid = $getmusics->song_number;
                        $artistmusicname = $getmusics->song_name;
                        $sepa = "\n";
                        $response .= $artistid . ". " . $artistname . " ($artistmusicname) " . "$sepa";

                    }
                    header('Content-type: text/plain');
                    echo $response;
                }

                elseif ($checklevels == 3 and $te == $te ){

                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '4','user_choice'=>$te]);

                    $getcate = LevelsTrack::where('user_session',$sessionId)->value('category_track');

                    $getplaylist_id = PlaylistInfo::where('playlist_cate', $getcate)->value('id');

                    $getmusic_name = SongInfo::where('song_number',$te)->where('playlist_id',$getplaylist_id)->value('song_name');
                    $getmusic_artist = SongInfo::where('song_number',$te)->where('playlist_id',$getplaylist_id)->value('song_artist');

                    $response = "CON Ugiye Gutora indirimbo $getmusic_name ya $getmusic_artist, amafaranga ni 50RWF\n";
                    $response .= "1. Kwemeza \n";
                    $response .= "2. Gusubira inyuma \n";
                    $response .= "3. Gusohoka \n";
                    header('Content-type: text/plain');
                    echo $response;

                }

                elseif ($checklevels == 4 and $te == 1 ){

                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '5','user_choice'=> $te]);
                    $getcate = LevelsTrack::where('user_session',$sessionId)->value('category_track');
                    $getplaylist_id = PlaylistInfo::where('playlist_cate', $getcate)->value('id');

                    $getmusic_name = SongInfo::where('song_number',$te)->where('playlist_id',$getplaylist_id)->value('song_name');
                    $getmusic_artist = SongInfo::where('song_number',$te)->where('playlist_id',$getplaylist_id)->value('song_artist');

//                    foreach ($getmusic as $getmusics){
//                        $artistname = $getmusics->song_artist;
//                        $artistid = $getmusics->song_number;
//                        $artistmusicname = $getmusics->song_name;
//                        $sepa = "\n";
//
//                    }

                    $firstday = new Carbon('first day of last month');
                    $lastday = new Carbon('last day of last month');
                    $lastday_ = $lastday->subDays(7);

                    $firstdaydate = $firstday->toDateString();
                    $lastdaydate = $lastday_->toDateString();

                    $vote = new Votes();
                    $vote->starting_date = $firstdaydate;
                    $vote->ending_date = $lastdaydate;
                    $vote->voterphonenumber = $phoneNumber;
                    $vote->vote = "1";
                    $vote->voter_status = "PENDING";
                    $vote->voter_artist = $getmusic_name;
                    $vote->voter_artist_song = $getmusic_artist;
                    $vote->save();
                    $last_id = $vote->id;

                    $str_number = substr($phoneNumber, 3);
                    $phonenumber = $str_number;
//                    $phonenumber = $phoneNumber;
                    $amount = '50';
                    $idt = mt_rand(10, 99);
                    //Generating Payment Gateway
                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "http://akokanya.com/mtn-pay?amount=$amount&phone=$phonenumber&company_name=MNI&payment_code=$idt",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => "",
                        CURLOPT_HTTPHEADER => array(
                            "Accept: */*",
                            "Cache-Control: no-cache",
                            "Connection: keep-alive",
                            "Host: akokanya.com",
                            "Postman-Token: caddbf9d-3cf9-4fc3-b051-bc0ee25a2561,de8e362a-f9dd-4a20-a3bf-099dba0f6b26",
                            "User-Agent: PostmanRuntime/7.11.0",
                            "accept-encoding: gzip, deflate",
                            "cache-control: no-cache",
                            "content-length: "
                        ),
                    ));

                    $responseapi = curl_exec($curl);
//            dd($response);
                    $err = curl_error($curl);

                    curl_close($curl);

                    if ($err) {
                        //echo "cURL Error #:" . $err;
                        $title="Not Connected";
                        $message="Sorry you do not have no internet connection";
                        //return redirect()->route('balance')->with($title,$message);
                        echo "$err";

                    } else {
                        //echo $response;

                        //Showing Status
                        //second part Status checking

                        $responseid=json_decode($responseapi);
//                        dd($responseid);

                        $curl = curl_init();

                        if($responseapi=='{"@attributes":{"errorcode":"TARGET_AUTHORIZATION_ERROR"}}'){
                            //checking balance
                            $maintitle="Balance";
                            $title="Not Enough Balance";
                            $response ="Sorry you do not have enough money on your account to make this transaction, please try again after topping up your account";
                            //return redirect()->route('balance')->with($title,$message);
                            echo "$response";
                        }
                        elseif($responseapi=='{"@attributes":{"errorcode":"ACCOUNTHOLDER_WITH_FRI_NOT_FOUND"},"arguments":{"@attributes":{"name":"fri","value":"FRI:25'.$phonenumber.'\/MSISDN"}}}'){
                            //User not registered in Momo
                            $maintitle="Not Registered";
                            $title="Not Registered";
                            $response="Sorry you're not registered with MTN Mobile Rwanda";
                            echo "$response";
                        }
                        elseif($responseapi=='{"error":"the minimum amount is 100"}'){
                            //User not registered in Momo
                            $maintitle="Minimum amount";
                            $title="Minimum amount";
                            $response="Sorry the minimum amount to send is 100Frw";
                            echo "$response";
                        }
                        elseif($responseapi=='{"@attributes":{"errorcode":"AUTHORIZATION_MAXIMUM_AMOUNT_ALLOWED_TO_SEND"}}'){
                            //User not registered in Momo
                            $maintitle="Maximum amount";
                            $title="Maximum amount";
                            $response="Sorry the maximum amount to send is 2,000,000Frw";
                            //return redirect()->route('balance')->with($title,$message);
                            echo "$response";
                        }
                        else{
                            curl_setopt_array($curl, array(
                                CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$responseid->transactionid",
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 30,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "GET",
                                CURLOPT_POSTFIELDS => "",
                                CURLOPT_HTTPHEADER => array(
                                    "Accept: */*",
                                    "Cache-Control: no-cache",
                                    "Connection: keep-alive",
                                    "Host: akokanya.com",
                                    "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                                    "User-Agent: PostmanRuntime/7.11.0",
                                    "accept-encoding: gzip, deflate",
                                    "cache-control: no-cache",
                                    "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                                    "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
                                ),
                            ));

                            $responseapis = curl_exec($curl);
                            $err = curl_error($curl);

                            curl_close($curl);

                            if ($err) {
                                echo "cURL Error #:" . $err;
                            } else {
                                //echo $response;


                                $responsedata=json_decode($responseapis);
//                        dd($responsedata);
                                //Save to database Momo payments
                                //dd( $amount);
                                $deletedat=0;
                                $Momopayment = MomoTransaction::create([
                                    'phone'=> $phonenumber,
                                    'voter_id'=> $last_id,
                                    'transactionid'=> $responsedata[0]->external_payment_code,
                                    'status'=> $responsedata[0]->payment_status,
                                    'assignedid'=> $responsedata[0]->id,
                                    'company_name'=> $responsedata[0]->company_name,
                                    'code'=> $responsedata[0]->code,
                                    'amount'=> $amount,
                                    'artist_name'=> $getmusic_artist,
                                    'artist_song'=> $getmusic_name,
                                    'payment_code'=> $responsedata[0]->payment_code,
                                    'external_payment_code'=> $responsedata[0]->external_payment_code,
                                    'payment_status'=> $responsedata[0]->payment_status,
                                    'payment_type'=> $responsedata[0]->payment_type,
                                    'callback_url'=> $responsedata[0]->callback_url,
                                    'momodeleted_at'=> $deletedat,
                                    'momocreated_at'=> $responsedata[0]->created_at,
                                    'momoupdated_at'=> $responsedata[0]->updated_at,
                                ]);
                                if($Momopayment){

                                    $response = "CON Urakoze gutora $getmusic_name ya $getmusic_artist, amafaranga ni 50RWF,  Kanda *182*7# Ukurikize amabwiriza \n";
                                    $response .= "1. Gusohoka \n";
                                    header('Content-type: text/plain');
                                    echo $response;
                                }
                                //end saving to database Momo payments
                            }
                            //End Status showing
                        }
                    }

                }
                //////////////////////End Session after voting//////////////////////////////////////

                elseif ($checklevels == 5 and $te == '1' ){
                    $deletesession = LevelsTrack::where('user_session',$sessionId)->delete();
                    $response = "END Murakoze Gukoresha MNI. \n";
                    echo $response;
                }

                ////////////////////// Back on the playlist //////////////////////////////////////

                elseif ($checklevels == 4 and $te == '2'){
                    $getcate = LevelsTrack::where('user_session',$sessionId)->value('category_track');
                    $getuserchoice = LevelsTrack::where('user_session',$sessionId)->value('user_choice');
                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '3','user_choice'=>$getuserchoice,'category_track'=>$getcate]);
                    $firstday = new Carbon('first day of this month');
                    $lastday = new Carbon('last day of this month');
                    $lastday_ = $lastday->subDays(7);

                    $firstdaydate = $firstday->toDateString();
                    $lastdaydate = $lastday_->toDateString();


                    $getplaylist = PlaylistInfo::where('starting_date', $firstdaydate)->where('ending_date', $lastdaydate)->where('playlist_cate', $getcate)->value('id');
                    $getcategory = PlaylistCategory::where('id', $te)->value('category');
                    $getmusic = SongInfo::where('playlist_id', $getplaylist)->get();

                    $response = "CON MNI $getcategory Playlist \n";

                    foreach ($getmusic as $getmusics) {
                        $artistname = $getmusics->song_artist;
                        $artistid = $getmusics->song_number;
                        $artistmusicname = $getmusics->song_name;
                        $sepa = "\n";
                        $response .= $artistid . ". " . $artistname . " ($artistmusicname) " . "$sepa";
                    }
                    header('Content-type: text/plain');
                    echo $response;
                }

                ////////////////////// End Session on playlist ////////////////////////////////////

                elseif ($checklevels == 4 and $te == '3' ){
                    $deletesession = LevelsTrack::where('user_session',$sessionId)->delete();
                    $response = "END Murakoze Gukoresha MNI. \n";
                    echo $response;
                }


                elseif ($checklevels == 3 and $te == 2 ){

                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '2','user_choice'=>'1']);

                    $firstday = new Carbon('first day of last month');
                    $lastday = new Carbon('last day of last month');
                    $lastday_ = $lastday->subDays(7);

                    $firstdaydate = $firstday->toDateString();
                    $lastdaydate = $lastday_->toDateString();

                    $getplaylist = PlaylistInfo::where('starting_date', $firstdaydate)->where('ending_date', $lastdaydate)->value('id');
                    //            dd($getplaylist);

                    $getmusic = SongInfo::where('playlist_id', $getplaylist)->get();

                    $response = "CON MNI Playlist \n";

                    foreach ($getmusic as $getmusics) {
                        $artistname = $getmusics->song_artist;
                        $artistid = $getmusics->song_number;
                        $artistmusicname = $getmusics->song_name;
                        $sepa = "\n";
                        $response .= $artistid . ". " . $artistname . " ($artistmusicname) " . "$sepa";

                    }
                    header('Content-type: text/plain');
                    echo $response;
                }

                elseif ($checklevels == 3 and $te == 3 ){
                    $deletesession = LevelsTrack::where('user_session',$sessionId)->delete();
                    $response = "END Murakoze Gukoresha MNI. \n";
                    header('Content-type: text/plain');
                    echo $response;
                }

                ////////////////////// End voting ////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

////////////////////// Nominate ////////////////////////////////////
//////////////////////////////////////////////////////////////////
///
                elseif ($checklevels == 1 and $text == 2 ){
                    $checknumber = SongNominations::where('user_session',$sessionId)->value('user_nominee');
                    if(empty($checknumber)){
                        $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '6','user_choice'=>'2']);
                        $response = "CON Tanga indirimbo. \n";
                        header('Content-type: text/plain');
                        echo $response;
                    }else{
                        $deletesession = LevelsTrack::where('user_session',$sessionId)->delete();
                        $response = "END Tango wemere tuganga indirimbi incuro ebyiri! Murakoze Gukoresha MNI. \n";
                        header('Content-type: text/plain');
                        echo $response;
                    }

                }
                elseif ($checklevels == 6){
                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '7','user_choice'=>'1','song_nominated'=>$te]);

                    $response = "CON Tanga Umuririmbyi. \n";
                    header('Content-type: text/plain');
                    echo $response;
                }

                elseif ($checklevels == 7){
                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '8','artist_nominated'=>$te]);
                    $response = "CON 1. Kwemeza igikorwa. \n";
                    header('Content-type: text/plain');
                    echo $response;
                }

                elseif ($checklevels == 8 and $te == 1){
                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '9','user_choice'=>$te]);
                    $getsong_nominated = LevelsTrack::where('user_session',$sessionId)->value('song_nominated');
                    $getartist_nominated = LevelsTrack::where('user_session',$sessionId)->value('artist_nominated');
                    $addnomination = new SongNominations();
                    $addnomination->user_nominee = $phoneNumber;
                    $addnomination->user_session = $sessionId;
                    $addnomination->artist = $getartist_nominated;
                    $addnomination->song = $getsong_nominated;
                    $addnomination->save();
                    $sepa = "\n";



                }


                elseif ($checklevels == 9 and $te == 1 ){
                    $deletesession = LevelsTrack::where('user_session',$sessionId)->delete();
                    $response = "END Murakoze Gukoresha MNI. \n";
                    header('Content-type: text/plain');
                    echo $response;
                }

                ////////////////////// End Nominate ////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////


                ////////////////////// Start of voting ////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
            }

            else{
                $addsession = new LevelsTrack();
                $addsession->phonenumber = $phoneNumber;
                $addsession->user_session = $sessionId;
                $addsession->user_level = '1';
                $addsession->user_choice = '0';
                $addsession->save();

                if ($text == "747*77") {
                    $response = "Urakaza Neza Kuri MNI Playlist, Kanda \n";
                    $response .= "1. Gutora \n";
                    $response .= "2. Gutanga Indirimbo";

                    header('Content-type: text/json');
                    $response2 = [
                        "RESPONSE_DESCRIPTION" => 'Describe',
                        "RESPONSE_STRING" => $response,
                        "SESSION_STATE" => "CONTINUE",
                        "SESSION_ID" => $sessionId,
                        "MSISDN" => $phoneNumber,
                    ];
                    echo json_encode($response2);

                }
            }

        }
    }

    public function USSDTestV2(){

//        $MSISDN = $_POST['MSISDN'];
//        $input = base64_decode($_POST['input']);
//        $sessionID = $_POST['sessionID'];
//
//        $sessionId   = $sessionID;
//        $phoneNumber = $MSISDN;
//        $text        = $input;

        $sessionId   = $_POST["sessionId"];
        $serviceCode = $_POST["serviceCode"];
        $phoneNumber = $_POST["phoneNumber"];
        $text        = $_POST["text"];


//        $sessionId   = $_GET["sessionId"];
//        $serviceCode = $_GET["serviceCode"];
//        $phoneNumber = $_GET["phoneNumber"];
//        $text        = $_GET["text"];
//
        $ussd_string_exploded = explode ("*",$text);
        $level = count($ussd_string_exploded);
        $url ='1*1*1*1';
        $te = substr($text, strrpos($text, '*' )+1);
        $checklevels = LevelsTrack::where('phonenumber',$phoneNumber)->value('user_level');


//////////////////// Nominate ////////////////////////////////////
////////////////////////////////////////////////////////////////

        if (isset($text)) {

//            $checksession = LevelsTrack::where('phonenumber',$phoneNumber)->value('user_session');
            $checksession = LevelsTrack::where('user_session',$sessionId)->value('user_session');

            if($checksession == $sessionId){

                $checklevels = LevelsTrack::where('phonenumber',$phoneNumber)
                    ->where('user_session',$sessionId)
                    ->value('user_level');

                if($checklevels == 1 and $text == ""){

                    $response = "CON Urakaza Neza Kuri MNI Selection, Kanda \n";
                    $response .= "1. Gutora Indirimbo ukoreshe code \n";
                    $response .= "2. Kureba Code yindirimbo \n";
                    $response .= "3. Kureba Amanota \n";

                    header('Content-type: text/plain');
                    echo $response;

                }elseif ($checklevels == 1 and $text == 1){
                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '2','user_choice'=>'1']);
                    $response = "CON Tanga code y'indirimbo \n";
                    header('Content-type: text/plain');
                    echo $response;
                }

                elseif ($checklevels == 2 and $te == $te ){

                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '3','user_choice'=>$te,'category_track'=>$te]);
                    $getmusic_id = SongInfo::where('playlist_code',$te)->value('id');
                    $getmusic_name = SongInfo::where('playlist_code',$te)->value('song_name');
                    $getmusic_artist = SongInfo::where('playlist_code',$te)->value('song_artist');

                    if(0 == count($getmusic_id)){
                        $deletesession = LevelsTrack::where('user_session',$sessionId)->delete();
                        $response = "END Indirimbo ntayo dufite, murakoze gukoresha MNI Selection";
                        header('Content-type: text/plain');
                        echo $response;
                    }else{
                        $response = "CON Ugiye Gutora indirimbo $getmusic_name ya $getmusic_artist, amafaranga ni 50RWF\n";
                        $response .= "1. Kwemeza \n";
                        $response .= "2. Gusubira inyuma \n";
                        $response .= "3. Gusohoka \n";
                        header('Content-type: text/plain');
                        echo $response;
                    }

                }
                elseif ($checklevels == 3 and $te == 1 ){

                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '4','user_choice'=>$te]);

                    $getcode = LevelsTrack::where('user_session',$sessionId)->value('category_track');
                    $getplaylist_id = SongInfo::where('playlist_code', $getcode)->value('id');
                    $getmusic_name = SongInfo::where('id',$getplaylist_id)->value('song_name');
                    $getmusic_artist = SongInfo::where('id',$getplaylist_id)->value('song_artist');
//
                    $firstday = new Carbon('first day of last month');
                    $lastday = new Carbon('last day of last month');
                    $lastday_ = $lastday->subDays(7);
//
                    $firstdaydate = $firstday->toDateString();
                    $lastdaydate = $lastday_->toDateString();



                    $vote = new Votes();
                    $vote->starting_date = $firstdaydate;
                    $vote->ending_date = $lastdaydate;
                    $vote->voterphonenumber = $phoneNumber;
                    $vote->vote = "1";
                    $vote->voter_status = "PENDING";
                    $vote->voter_artist = $getmusic_name;
                    $vote->voter_artist_song = $getmusic_artist;
                    $vote->save();
                    $last_id = $vote->id;

                    $str_number = substr($phoneNumber, 3);
                    $phonenumber = $str_number;
//                    $phonenumber = $phoneNumber;
                    $amount = '50';
                    $idt = mt_rand(10, 99);
//                    //Generating Payment Gateway
                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "http://akokanya.com/mtn-pay?amount=$amount&phone=$phonenumber&company_name=MNI&payment_code=$idt",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => "",
                        CURLOPT_HTTPHEADER => array(
                            "Accept: */*",
                            "Cache-Control: no-cache",
                            "Connection: keep-alive",
                            "Host: akokanya.com",
                            "Postman-Token: caddbf9d-3cf9-4fc3-b051-bc0ee25a2561,de8e362a-f9dd-4a20-a3bf-099dba0f6b26",
                            "User-Agent: PostmanRuntime/7.11.0",
                            "accept-encoding: gzip, deflate",
                            "cache-control: no-cache",
                            "content-length: "
                        ),
                    ));

                    $responseapi = curl_exec($curl);
//            dd($response);
                    $err = curl_error($curl);

                    curl_close($curl);

                    if ($err) {
                        //echo "cURL Error #:" . $err;
                        $title="Not Connected";
                        $message="Sorry you do not have no internet connection";
                        //return redirect()->route('balance')->with($title,$message);
                        echo "$err";

                    }
                    else {
                        //echo $response;

                        //Showing Status
                        //second part Status checking

                        $responseid=json_decode($responseapi);
//                        dd($responseid);

                        $curl = curl_init();

                        if($responseapi=='{"@attributes":{"errorcode":"TARGET_AUTHORIZATION_ERROR"}}'){
                            //checking balance
                            $maintitle="Balance";
                            $title="Not Enough Balance";
                            $deletesession = LevelsTrack::where('user_session',$sessionId)->delete();
                            $response ="END Sorry you do not have enough money on your account to make this transaction, please try again after topping up your account";
                            //return redirect()->route('balance')->with($title,$message);
                            echo "$response";
                        }
                        elseif($responseapi=='{"@attributes":{"errorcode":"ACCOUNTHOLDER_WITH_FRI_NOT_FOUND"},"arguments":{"@attributes":{"name":"fri","value":"FRI:25'.$phonenumber.'\/MSISDN"}}}'){
                            //User not registered in Momo
                            $maintitle="Not Registered";
                            $title="Not Registered";
                            $deletesession = LevelsTrack::where('user_session',$sessionId)->delete();
                            $response="END Sorry you're not registered with MTN Mobile Rwanda";
                            echo "$response";
                        }
                        elseif($responseapi=='{"error":"the minimum amount is 100"}'){
                            //User not registered in Momo
                            $maintitle="Minimum amount";
                            $title="Minimum amount";
                            $deletesession = LevelsTrack::where('user_session',$sessionId)->delete();
                            $response="END Sorry the minimum amount to send is 100Frw";
                            echo "$response";
                        }
                        elseif($responseapi=='{"@attributes":{"errorcode":"AUTHORIZATION_MAXIMUM_AMOUNT_ALLOWED_TO_SEND"}}'){
                            //User not registered in Momo
                            $maintitle="Maximum amount";
                            $title="Maximum amount";
                            $deletesession = LevelsTrack::where('user_session',$sessionId)->delete();
                            $response="END Sorry the maximum amount to send is 2,000,000Frw";
                            //return redirect()->route('balance')->with($title,$message);
                            echo "$response";
                        }
                        else{
                            curl_setopt_array($curl, array(
                                CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$responseid->transactionid",
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 30,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "GET",
                                CURLOPT_POSTFIELDS => "",
                                CURLOPT_HTTPHEADER => array(
                                    "Accept: */*",
                                    "Cache-Control: no-cache",
                                    "Connection: keep-alive",
                                    "Host: akokanya.com",
                                    "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                                    "User-Agent: PostmanRuntime/7.11.0",
                                    "accept-encoding: gzip, deflate",
                                    "cache-control: no-cache",
                                    "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                                    "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
                                ),
                            ));

                            $responseapis = curl_exec($curl);
                            $err = curl_error($curl);

                            curl_close($curl);

                            if ($err) {
                                echo "cURL Error #:" . $err;
                            } else {
                                //echo $response;


                                $responsedata=json_decode($responseapis);
//                        dd($responsedata);
                                //Save to database Momo payments
                                //dd( $amount);
                                $deletedat=0;
                                $Momopayment = MomoTransaction::create([
                                    'phone'=> $phonenumber,
                                    'voter_id'=> $last_id,
                                    'transactionid'=> $responsedata[0]->external_payment_code,
                                    'status'=> $responsedata[0]->payment_status,
                                    'assignedid'=> $responsedata[0]->id,
                                    'company_name'=> $responsedata[0]->company_name,
                                    'code'=> $responsedata[0]->code,
                                    'amount'=> $amount,
                                    'artist_name'=> 'Bushali',
                                    'artist_song'=> 'Tsikizo',
                                    'payment_code'=> $responsedata[0]->payment_code,
                                    'external_payment_code'=> $responsedata[0]->external_payment_code,
                                    'payment_status'=> $responsedata[0]->payment_status,
                                    'payment_type'=> $responsedata[0]->payment_type,
                                    'callback_url'=> $responsedata[0]->callback_url,
                                    'momodeleted_at'=> $deletedat,
                                    'momocreated_at'=> $responsedata[0]->created_at,
                                    'momoupdated_at'=> $responsedata[0]->updated_at,
                                ]);
                                if($Momopayment){

                                    $deletesession = LevelsTrack::where('user_session',$sessionId)->delete();
                                    $response = "END Urakoze gutora $getmusic_name ya $getmusic_artist, amafaranga ni 50RWF,  Kanda *182*7# Ukurikize amabwiriza \n";
//                                    $response .= "1. Gusohoka \n";
                                    header('Content-type: text/plain');
                                    echo $response;
                                }
                                //end saving to database Momo payments
                            }
                            //End Status showing
                        }
                    }

                }
                elseif ($checklevels == 3 and $te == 2 ){
                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '2','user_choice'=>'1']);
                    $response = "CON Tanga code y'indirimbo \n";
                    header('Content-type: text/plain');
                    echo $response;
                }
                elseif ($checklevels == 3 and $te == 3 ){
                    $deletesession = LevelsTrack::where('user_session',$sessionId)->delete();
                    $response = "END Murakoze Gukoresha MNI Selection. \n";
                    header('Content-type: text/plain');
                    echo $response;
                }


                //////////////////////END OF VOTING //////////////////////////////////////
                ///  ///
                ///
                ///  ///
                ///
                ///  ///

                elseif ($checklevels == 1 and $text == 2 ){

                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '5','user_choice'=>$text]);
                    $getplaylist = PlaylistCategory::all();

                    $response = "CON MNI Playlist Category \n";
                    foreach ($getplaylist as $getplaylists){
                        $category = $getplaylists->category;
                        $cate_id = $getplaylists->id;
                        $sepa = "\n";
                        $response .= $cate_id . ". " . $category . "$sepa";
                    }
                    header('Content-type: text/plain');
                    echo $response;
                }

                elseif ($checklevels == 5 and $te == $te ){

                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '6','user_choice'=>$te,'category_track'=>$te]);
                    $getmusics = SongInfo::where('playlist_cate',$te)->get();
                    $getcategory = PlaylistCategory::where('id', $te)->value('category');

                    if(0 == count($getmusics)){
                        $deletesession = LevelsTrack::where('user_session',$sessionId)->delete();
                        $response = "END Muri $getcategory ntandirimbo dufitemo, Murakoze gukoresha MNI Selection";
                        header('Content-type: text/plain');
                        echo $response;
                    }else{
                        $deletesession = LevelsTrack::where('user_session',$sessionId)->delete();
                        $response = "END MNI $getcategory Selection \n";
                        foreach ($getmusics as $getmusicss) {
                            $artistname = $getmusicss->song_artist;
                            $artistid = $getmusicss->id;
                            $artistmusicname = $getmusicss->song_name;
                            $artistcode = $getmusicss->playlist_code;
                            $sepa = "\n";
                            $response .= $artistid . ". " . $artistmusicname  ." ya". " $artistname ". " ($artistcode) " . "$sepa";

                        }
                        header('Content-type: text/plain');
                        echo $response;
                    }

                }

                ////////////////////// End OF CHECKING CODE//////////////////////////////////////
                ///  ///
                ///
                ///  ///
                ///
                ///  ///


                elseif ($checklevels == 1 and $text == 3 ){
                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '6','user_choice'=>$text]);
                    $getplaylist = PlaylistCategory::all();

                    $response = "CON MNI Playlist Category \n";
                    foreach ($getplaylist as $getplaylists){
                        $category = $getplaylists->category;
                        $cate_id = $getplaylists->id;
                        $sepa = "\n";
                        $response .= $cate_id . ". " . $category . "$sepa";
                    }
                    header('Content-type: text/plain');
                    echo $response;
                }
                elseif ($checklevels == 6 and $te == $te ){
                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '7','user_choice'=>$te,'category_track'=>$te]);
                    $getmusics = SongInfo::where('playlist_cate',$te)->get();
                    $getcategory = PlaylistCategory::where('id', $te)->value('category');

                    if(0 == count($getmusics)){
                        $deletesession = LevelsTrack::where('user_session',$sessionId)->delete();
                        $response = "END Muri $getcategory ntandirimbo dufitemo, Murakoze gukoresha MNI Selection";
                        header('Content-type: text/plain');
                        echo $response;
                    }else{

                        $deletesession = LevelsTrack::where('user_session',$sessionId)->delete();
                        $response = "END MNI $getcategory Selection \n";
                        foreach ($getmusics as $getmusicss) {


                            $artistname = $getmusicss->song_artist;
                            $artistid = $getmusicss->id;
                            $artistmusicname = $getmusicss->song_name;
                            $artistcode = $getmusicss->playlist_code;
                            $sepa = "\n";
                            $votes = Votes::select(DB::raw('count(id) as votes'))
                                ->where('voter_artist_song',$artistmusicname)
                                ->where('voter_artist',$artistname)
                                ->where('vote','1')
                                ->value('votes');

                            if ($votes == 0){
                                $response .= $artistid . ". " . $artistmusicname  ." ya". " $artistname ". " (0) " . "$sepa";
                            }else{
                                $response .= $artistid . ". " . $artistmusicname  ." ya". " $artistname ". " ($votes) " . "$sepa";
                            }

                        }
                        header('Content-type: text/plain');
                        echo $response;
                    }
                }

                ////////////////////// End OF CHECKING VOTES //////////////////////////////////////
                ///  ///
                ///
                ///  ///
                ///
                ///  ///

            }else{

                $addsession = new LevelsTrack();
                $addsession->phonenumber = $phoneNumber;
                $addsession->user_session = $sessionId;
                $addsession->user_level = '1';
                $addsession->user_choice = '0';
                $addsession->save();

                if ($text == "") {

                    $response = "CON Urakaza Neza Kuri MNI Selection, Kanda \n";
                    $response .= "1. Gutora Indirimbo ukoreshe code \n";
                    $response .= "2. Kureba Code yindirimbo \n";
                    $response .= "3. Kureba Amanota \n";
                    header('Content-type: text/plain');
                    echo $response;

                }
            }

        }


//        if($checklevels == null){
//            $addlevel = new LevelsTrack();
//            $addlevel->user_level = '2';
//            $addlevel->phonenumber = $phoneNumber;
//            $addlevel->save();
////            $updatelevel = LevelsTrack::where('phonenumber',$phoneNumber)->update(['user_level' => '1']);
//
//            if($level == 1 or $level == 0){
//
//                $response  = "CON Urakaza Neza Kuri MNI Playlist, Kanda \n";
//                $response .= "1. Gutora \n";
//                $response .= "2. Gusohoka";
//
//                echo $response;
//
//                $level = $level +1;
//
//            }elseif ($text == 1){
//                $firstday = new Carbon('first day of this month');
//                $lastday = new Carbon('last day of this month');
//                $lastday_ = $lastday->subDays(7);
//
//                $firstdaydate = $firstday->toDateString();
//                $lastdaydate = $lastday_->toDateString();
//
//                $getplaylist = PlaylistInfo::where('starting_date', $firstdaydate)->where('ending_date',$lastdaydate)->value('id');
//                //            dd($getplaylist);
//
//                $getmusic = SongInfo::where('playlist_id',$getplaylist)->get();
//
//                $response  = "CON MNI Playlist \n";
//
//                foreach ($getmusic as $getmusics){
//                    $artistname = $getmusics->song_artist;
//                    $artistid = $getmusics->song_number;
//                    $artistmusicname = $getmusics->song_name;
//                    $sepa = "\n";
//                    $response .= $artistid . ". " . $artistname ." ($artistmusicname) ". "$sepa";
//
//                }
//
//                echo $response;
//            }elseif ($text == 2){
//                $response = "END Murakoze Gukoresha MNI. \n";
//                echo $response;
//            }
//
//        }else if( $checklevels == "2"){
//                $firstday = new Carbon('first day of this month');
//                $lastday = new Carbon('last day of this month');
//                $lastday_ = $lastday->subDays(7);
//
//                $firstdaydate = $firstday->toDateString();
//                $lastdaydate = $lastday_->toDateString();
//
//                $getplaylist = PlaylistInfo::where('starting_date', $firstdaydate)->where('ending_date',$lastdaydate)->value('id');
//    //            dd($getplaylist);
//
//                $getmusic = SongInfo::where('playlist_id',$getplaylist)->get();
//
//                $response  = "CON MNI Playlist \n";
//
//                foreach ($getmusic as $getmusics){
//                    $artistname = $getmusics->song_artist;
//                    $artistid = $getmusics->song_number;
//                    $artistmusicname = $getmusics->song_name;
//                    $sepa = "\n";
//                    $response .= $artistid . ". " . $artistname ." ($artistmusicname) ". "$sepa";
//
//                }
//
//                echo $response;
//        }

//        if($text == 0){
//
//            $response  = "CON Murakaza Neza Kuri MNI Playlist, Kanda \n";
//            $response .= "1. Gutora \n";
//            $response .= "2. Gusohoka";
//
//            echo $response;
//
////            $level = $text +1;
//        }

//        if ($level > 1)
//        {
//            if ($ussd_string_exploded[0] == "1")
//            {
//                $firstday = new Carbon('first day of this month');
//                $lastday = new Carbon('last day of this month');
//                $lastday_ = $lastday->subDays(7);
//
//                $firstdaydate = $firstday->toDateString();
//                $lastdaydate = $lastday_->toDateString();
//
//                $getplaylist = PlaylistInfo::where('starting_date', $firstdaydate)->where('ending_date',$lastdaydate)->value('id');
//    //            dd($getplaylist);
//
//                $getmusic = SongInfo::where('playlist_id',$getplaylist)->get();
//
//                $response  = "CON MNI Playlist \n";
//
//                foreach ($getmusic as $getmusics){
//                    $artistname = $getmusics->song_artist;
//                    $artistid = $getmusics->song_number;
//                    $artistmusicname = $getmusics->song_name;
//                    $sepa = "\n";
//                    $response .= $artistid . ". " . $artistname ." ($artistmusicname) ". "$sepa";
//
//                }
//
//                echo $response;
//
//            }
//            else if ($ussd_string_exploded[1] == "2"){
//                //If user selected 2, send them to the about menu
//                echo "yes";
//            }
//        }

////
//        else if ($text == "1") {
//            $firstday = new Carbon('first day of this month');
//            $lastday = new Carbon('last day of this month');
//            $lastday_ = $lastday->subDays(7);
//
//            $firstdaydate = $firstday->toDateString();
//            $lastdaydate = $lastday_->toDateString();
//
//            $getplaylist = PlaylistInfo::where('starting_date', $firstdaydate)->where('ending_date',$lastdaydate)->value('id');
////            dd($getplaylist);
//
//            $getmusic = SongInfo::where('playlist_id',$getplaylist)->get();
//
//            $response  = "CON  Hitamo Indirimbo \n";
//
//            foreach ($getmusic as $getmusics){
//                $artistname = $getmusics->song_artist;
//                $artistid = $getmusics->song_number;
//                $artistmusicname = $getmusics->song_name;
//                $sepa = "\n";
//                $response .= $artistid . ". " . $artistname ." ($artistmusicname) ". "$sepa";
//
//            }
//
//            echo $response;
//
//        }
//
//        else if ($text == "1*1") {
//
//            $getmusic = SongInfo::where('song_number','1')->get();
//            foreach ($getmusic as $getmusics){
//                $artistname = $getmusics->song_artist;
//                $artistid = $getmusics->song_number;
//                $artistmusicname = $getmusics->song_name;
//                $sepa = "\n";
//
//            }
//            $response = "CON Ugiye Gutora indirimbo $artistmusicname ya $artistname, amafaranga ni 50RWF\n";
//            $response .= "1. Kwemeza \n";
//            $response .= "2. Gusubira inyuma \n";
//            $response .= "3. Gusohoka \n";
//            echo $response;
//
//
//        }
//        else if ($text == "1*1*1") {
//
//            $getmusic = SongInfo::where('song_number','1')->get();
//
//            foreach ($getmusic as $getmusics){
//                $artistname = $getmusics->song_artist;
//                $artistid = $getmusics->song_number;
//                $artistmusicname = $getmusics->song_name;
//                $sepa = "\n";
//
//            }
//            $firstday = new Carbon('first day of this month');
//            $lastday = new Carbon('last day of this month');
//            $lastday_ = $lastday->subDays(7);
//
//            $firstdaydate = $firstday->toDateString();
//            $lastdaydate = $lastday_->toDateString();
//
//            $vote = new Votes();
//            $vote->starting_date = $firstdaydate;
//            $vote->ending_date = $lastdaydate;
//            $vote->voterphonenumber = $phoneNumber;
//            $vote->vote = "1";
//            $vote->voter_artist = $artistname;
//            $vote->voter_artist_song = $artistmusicname;
//            $vote->save();
//
//            $response = "CON Murakoze gutora $artistmusicname ya $artistname, amafaranga ni 50RWF\n";
//            $response .= "1. Ongera utore \n";
//            $response .= "2. Gusohoka \n";
//
//            echo $response;
//
//        }
//
//        else if ($text == "1*1*1*2") {
//            //note that we are using the $phoneNumber variable we got form the HTTP POST data.
//            $response = "END Murakoze Gukoresha MNI. \n";
//
//            echo $response;
//        }
//
//
////Menu for a user who selects '2'
//
//
//        else if ($text == "2") {
//            //note that we are using the $phoneNumber variable we got form the HTTP POST data.
//            $response = "END Murakoze Gukoresha MNI. \n";
//
//            echo $response;
//        }
//
//
////Menu for a user who selects '1' from second menu
//
//
//
//
////        else if ($text == "1*1*1") {
////            $response = "CON Urakoze Gutora Riderman\n";
////            $response .= "1. Ongera Utore \n";
////            $response .= "2. Gusohoka \n";
////            echo $response;
////        }
//        else if ($text == "1*1*3") {
//            $response = "END Murakoze Gukoresha MNI. \n";
//
//            echo $response;
//        }
//        else if ($text == "1*1*1*2") {
//            $response = "END Murakoze Gukoresha MNI. \n";
//
//            echo $response;
//        }
////Menu for a user who selects '2' from second menu
//
//        else if ($text == "1*2") {
//            $response = "END Your balance is USD 10.78\n";
//            echo $response;
//        }
    }
    public function USSDTest(){

//        $MSISDN = $_POST['MSISDN'];
//        $input = base64_decode($_POST['input']);
//        $sessionID = $_POST['sessionID'];
//
//        $sessionId   = $sessionID;
//        $phoneNumber = $MSISDN;
//        $text        = $input;

//        $sessionId   = $_POST["sessionId"];
//        $serviceCode = $_POST["serviceCode"];
//        $phoneNumber = $_POST["phoneNumber"];
//        $text        = $_POST["text"];


        $sessionId   = $_GET["sessionId"];
        $serviceCode = $_GET["serviceCode"];
        $phoneNumber = $_GET["phoneNumber"];
        $text        = $_GET["text"];
//
        $ussd_string_exploded = explode ("*",$text);
        $level = count($ussd_string_exploded);
        $url ='1*1*1*1';
        $te = substr($text, strrpos($text, '*' )+1);
        $checklevels = LevelsTrack::where('phonenumber',$phoneNumber)->value('user_level');


//////////////////// Nominate ////////////////////////////////////
////////////////////////////////////////////////////////////////

        if (isset($text)) {

            $checksession = LevelsTrack::where('phonenumber',$phoneNumber)->value('user_session');

            if($checksession == $sessionId){

                $checklevels = LevelsTrack::where('phonenumber',$phoneNumber)->value('user_level');

                if($checklevels == 1 and $text == ""){

                    $response = "CON Urakaza Neza Kuri MNI Playlist, Kanda \n";
                    $response .= "1. Gutora \n";
                    $response .= "2. Gutanga Indirimbo";

//                    header('Content-type: text/json');
//                    $response2 = [
//                        "RESPONSE_DESCRIPTION" => 'Describe',
//                        "RESPONSE_STRING" => $response,
//                        "SESSION_STATE" => "CONTINUE",
//                        "SESSION_ID" => $sessionId,
//                        "MSISDN" => $phoneNumber,
//                    ];
//                    echo json_encode($response2);

                    //echo $response;
                    header('Content-type: text/plain');
                    echo $response;

                }elseif ($checklevels == 1 and $text == 1 ){

                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '2','user_choice'=>'1']);
                    $getplaylist = PlaylistCategory::all();

                    $response = "CON MNI Playlist Category \n";
                    foreach ($getplaylist as $getplaylists){
                        $category = $getplaylists->category;
                        $cate_id = $getplaylists->id;
                        $sepa = "\n";
                        $response .= $cate_id . ". " . $category . "$sepa";
                    }
                    header('Content-type: text/plain');
                    echo $response;
                }

                elseif ($checklevels == 2 and $te == $te ){

                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '3','user_choice'=>$te,'category_track'=>$te]);
                    $getplaylist = PlaylistCategory::all();

                    $firstday = new Carbon('first day of this month');
                    $lastday = new Carbon('last day of this month');
                    $lastday_ = $lastday->subDays(7);

                    $firstdaydate = $firstday->toDateString();
                    $lastdaydate = $lastday_->toDateString();

                    $getplaylist = PlaylistInfo::where('starting_date', $firstdaydate)->where('ending_date', $lastdaydate)->where('playlist_cate', $te)->value('id');
                    $getcategory = PlaylistCategory::where('id', $te)->value('category');
                    $getmusic = SongInfo::where('playlist_id', $getplaylist)->get();

                    $response = "CON MNI $getcategory Playlist \n";

                    foreach ($getmusic as $getmusics) {
                        $artistname = $getmusics->song_artist;
                        $artistid = $getmusics->song_number;
                        $artistmusicname = $getmusics->song_name;
                        $sepa = "\n";
                        $response .= $artistid . ". " . $artistname . " ($artistmusicname) " . "$sepa";

                    }
                    header('Content-type: text/plain');
                    echo $response;
                }

                elseif ($checklevels == 3 and $te == $te ){

                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '4','user_choice'=>$te]);

                    $getcate = LevelsTrack::where('user_session',$sessionId)->value('category_track');

                    $getplaylist_id = PlaylistInfo::where('playlist_cate', $getcate)->value('id');

                    $getmusic_name = SongInfo::where('song_number',$te)->where('playlist_id',$getplaylist_id)->value('song_name');
                    $getmusic_artist = SongInfo::where('song_number',$te)->where('playlist_id',$getplaylist_id)->value('song_artist');

                    $response = "CON Ugiye Gutora indirimbo $getmusic_name ya $getmusic_artist, amafaranga ni 50RWF\n";
                    $response .= "1. Kwemeza \n";
                    $response .= "2. Gusubira inyuma \n";
                    $response .= "3. Gusohoka \n";
                    header('Content-type: text/plain');
                    echo $response;

                }

                elseif ($checklevels == 4 and $te == 1 ){

                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '5','user_choice'=> $te]);
                    $getcate = LevelsTrack::where('user_session',$sessionId)->value('category_track');
                    $getplaylist_id = PlaylistInfo::where('playlist_cate', $getcate)->value('id');

                    $getmusic_name = SongInfo::where('song_number',$te)->where('playlist_id',$getplaylist_id)->value('song_name');
                    $getmusic_artist = SongInfo::where('song_number',$te)->where('playlist_id',$getplaylist_id)->value('song_artist');

//                    foreach ($getmusic as $getmusics){
//                        $artistname = $getmusics->song_artist;
//                        $artistid = $getmusics->song_number;
//                        $artistmusicname = $getmusics->song_name;
//                        $sepa = "\n";
//
//                    }

                    $firstday = new Carbon('first day of last month');
                    $lastday = new Carbon('last day of last month');
                    $lastday_ = $lastday->subDays(7);

                    $firstdaydate = $firstday->toDateString();
                    $lastdaydate = $lastday_->toDateString();

                    $vote = new Votes();
                    $vote->starting_date = $firstdaydate;
                    $vote->ending_date = $lastdaydate;
                    $vote->voterphonenumber = $phoneNumber;
                    $vote->vote = "1";
                    $vote->voter_status = "PENDING";
                    $vote->voter_artist = $getmusic_name;
                    $vote->voter_artist_song = $getmusic_artist;
                    $vote->save();
                    $last_id = $vote->id;

                    $str_number = substr($phoneNumber, 3);
                    $phonenumber = $str_number;
//                    $phonenumber = $phoneNumber;
                    $amount = '50';
                    $idt = mt_rand(10, 99);
                    //Generating Payment Gateway
                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "http://akokanya.com/mtn-pay?amount=$amount&phone=$phonenumber&company_name=MNI&payment_code=$idt",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => "",
                        CURLOPT_HTTPHEADER => array(
                            "Accept: */*",
                            "Cache-Control: no-cache",
                            "Connection: keep-alive",
                            "Host: akokanya.com",
                            "Postman-Token: caddbf9d-3cf9-4fc3-b051-bc0ee25a2561,de8e362a-f9dd-4a20-a3bf-099dba0f6b26",
                            "User-Agent: PostmanRuntime/7.11.0",
                            "accept-encoding: gzip, deflate",
                            "cache-control: no-cache",
                            "content-length: "
                        ),
                    ));

                    $responseapi = curl_exec($curl);
//            dd($response);
                    $err = curl_error($curl);

                    curl_close($curl);

                    if ($err) {
                        //echo "cURL Error #:" . $err;
                        $title="Not Connected";
                        $message="Sorry you do not have no internet connection";
                        //return redirect()->route('balance')->with($title,$message);
                        echo "$err";

                    } else {
                        //echo $response;

                        //Showing Status
                        //second part Status checking

                        $responseid=json_decode($responseapi);
//                        dd($responseid);

                        $curl = curl_init();

                        if($responseapi=='{"@attributes":{"errorcode":"TARGET_AUTHORIZATION_ERROR"}}'){
                            //checking balance
                            $maintitle="Balance";
                            $title="Not Enough Balance";
                            $response ="Sorry you do not have enough money on your account to make this transaction, please try again after topping up your account";
                            //return redirect()->route('balance')->with($title,$message);
                            echo "$response";
                        }
                        elseif($responseapi=='{"@attributes":{"errorcode":"ACCOUNTHOLDER_WITH_FRI_NOT_FOUND"},"arguments":{"@attributes":{"name":"fri","value":"FRI:25'.$phonenumber.'\/MSISDN"}}}'){
                            //User not registered in Momo
                            $maintitle="Not Registered";
                            $title="Not Registered";
                            $response="Sorry you're not registered with MTN Mobile Rwanda";
                            echo "$response";
                        }
                        elseif($responseapi=='{"error":"the minimum amount is 100"}'){
                            //User not registered in Momo
                            $maintitle="Minimum amount";
                            $title="Minimum amount";
                            $response="Sorry the minimum amount to send is 100Frw";
                            echo "$response";
                        }
                        elseif($responseapi=='{"@attributes":{"errorcode":"AUTHORIZATION_MAXIMUM_AMOUNT_ALLOWED_TO_SEND"}}'){
                            //User not registered in Momo
                            $maintitle="Maximum amount";
                            $title="Maximum amount";
                            $response="Sorry the maximum amount to send is 2,000,000Frw";
                            //return redirect()->route('balance')->with($title,$message);
                            echo "$response";
                        }
                        else{
                            curl_setopt_array($curl, array(
                                CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$responseid->transactionid",
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 30,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "GET",
                                CURLOPT_POSTFIELDS => "",
                                CURLOPT_HTTPHEADER => array(
                                    "Accept: */*",
                                    "Cache-Control: no-cache",
                                    "Connection: keep-alive",
                                    "Host: akokanya.com",
                                    "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                                    "User-Agent: PostmanRuntime/7.11.0",
                                    "accept-encoding: gzip, deflate",
                                    "cache-control: no-cache",
                                    "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                                    "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
                                ),
                            ));

                            $responseapis = curl_exec($curl);
                            $err = curl_error($curl);

                            curl_close($curl);

                            if ($err) {
                                echo "cURL Error #:" . $err;
                            } else {
                                //echo $response;


                                $responsedata=json_decode($responseapis);
//                        dd($responsedata);
                                //Save to database Momo payments
                                //dd( $amount);
                                $deletedat=0;
                                $Momopayment = MomoTransaction::create([
                                    'phone'=> $phonenumber,
                                    'voter_id'=> $last_id,
                                    'transactionid'=> $responsedata[0]->external_payment_code,
                                    'status'=> $responsedata[0]->payment_status,
                                    'assignedid'=> $responsedata[0]->id,
                                    'company_name'=> $responsedata[0]->company_name,
                                    'code'=> $responsedata[0]->code,
                                    'amount'=> $amount,
                                    'artist_name'=> 'Bushali',
                                    'artist_song'=> 'Tsikizo',
                                    'payment_code'=> $responsedata[0]->payment_code,
                                    'external_payment_code'=> $responsedata[0]->external_payment_code,
                                    'payment_status'=> $responsedata[0]->payment_status,
                                    'payment_type'=> $responsedata[0]->payment_type,
                                    'callback_url'=> $responsedata[0]->callback_url,
                                    'momodeleted_at'=> $deletedat,
                                    'momocreated_at'=> $responsedata[0]->created_at,
                                    'momoupdated_at'=> $responsedata[0]->updated_at,
                                ]);
                                if($Momopayment){

                                    $response = "CON Urakoze gutora $getmusic_name ya $getmusic_artist, amafaranga ni 50RWF,  Kanda *182*7# Ukurikize amabwiriza \n";
                                    $response .= "1. Gusohoka \n";
                                    header('Content-type: text/plain');
                                    echo $response;
                                }
                                //end saving to database Momo payments
                            }
                            //End Status showing
                        }
                    }

                }
                //////////////////////End Session after voting//////////////////////////////////////

                elseif ($checklevels == 5 and $te == '1' ){
                    $deletesession = LevelsTrack::where('user_session',$sessionId)->delete();
                    $response = "END Murakoze Gukoresha MNI. \n";
                    echo $response;
                }

                ////////////////////// Back on the playlist //////////////////////////////////////

                elseif ($checklevels == 4 and $te == '2'){
                    $getcate = LevelsTrack::where('user_session',$sessionId)->value('category_track');
                    $getuserchoice = LevelsTrack::where('user_session',$sessionId)->value('user_choice');
                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '3','user_choice'=>$getuserchoice,'category_track'=>$getcate]);
                    $firstday = new Carbon('first day of this month');
                    $lastday = new Carbon('last day of this month');
                    $lastday_ = $lastday->subDays(7);

                    $firstdaydate = $firstday->toDateString();
                    $lastdaydate = $lastday_->toDateString();


                    $getplaylist = PlaylistInfo::where('starting_date', $firstdaydate)->where('ending_date', $lastdaydate)->where('playlist_cate', $getcate)->value('id');
                    $getcategory = PlaylistCategory::where('id', $te)->value('category');
                    $getmusic = SongInfo::where('playlist_id', $getplaylist)->get();

                    $response = "CON MNI $getcategory Playlist \n";

                    foreach ($getmusic as $getmusics) {
                        $artistname = $getmusics->song_artist;
                        $artistid = $getmusics->song_number;
                        $artistmusicname = $getmusics->song_name;
                        $sepa = "\n";
                        $response .= $artistid . ". " . $artistname . " ($artistmusicname) " . "$sepa";
                    }
                    header('Content-type: text/plain');
                    echo $response;
                }

                ////////////////////// End Session on playlist ////////////////////////////////////

                elseif ($checklevels == 4 and $te == '3' ){
                    $deletesession = LevelsTrack::where('user_session',$sessionId)->delete();
                    $response = "END Murakoze Gukoresha MNI. \n";
                    echo $response;
                }


                elseif ($checklevels == 3 and $te == 2 ){

                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '2','user_choice'=>'1']);

                    $firstday = new Carbon('first day of last month');
                    $lastday = new Carbon('last day of last month');
                    $lastday_ = $lastday->subDays(7);

                    $firstdaydate = $firstday->toDateString();
                    $lastdaydate = $lastday_->toDateString();

                    $getplaylist = PlaylistInfo::where('starting_date', $firstdaydate)->where('ending_date', $lastdaydate)->value('id');
                    //            dd($getplaylist);

                    $getmusic = SongInfo::where('playlist_id', $getplaylist)->get();

                    $response = "CON MNI Playlist \n";

                    foreach ($getmusic as $getmusics) {
                        $artistname = $getmusics->song_artist;
                        $artistid = $getmusics->song_number;
                        $artistmusicname = $getmusics->song_name;
                        $sepa = "\n";
                        $response .= $artistid . ". " . $artistname . " ($artistmusicname) " . "$sepa";

                    }
                    header('Content-type: text/plain');
                    echo $response;
                }

                elseif ($checklevels == 3 and $te == 3 ){
                    $deletesession = LevelsTrack::where('user_session',$sessionId)->delete();
                    $response = "END Murakoze Gukoresha MNI. \n";
                    header('Content-type: text/plain');
                    echo $response;
                }

                ////////////////////// End voting ////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

////////////////////// Nominate ////////////////////////////////////
//////////////////////////////////////////////////////////////////
///
                elseif ($checklevels == 1 and $text == 2 ){
                    $checknumber = SongNominations::where('user_session',$sessionId)->value('user_nominee');
                    if(empty($checknumber)){
                        $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '6','user_choice'=>'2']);
                        $response = "CON Tanga indirimbo. \n";
                        header('Content-type: text/plain');
                        echo $response;
                    }else{
                        $deletesession = LevelsTrack::where('user_session',$sessionId)->delete();
                        $response = "END Tango wemere tuganga indirimbi incuro ebyiri! Murakoze Gukoresha MNI. \n";
                        header('Content-type: text/plain');
                        echo $response;
                    }

                }
                elseif ($checklevels == 6){
                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '7','user_choice'=>'1','song_nominated'=>$te]);

                    $response = "CON Tanga Umuririmbyi. \n";
                    header('Content-type: text/plain');
                    echo $response;
                }

                elseif ($checklevels == 7){
                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '8','artist_nominated'=>$te]);
                    $response = "CON 1. Kwemeza igikorwa. \n";
                    header('Content-type: text/plain');
                    echo $response;
                }

                elseif ($checklevels == 8 and $te == 1){
                    $updatelevel = LevelsTrack::where('user_session',$sessionId)->update(['user_level' => '9','user_choice'=>$te]);
                    $getsong_nominated = LevelsTrack::where('user_session',$sessionId)->value('song_nominated');
                    $getartist_nominated = LevelsTrack::where('user_session',$sessionId)->value('artist_nominated');
                    $addnomination = new SongNominations();
                    $addnomination->user_nominee = $phoneNumber;
                    $addnomination->user_session = $sessionId;
                    $addnomination->artist = $getartist_nominated;
                    $addnomination->song = $getsong_nominated;
                    $addnomination->save();
                    $sepa = "\n";
                    $str_number = substr($phoneNumber, 3);
                    $phonenumber = $str_number;
//                    $phonenumber = $phoneNumber;
                    $amount = '50';
                    $idt = mt_rand(10, 99);
                    //Generating Payment Gateway
                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "http://akokanya.com/mtn-pay?amount=$amount&phone=$phonenumber&company_name=MNI&payment_code=$idt",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => "",
                        CURLOPT_HTTPHEADER => array(
                            "Accept: */*",
                            "Cache-Control: no-cache",
                            "Connection: keep-alive",
                            "Host: akokanya.com",
                            "Postman-Token: caddbf9d-3cf9-4fc3-b051-bc0ee25a2561,de8e362a-f9dd-4a20-a3bf-099dba0f6b26",
                            "User-Agent: PostmanRuntime/7.11.0",
                            "accept-encoding: gzip, deflate",
                            "cache-control: no-cache",
                            "content-length: "
                        ),
                    ));

                    $responseapi = curl_exec($curl);
//            dd($response);
                    $err = curl_error($curl);

                    curl_close($curl);

                    if ($err) {
                        //echo "cURL Error #:" . $err;
                        $title="Not Connected";
                        $message="Sorry you do not have no internet connection";
                        //return redirect()->route('balance')->with($title,$message);
                        echo "$err";

                    } else {
                        //echo $response;

                        //Showing Status
                        //second part Status checking

                        $responseid=json_decode($responseapi);
//                        dd($responseid);

                        $curl = curl_init();

                        if($responseapi=='{"@attributes":{"errorcode":"TARGET_AUTHORIZATION_ERROR"}}'){
                            //checking balance
                            $maintitle="Balance";
                            $title="Not Enough Balance";
                            $response ="Sorry you do not have enough money on your account to make this transaction, please try again after topping up your account";
                            //return redirect()->route('balance')->with($title,$message);
                            echo "$response";
                        }
                        elseif($responseapi=='{"@attributes":{"errorcode":"ACCOUNTHOLDER_WITH_FRI_NOT_FOUND"},"arguments":{"@attributes":{"name":"fri","value":"FRI:25'.$phonenumber.'\/MSISDN"}}}'){
                            //User not registered in Momo
                            $maintitle="Not Registered";
                            $title="Not Registered";
                            $response="Sorry you're not registered with MTN Mobile Rwanda";
                            echo "$response";
                        }
                        elseif($responseapi=='{"error":"the minimum amount is 100"}'){
                            //User not registered in Momo
                            $maintitle="Minimum amount";
                            $title="Minimum amount";
                            $response="Sorry the minimum amount to send is 100Frw";
                            echo "$response";
                        }
                        elseif($responseapi=='{"@attributes":{"errorcode":"AUTHORIZATION_MAXIMUM_AMOUNT_ALLOWED_TO_SEND"}}'){
                            //User not registered in Momo
                            $maintitle="Maximum amount";
                            $title="Maximum amount";
                            $response="Sorry the maximum amount to send is 2,000,000Frw";
                            //return redirect()->route('balance')->with($title,$message);
                            echo "$response";
                        }
                        else{
                            curl_setopt_array($curl, array(
                                CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$responseid->transactionid",
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => "",
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 30,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => "GET",
                                CURLOPT_POSTFIELDS => "",
                                CURLOPT_HTTPHEADER => array(
                                    "Accept: */*",
                                    "Cache-Control: no-cache",
                                    "Connection: keep-alive",
                                    "Host: akokanya.com",
                                    "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                                    "User-Agent: PostmanRuntime/7.11.0",
                                    "accept-encoding: gzip, deflate",
                                    "cache-control: no-cache",
                                    "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                                    "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
                                ),
                            ));

                            $responseapis = curl_exec($curl);
                            $err = curl_error($curl);

                            curl_close($curl);

                            if ($err) {
                                echo "cURL Error #:" . $err;
                            } else {
                                //echo $response;


                                $responsedata=json_decode($responseapis);
//                        dd($responsedata);
                                //Save to database Momo payments
                                //dd( $amount);
                                $deletedat=0;
                                $Momopayment = MomoTransaction::create([
                                    'phone'=> $phonenumber,
                                    'voter_id'=> 'nominate',
                                    'transactionid'=> $responsedata[0]->external_payment_code,
                                    'status'=> $responsedata[0]->payment_status,
                                    'assignedid'=> $responsedata[0]->id,
                                    'company_name'=> $responsedata[0]->company_name,
                                    'code'=> $responsedata[0]->code,
                                    'amount'=> $amount,
                                    'artist_name'=> $getartist_nominated,
                                    'artist_song'=> $getsong_nominated,
                                    'payment_code'=> $responsedata[0]->payment_code,
                                    'external_payment_code'=> $responsedata[0]->external_payment_code,
                                    'payment_status'=> $responsedata[0]->payment_status,
                                    'payment_type'=> $responsedata[0]->payment_type,
                                    'callback_url'=> $responsedata[0]->callback_url,
                                    'momodeleted_at'=> $deletedat,
                                    'momocreated_at'=> $responsedata[0]->created_at,
                                    'momoupdated_at'=> $responsedata[0]->updated_at,
                                ]);

                                if($Momopayment){
                                    $response = "CON Murakoze Gutanga . $getsong_nominated ya  $getartist_nominated , amafaranga ni 50RWF,  Kanda *182*7# Ukurikize amabwiriza \n";
                                    $response .= "1. Gusohoka \n";
                                    header('Content-type: text/plain');
                                    echo $response;
                                }
                                //end saving to database Momo payments
                            }
                            //End Status showing
                        }
                    }
                }


                elseif ($checklevels == 9 and $te == 1 ){
                    $deletesession = LevelsTrack::where('user_session',$sessionId)->delete();
                    $response = "END Murakoze Gukoresha MNI. \n";
                    header('Content-type: text/plain');
                    echo $response;
                }

                ////////////////////// End Nominate ////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////


                ////////////////////// Start of voting ////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
            }else{

                $addsession = new LevelsTrack();
                $addsession->phonenumber = $phoneNumber;
                $addsession->user_session = $sessionId;
                $addsession->user_level = '1';
                $addsession->user_choice = '0';
                $addsession->save();

                if ($text == "") {

                    $response = "CON Urakaza Neza Kuri MNI Playlist, Kanda \n";
                    $response .= "1. Gutora \n";
                    $response .= "2. Gutanga Indirimbo";
                    header('Content-type: text/plain');
                    echo $response;

                }
            }

        }


//        if($checklevels == null){
//            $addlevel = new LevelsTrack();
//            $addlevel->user_level = '2';
//            $addlevel->phonenumber = $phoneNumber;
//            $addlevel->save();
////            $updatelevel = LevelsTrack::where('phonenumber',$phoneNumber)->update(['user_level' => '1']);
//
//            if($level == 1 or $level == 0){
//
//                $response  = "CON Urakaza Neza Kuri MNI Playlist, Kanda \n";
//                $response .= "1. Gutora \n";
//                $response .= "2. Gusohoka";
//
//                echo $response;
//
//                $level = $level +1;
//
//            }elseif ($text == 1){
//                $firstday = new Carbon('first day of this month');
//                $lastday = new Carbon('last day of this month');
//                $lastday_ = $lastday->subDays(7);
//
//                $firstdaydate = $firstday->toDateString();
//                $lastdaydate = $lastday_->toDateString();
//
//                $getplaylist = PlaylistInfo::where('starting_date', $firstdaydate)->where('ending_date',$lastdaydate)->value('id');
//                //            dd($getplaylist);
//
//                $getmusic = SongInfo::where('playlist_id',$getplaylist)->get();
//
//                $response  = "CON MNI Playlist \n";
//
//                foreach ($getmusic as $getmusics){
//                    $artistname = $getmusics->song_artist;
//                    $artistid = $getmusics->song_number;
//                    $artistmusicname = $getmusics->song_name;
//                    $sepa = "\n";
//                    $response .= $artistid . ". " . $artistname ." ($artistmusicname) ". "$sepa";
//
//                }
//
//                echo $response;
//            }elseif ($text == 2){
//                $response = "END Murakoze Gukoresha MNI. \n";
//                echo $response;
//            }
//
//        }else if( $checklevels == "2"){
//                $firstday = new Carbon('first day of this month');
//                $lastday = new Carbon('last day of this month');
//                $lastday_ = $lastday->subDays(7);
//
//                $firstdaydate = $firstday->toDateString();
//                $lastdaydate = $lastday_->toDateString();
//
//                $getplaylist = PlaylistInfo::where('starting_date', $firstdaydate)->where('ending_date',$lastdaydate)->value('id');
//    //            dd($getplaylist);
//
//                $getmusic = SongInfo::where('playlist_id',$getplaylist)->get();
//
//                $response  = "CON MNI Playlist \n";
//
//                foreach ($getmusic as $getmusics){
//                    $artistname = $getmusics->song_artist;
//                    $artistid = $getmusics->song_number;
//                    $artistmusicname = $getmusics->song_name;
//                    $sepa = "\n";
//                    $response .= $artistid . ". " . $artistname ." ($artistmusicname) ". "$sepa";
//
//                }
//
//                echo $response;
//        }

//        if($text == 0){
//
//            $response  = "CON Murakaza Neza Kuri MNI Playlist, Kanda \n";
//            $response .= "1. Gutora \n";
//            $response .= "2. Gusohoka";
//
//            echo $response;
//
////            $level = $text +1;
//        }

//        if ($level > 1)
//        {
//            if ($ussd_string_exploded[0] == "1")
//            {
//                $firstday = new Carbon('first day of this month');
//                $lastday = new Carbon('last day of this month');
//                $lastday_ = $lastday->subDays(7);
//
//                $firstdaydate = $firstday->toDateString();
//                $lastdaydate = $lastday_->toDateString();
//
//                $getplaylist = PlaylistInfo::where('starting_date', $firstdaydate)->where('ending_date',$lastdaydate)->value('id');
//    //            dd($getplaylist);
//
//                $getmusic = SongInfo::where('playlist_id',$getplaylist)->get();
//
//                $response  = "CON MNI Playlist \n";
//
//                foreach ($getmusic as $getmusics){
//                    $artistname = $getmusics->song_artist;
//                    $artistid = $getmusics->song_number;
//                    $artistmusicname = $getmusics->song_name;
//                    $sepa = "\n";
//                    $response .= $artistid . ". " . $artistname ." ($artistmusicname) ". "$sepa";
//
//                }
//
//                echo $response;
//
//            }
//            else if ($ussd_string_exploded[1] == "2"){
//                //If user selected 2, send them to the about menu
//                echo "yes";
//            }
//        }

////
//        else if ($text == "1") {
//            $firstday = new Carbon('first day of this month');
//            $lastday = new Carbon('last day of this month');
//            $lastday_ = $lastday->subDays(7);
//
//            $firstdaydate = $firstday->toDateString();
//            $lastdaydate = $lastday_->toDateString();
//
//            $getplaylist = PlaylistInfo::where('starting_date', $firstdaydate)->where('ending_date',$lastdaydate)->value('id');
////            dd($getplaylist);
//
//            $getmusic = SongInfo::where('playlist_id',$getplaylist)->get();
//
//            $response  = "CON  Hitamo Indirimbo \n";
//
//            foreach ($getmusic as $getmusics){
//                $artistname = $getmusics->song_artist;
//                $artistid = $getmusics->song_number;
//                $artistmusicname = $getmusics->song_name;
//                $sepa = "\n";
//                $response .= $artistid . ". " . $artistname ." ($artistmusicname) ". "$sepa";
//
//            }
//
//            echo $response;
//
//        }
//
//        else if ($text == "1*1") {
//
//            $getmusic = SongInfo::where('song_number','1')->get();
//            foreach ($getmusic as $getmusics){
//                $artistname = $getmusics->song_artist;
//                $artistid = $getmusics->song_number;
//                $artistmusicname = $getmusics->song_name;
//                $sepa = "\n";
//
//            }
//            $response = "CON Ugiye Gutora indirimbo $artistmusicname ya $artistname, amafaranga ni 50RWF\n";
//            $response .= "1. Kwemeza \n";
//            $response .= "2. Gusubira inyuma \n";
//            $response .= "3. Gusohoka \n";
//            echo $response;
//
//
//        }
//        else if ($text == "1*1*1") {
//
//            $getmusic = SongInfo::where('song_number','1')->get();
//
//            foreach ($getmusic as $getmusics){
//                $artistname = $getmusics->song_artist;
//                $artistid = $getmusics->song_number;
//                $artistmusicname = $getmusics->song_name;
//                $sepa = "\n";
//
//            }
//            $firstday = new Carbon('first day of this month');
//            $lastday = new Carbon('last day of this month');
//            $lastday_ = $lastday->subDays(7);
//
//            $firstdaydate = $firstday->toDateString();
//            $lastdaydate = $lastday_->toDateString();
//
//            $vote = new Votes();
//            $vote->starting_date = $firstdaydate;
//            $vote->ending_date = $lastdaydate;
//            $vote->voterphonenumber = $phoneNumber;
//            $vote->vote = "1";
//            $vote->voter_artist = $artistname;
//            $vote->voter_artist_song = $artistmusicname;
//            $vote->save();
//
//            $response = "CON Murakoze gutora $artistmusicname ya $artistname, amafaranga ni 50RWF\n";
//            $response .= "1. Ongera utore \n";
//            $response .= "2. Gusohoka \n";
//
//            echo $response;
//
//        }
//
//        else if ($text == "1*1*1*2") {
//            //note that we are using the $phoneNumber variable we got form the HTTP POST data.
//            $response = "END Murakoze Gukoresha MNI. \n";
//
//            echo $response;
//        }
//
//
////Menu for a user who selects '2'
//
//
//        else if ($text == "2") {
//            //note that we are using the $phoneNumber variable we got form the HTTP POST data.
//            $response = "END Murakoze Gukoresha MNI. \n";
//
//            echo $response;
//        }
//
//
////Menu for a user who selects '1' from second menu
//
//
//
//
////        else if ($text == "1*1*1") {
////            $response = "CON Urakoze Gutora Riderman\n";
////            $response .= "1. Ongera Utore \n";
////            $response .= "2. Gusohoka \n";
////            echo $response;
////        }
//        else if ($text == "1*1*3") {
//            $response = "END Murakoze Gukoresha MNI. \n";
//
//            echo $response;
//        }
//        else if ($text == "1*1*1*2") {
//            $response = "END Murakoze Gukoresha MNI. \n";
//
//            echo $response;
//        }
////Menu for a user who selects '2' from second menu
//
//        else if ($text == "1*2") {
//            $response = "END Your balance is USD 10.78\n";
//            echo $response;
//        }
    }
    public function Momo(){
            //First Part POST T0 API
            $phonenumber = '0782384772';
            $amount = '100';
            //dd($phonenumber);
            $idt = mt_rand(10, 99);
            //Generating Payment Gateway
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://akokanya.com/mtn-pay?amount=$amount&phone=$phonenumber&company_name=MNI&payment_code=$idt",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "Accept: */*",
                    "Cache-Control: no-cache",
                    "Connection: keep-alive",
                    "Host: akokanya.com",
                    "Postman-Token: caddbf9d-3cf9-4fc3-b051-bc0ee25a2561,de8e362a-f9dd-4a20-a3bf-099dba0f6b26",
                    "User-Agent: PostmanRuntime/7.11.0",
                    "accept-encoding: gzip, deflate",
                    "cache-control: no-cache",
                    "content-length: "
                ),
            ));

            $responseapi = curl_exec($curl);
//            dd($response);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                //echo "cURL Error #:" . $err;
                $title="Not Connected";
                $message="Sorry you do not have no internet connection";
                //return redirect()->route('balance')->with($title,$message);
                echo "$err";

            } else {
                //echo $response;

                //Showing Status
                //second part Status checking

                $responseid=json_decode($responseapi);

                $curl = curl_init();

                if($responseapi=='{"@attributes":{"errorcode":"TARGET_AUTHORIZATION_ERROR"}}'){
                    //checking balance
                    $maintitle="Balance";
                    $title="Not Enough Balance";
                    $response="Sorry you do not have enough money on your account to make this transaction, please try again after topping up your account";
                    //return redirect()->route('balance')->with($title,$message);
                    echo "$response";
                }
                elseif($responseapi=='{"@attributes":{"errorcode":"ACCOUNTHOLDER_WITH_FRI_NOT_FOUND"},"arguments":{"@attributes":{"name":"fri","value":"FRI:25'.$phonenumber.'\/MSISDN"}}}'){
                    //User not registered in Momo
                    $maintitle="Not Registered";
                    $title="Not Registered";
                    $response="Sorry you're not registered with MTN Mobile Rwanda";
                    echo "$response";
                }
                elseif($responseapi=='{"error":"the minimum amount is 100"}'){
                    //User not registered in Momo
                    $maintitle="Minimum amount";
                    $title="Minimum amount";
                    $response="Sorry the minimum amount to send is 100Frw";
                    echo "$response";
                }
                elseif($responseapi=='{"@attributes":{"errorcode":"AUTHORIZATION_MAXIMUM_AMOUNT_ALLOWED_TO_SEND"}}'){
                    //User not registered in Momo
                    $maintitle="Maximum amount";
                    $title="Maximum amount";
                    $response="Sorry the maximum amount to send is 2,000,000Frw";
                    //return redirect()->route('balance')->with($title,$message);
                    echo "$response";
                }
                else{
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$responseid->transactionid",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "GET",
                        CURLOPT_POSTFIELDS => "",
                        CURLOPT_HTTPHEADER => array(
                            "Accept: */*",
                            "Cache-Control: no-cache",
                            "Connection: keep-alive",
                            "Host: akokanya.com",
                            "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                            "User-Agent: PostmanRuntime/7.11.0",
                            "accept-encoding: gzip, deflate",
                            "cache-control: no-cache",
                            "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                            "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
                        ),
                    ));

                    $responseapis = curl_exec($curl);
                    $err = curl_error($curl);

                    curl_close($curl);

                    if ($err) {
                        echo "cURL Error #:" . $err;
                    } else {
                        //echo $response;


                        $responsedata=json_decode($responseapis);
//                        dd($responsedata);
                        //Save to database Momo payments
                        //dd( $amount);
                        $deletedat=0;
                        $Momopayment = MomoTransaction::create([
                            'phone'=> $phonenumber,
                            'transactionid'=> $responsedata[0]->external_payment_code,
                            'status'=> $responsedata[0]->payment_status,
                            'assignedid'=> $responsedata[0]->id,
                            'company_name'=> $responsedata[0]->company_name,
                            'code'=> $responsedata[0]->code,
                            'amount'=> $amount,
                            'artist_name'=> 'Bushali',
                            'artist_song'=> 'Tsikizo',
                            'payment_code'=> $responsedata[0]->payment_code,
                            'external_payment_code'=> $responsedata[0]->external_payment_code,
                            'payment_status'=> $responsedata[0]->payment_status,
                            'payment_type'=> $responsedata[0]->payment_type,
                            'callback_url'=> $responsedata[0]->callback_url,
                            'momodeleted_at'=> $deletedat,
                            'momocreated_at'=> $responsedata[0]->created_at,
                            'momoupdated_at'=> $responsedata[0]->updated_at,
                        ]);
                        if($Momopayment){


                            echo "success";
                        }
                        //end saving to database Momo payments
                    }
                    //End Status showing
                }
            }
            //End Generating payment Gateway


        }
        public function FilterDates(){
        $dates = Carbon::now();
        $firstdaydate = $dates->toDateString();
        $query = SongInfo::whereRaw('? between starting_date and ending_date',[$firstdaydate])->get();
        return $query->toArray();
        }

        public function CheckStatus(){

           $hello  = '+250782384772';

            $str = substr($hello, 3);
            dd($str);

//            $stat='PENDING';
//            $transactions = MomoTransaction::get()->count();
//            $transacs = DB::table('momotransaction')->where('status','like','%'.$stat.'%')->get();
//            //Updateloop
//            $num = count((array)$transactions);
//
//            $nums = array(count((array)$transactions)=>$num);
//
//            //dd($transacs);
//            //return view('backend.dashboard')->with(['num'=>$num]);
//
//            foreach($transacs as $transac){
//
//                $curl = curl_init();
//
//                curl_setopt_array($curl, array(
//                    CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$transac->transactionid",
//                    CURLOPT_RETURNTRANSFER => true,
//                    CURLOPT_ENCODING => "",
//                    CURLOPT_MAXREDIRS => 10,
//                    CURLOPT_TIMEOUT => 30,
//                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//                    CURLOPT_CUSTOMREQUEST => "GET",
//                    CURLOPT_POSTFIELDS => "",
//                    CURLOPT_HTTPHEADER => array(
//                        "Accept: */*",
//                        "Cache-Control: no-cache",
//                        "Connection: keep-alive",
//                        "Host: akokanya.com",
//                        "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
//                        "User-Agent: PostmanRuntime/7.11.0",
//                        "accept-encoding: gzip, deflate",
//                        "cache-control: no-cache",
//                        "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
//                        "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
//                    ),
//                ));
//
//                $response = curl_exec($curl);
//                $err = curl_error($curl);
//
//                curl_close($curl);
//
//                if ($err) {
//                    echo "cURL Error #:" . $err;
//                } else {
//                    //echo $response;
//
//                    $responsedata=json_decode($response);
//                    //Update to database Momo payments
//                    //echo $responsedata[0]->payment_status;
//
//                    $deletedat=0;
//
//                    $Momopayment = MomoTransaction::where('transactionid',$responsedata[0]->external_payment_code)
//                        ->update([
//                            'transactionid'=> $responsedata[0]->external_payment_code,
//                            'status'=> $responsedata[0]->payment_status,
//                            'assignedid'=> $responsedata[0]->id,
//                            'company_name'=> $responsedata[0]->company_name,
//                            'code'=> $responsedata[0]->code,
//                            // 'amount'=> $responsedata[0]->amount,
//                            'payment_code'=> $responsedata[0]->payment_code,
//                            'external_payment_code'=> $responsedata[0]->external_payment_code,
//                            'payment_status'=> $responsedata[0]->payment_status,
//                            'payment_type'=> $responsedata[0]->payment_type,
//                            'callback_url'=> $responsedata[0]->callback_url,
//                            'momodeleted_at'=> $deletedat,
//                            'momocreated_at'=> $responsedata[0]->created_at,
//                            'momoupdated_at'=> $responsedata[0]->updated_at,
//
//                        ]);
//                    if($Momopayment){
//                        echo "success";
//
//                    }else{
//                        echo "failed";
//                    }
//
//                    //end Update to database Momo payments
//                }
//                //End Status showing
//
//            }
//            //end cron test

        }

    }
