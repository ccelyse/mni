<?php

namespace App\Http\Controllers;

use App\Archives;
use App\archivessongs;
use App\ArtistAccounts;
use App\Attractions;
use App\BroadCast;
use App\BroadCastComment;
use App\BroadcastTopic;
use App\CommentStatus;
use App\CompetitionCardTransactions;
use App\Competitions;
use App\CompetitionSlider;
use App\CompetitionTransactions;
use App\CompVotes;
use App\Contestants;
use App\Country;
use App\Events\Event;
use App\Funds;
use App\FundsSliders;
use App\FundTransaction;
use App\Groups;
use App\HomeSlider;
use App\JoinMember;
use App\MomoTransaction;
use App\MusicStakeHolders;
use App\Nominee;
use App\PlaylistCategory;
use App\PlaylistInfo;
use App\Post;
use App\SongInfo;
use App\SongVotes;
use App\SpennTransactions;
use App\User;
use App\Votes;
use App\VotingCardTransactions;
use App\VotingStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class FrontendController extends Controller

{

    public function Home(){
        $firstday = new Carbon('first day of this month');
        $dt = Carbon::now()->addDay();
        $yesterdaydt = Carbon::now()->subDays(1);
        $tomorrow = $dt->toDateString();
        $yesterday = $yesterdaydt->toDateString();


        $currentdate = $firstday->subMonth();
        $testindate = $dt->diffForHumans($firstday);

        $lastday = new Carbon('last day of this month');
        $endofmonth = new Carbon('last day of this month');
        $lastday_ = $lastday->subDays(7);

        $testdate = Carbon::parse($firstday);
        $testdateL = Carbon::parse($lastday_);
        $newfirst = $testdate->format('jS F Y');
        $newlast = $testdateL->format('jS F Y');

        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();
        $endmontdate = $endofmonth->toDateString();


        $getmusicind = SongInfo::where('song_number','1')->get();

        $getmusicmodern = SongInfo::where('archives_status',null)->where('voter_status','SUCCESSFUL')->leftJoin('votes', 'songinfo.id', '=', 'votes.song_id')
            ->select('songinfo.id','songinfo.created_at','songinfo.song_artist','songinfo.song_name','songinfo.song_youtube_link',
                'songinfo.song_cover_picture','songinfo.playlist_code','songinfo.starting_date',
                'songinfo.ending_date', DB::raw("count(votes.id) as votesNumber"))
            ->groupBy('votes.song_id')
            ->orderBy('votesNumber','DESC')
            ->limit(30)
            ->get();

//        $getmusicmodern = SongInfo::limit(30)
//            ->orderBy('votesNumber','DESC')
//            ->get();

        foreach ($getmusicmodern as $datas){
            $this->CheckVotes($datas);
            $this->CheckVotesMonthly($datas);
//            $datas['votesNumber'] = (double) $this->CheckVotes($datas);
            $datas['votesNumberMon'] = (double) $this->CheckVotesMonthly($datas);
            $datas['votesNumberLastMon'] = (double) $this->CheckVotesLastMonth($datas);
        }
        $check_status = VotingStatus::value('voting_status');
//        return response()->json($getmusicmodern);
//        return view('newwelcome')->with(['getmusicmodern'=>$getmusicmodern,'getmusicind'=>$getmusicind,'firstday'=>$firstdaydate,'lastday'=>$lastdaydate]);
        return view('HomePage')->with(['check_status'=>$check_status,'getmusicmodern'=>$getmusicmodern,'getmusicind'=>$getmusicind,'firstday'=>$firstdaydate,'lastday'=>$lastdaydate]);

    }
    public function Discover(){
        $list_stake = MusicStakeHolders::all();
        $list_slider = HomeSlider::all();
        $music_Stake = ArtistAccounts::where('artist_category','Musicians')->count();
        $songs_ = SongInfo::count();
        $funds = FundTransaction::where('payment_status','SUCCESSFUL')->sum('amount');
        return view('V2.Discover')->with(['music_Stake'=>$music_Stake,'songs_'=>$songs_,'funds'=>$funds,'list_slider'=>$list_slider,'list_stake'=>$list_stake]);

    }
    public function Broadcast(){
//        $list_slider = BroadCast::all();
        $list_slider = HomeSlider::all();
        $last_id = BroadCast::latest('id')->first();
        $list_comment = BroadCastComment::all();
        return view('V2.Broadcast')->with(['list_slider'=>$list_slider,'last_id'=>$last_id,'list_comment'=>$list_comment]);
    }
    public function BroadcastV2(){
        $list_slider = BroadCast::all();
        $last_id = BroadcastTopic::latest('id')->first();
        $list_comment = BroadCastComment::where('broadcast_id',$last_id->id)->get();
        $comment_status = CommentStatus::value('comment_status');
        return view('V2.BroadCastV2')->with(['comment_status'=>$comment_status,'list_slider'=>$list_slider,'last_id'=>$last_id,'list_comment'=>$list_comment]);

    }
    public function AddBroadcastComment(Request $request){
        $all = $request->all();
        $add_comment = new BroadCastComment();
        $add_comment->names = $request['names'];
        $add_comment->broadcast_id = $request['broadcast_id'];
        $add_comment->email = $request['email'];
        $add_comment->comment = $request['comment'];
        $add_comment->save();

        return back()->with('success','You have successfully added your comment');
    }

    public function HomeV2(){
        $firstday = new Carbon('first day of this month');
        $dt = Carbon::now()->addDay();
        $yesterdaydt = Carbon::now()->subDays(1);
        $tomorrow = $dt->toDateString();
        $yesterday = $yesterdaydt->toDateString();


        $currentdate = $firstday->subMonth();
        $testindate = $dt->diffForHumans($firstday);

        $lastday = new Carbon('last day of this month');
        $endofmonth = new Carbon('last day of this month');
        $lastday_ = $lastday->subDays(7);

        $testdate = Carbon::parse($firstday);
        $testdateL = Carbon::parse($lastday_);
        $newfirst = $testdate->format('jS F Y');
        $newlast = $testdateL->format('jS F Y');

        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();
        $endmontdate = $endofmonth->toDateString();


        $getmusicind = SongInfo::where('song_number','1')->get();

//        $getmusicmodern = SongInfo::where('archives_status',null)->where('voter_status','SUCCESSFUL')->leftJoin('votes', 'songinfo.id', '=', 'votes.song_id')
//            ->select('songinfo.id','songinfo.created_at','songinfo.song_artist','songinfo.song_name','songinfo.song_youtube_link',
//                'songinfo.song_cover_picture','songinfo.playlist_code','songinfo.starting_date',
//                'songinfo.ending_date', DB::raw("count(votes.id) as votesNumber"))
//            ->groupBy('votes.song_id')
//            ->orderBy('votesNumber','DESC')
//            ->limit(30)
//            ->get();
        $getmusicmodern_promoted = SongInfo::where('promotion_status','promoted')
            ->where('archives_status',null)->where('voter_status','SUCCESSFUL')
            ->leftJoin('votes', 'songinfo.id', '=', 'votes.song_id')
            ->select('songinfo.id','songinfo.created_at','songinfo.song_artist','songinfo.song_name','songinfo.song_youtube_link',
                'songinfo.song_cover_picture','songinfo.playlist_code','songinfo.starting_date','songinfo.user_id',
                'songinfo.ending_date', DB::raw("sum(vote) as votesNumber"))
            ->groupBy('votes.song_id')
            ->orderBy('votesNumber','DESC')
            ->limit(10)
            ->get();

        $getmusicmodern = SongInfo::where('archives_status',null)->where('voter_status','SUCCESSFUL')->leftJoin('votes', 'songinfo.id', '=', 'votes.song_id')
            ->select('songinfo.id','songinfo.created_at','songinfo.song_artist','songinfo.song_name','songinfo.song_youtube_link',
                'songinfo.song_cover_picture','songinfo.playlist_code','songinfo.starting_date','songinfo.user_id',
                'songinfo.ending_date', DB::raw("sum(vote) as votesNumber"))
            ->groupBy('votes.song_id')
            ->orderBy('votesNumber','DESC')
            ->limit(10)
            ->get();

        foreach ($getmusicmodern_promoted as $datas){
            $this->CheckVotesUnlimited($datas);
            $this->CheckVotesMonthlyUnlimited($datas);
            $datas['votesNumber'] = (double) $this->CheckVotesUnlimited($datas);
            $datas['votesNumberMon'] = (double) $this->CheckVotesMonthlyUnlimited($datas);
            $datas['votesNumberLastMon'] = (double) $this->CheckVotesLastMonthUnlimited($datas);
        }

        foreach ($getmusicmodern as $datas){
            $this->CheckVotesUnlimited($datas);
            $this->CheckVotesMonthlyUnlimited($datas);
            $datas['votesNumber'] = (double) $this->CheckVotesUnlimited($datas);
            $datas['votesNumberMon'] = (double) $this->CheckVotesMonthlyUnlimited($datas);
            $datas['votesNumberLastMon'] = (double) $this->CheckVotesLastMonthUnlimited($datas);
        }
//        dd($getmusicmodern);
//        return response()->json($getmusicmodern);
        $check_status = VotingStatus::value('voting_status');
        $list_stake = MusicStakeHolders::all();
        $list_slider = HomeSlider::all();
        $music_Stake = ArtistAccounts::where('artist_category','Musicians')->count();
        $songs_ = SongInfo::count();
        $funds = FundTransaction::where('payment_status','SUCCESSFUL')->sum('amount');
        $firstdayMNi = new Carbon('first day of this month');
        $newfirstYear = $firstdayMNi->format('F Y');
        return view('V2.HomeV2')->with(['newfirstYear'=>$newfirstYear,'music_Stake'=>$music_Stake,'songs_'=>$songs_,'funds'=>$funds,'list_slider'=>$list_slider,'list_stake'=>$list_stake,'check_status'=>$check_status,'getmusicmodern_promoted'=>$getmusicmodern_promoted,'getmusicmodern'=>$getmusicmodern,'getmusicind'=>$getmusicind,'firstday'=>$firstdaydate,'lastday'=>$lastdaydate]);

    }
    public function HomePage(){
        $firstday = new Carbon('first day of this month');
        $dt = Carbon::now()->addDay();
        $yesterdaydt = Carbon::now()->subDays(1);
        $tomorrow = $dt->toDateString();
        $yesterday = $yesterdaydt->toDateString();


        $currentdate = $firstday->subMonth();
        $testindate = $dt->diffForHumans($firstday);

        $lastday = new Carbon('last day of this month');
        $endofmonth = new Carbon('last day of this month');
        $lastday_ = $lastday->subDays(7);

        $testdate = Carbon::parse($firstday);
        $testdateL = Carbon::parse($lastday_);
        $newfirst = $testdate->format('jS F Y');
        $newlast = $testdateL->format('jS F Y');

        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();
        $endmontdate = $endofmonth->toDateString();


        $getmusicind = SongInfo::where('song_number','1')->get();

//        $getmusicmodern = SongInfo::where('archives_status',null)->where('voter_status','SUCCESSFUL')->leftJoin('votes', 'songinfo.id', '=', 'votes.song_id')
//            ->select('songinfo.id','songinfo.created_at','songinfo.song_artist','songinfo.song_name','songinfo.song_youtube_link',
//                'songinfo.song_cover_picture','songinfo.playlist_code','songinfo.starting_date',
//                'songinfo.ending_date', DB::raw("count(votes.id) as votesNumber"))
//            ->groupBy('votes.song_id')
//            ->orderBy('votesNumber','DESC')
//            ->limit(30)
//            ->get();
        $getmusicmodern = SongInfo::where('archives_status',null)->where('voter_status','SUCCESSFUL')->leftJoin('votes', 'songinfo.id', '=', 'votes.song_id')
            ->select('songinfo.id','songinfo.created_at','songinfo.song_artist','songinfo.song_name','songinfo.song_youtube_link',
                'songinfo.song_cover_picture','songinfo.playlist_code','songinfo.starting_date',
                'songinfo.ending_date', DB::raw("sum(vote) as votesNumber"))
            ->groupBy('votes.song_id')
            ->orderBy('votesNumber','DESC')
            ->limit(30)
            ->get();

        foreach ($getmusicmodern as $datas){
            $this->CheckVotesUnlimited($datas);
            $this->CheckVotesMonthlyUnlimited($datas);
            $datas['votesNumber'] = (double) $this->CheckVotesUnlimited($datas);
            $datas['votesNumberMon'] = (double) $this->CheckVotesMonthlyUnlimited($datas);
            $datas['votesNumberLastMon'] = (double) $this->CheckVotesLastMonthUnlimited($datas);
        }
//        dd($getmusicmodern);
//        return response()->json($getmusicmodern);
        $check_status = VotingStatus::value('voting_status');
        return view('HomePage')->with(['check_status'=>$check_status,'getmusicmodern'=>$getmusicmodern,'getmusicind'=>$getmusicind,'firstday'=>$firstdaydate,'lastday'=>$lastdaydate]);

    }

    public function ListArchives(){
        $list_slider = HomeSlider::all();
        $all = Archives::all();
        return view('V2.ListArchives')->with(['list_slider'=>$list_slider,'all'=>$all]);
    }
    public function Archives(Request $request){
        $all = $request->all();

        $firstday = new Carbon('first day of this month');
        $dt = Carbon::now();
        $currentdate = $firstday->subMonth();
        $testindate = $dt->diffForHumans($firstday);

        $lastday = new Carbon('last day of this month');
        $endofmonth = new Carbon('last day of this month');
        $lastday_ = $lastday->subDays(7);

        $testdate = Carbon::parse($firstday);

        $testdateL = Carbon::parse($lastday_);
        $newfirst = $testdate->format('jS F Y');

        $firstdayMNi = new Carbon('first day of this month');
        $newfirstYear = $firstdayMNi->format('F Y');


        $newlast = $testdateL->format('jS F Y');


        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();
        $endmontdate = $endofmonth->toDateString();

//        $getplaylist = PlaylistInfo::where('starting_date', $firstdaydate)->where('ending_date', $lastdaydate)->value('id');
        $getmusicind = SongInfo::where('song_number','1')->get();

//        $getmusicmodern = SongInfo::where('voter_status','SUCCESSFUL')->leftJoin('votes', 'songinfo.id', '=', 'votes.song_id')
//            ->select('songinfo.id','songinfo.created_at','songinfo.song_artist','songinfo.song_name','songinfo.song_youtube_link',
//                'songinfo.song_cover_picture','songinfo.playlist_code','songinfo.starting_date',
//                'songinfo.ending_date', DB::raw("count(votes.id) as votesNumber"))
//            ->whereBetween(DB::raw('votes.created_at'), [DB::raw($firstdaydate), DB::raw($endmontdate)])
//            ->groupBy('votes.song_id')
//            ->orderBy('votesNumber','DESC')
//            ->get();
//            ->paginate(100);
        $archives = archivessongs::where('archive_id',$request['id'])
            ->get();
        $new = json_decode($archives);
        foreach ($new as $data){
            $ids = $data->song_id;
            $archive_id = $data->archive_id;
        }

        $archive_name = Archives::where('id',$archive_id)->value('archive_name');
        $ids= array();
        foreach($new as $data){
            array_push($ids, $data->song_id);
        }

        $getmusicmodern = SongInfo::whereIn('songinfo.id',$ids)->where('archives_status','archived')->where('voter_status','SUCCESSFUL')->leftJoin('votes', 'songinfo.id', '=', 'votes.song_id')
            ->select('songinfo.id','songinfo.created_at','songinfo.song_artist','songinfo.song_name','songinfo.song_youtube_link',
                'songinfo.song_cover_picture','songinfo.playlist_code','songinfo.starting_date','songinfo.user_id',
                'songinfo.ending_date', DB::raw("count(votes.id) as votesNumber"))
            ->groupBy('votes.song_id')
            ->orderBy('votesNumber','DESC')
            ->limit(100)
            ->get();

//        foreach ($getmusicmodern as $datas){
//            $this->CheckVotes($datas);
//            $this->CheckVotesMonthly($datas);
//            $datas['votesNumber'] = (double) $this->CheckVotes($datas);
//            $datas['votesNumberMon'] = (double) $this->CheckVotesMonthly($datas);
//            $datas['votesNumberLastMon'] = (double) $this->CheckVotesLastMonth($datas);
//        }
        foreach ($getmusicmodern as $datas){
            $this->CheckVotesUnlimited($datas);
            $this->CheckVotesMonthlyUnlimited($datas);
            $datas['votesNumber'] = (double) $this->CheckVotesUnlimited($datas);
            $datas['votesNumberMon'] = (double) $this->CheckVotesMonthlyUnlimited($datas);
            $datas['votesNumberLastMon'] = (double) $this->CheckVotesLastMonthUnlimited($datas);
        }
        $list_slider = HomeSlider::all();
        $check_status = VotingStatus::value('voting_status');
        return view('V2.Archives')->with(['check_status'=>$check_status,'list_slider'=>$list_slider,'archive_name'=>$archive_name,'newfirstYear'=>$newfirstYear,'getmusicmodern'=>$getmusicmodern,'getmusicind'=>$getmusicind,'firstday'=>$firstdaydate,'lastday'=>$lastdaydate]);
    }
    public function Urutonde(){
        $firstday = new Carbon('first day of this month');
        $dt = Carbon::now();
        $currentdate = $firstday->subMonth();
        $testindate = $dt->diffForHumans($firstday);

        $lastday = new Carbon('last day of this month');
        $endofmonth = new Carbon('last day of this month');
        $lastday_ = $lastday->subDays(7);

        $testdate = Carbon::parse($firstday);

        $testdateL = Carbon::parse($lastday_);
        $newfirst = $testdate->format('jS F Y');

        $firstdayMNi = new Carbon('first day of this month');
        $newfirstYear = $firstdayMNi->format('F Y');


        $newlast = $testdateL->format('jS F Y');


        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();
        $endmontdate = $endofmonth->toDateString();

//        $getplaylist = PlaylistInfo::where('starting_date', $firstdaydate)->where('ending_date', $lastdaydate)->value('id');
        $getmusicind = SongInfo::where('song_number','1')->get();

//        $getmusicmodern = SongInfo::where('voter_status','SUCCESSFUL')->leftJoin('votes', 'songinfo.id', '=', 'votes.song_id')
//            ->select('songinfo.id','songinfo.created_at','songinfo.song_artist','songinfo.song_name','songinfo.song_youtube_link',
//                'songinfo.song_cover_picture','songinfo.playlist_code','songinfo.starting_date',
//                'songinfo.ending_date', DB::raw("count(votes.id) as votesNumber"))
//            ->whereBetween(DB::raw('votes.created_at'), [DB::raw($firstdaydate), DB::raw($endmontdate)])
//            ->groupBy('votes.song_id')
//            ->orderBy('votesNumber','DESC')
//            ->get();
//            ->paginate(100);

        $getmusicmodern = SongInfo::where('archives_status',null)->where('voter_status','SUCCESSFUL')->leftJoin('votes', 'songinfo.id', '=', 'votes.song_id')
            ->select('songinfo.id','songinfo.created_at','songinfo.song_artist','songinfo.song_name','songinfo.song_youtube_link',
                'songinfo.song_cover_picture','songinfo.playlist_code','songinfo.starting_date',
                'songinfo.ending_date', DB::raw("count(votes.id) as votesNumber"))
            ->groupBy('votes.song_id')
            ->orderBy('votesNumber','DESC')
            ->limit(100)
            ->get();
//        $getmusicmodern = SongInfo::limit(100)->get();
//        foreach ($getmusicmodern as $datas){
//            $this->CheckVotes($datas);
//            $this->CheckVotesMonthly($datas);
//            $datas['votesNumber'] = (double) $this->CheckVotes($datas);
//            $datas['votesNumberMon'] = (double) $this->CheckVotesMonthly($datas);
//            $datas['votesNumberLastMon'] = (double) $this->CheckVotesLastMonth($datas);
//        }
        foreach ($getmusicmodern as $datas){
            $this->CheckVotesUnlimited($datas);
            $this->CheckVotesMonthlyUnlimited($datas);
            $datas['votesNumber'] = (double) $this->CheckVotesUnlimited($datas);
            $datas['votesNumberMon'] = (double) $this->CheckVotesMonthlyUnlimited($datas);
            $datas['votesNumberLastMon'] = (double) $this->CheckVotesLastMonthUnlimited($datas);
        }
        $check_status = VotingStatus::value('voting_status');
        return view('Urutonde')->with(['check_status'=>$check_status,'newfirstYear'=>$newfirstYear,'getmusicmodern'=>$getmusicmodern,'getmusicind'=>$getmusicind,'firstday'=>$firstdaydate,'lastday'=>$lastdaydate]);
    }
    public function StakeHolders(Request $request){
        $id = $request['id'];
        $get_cat = MusicStakeHolders::where('id',$id)->value('slider_title');
        $artist = ArtistAccounts::where('artist_category',$get_cat)->where('artist_account_status','Approved')->get();
        $list_stake = MusicStakeHolders::all();
        $list_slider = HomeSlider::all();
        return view('V2.StakeHolders')->with(['artist'=>$artist,'list_stake'=>$list_stake,'list_slider'=>$list_slider]);

    }
    public function StakeHoldersRegister(Request $request){
        $list_stake = MusicStakeHolders::all();
        $list_slider = HomeSlider::all();
        return view('V2.StakeHoldersRegister')->with(['list_stake'=>$list_stake,'list_slider'=>$list_slider]);
    }
    public function Charts(){
        $firstday = new Carbon('first day of this month');
        $dt = Carbon::now();
        $currentdate = $firstday->subMonth();
        $testindate = $dt->diffForHumans($firstday);

        $lastday = new Carbon('last day of this month');
        $endofmonth = new Carbon('last day of this month');
        $lastday_ = $lastday->subDays(7);

        $testdate = Carbon::parse($firstday);

        $testdateL = Carbon::parse($lastday_);
        $newfirst = $testdate->format('jS F Y');

        $firstdayMNi = new Carbon('first day of this month');
        $newfirstYear = $firstdayMNi->format('F Y');


        $newlast = $testdateL->format('jS F Y');


        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();
        $endmontdate = $endofmonth->toDateString();

        $getmusicind = SongInfo::where('song_number','1')->get();

        $getmusicmodern_promoted = SongInfo::where('promotion_status','promoted')->where('archives_status',null)->where('voter_status','SUCCESSFUL')->leftJoin('votes', 'songinfo.id', '=', 'votes.song_id')
            ->select('songinfo.id','songinfo.created_at','songinfo.song_artist','songinfo.song_name','songinfo.song_youtube_link',
                'songinfo.song_cover_picture','songinfo.playlist_code','songinfo.starting_date','songinfo.user_id',
                'songinfo.ending_date', DB::raw("sum(vote) as votesNumber"))
            ->groupBy('votes.song_id')
            ->orderBy('votesNumber','DESC')
            ->limit(10)
            ->get();

        $getmusicmodern = SongInfo::where('archives_status',null)->where('voter_status','SUCCESSFUL')->leftJoin('votes', 'songinfo.id', '=', 'votes.song_id')
            ->select('songinfo.id','songinfo.created_at','songinfo.song_artist','songinfo.song_name','songinfo.song_youtube_link',
                'songinfo.song_cover_picture','songinfo.playlist_code','songinfo.starting_date','songinfo.user_id',
                'songinfo.ending_date', DB::raw("sum(vote) as votesNumber"))
            ->groupBy('votes.song_id')
            ->orderBy('votesNumber','DESC')
//            ->limit(10)
            ->get();

        foreach ($getmusicmodern_promoted as $datas){
            $this->CheckVotesUnlimited($datas);
            $this->CheckVotesMonthlyUnlimited($datas);
            $datas['votesNumber'] = (double) $this->CheckVotesUnlimited($datas);
            $datas['votesNumberMon'] = (double) $this->CheckVotesMonthlyUnlimited($datas);
            $datas['votesNumberLastMon'] = (double) $this->CheckVotesLastMonthUnlimited($datas);
        }

        foreach ($getmusicmodern as $datas){
            $this->CheckVotesUnlimited($datas);
            $this->CheckVotesMonthlyUnlimited($datas);
            $datas['votesNumber'] = (double) $this->CheckVotesUnlimited($datas);
            $datas['votesNumberMon'] = (double) $this->CheckVotesMonthlyUnlimited($datas);
            $datas['votesNumberLastMon'] = (double) $this->CheckVotesLastMonthUnlimited($datas);
        }

        $check_status = VotingStatus::value('voting_status');
        $list_slider = HomeSlider::all();
        return view('V2.Charts')->with(['list_slider'=>$list_slider,'check_status'=>$check_status,'newfirstYear'=>$newfirstYear,'getmusicmodern'=>$getmusicmodern,'getmusicind'=>$getmusicind,'firstday'=>$firstdaydate,'lastday'=>$lastdaydate]);
    }

    public function ChartsData(Request $request){
        $limit = $request['limit'];
        $start = $request['start'];
        $firstday = new Carbon('first day of this month');
        $dt = Carbon::now();
        $currentdate = $firstday->subMonth();
        $testindate = $dt->diffForHumans($firstday);

        $lastday = new Carbon('last day of this month');
        $endofmonth = new Carbon('last day of this month');
        $lastday_ = $lastday->subDays(7);

        $testdate = Carbon::parse($firstday);

        $testdateL = Carbon::parse($lastday_);
        $newfirst = $testdate->format('jS F Y');

        $firstdayMNi = new Carbon('first day of this month');


        $getmusicmodern = SongInfo::where('archives_status',null)->where('voter_status','SUCCESSFUL')->leftJoin('votes', 'songinfo.id', '=', 'votes.song_id')
            ->select('songinfo.id','songinfo.created_at','songinfo.song_artist','songinfo.song_name','songinfo.song_youtube_link',
                'songinfo.song_cover_picture','songinfo.playlist_code','songinfo.starting_date',
                'songinfo.ending_date', DB::raw("count(votes.id) as votesNumber"))
            ->groupBy('votes.song_id')
            ->orderBy('votesNumber','DESC')
            ->limit(30)
            ->get();

        foreach ($getmusicmodern as $datas){
            $this->CheckVotesUnlimited($datas);
            $this->CheckVotesMonthlyUnlimited($datas);
            $datas['votesNumber'] = (double) $this->CheckVotesUnlimited($datas);
            $datas['votesNumberMon'] = (double) $this->CheckVotesMonthlyUnlimited($datas);
            $datas['votesNumberLastMon'] = (double) $this->CheckVotesLastMonthUnlimited($datas);
        }

        foreach($getmusicmodern as $index => $getmusicsmodern){

            $created_at = $getmusicsmodern->created_at;
            $lastTimeLoggedOut = \Illuminate\Support\Carbon::parse($created_at)->diffForHumans();

            $votesbe = $getmusicsmodern->votesNumberMon;
            $newvotes = $votesbe - 1;

            echo "
            <li data-src=\"audio/sound_1.mp3\">
                <table>
                    <tbody>
                    <tr>
                        <td class=\"secondary-color numbering\">
                            {{ $index+1 }} /
                        </td>
                        <td class=\"artist\">
                            <img src=\"V2/SongImages/{{$getmusicsmodern->song_cover_picture}}\" alt=\"\">
                            <p class=\"secondary-color\">{{$getmusicsmodern->song_artist}}</p>
                        </td>
                        <td class=\"title\">
                            <span class=\"name secondary-color\">{{$getmusicsmodern->song_name}}</span>
                        </td>
                        <td class=\"secondary-color\">
                        $lastTimeLoggedOut
                        </td>
                        <td class=\"secondary-color\">
                          
                        </td>
                        <td>
                            <a href=\"{{ route('VoteNowV2',['id'=> $getmusicsmodern->id])}}\" class=\"dlw-btn\">Vote Now</a>
                            <div style=\"margin-right: 10px;\">
                                <span><a href=\"{{$getmusicsmodern->song_youtube_link}}\" target=\"_blank\"><i class=\"fa fa-youtube\" aria-hidden=\"true\" style=\"font-size: 30px;color:#1c706d;\"></i></a></span>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </li>
            ";
        }
    }
    public function VoteNow(Request $request){
        $id = $request['id'];
//        $id = "4";

        $firstday = new Carbon('first day of this month');
        $dt = Carbon::now();
        $currentdate = $firstday->subMonth();
        $testindate = $dt->diffForHumans($firstday);

        $lastday = new Carbon('last day of this month');
        $endofmonth = new Carbon('last day of this month');
        $lastday_ = $lastday->subDays(7);

        $testdate = Carbon::parse($firstday);
        $testdateL = Carbon::parse($lastday_);
        $newfirst = $testdate->format('jS F Y');
        $newlast = $testdateL->format('jS F Y');

        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();
        $endmontdate = $endofmonth->toDateString();

//        $getmusicmodern= SongInfo::where('id', $id)->get();
        $getmusicmodern = SongInfo::where('songinfo.id', $id)->where('voter_status','SUCCESSFUL')->leftJoin('votes', 'songinfo.id', '=', 'votes.song_id')
            ->select('songinfo.id','songinfo.created_at','songinfo.song_artist','songinfo.song_name','songinfo.song_youtube_link',
                'songinfo.song_cover_picture','songinfo.playlist_code','songinfo.starting_date',
                'songinfo.ending_date', DB::raw("count(votes.id) as votesNumber"))
//            ->whereBetween(DB::raw('votes.created_at'), [DB::raw($firstdaydate), DB::raw($endmontdate)])
            ->groupBy('votes.song_id')
//            ->orderBy('votes.song_id','DESC')
            ->orderBy('votesNumber','DESC')
//            ->limit(25)
            ->get();

//        $getmusicmodern = SongInfo::where('id', $id)->get();

        foreach ($getmusicmodern as $datas){
            $this->CheckVotes($datas);
            $this->CheckVotesMonthly($datas);
            $datas['votesNumber'] = (double) $this->CheckVotes($datas);
            $datas['votesNumberMon'] = (double) $this->CheckVotesMonthly($datas);
            $datas['votesNumberLastMon'] = (double) $this->CheckVotesLastMonth($datas);
        }
//        return response()->json($getmusicmodern);
        return view('VoteNow')->with(['getmusicmodern'=>$getmusicmodern]);
    }
    public function NewVoteNow(Request $request){
        $id = $request['id'];
//        $id = "4";

        $firstday = new Carbon('first day of this month');
        $dt = Carbon::now();
        $currentdate = $firstday->subMonth();
        $testindate = $dt->diffForHumans($firstday);

        $lastday = new Carbon('last day of this month');
        $endofmonth = new Carbon('last day of this month');
        $lastday_ = $lastday->subDays(7);

        $testdate = Carbon::parse($firstday);
        $testdateL = Carbon::parse($lastday_);
        $newfirst = $testdate->format('jS F Y');
        $newlast = $testdateL->format('jS F Y');

        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();
        $endmontdate = $endofmonth->toDateString();

//        $getmusicmodern= SongInfo::where('id', $id)->get();
        $getmusicmodern = SongInfo::where('songinfo.id', $id)->where('voter_status','SUCCESSFUL')->leftJoin('votes', 'songinfo.id', '=', 'votes.song_id')
            ->select('songinfo.id','songinfo.created_at','songinfo.song_artist','songinfo.song_name','songinfo.song_youtube_link',
                'songinfo.song_cover_picture','songinfo.playlist_code','songinfo.starting_date',
                'songinfo.ending_date', DB::raw("count(votes.id) as votesNumber"))
//            ->whereBetween(DB::raw('votes.created_at'), [DB::raw($firstdaydate), DB::raw($endmontdate)])
            ->groupBy('votes.song_id')
//            ->orderBy('votes.song_id','DESC')
            ->orderBy('votesNumber','DESC')
//            ->limit(25)
            ->get();

//        $getmusicmodern = SongInfo::where('id', $id)->get();

        foreach ($getmusicmodern as $datas){
            $this->CheckVotesUnlimited($datas);
            $this->CheckVotesMonthlyUnlimited($datas);
            $this->CheckVotesLastMonthUnlimited($datas);
            $datas['votesNumber'] = (double) $this->CheckVotesUnlimited($datas);
            $datas['votesNumberMon'] = (double) $this->CheckVotesMonthlyUnlimited($datas);
            $datas['votesNumberLastMon'] = (double) $this->CheckVotesLastMonthUnlimited($datas);
        }
//        return response()->json($getmusicmodern);
        $check_status = VotingStatus::value('voting_status');
        return view('NewVoteNow')->with(['check_status'=>$check_status,'getmusicmodern'=>$getmusicmodern]);
    }

    public function VoteNowV2(Request $request){
        $country = Country::all();
        $id = $request['id'];
//        $id = "4";

        $firstday = new Carbon('first day of this month');
        $dt = Carbon::now();
        $currentdate = $firstday->subMonth();
        $testindate = $dt->diffForHumans($firstday);

        $lastday = new Carbon('last day of this month');
        $endofmonth = new Carbon('last day of this month');
        $lastday_ = $lastday->subDays(7);

        $testdate = Carbon::parse($firstday);
        $testdateL = Carbon::parse($lastday_);
        $newfirst = $testdate->format('jS F Y');
        $newlast = $testdateL->format('jS F Y');

        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();
        $endmontdate = $endofmonth->toDateString();

//        $getmusicmodern= SongInfo::where('id', $id)->get();
        $getmusicmodern = SongInfo::where('songinfo.id', $id)->where('voter_status','SUCCESSFUL')->leftJoin('votes', 'songinfo.id', '=', 'votes.song_id')
            ->select('songinfo.id','songinfo.created_at','songinfo.song_artist','songinfo.song_name','songinfo.song_youtube_link',
                'songinfo.song_cover_picture','songinfo.playlist_code','songinfo.starting_date',
                'songinfo.ending_date', DB::raw("count(votes.id) as votesNumber"))
//            ->whereBetween(DB::raw('votes.created_at'), [DB::raw($firstdaydate), DB::raw($endmontdate)])
            ->groupBy('votes.song_id')
//            ->orderBy('votes.song_id','DESC')
            ->orderBy('votesNumber','DESC')
//            ->limit(25)
            ->get();

//        $getmusicmodern = SongInfo::where('id', $id)->get();

        foreach ($getmusicmodern as $datas){
            $this->CheckVotesUnlimited($datas);
            $this->CheckVotesMonthlyUnlimited($datas);
            $this->CheckVotesLastMonthUnlimited($datas);
            $datas['votesNumber'] = (double) $this->CheckVotesUnlimited($datas);
            $datas['votesNumberMon'] = (double) $this->CheckVotesMonthlyUnlimited($datas);
            $datas['votesNumberLastMon'] = (double) $this->CheckVotesLastMonthUnlimited($datas);
        }
//        return response()->json($getmusicmodern);
        $check_status = VotingStatus::value('voting_status');
        return view('V2.VoteNow')->with(['country'=>$country,'check_status'=>$check_status,'getmusicmodern'=>$getmusicmodern]);
    }
    public function CheckVotes($datas){
        $getvotes = Votes::where('song_id',$datas->id)
            ->where('voter_status','SUCCESSFUL')
            ->count();

        return json_encode($getvotes);
    }
    public function CheckVotesMonthly($datas){
        $firstday = new Carbon('first day of this month');
        $endofmonth = new Carbon('last day of this month');

        $firstdaydate = $firstday->toDateString();
        $endmontdate = $endofmonth->toDateString();

        $getvotesM = Votes::where('song_id',$datas->id)->where('voter_status','SUCCESSFUL')
            ->whereBetween('created_at', [$firstdaydate,$endmontdate])
            ->count();
//
        return json_encode($getvotesM);
    }
    public function CheckVotesLastMonth($datas){
        $firstday = new Carbon('first day of last month');
        $endofmonth = new Carbon('last day of last month');
        $firstdaynexm = new Carbon('first day of this month');

        $firstdaydate = $firstday->toDateString();
        $endmontdate = $endofmonth->toDateString();
        $getvotesM = Votes::where('song_id',$datas->id)->where('voter_status','SUCCESSFUL')
            ->whereBetween('created_at', [$firstdaydate,$firstdaynexm])
            ->count();

//        dd($getvotesM);
        return json_encode($getvotesM);
    }
    public function CheckVotesUnlimited($datas){
//        $getvotes = Votes::where('song_id',$datas->id)
//            ->where('voter_status','SUCCESSFUL')
//            ->count();
        $getvotes = Votes::where('song_id',$datas->id)
            ->where('voter_status','SUCCESSFUL')
            ->sum('vote');

        return json_encode($getvotes);
    }
    public function CheckVotesMonthlyUnlimited($datas){
        $firstday = new Carbon('first day of this month');
        $endofmonth = new Carbon('last day of this month');

        $firstdaydate = $firstday->toDateString();
        $endmontdate = $endofmonth->toDateString();

//        $getvotesM = Votes::where('song_id',$datas->id)->where('voter_status','SUCCESSFUL')
//            ->whereBetween('created_at', [$firstdaydate,$endmontdate])
//            ->count();
        $getvotesM = Votes::where('song_id',$datas->id)->where('voter_status','SUCCESSFUL')
            ->whereBetween('created_at', [$firstdaydate,$endmontdate])
            ->sum('vote');
        return json_encode($getvotesM);
    }

    public function CheckVotesLastMonthUnlimited($datas){
        $firstday = new Carbon('first day of last month');
        $endofmonth = new Carbon('last day of last month');
        $firstdaynexm = new Carbon('first day of this month');

        $firstdaydate = $firstday->toDateString();
        $endmontdate = $endofmonth->toDateString();
        $getvotesM = Votes::where('song_id',$datas->id)->where('voter_status','SUCCESSFUL')
            ->whereBetween('created_at', [$firstdaydate,$firstdaynexm])
            ->sum('vote');
        return json_encode($getvotesM);
    }
    public function testdates(){
        $firstday = new Carbon('first day of this month');
        $dt = Carbon::now();
        $currentdate = $firstday->subMonth();
        $testindate = $dt->diffForHumans($firstday);

        $lastday = new Carbon('last day of this month');
        $endofmonth = new Carbon('last day of this month');
        $lastday_ = $lastday->subDays(7);

        $testdate = Carbon::parse($firstday);
        $testdateL = Carbon::parse($lastday_);
        $newfirst = $testdate->format('jS F Y');
        $newlast = $testdateL->format('jS F Y');

        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();
        $endmontdate = $endofmonth->toDateString();

        dd($firstday);
    }

    public function VoteNowNumber(Request $request){
        $id = $request['id'];
        $phonenumbers = $request['phonenumber'];
        $song_name = $request['song_name'];
        $song_artist = $request['song_artist'];

        $firstday = new Carbon('first day of last month');
        $lastday = new Carbon('last day of last month');
        $lastday_ = $lastday->subDays(7);
        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();

        $vote = new Votes();
        $vote->starting_date = $firstdaydate;
        $vote->ending_date = $lastdaydate;
        $vote->voterphonenumber = $phonenumbers;
        $vote->vote = "1";
        $vote->voter_status = "PENDING";
        $vote->voter_artist = $song_artist;
        $vote->voter_artist_song = $song_name;
        $vote->song_id = $id;
        $vote->save();
        $last_id = $vote->id;

        $votepivot = new SongVotes();
        $votepivot->song_id = $id;
        $votepivot->vote_id = $last_id;
        $votepivot->save();


        $str_number = substr($phonenumbers, 3);
        $phonenumber = $phonenumbers;
//                    $phonenumber = $phoneNumber;
        $amount = '50';
        $idt = mt_rand(10, 99);
        //Generating Payment Gateway
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://akokanya.com/mtn-pay?amount=$amount&phone=$phonenumber&company_name=MNI&payment_code=$idt",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Host: akokanya.com",
                "Postman-Token: caddbf9d-3cf9-4fc3-b051-bc0ee25a2561,de8e362a-f9dd-4a20-a3bf-099dba0f6b26",
                "User-Agent: PostmanRuntime/7.11.0",
                "accept-encoding: gzip, deflate",
                "cache-control: no-cache",
                "content-length: "
            ),
        ));

        $responseapi = curl_exec($curl);
//            dd($response);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            //echo "cURL Error #:" . $err;
            $title="Not Connected";
            $message="Sorry you do not have no internet connection";
            //return redirect()->route('balance')->with($title,$message);
//            echo "$err";
            return response()->json([
                'message' =>$message,
            ]);

        } else {
            //echo $response;

            //Showing Status
            //second part Status checking

            $responseid=json_decode($responseapi);
//                        dd($responseid);

            $curl = curl_init();

            if($responseapi=='{"@attributes":{"errorcode":"TARGET_AUTHORIZATION_ERROR"}}'){
                //checking balance
                $maintitle="Balance";
                $title="Not Enough Balance";
                $response ="Sorry you do not have enough money on your account to make this transaction, please try again after topping up your account";
                //return redirect()->route('balance')->with($title,$message);
                return response()->json([
                    'message' =>$response,
                ]);
//                return view('VotePayError')->with(['response'=>$response]);
            }
            elseif($responseapi=='{"@attributes":{"errorcode":"ACCOUNTHOLDER_WITH_FRI_NOT_FOUND"},"arguments":{"@attributes":{"name":"fri","value":"FRI:25'.$phonenumber.'\/MSISDN"}}}'){
                //User not registered in Momo
                $maintitle="Not Registered";
                $title="Not Registered";
                $response="Sorry you're not registered with MTN Mobile Rwanda";
//                return view('VotePayError')->with(['response'=>$response]);
                return response()->json([
                    'message' =>$response,
                ]);
            }
            elseif($responseapi=='{"error":"the minimum amount is 100"}'){
                //User not registered in Momo
                $maintitle="Minimum amount";
                $title="Minimum amount";
                $response="Sorry the minimum amount to send is 100Frw";
//                return view('VotePayError')->with(['response'=>$response]);
                return response()->json([
                    'message' =>$response,
                ]);
            }
            elseif($responseapi=='{"@attributes":{"errorcode":"AUTHORIZATION_MAXIMUM_AMOUNT_ALLOWED_TO_SEND"}}'){
                //User not registered in Momo
                $maintitle="Maximum amount";
                $title="Maximum amount";
                $response="Sorry the maximum amount to send is 2,000,000Frw";
                //return redirect()->route('balance')->with($title,$message);
//                return view('VotePayError')->with(['response'=>$response]);
                return response()->json([
                    'message' =>$response,
                ]);
            }
            else{
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$responseid->transactionid",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_POSTFIELDS => "",
                    CURLOPT_HTTPHEADER => array(
                        "Accept: */*",
                        "Cache-Control: no-cache",
                        "Connection: keep-alive",
                        "Host: akokanya.com",
                        "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                        "User-Agent: PostmanRuntime/7.11.0",
                        "accept-encoding: gzip, deflate",
                        "cache-control: no-cache",
                        "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                        "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
                    ),
                ));

                $responseapis = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if ($err) {
                    echo "cURL Error #:" . $err;
                } else {
                    //echo $response;

                    $responsedata=json_decode($responseapis);
//                        dd($responsedata);
                    //Save to database Momo payments
                    //dd( $amount);
                    $deletedat=0;
                    $Momopayment = MomoTransaction::create([
                        'phone'=> $phonenumber,
                        'voter_id'=> $last_id,
                        'transactionid'=> $responsedata[0]->external_payment_code,
                        'status'=> $responsedata[0]->payment_status,
                        'assignedid'=> $responsedata[0]->id,
                        'company_name'=> $responsedata[0]->company_name,
                        'code'=> $responsedata[0]->code,
                        'amount'=> $amount,
                        'artist_name'=> $song_artist,
                        'artist_song'=> $song_name,
                        'payment_code'=> $responsedata[0]->payment_code,
                        'external_payment_code'=> $responsedata[0]->external_payment_code,
                        'payment_status'=> $responsedata[0]->payment_status,
                        'payment_type'=> $responsedata[0]->payment_type,
                        'callback_url'=> $responsedata[0]->callback_url,
                        'momodeleted_at'=> $deletedat,
                        'momocreated_at'=> $responsedata[0]->created_at,
                        'momoupdated_at'=> $responsedata[0]->updated_at,
                    ]);
                    if($Momopayment){
//                        event(new Event($Momopayment));
//                        $link = "<a href=\"tel:*182*7#\" class=\"phone\" style=\"color: #22706c;font-size: 16px; font-weight: bold;\"><i class=\"fa fa-phone\" ></i> *182*7#</a>";
                        $response="Ntufunge iyi paji izenguruka (Loading). Emeza igikorwa cyo kwishyura wakiriye";
                        return response()->json([
                            'message' =>$response,
                            'transactionid'=>$responsedata[0]->external_payment_code,
                            'payment_status'=>$responsedata[0]->payment_status
                        ]);
//                        return view('Thankyou')->with(['song_name'=>$song_name,'song_artist'=>$song_artist]);
                    }
                    //end saving to database Momo payments
                }
            }
        }
    }


    public function TestTime(){
        $check_last_time = Votes::where('voterphonenumber',"0782384772")->latest('id')->first();
        $dt = Carbon::now();
        $date = new Carbon('2020-07-07 11:51:00' );
        dd($dt->diffInRealMinutes($date));
    }
    public function TestSpeen(){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://uat-idsrv.spenn.com/token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'grant_type=api_key&api_key=R1b4GGTZozgCiA4NS3N4uF6tL8z8zDSbHzFIOMe5w2tR1ygGNeXbYDfEpY3jHszxgORoQss4oO8%3D&client_id=SpennBusinessApiKey&client_secret=1234&audience=SpennBusiness',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded',
                'Cookie: ARRAffinity=0f00b54e827330349bfdd55ab47acf726d84f1d1f55044d12202f8cfbb151c8e; ARRAffinitySameSite=0f00b54e827330349bfdd55ab47acf726d84f1d1f55044d12202f8cfbb151c8e'
            ),
        ));
        $response = curl_exec($curl);
        $response_decode=json_decode($response);
        $token = $response_decode->access_token;
        $postData = ["amount" => "100", "phoneNumber" => "+25078305474‬", "message" => "Test from MNI","callbackUrl"=>""];

        if($token) {
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://uat-businessapi.spenn.com/api/Partner/transaction/request',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => json_encode($postData),
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    "Authorization:Bearer $token",
                    'Cookie: ARRAffinity=0f00b54e827330349bfdd55ab47acf726d84f1d1f55044d12202f8cfbb151c8e; ARRAffinitySameSite=0f00b54e827330349bfdd55ab47acf726d84f1d1f55044d12202f8cfbb151c8e'
                ),
            ));
            $response_v1 = curl_exec($curl);
            $json_response_api = json_decode($response_v1);
            if (!curl_errno($curl)) {
                switch ($http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE)) {
                    case 200:  # OK
                        dd($json_response_api);
                        break;
                    default:
                        echo 'Unexpected HTTP code: ', $http_code, "\n";
                }
            }
        }
    }
    public function VoteWithSpenn(Request $request){
        $id = $request['id'];
        $phonenumbers = $request['phonenumber'];
        $unlimitedamount = $request['amount'];
        $song_name = $request['song_name'];
        $song_artist = $request['song_artist'];
        $new_votess = $unlimitedamount / 25 ;

        $firstday = new Carbon('first day of last month');
        $lastday = new Carbon('last day of last month');
        $lastday_ = $lastday->subDays(7);
        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();

        $new_num = '+25'.$phonenumbers;
        $check_last_time = Votes::where('voterphonenumber',$phonenumbers)->where('voter_status',"SUCCESSFUL")->latest('id')->first();
        $dt = Carbon::now();
        $curl = curl_init();

        //Generating Payment Gateway

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://uat-idsrv.spenn.com/token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'grant_type=api_key&api_key=R1b4GGTZozgCiA4NS3N4uF6tL8z8zDSbHzFIOMe5w2tR1ygGNeXbYDfEpY3jHszxgORoQss4oO8%3D&client_id=SpennBusinessApiKey&client_secret=1234&audience=SpennBusiness',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded',
                'Cookie: ARRAffinity=0f00b54e827330349bfdd55ab47acf726d84f1d1f55044d12202f8cfbb151c8e; ARRAffinitySameSite=0f00b54e827330349bfdd55ab47acf726d84f1d1f55044d12202f8cfbb151c8e'
            ),
        ));
        $response = curl_exec($curl);
        $response_decode=json_decode($response);
        $token = $response_decode->access_token;
        $redirectUrl="https://mni.rw/api/UpdatePaymentSpenn";
        $postData = ["amount" => $unlimitedamount, "phoneNumber" => "$new_num", "message" => "Test from MNI","callbackUrl"=>"$redirectUrl"];

        if($token){
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://uat-businessapi.spenn.com/api/Partner/transaction/request',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => json_encode($postData),
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    "Authorization:Bearer $token",
                    'Cookie: ARRAffinity=0f00b54e827330349bfdd55ab47acf726d84f1d1f55044d12202f8cfbb151c8e; ARRAffinitySameSite=0f00b54e827330349bfdd55ab47acf726d84f1d1f55044d12202f8cfbb151c8e'
                ),
            ));
            $response_v1 = curl_exec($curl);
            $json_response_api = json_decode($response_v1);

            if (!curl_errno($curl)) {
                switch ($http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE)) {
                    case 200:  # OK
                        $vote = new Votes();
                        $vote->starting_date = $firstdaydate;
                        $vote->ending_date = $lastdaydate;
                        $vote->voterphonenumber = $phonenumbers;
                        $vote->vote = $new_votess;
                        $vote->voter_status = "PENDING";
                        $vote->voter_artist = $song_artist;
                        $vote->voter_artist_song = $song_name;
                        $vote->song_id = $id;
                        $vote->save();
                        $last_id = $vote->id;

                        $votepivot = new SongVotes();
                        $votepivot->song_id = $id;
                        $votepivot->vote_id = $last_id;
                        $votepivot->save();

                        $phonenumber = $phonenumbers;
//            $phonenumber = "0785093107";

                        $amount = $unlimitedamount;
                        $idt = mt_rand(10, 99);
                        $Momopayment = SpennTransactions::create([
                            'phonenumber'=> $phonenumber,
                            'amount'=> $unlimitedamount,
                            'voter_id'=> $last_id,
                            'transaction_id'=> $json_response_api->requestId,
                            'status'=> $json_response_api->status,
                            'vote'=> $amount,
                            'competition_id'=> $amount,
                            'contestant_id'=> $amount,
                        ]);
                        if($Momopayment){
                            $response="Ntufunge iyi paji izenguruka (Loading). Emeza igikorwa cyo kwishyura wakiriye.";
                            return response()->json([
                                'message' =>$response,
                                'transaction_id'=>$json_response_api->requestId,
                                'payment_status'=>$json_response_api->status,
                                'response_status'=>'success',
                            ]);
                        }
                        break;
                    default:
                        return response()->json([
                            'message' =>"Sorry ,You are not registered with Spenn",
                            'response_status'=>'failed'
                        ]);
                }
            }
        }
    }
    public function CallBackMomoSpenn(Request $request){
        $transactionid = $request['transactionid'];
//        $transactionid = "2367841156";
        $getVoter_id = SpennTransactions::where('transaction_id',$transactionid)->get();
        $getVoter_status = SpennTransactions::where('transaction_id',$transactionid)->value('status');

        if(count($getVoter_id) < 0){
            return response()->json([
                'message' =>"Sorry, Transaction does not exist",
                'status' =>false,
            ]);
        }else{
            return response()->json([
                'message' =>"Transaction is $getVoter_status",
                'status' =>false,
            ]);
        }
    }
    public function UpdatePaymentSpenn(Request $request){
        if($request->RequestStatus == "2"){
            $get_vote_id = SpennTransactions::where('transaction_id',$request->RequestGuid)->value('voter_id');
            $update_speen = SpennTransactions::where('transaction_id',$request->RequestGuid)->update(['status'=>'SUCCESSFUL']);
            $update_votes = Votes::where('id',$get_vote_id)->update(['voter_status'=>"SUCCESSFUL"]);
            return response()->json([
                'message' =>"Payment successfully updated",
                'response_status'=>'success'
            ]);
        }else{
            $update_speen = SpennTransactions::where('transaction_id',$request->RequestGuid)->update(['status'=>$request->RequestStatus]);
            return response()->json([
                'message' =>"Payment status not updated",
                'response_status'=>'failed'
            ]);
        }
    }
    public function VoteNowNumberUnlimited(Request $request){
        $id = $request['id'];
        $phonenumbers = $request['phonenumber'];
        $unlimitedamount = $request['amount'];
        $song_name = $request['song_name'];
        $song_artist = $request['song_artist'];
        $new_votess = $unlimitedamount / 50 ;

        $firstday = new Carbon('first day of last month');
        $lastday = new Carbon('last day of last month');
        $lastday_ = $lastday->subDays(7);
        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();

        $check_last_time = Votes::where('voterphonenumber',$phonenumbers)->where('voter_status',"SUCCESSFUL")->latest('id')->first();
        $dt = Carbon::now();

        if(0 == count($check_last_time)){
            $vote = new Votes();
            $vote->starting_date = $firstdaydate;
            $vote->ending_date = $lastdaydate;
            $vote->voterphonenumber = $phonenumbers;
            $vote->vote = $new_votess;
            $vote->voter_status = "PENDING";
            $vote->voter_artist = $song_artist;
            $vote->voter_artist_song = $song_name;
            $vote->song_id = $id;
            $vote->save();
            $last_id = $vote->id;

            $votepivot = new SongVotes();
            $votepivot->song_id = $id;
            $votepivot->vote_id = $last_id;
            $votepivot->save();

            $phonenumber = $phonenumbers;
//            $phonenumber = "0785093107";

            $amount = $unlimitedamount;
            $idt = mt_rand(10, 99);
            //Generating Payment Gateway
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://akokanya.com/mtn-pay?amount=$amount&phone=$phonenumber&company_name=MNI&payment_code=$idt",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "Accept: */*",
                    "Cache-Control: no-cache",
                    "Connection: keep-alive",
                    "Host: akokanya.com",
                    "Postman-Token: caddbf9d-3cf9-4fc3-b051-bc0ee25a2561,de8e362a-f9dd-4a20-a3bf-099dba0f6b26",
                    "User-Agent: PostmanRuntime/7.11.0",
                    "accept-encoding: gzip, deflate",
                    "cache-control: no-cache",
                    "content-length: "
                ),
            ));

            $responseapi = curl_exec($curl);
//            dd($response);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                //echo "cURL Error #:" . $err;
                $title="Not Connected";
                $message="Sorry you do not have internet connection";
                //return redirect()->route('balance')->with($title,$message);
//            echo "$err";
                return response()->json([
                    'message' =>$message,
                ]);

            } else {
                //echo $response;

                //Showing Status
                //second part Status checking

                $responseid=json_decode($responseapi);
//                        dd($responseid);

                $curl = curl_init();

                if($responseapi=='{"@attributes":{"errorcode":"TARGET_AUTHORIZATION_ERROR"}}'){
                    //checking balance
                    $maintitle="Balance";
                    $title="Not Enough Balance";
                    $response ="Sorry you do not have enough money on your account to make this transaction, please try again after topping up your account";
                    return response()->json([
                        'message' =>$response,
                    ]);
                }
                elseif($responseapi=='{"@attributes":{"errorcode":"ACCOUNTHOLDER_WITH_FRI_NOT_FOUND"},"arguments":{"@attributes":{"name":"fri","value":"FRI:25'.$phonenumber.'\/MSISDN"}}}'){
                    //User not registered in Momo
                    $maintitle="Not Registered";
                    $title="Not Registered";
                    $response="Sorry you're not registered with MTN Mobile Rwanda";

                    return response()->json([
                        'message' =>$response,
                    ]);
                }
                elseif($responseapi=='{"error":"the minimum amount is 100"}'){
                    //User not registered in Momo
                    $maintitle="Minimum amount";
                    $title="Minimum amount";
                    $response="Sorry the minimum amount to send is 100Frw";
                    return response()->json([
                        'message' =>$response,
                    ]);
                }
                elseif($responseapi=='{"@attributes":{"errorcode":"AUTHORIZATION_MAXIMUM_AMOUNT_ALLOWED_TO_SEND"}}'){
                    //User not registered in Momo
                    $maintitle="Maximum amount";
                    $title="Maximum amount";
                    $response="Sorry the maximum amount to send is 2,000,000Frw";
                    return response()->json([
                        'message' =>$response,
                    ]);
                }
                else{
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$responseid->transactionid",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "GET",
                        CURLOPT_POSTFIELDS => "",
                        CURLOPT_HTTPHEADER => array(
                            "Accept: */*",
                            "Cache-Control: no-cache",
                            "Connection: keep-alive",
                            "Host: akokanya.com",
                            "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                            "User-Agent: PostmanRuntime/7.11.0",
                            "accept-encoding: gzip, deflate",
                            "cache-control: no-cache",
                            "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                            "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
                        ),
                    ));

                    $responseapis = curl_exec($curl);
                    $err = curl_error($curl);

                    curl_close($curl);

                    if ($err) {
                        echo "cURL Error #:" . $err;
                    } else {
                        //echo $response;

                        $responsedata=json_decode($responseapis);
//                        dd($responsedata);
                        //Save to database Momo payments
                        //dd( $amount);
                        $deletedat=0;
                        $Momopayment = MomoTransaction::create([
                            'phone'=> $phonenumber,
                            'voter_id'=> $last_id,
                            'transactionid'=> $responsedata[0]->external_payment_code,
                            'status'=> $responsedata[0]->payment_status,
                            'assignedid'=> $responsedata[0]->id,
                            'company_name'=> $responsedata[0]->company_name,
                            'code'=> $responsedata[0]->code,
                            'amount'=> $amount,
                            'artist_name'=> $song_artist,
                            'artist_song'=> $song_name,
                            'payment_code'=> $responsedata[0]->payment_code,
                            'external_payment_code'=> $responsedata[0]->external_payment_code,
                            'payment_status'=> $responsedata[0]->payment_status,
                            'payment_type'=> $responsedata[0]->payment_type,
                            'callback_url'=> $responsedata[0]->callback_url,
                            'momodeleted_at'=> $deletedat,
                            'momocreated_at'=> $responsedata[0]->created_at,
                            'momoupdated_at'=> $responsedata[0]->updated_at,
                        ]);
                        if($Momopayment){
//                        event(new Event($Momopayment));
//                            $link = "<a href=\"tel:*182*7#\" class=\"phone\" style=\"color: #22706c;font-size: 16px; font-weight: bold;\"><i class=\"fa fa-phone\" ></i> *182*7#</a>";
                            $response="Ntufunge iyi paji izenguruka (Loading). Emeza igikorwa cyo kwishyura wakiriye";
                            return response()->json([
                                'message' =>$response,
                                'transactionid'=>$responsedata[0]->external_payment_code,
                                'payment_status'=>$responsedata[0]->payment_status
                            ]);
//                        return view('Thankyou')->with(['song_name'=>$song_name,'song_artist'=>$song_artist]);
                        }
                        //end saving to database Momo payments
                    }
                }
            }
        }else{
            $date =$check_last_time->created_at;
            if($dt->diffInRealMinutes($date) >= 30){

                $vote = new Votes();
                $vote->starting_date = $firstdaydate;
                $vote->ending_date = $lastdaydate;
                $vote->voterphonenumber = $phonenumbers;
                $vote->vote = $new_votess;
                $vote->voter_status = "PENDING";
                $vote->voter_artist = $song_artist;
                $vote->voter_artist_song = $song_name;
                $vote->song_id = $id;
                $vote->save();
                $last_id = $vote->id;

                $votepivot = new SongVotes();
                $votepivot->song_id = $id;
                $votepivot->vote_id = $last_id;
                $votepivot->save();

            $phonenumber = $phonenumbers;
//                $phonenumber = "0785093107";

                $amount = $unlimitedamount;
                $idt = mt_rand(10, 99);
                //Generating Payment Gateway
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://akokanya.com/mtn-pay?amount=$amount&phone=$phonenumber&company_name=MNI&payment_code=$idt",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => "",
                    CURLOPT_HTTPHEADER => array(
                        "Accept: */*",
                        "Cache-Control: no-cache",
                        "Connection: keep-alive",
                        "Host: akokanya.com",
                        "Postman-Token: caddbf9d-3cf9-4fc3-b051-bc0ee25a2561,de8e362a-f9dd-4a20-a3bf-099dba0f6b26",
                        "User-Agent: PostmanRuntime/7.11.0",
                        "accept-encoding: gzip, deflate",
                        "cache-control: no-cache",
                        "content-length: "
                    ),
                ));

                $responseapi = curl_exec($curl);
//            dd($response);
                $err = curl_error($curl);

                curl_close($curl);

                if ($err) {
                    //echo "cURL Error #:" . $err;
                    $title="Not Connected";
                    $message="Sorry you do not have internet connection";
                    //return redirect()->route('balance')->with($title,$message);
//            echo "$err";
                    return response()->json([
                        'message' =>$message,
                    ]);

                } else {
                    //echo $response;

                    //Showing Status
                    //second part Status checking

                    $responseid=json_decode($responseapi);
//                        dd($responseid);

                    $curl = curl_init();

                    if($responseapi=='{"@attributes":{"errorcode":"TARGET_AUTHORIZATION_ERROR"}}'){
                        //checking balance
                        $maintitle="Balance";
                        $title="Not Enough Balance";
                        $response ="Sorry you do not have enough money on your account to make this transaction, please try again after topping up your account";
                        return response()->json([
                            'message' =>$response,
                        ]);
                    }
                    elseif($responseapi=='{"@attributes":{"errorcode":"ACCOUNTHOLDER_WITH_FRI_NOT_FOUND"},"arguments":{"@attributes":{"name":"fri","value":"FRI:25'.$phonenumber.'\/MSISDN"}}}'){
                        //User not registered in Momo
                        $maintitle="Not Registered";
                        $title="Not Registered";
                        $response="Sorry you're not registered with MTN Mobile Rwanda";

                        return response()->json([
                            'message' =>$response,
                        ]);
                    }
                    elseif($responseapi=='{"error":"the minimum amount is 100"}'){
                        //User not registered in Momo
                        $maintitle="Minimum amount";
                        $title="Minimum amount";
                        $response="Sorry the minimum amount to send is 100Frw";
                        return response()->json([
                            'message' =>$response,
                        ]);
                    }
                    elseif($responseapi=='{"@attributes":{"errorcode":"AUTHORIZATION_MAXIMUM_AMOUNT_ALLOWED_TO_SEND"}}'){
                        //User not registered in Momo
                        $maintitle="Maximum amount";
                        $title="Maximum amount";
                        $response="Sorry the maximum amount to send is 2,000,000Frw";
                        return response()->json([
                            'message' =>$response,
                        ]);
                    }
                    else{
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$responseid->transactionid",
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "GET",
                            CURLOPT_POSTFIELDS => "",
                            CURLOPT_HTTPHEADER => array(
                                "Accept: */*",
                                "Cache-Control: no-cache",
                                "Connection: keep-alive",
                                "Host: akokanya.com",
                                "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                                "User-Agent: PostmanRuntime/7.11.0",
                                "accept-encoding: gzip, deflate",
                                "cache-control: no-cache",
                                "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                                "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
                            ),
                        ));

                        $responseapis = curl_exec($curl);
                        $err = curl_error($curl);

                        curl_close($curl);

                        if ($err) {
                            echo "cURL Error #:" . $err;
                        } else {
                            //echo $response;

                            $responsedata=json_decode($responseapis);
//                        dd($responsedata);
                            //Save to database Momo payments
                            //dd( $amount);
                            $deletedat=0;
                            $Momopayment = MomoTransaction::create([
                                'phone'=> $phonenumber,
                                'voter_id'=> $last_id,
                                'transactionid'=> $responsedata[0]->external_payment_code,
                                'status'=> $responsedata[0]->payment_status,
                                'assignedid'=> $responsedata[0]->id,
                                'company_name'=> $responsedata[0]->company_name,
                                'code'=> $responsedata[0]->code,
                                'amount'=> $amount,
                                'artist_name'=> $song_artist,
                                'artist_song'=> $song_name,
                                'payment_code'=> $responsedata[0]->payment_code,
                                'external_payment_code'=> $responsedata[0]->external_payment_code,
                                'payment_status'=> $responsedata[0]->payment_status,
                                'payment_type'=> $responsedata[0]->payment_type,
                                'callback_url'=> $responsedata[0]->callback_url,
                                'momodeleted_at'=> $deletedat,
                                'momocreated_at'=> $responsedata[0]->created_at,
                                'momoupdated_at'=> $responsedata[0]->updated_at,
                            ]);
                            if($Momopayment){
//                        event(new Event($Momopayment));
//                                $link = "<a href=\"tel:*182*7#\" class=\"phone\" style=\"color: #22706c;font-size: 16px; font-weight: bold;\"><i class=\"fa fa-phone\" ></i> *182*7#</a>";
                                $response="Ntufunge iyi paji izenguruka (Loading). Emeza igikorwa cyo kwishyura wakiriye.";
                                return response()->json([
                                    'message' =>$response,
                                    'transactionid'=>$responsedata[0]->external_payment_code,
                                    'payment_status'=>$responsedata[0]->payment_status
                                ]);
//                        return view('Thankyou')->with(['song_name'=>$song_name,'song_artist'=>$song_artist]);
                            }
                            //end saving to database Momo payments
                        }
                    }
                }

            }else{
                $response ="We are sorry , you can not vote twice in less than 30 minutes";
                //return redirect()->route('balance')->with($title,$message);
                return response()->json([
                    'message' =>$response,
                    'response_status'=> false
                ]);
            }
        }
    }
    public function VoteNowCard(Request $request){
        $currentMonth = Carbon::now()->format('m');
        $code = mt_rand(10, 999999999). $currentMonth;
        $id = $request['id'];

        $firstday = new Carbon('first day of last month');
        $lastday = new Carbon('last day of last month');
        $lastday_ = $lastday->subDays(7);
        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();

        $fname= $request['first_name'];
        $lname= $request['last_name'];
        $phone_number= $request['phonenumber_card'];
        $customer_email= $request['email'];
//        $zipcode=Auth::guard('client')->user()->postcode;
        $city= $request['city'];
        $country= $request['country'];
        $total = $request['amount_card'];

        $hash = hash('sha256', ($phone_number.''.$code));
        $unit_price = $request['amount_card'];
        $pay_amount = $request['amount_card'];
//        dd($pay_amount);
//        $gateway = $request['gateway'];
        $currency="RWF";
        $fullname=$fname.' '.$lname;
//        $redirectUrl="http://127.0.0.1:9092/PaymentRedirect";
        $redirectUrl="https://mni.rw/PaymentRedirect";

        $song_name = $request['song_artist_card'];
        $song_artist = $request['song_name_card'];
        $new_votess = $total / 50;

        $vote = new Votes();
        $vote->starting_date = $firstdaydate;
        $vote->ending_date = $lastdaydate;
        $vote->voterphonenumber = $phone_number;
        $vote->vote = $new_votess;
        $vote->voter_status = "PENDING";
        $vote->voter_artist = $song_artist;
        $vote->voter_artist_song = $song_name;
        $vote->song_id = $id;
        $vote->save();
        $last_id = $vote->id;

        $votepivot = new SongVotes();
        $votepivot->song_id = $id;
        $votepivot->vote_id = $last_id;
        $votepivot->save();

        $Cardpayment = VotingCardTransactions::create([
            'first_name'=> $fname,
            'last_name'=> $lname,
            'phonenumber'=> $phone_number,
            'email'=> $customer_email,
            'city'=> $city,
            'country'=> $country,
            'amount'=> $pay_amount,
            'tx_ref'=> $hash,
            'song_name'=> $song_name,
            'song_artist'=> $song_artist,
            'currency'=> $currency,
            'status'=> "Pending",
            'payment_options'=> "Card",
            'voter_id'=> $last_id,
        ]);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.flutterwave.com/v3/payments",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>"{\r\n   \"tx_ref\":\"$hash\",\r\n   \"amount\":\"$pay_amount \",\r\n   \"currency\":\"$currency\",\r\n   \"redirect_url\":\"$redirectUrl\",\r\n   \"payment_options\":\"card\",\r\n   \"meta\":{\r\n },\r\n   \"customer\":{\r\n      \"email\":\"$customer_email\",\r\n      \"phonenumber\":\"$phone_number\",\r\n      \"name\":\"$fullname\"\r\n   },\r\n   \"customizations\":{\r\n      \"title\":\"MNI -  Gutora indirimbo $song_name ya $song_artist\",\r\n      \"description\":\" MNI -  Gutora indirimbo $song_name ya $song_artist \",\r\n      \"logo\":\"https://mni.rw/front/images/logo/logo_icon.png\"\r\n   }\r\n}",
            CURLOPT_HTTPHEADER => array(
                ": ",
                "Authorization: Bearer FLWSECK-7f33477468053f39715ec7192f950b71-X",
//                "Authorization: Bearer FLWSECK_TEST-3f0b1552abcc7bb5351f23ff928769bb-X",
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $flutter_resp = json_decode($response, true);
        if($flutter_resp['status']=='success'){
            $redirect_link=$flutter_resp['data']['link'];
            return redirect($redirect_link);
        }
        else{
            return response()->json([
                'message' =>$response,
                'response_status'=> false
            ]);
//            return redirect('user/sms/buy-unit')->with([
//                'message' => "Something went wrong please contact support to assist",
//                'message_important'=>true ]);
        }
    }
    public function PaymentRedirect(Request $request){
        $all = $request->all();
        if($request['status'] == "successful"){
            $getVoter_id = VotingCardTransactions::where('tx_ref',$request['tx_ref'])->value('voter_id');
            $getsong_id= Votes::where('id',$getVoter_id)->value('song_id');
            $updateverstatus = Votes::where('id',$getVoter_id)->update(['voter_status'=> 'SUCCESSFUL']);
            $update_transaction = VotingCardTransactions::where('tx_ref',$request['tx_ref'])->update(['status'=> $request['status']]);
            $redirect_link= 'ThankYou?id='.$getsong_id;
            return redirect($redirect_link);
        }else{

        }
    }
    public function CompetitionVoteNowCard(Request $request){
        $currentMonth = Carbon::now()->format('m');
        $code = mt_rand(10, 999999999). $currentMonth;
        $id = $request['id'];

        $firstday = new Carbon('first day of last month');
        $lastday = new Carbon('last day of last month');
        $lastday_ = $lastday->subDays(7);
        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();

        $fname= $request['first_name'];
        $lname= $request['last_name'];
        $names= $request['names'];
        $phone_number= $request['phonenumber_card'];
        $customer_email= $request['email'];
//        $zipcode=Auth::guard('client')->user()->postcode;
        $city= $request['city'];
        $country= $request['country'];
        $total = $request['amount_card'];

        $hash = hash('sha256', ($phone_number.''.$code));
        $unit_price = $request['amount_card'];
        $pay_amount = $request['amount_card'];
//        dd($pay_amount);
//        $gateway = $request['gateway'];
        $currency="RWF";
        $fullname=$fname.' '.$lname;
//        $redirectUrl="http://127.0.0.1:9094/CompetitionPaymentCallback";
        $redirectUrl="https://mni.rw/CompetitionPaymentCallback";

        $song_name = $request['song_artist_card'];
        $song_artist = $request['song_name_card'];
        $new_votess = $total / 50;
        $contestant_id = $request['contestant_id'];


        $CardPayment = CompetitionCardTransactions::create([
            'vote'=> $new_votess,
            'competition_id'=> $request['competition_id'],
            'contestant_id'=> $request['contestant_id'],
            'first_name'=> $fname,
            'last_name'=> $lname,
            'phonenumber'=> $phone_number,
            'email'=> $customer_email,
            'city'=> $city,
            'country'=> $country,
            'amount'=> $pay_amount,
            'tx_ref'=> $hash,
            'song_name'=> $song_name,
            'song_artist'=> $song_artist,
            'currency'=> $currency,
            'status'=> "Pending",
            'voter_id'=> null,
            'payment_options'=> "Card",
        ]);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.flutterwave.com/v3/payments",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>"{\r\n   \"tx_ref\":\"$hash\",\r\n   \"amount\":\"$pay_amount \",\r\n   \"currency\":\"$currency\",\r\n   \"redirect_url\":\"$redirectUrl\",\r\n   \"payment_options\":\"card\",\r\n   \"meta\":{\r\n \"contestant_id\":\"$contestant_id\"\r\n },\r\n   \"customer\":{\r\n      \"email\":\"$customer_email\",\r\n      \"phonenumber\":\"$phone_number\",\r\n      \"name\":\"$fullname\"\r\n   },\r\n   \"customizations\":{\r\n      \"title\":\"MNI -  Gutora $names \",\r\n      \"description\":\" MNI -  Gutora $names \",\r\n      \"logo\":\"https://mni.rw/front/images/logo/logo_icon.png\"\r\n   }\r\n}",
            CURLOPT_HTTPHEADER => array(
                ": ",
//                "Authorization: Bearer FLWSECK_TEST-3f0b1552abcc7bb5351f23ff928769bb-X",
                "Authorization: Bearer FLWSECK-7f33477468053f39715ec7192f950b71-X",
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $flutter_resp = json_decode($response, true);
        if($flutter_resp['status']=='success'){
            $redirect_link=$flutter_resp['data']['link'];
            return redirect($redirect_link);
        }
        else{
            return response()->json([
                'message' =>$response,
                'response_status'=> false
            ]);
//            return redirect('user/sms/buy-unit')->with([
//                'message' => "Something went wrong please contact support to assist",
//                'message_important'=>true ]);
        }
    }
    public function CompetitionPaymentCallback(Request $request){
        $all = $request->all();
        $checkpayment_ = CompVotes::where('payment_tx_ref',$request['transaction_id'])->get();
        $getid = CompetitionCardTransactions::where('tx_ref',$request['tx_ref'])->value('contestant_id');
        $contestant = Contestants::where('id',$getid)->value('names');
        $get_content = CompetitionCardTransactions::where('tx_ref',$request['tx_ref'])->get();

        if($request['status'] == "successful" and $checkpayment_->isEmpty()){
            foreach ($get_content as $voter){
                $contestant_id_ = $voter->contestant_id;
                $competition_id_ = $voter->competition_id;
                $amount = $voter->amount;
                $tx_ref = $voter->transaction_id;
            }
            $new_votess = $amount / 50;
            $addvote = new CompVotes();
            $addvote->vote = $new_votess;
            $addvote->names = $contestant;
            $addvote->competition_id = $competition_id_;
            $addvote->contestant_id = $contestant_id_;
            $addvote->payment_from = "CARD";
            $addvote->payment_tx_ref = $request['transaction_id'];
            $addvote->status = "SUCCESSFUL";
            $addvote->save();
            $last_id = $addvote->id;

            $update_transaction = CompetitionCardTransactions::where('tx_ref',$request['tx_ref'])->update(['status'=> $request['status'],'voter_id'=>$last_id,'transaction_id'=>$request['transaction_id']]);
            $redirect_link= 'CompetitionThankYou?id='.$getid;
            return redirect($redirect_link);
        }else{
            $redirect_link= 'CompetitionThankYou?id='.$getid;
            return redirect($redirect_link);
        }
    }
    public function ValidateCardPayment(Request $request){
        $email = $request['email'];
        $get_content = CompetitionCardTransactions::where('email',$request['email'])->get();
        if(0 == count($get_content)){
            return response()->json([
                'response_message' => "transaction does not exist",
                'response_status' =>false
            ]);
        }else{
            foreach ($get_content as $voter){
                $contestant_id_ = $voter->contestant_id;
                $competition_id_ = $voter->competition_id;
                $amount = $voter->amount;
                $status = $voter->status;
                $tx_ref = $voter->transaction_id;
            }
            $contestant = Contestants::where('id',$contestant_id_)->value('names');
            $new_votess = $amount / 50;
            $addvote = new CompVotes();
            $addvote->vote = $new_votess;
            $addvote->names = $contestant;
            $addvote->competition_id = $competition_id_;
            $addvote->contestant_id = $contestant_id_;
            $addvote->payment_from = "CARD";
            $addvote->payment_tx_ref = $tx_ref;
            $addvote->status = "SUCCESSFUL";
            $addvote->save();
            $last_id = $addvote->id;
            $update_transaction = CompetitionCardTransactions::where('email',$request['email'])
                ->update(['status'=>"successful",'voter_id'=>$last_id]);
            return response()->json([
                'response_message' => "successfully updated vote",
                'response_status' =>true
            ]);
        }

    }
    public function VerifyCardPayment(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.flutterwave.com/v3/transactions/76b38a6b2819b0c51bed6e812c8f4e504dfa63fd8f674ab0ba59dbb609a9604c/verify",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Authorization: Bearer FLWSECK-7f33477468053f39715ec7192f950b71-X"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }
    public function TestNUmber(){

        $phonenumber = '0782384772';
//        $phonenumber = '0785093107';
        $amount = 100;
        $idt = mt_rand(10, 99);
        //Generating Payment Gateway
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://akokanya.com/mtn-pay?amount=$amount&phone=$phonenumber&company_name=MNI&payment_code=$idt",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Host: akokanya.com",
                "Postman-Token: caddbf9d-3cf9-4fc3-b051-bc0ee25a2561,de8e362a-f9dd-4a20-a3bf-099dba0f6b26",
                "User-Agent: PostmanRuntime/7.11.0",
                "accept-encoding: gzip, deflate",
                "cache-control: no-cache",
                "content-length: "
            ),
        ));

        $responseapi = curl_exec($curl);
//            dd($response);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            //echo "cURL Error #:" . $err;
            $title="Not Connected";
            $message="Sorry you do not have no internet connection";
            //return redirect()->route('balance')->with($title,$message);
//            echo "$err";
            dd($message);

        } else {
            //echo $response;

            //Showing Status
            //second part Status checking

            $responseid=json_decode($responseapi);
//                        dd($responseid);

            $curl = curl_init();

            if($responseapi=='{"@attributes":{"errorcode":"TARGET_AUTHORIZATION_ERROR"}}'){
                //checking balance
                $maintitle="Balance";
                $title="Not Enough Balance";
                $response ="Sorry you do not have enough money on your account to make this transaction, please try again after topping up your account";
                //return redirect()->route('balance')->with($title,$message);
                dd($response);
//                return view('VotePayError')->with(['response'=>$response]);
            }
            elseif($responseapi=='{"@attributes":{"errorcode":"ACCOUNTHOLDER_WITH_FRI_NOT_FOUND"},"arguments":{"@attributes":{"name":"fri","value":"FRI:25'.$phonenumber.'\/MSISDN"}}}'){
                //User not registered in Momo
                $maintitle="Not Registered";
                $title="Not Registered";
                $response="Sorry you're not registered with MTN Mobile Rwanda";
//                return view('VotePayError')->with(['response'=>$response]);
                dd($response);
            }
            elseif($responseapi=='{"error":"the minimum amount is 100"}'){
                //User not registered in Momo
                $maintitle="Minimum amount";
                $title="Minimum amount";
                $response="Sorry the minimum amount to send is 100Frw";
//                return view('VotePayError')->with(['response'=>$response]);
                dd($response);
            }
            elseif($responseapi=='{"@attributes":{"errorcode":"AUTHORIZATION_MAXIMUM_AMOUNT_ALLOWED_TO_SEND"}}'){
                //User not registered in Momo
                $maintitle="Maximum amount";
                $title="Maximum amount";
                $response="Sorry the maximum amount to send is 2,000,000Frw";
                //return redirect()->route('balance')->with($title,$message);
//                return view('VotePayError')->with(['response'=>$response]);
                dd($response);
            }
            else{
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$responseid->transactionid",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_POSTFIELDS => "",
                    CURLOPT_HTTPHEADER => array(
                        "Accept: */*",
                        "Cache-Control: no-cache",
                        "Connection: keep-alive",
                        "Host: akokanya.com",
                        "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                        "User-Agent: PostmanRuntime/7.11.0",
                        "accept-encoding: gzip, deflate",
                        "cache-control: no-cache",
                        "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                        "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
                    ),
                ));

                $responseapis = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if ($err) {
                    echo "cURL Error #:" . $err;
                } else {
                    //echo $response;

                    $responsedata=json_decode($responseapis);
                        dd($responsedata);
                    //Save to database Momo payments
                    //dd( $amount);
                    $deletedat=0;
//                    $Momopayment = MomoTransaction::create([
//                        'phone'=> $phonenumber,
//                        'voter_id'=> $last_id,
//                        'transactionid'=> $responsedata[0]->external_payment_code,
//                        'status'=> $responsedata[0]->payment_status,
//                        'assignedid'=> $responsedata[0]->id,
//                        'company_name'=> $responsedata[0]->company_name,
//                        'code'=> $responsedata[0]->code,
//                        'amount'=> $amount,
//                        'artist_name'=> $song_artist,
//                        'artist_song'=> $song_name,
//                        'payment_code'=> $responsedata[0]->payment_code,
//                        'external_payment_code'=> $responsedata[0]->external_payment_code,
//                        'payment_status'=> $responsedata[0]->payment_status,
//                        'payment_type'=> $responsedata[0]->payment_type,
//                        'callback_url'=> $responsedata[0]->callback_url,
//                        'momodeleted_at'=> $deletedat,
//                        'momocreated_at'=> $responsedata[0]->created_at,
//                        'momoupdated_at'=> $responsedata[0]->updated_at,
//                    ]);

                    //end saving to database Momo payments
                }
            }
        }
    }

    public function CallBackMomo(Request $request){
        $transactionid = $request['transactionid'];
//        $transactionid = "2367841156";
        $getVoter_id = MomoTransaction::where('transactionid',$transactionid)->value('voter_id');

        if(0 == count($getVoter_id)){
            return response()->json([
                'message' =>"Sorry, Transaction does not exist",
                'status' =>false,
            ]);
        }else{
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$transactionid",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "Accept: */*",
                    "Cache-Control: no-cache",
                    "Connection: keep-alive",
                    "Host: akokanya.com",
                    "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                    "User-Agent: PostmanRuntime/7.11.0",
                    "accept-encoding: gzip, deflate",
                    "cache-control: no-cache",
                    "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                    "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                //echo $response;

                $responsedata=json_decode($response);

                $deletedat=0;

                $Momopayment = MomoTransaction::where('transactionid',$responsedata[0]->external_payment_code)
                    ->update([
                        'transactionid'=> $responsedata[0]->external_payment_code,
                        'status'=> $responsedata[0]->payment_status,
                        'assignedid'=> $responsedata[0]->id,
                        'company_name'=> $responsedata[0]->company_name,
                        'code'=> $responsedata[0]->code,
                        // 'amount'=> $responsedata[0]->amount,
                        'payment_code'=> $responsedata[0]->payment_code,
                        'external_payment_code'=> $responsedata[0]->external_payment_code,
                        'payment_status'=> $responsedata[0]->payment_status,
                        'payment_type'=> $responsedata[0]->payment_type,
                        'callback_url'=> $responsedata[0]->callback_url,
                        'momodeleted_at'=> $deletedat,
                        'momocreated_at'=> $responsedata[0]->created_at,
                        'momoupdated_at'=> $responsedata[0]->updated_at,

                    ]);
                if($responsedata[0]->payment_status == "SUCCESSFUL"){
                    $updateverstatus = Votes::where('id',$getVoter_id)->update(['voter_status'=> $responsedata[0]->payment_status]);
                    return response()->json([
                        'message' =>"Transaction successfully processed",
                        'status' =>"SUCCESSFUL",
                    ]);
                }else{
                    $status = $responsedata[0]->payment_status;
                    return response()->json([
                        'message' =>"Transaction is $status ",
                        'status' =>"PENDING",
                    ]);
                }

            }
        }

    }

    public function SearchVotingG(Request $request){
//
        $firstday = new Carbon('first day of this month');
        $lastday = new Carbon('last day of this month');
        $lastday_ = $lastday->subDays(7);

        $testdate = Carbon::parse($firstday);
        $testdateL = Carbon::parse($lastday_);
        $newfirst = $testdate->format('jS F Y');
        $newlast = $testdateL->format('jS F Y');

        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();
//        $name = "idsvvds";
        $name = $request['searchname'];

        $getplaylistgospel = PlaylistInfo::where('starting_date', $firstdaydate)->where('ending_date', $lastdaydate)->where('playlist_cate','1')->value('id');
        $searchG = SongInfo::where('playlist_id', $getplaylistgospel)->where('song_artist', 'like', '%' . $name . '%')->get();
//        dd($searchG);

        if (0 == count($searchG)) {
            echo "<div class=\"director-speech clearfix\" style=\"padding-top: 30px\"> <img src=\"front/images/logo/logojpeg.jpeg\" class=\"d-img\"> <div class=\"bio-block\"> <h6 class=\"name\" style=\"padding: 15px 0;\">Oops no results found</h6> </div></div>";
        }else{
//            echo "not empty";
            foreach ($searchG as $getmusics) {
                $votes = Votes::select(DB::raw('count(id) as votes'))->where('voter_artist_song',$getmusics->song_name)
                    ->where('voter_artist',$getmusics->song_artist)
                    ->where('vote','1')
                    ->value('votes');
                if (0 == count($votes)){
                    $votesword = "Amanota: 0";
                }else{
                    $votesword = "Amanota: $votes";
                }
                $voteroute = route('VoteNow',['id'=> $getmusics->id]);
                echo "<div class=\"director-speech clearfix\" style=\"padding-top: 30px\"> <img src=\"SongImages/$getmusics->song_cover_picture\" class=\"d-img\"> <div class=\"bio-block\"> <h6 class=\"name\">$getmusics->song_artist</h6> <span>$getmusics->song_name</span> <br><span><a href='$getmusics->song_youtube_link' target='_blank'><i class='fa fa-youtube' aria-hidden='true' style='font-size: 30px;color:#22706c;'></i></a></span></div><h6 class=\"sign\"> <a class='votebutton' href='$voteroute'>Tora</a><br> </h6> <div id=\"$getmusics->id-m\" class=\"modal\" data-backdrop=\"true\"> <div class=\"modal-dialog\"> <div class=\"modal-content\"> <div class=\"modal-header\"> <h5 class=\"modal-title\">$getmusics->song_name Music Video</h5> </div><div class=\"modal-body text-center p-lg\"> $getmusics->song_youtube_link</div><div class=\"modal-footer\"> <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Close</button> </div></div></div></div></div>";

            }
        }
    }

    public function SearchVotingM(Request $request){
//
        $firstday = new Carbon('first day of this month');
        $dt = Carbon::now();
        $currentdate = $firstday->subMonth();
        $testindate = $dt->diffForHumans($firstday);

        $lastday = new Carbon('last day of this month');
        $endofmonth = new Carbon('last day of this month');
        $lastday_ = $lastday->subDays(7);

        $testdate = Carbon::parse($firstday);
        $testdateL = Carbon::parse($lastday_);
        $newfirst = $testdate->format('jS F Y');
        $newlast = $testdateL->format('jS F Y');

        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();
        $endmontdate = $endofmonth->toDateString();
//        $name = "idsvvds";
        $name = $request['modernSearch'];

        $getplaylistmodern = PlaylistInfo::where('starting_date', $firstdaydate)->where('ending_date', $lastdaydate)->where('playlist_cate','4')->value('id');
//        $searchG= SongInfo::where('song_artist', 'like', '%' . $name . '%')->get();

$getmusicmodern = SongInfo::where('song_name', 'like', '%' . $name . '%')->where('voter_status','SUCCESSFUL')->leftJoin('votes', 'songinfo.id', '=', 'votes.song_id')
            ->select('songinfo.id','songinfo.created_at','songinfo.song_artist','songinfo.song_name','songinfo.song_youtube_link',
                'songinfo.song_cover_picture','songinfo.playlist_code','songinfo.starting_date',
                'songinfo.ending_date', DB::raw("count(votes.id) as votesNumber"))
//            ->whereBetween(DB::raw('votes.created_at'), [DB::raw($firstdaydate), DB::raw($endmontdate)])
            ->groupBy('votes.song_id')
//            ->orderBy('votes.song_id','DESC')
            ->orderBy('votesNumber','DESC')
            ->limit(30)
            ->get();

        if (0 == count($getmusicmodern)) {
            echo "<div class=\"director-speech clearfix\" style=\"padding-top: 30px\"> <img src=\"front/images/logo/logojpeg.jpeg\" class=\"d-img\"> <div class=\"bio-block\"> <h6 class=\"name\" style=\"padding: 15px 0;\">Oops no results found</h6> </div></div>";
        }else{
            foreach ($getmusicmodern as $index => $getmusicsmodern) {
                $votesbe=$getmusicsmodern->votesNumber;
                $newvotes=$votesbe - 1;
                $created_at=$getmusicsmodern->created_at;
                $lastTimeLoggedOut=\Illuminate\Support\Carbon::parse($created_at)->diffForHumans();
                $voteroute = route('NewVoteNow',['id'=> $getmusicsmodern->id]);
                $song_id=$getmusicsmodern->id;
                $votes=Votes::where('voter_status','SUCCESSFUL')->select(DB::raw('count(id) as votes')) ->where('song_id',$song_id)
                    ->where('vote','1')
                    ->value('votes');
                $newvotestotal=$votes - 1;
                $counts = $index+1;
                echo "<div class='row' style='border: 1px solid #1c706f;margin-bottom: 5px;'><div class='col-md-12 songinfo'> <div class='director-speech clearfix'> <img src='SongImages/$getmusicsmodern->song_cover_picture' alt='' class='d-img'> <div class='bio-block'> <h6 class='name' >$getmusicsmodern->song_name</h6> <span style='color: #848484 !important;'>$getmusicsmodern->song_artist</span><br><span><a href='$getmusicsmodern->song_youtube_link' target='_blank'><i class='fa fa-youtube' aria-hidden='true' style='font-size: 30px;color:#1c706d;'></i></a></span> <h6 class='name'> <span> Amajwi y'ukwezi: $newvotes </span><br><span style='font-size: 12px; !important;'>$lastTimeLoggedOut</span> </h6> </div><div class='bio-block' id='votesmobile' style='position: absolute; right: 20%; float: none; top: 14%;'> </div><h6 class='sign'> <a class='votebutton' href='$voteroute'>Tora</a><br><span style='top: 5px;position: relative;'> Total: $newvotestotal </span> </h6></div></div></div>";
//                echo "<div class=\"director-speech clearfix\" style=\"padding-top: 30px\"> <img src=\"SongImages/$getmusics->song_cover_picture\" class=\"d-img\"> <div class=\"bio-block\"> <h6 class=\"name\">$getmusics->song_artist</h6> <span>$getmusics->song_name</span><br><span><a href='$getmusics->song_youtube_link' target='_blank'><i class='fa fa-youtube' aria-hidden='true' style='font-size: 30px;color:#22706c;'></i></a></span></div><h6 class=\"sign\"> <a class='votebutton' href='$voteroute'>Tora</a><br> </h6> <div id=\"$getmusics->id-m\" class=\"modal\" data-backdrop=\"true\"> <div class=\"modal-dialog\"> <div class=\"modal-content\"> <div class=\"modal-header\"> <h5 class=\"modal-title\">$getmusics->song_name Music Video</h5> </div><div class=\"modal-body text-center p-lg\"> $getmusics->song_youtube_link</div><div class=\"modal-footer\"> <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Close</button> </div></div></div></div></div>";
            }
        }
    }

    public function SearchVotingT(Request $request){
//
        $firstday = new Carbon('first day of this month');
        $lastday = new Carbon('last day of this month');
        $lastday_ = $lastday->subDays(7);

        $testdate = Carbon::parse($firstday);
        $testdateL = Carbon::parse($lastday_);
        $newfirst = $testdate->format('jS F Y');
        $newlast = $testdateL->format('jS F Y');

        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();
//        $name = "idsvvds";
        $name = $request['traditionalSearch'];

        $getplaylisttraditional = PlaylistInfo::where('starting_date', $firstdaydate)->where('ending_date', $lastdaydate)->where('playlist_cate','3')->value('id');
        $searchG= SongInfo::where('playlist_id', $getplaylisttraditional)->where('song_artist', 'like', '%' . $name . '%')->get();

//        dd($searchG);
        if (0 == count($searchG)) {
            echo "<div class=\"director-speech clearfix\" style=\"padding-top: 30px\"> <img src=\"front/images/logo/logojpeg.jpeg\" class=\"d-img\"> <div class=\"bio-block\"> <h6 class=\"name\" style=\"padding: 15px 0;\">Oops no results found</h6> </div></div>";
        }else{
//            echo "not empty";
            foreach ($searchG as $getmusics) {
                $votes = Votes::select(DB::raw('count(id) as votes'))->where('voter_artist_song',$getmusics->song_name)
                    ->where('voter_artist',$getmusics->song_artist)
                    ->where('vote','1')
                    ->value('votes');
                if (0 == count($votes)){
                    $votesword = "Amanota: 0";
                }else{
                    $votesword = "Amanota: $votes";
                }
                $voteroute = route('VoteNow',['id'=> $getmusics->id]);
                echo "<div class=\"director-speech clearfix\" style=\"padding-top: 30px\"> <img src=\"SongImages/$getmusics->song_cover_picture\" class=\"d-img\"> <div class=\"bio-block\"> <h6 class=\"name\">$getmusics->song_artist</h6> <span>$getmusics->song_name</span> <br><span><a href='$getmusics->song_youtube_link' target='_blank'><i class='fa fa-youtube' aria-hidden='true' style='font-size: 30px;color:#22706c;'></i></a></span></div><h6 class=\"sign\"> <a class='votebutton' href='$voteroute'>Tora</a><br> </h6> <div id=\"$getmusics->id-m\" class=\"modal\" data-backdrop=\"true\"> <div class=\"modal-dialog\"> <div class=\"modal-content\"> <div class=\"modal-header\"> <h5 class=\"modal-title\">$getmusics->song_name Music Video</h5> </div><div class=\"modal-body text-center p-lg\"> $getmusics->song_youtube_link</div><div class=\"modal-footer\"> <button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Close</button> </div></div></div></div></div>";
            }
        }
    }

    public function AboutUs(){
        return view('V2.AboutUs');
    }
    public function Umuhanzi(){
        return view('Umuhanzi');
    }
    public function Project(){
        return view('Project');
    }
    public function PrivacyPolicy(){
        return view('V2.PrivacyPolicy');
    }
    public function Faq(){
        return view('V2.Faq');
    }
    public function TermsAndConditions(){
        return view('V2.TermsAndConditions');
    }
    public function TermsAndConditionsStakeHolders(){
        return view('V2.TermsAndConditionsStakeHolders');
    }
    public function SalesAndMarketing(){
        return view('V2.SalesAndMarketing');
    }
    public function AbahanziTermsandConditions(){
        return view ('AbahanziTermsandConditions');
    }
    public function Nominee(){
        $firstday = new Carbon('first day of this month');
        $lastday = new Carbon('last day of this month');
        $lastday_ = $lastday->subDays(7);

        $testdate = Carbon::parse($firstday);
        $testdateL = Carbon::parse($lastday_);
        $newfirst = $testdate->format('l jS F Y');
        $newlast = $testdateL->format('l jS F Y');

        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();

        $getplaylist = PlaylistInfo::where('starting_date', $firstdaydate)->where('ending_date', $lastdaydate)->value('id');
        $getmusicind = SongInfo::where('song_number','1')->get();
        $getmusic = SongInfo::where('playlist_id', $getplaylist)->get();
        $nominees = Nominee::all();
        $listcate = PlaylistCategory::all();
        $ipdf = request()->ip();
        $arr_ip = geoip()->getLocation($ipdf);
        $getlocation = $arr_ip->country;

        $getmusicgospel = Nominee::where('nominee_category', '1')->groupBy('artist_name')->groupBy('artist_song')->get();
        $getmusicmodern= Nominee::where('nominee_category', '4')->groupBy('artist_name')->groupBy('artist_song')->get();
        $getmusictraditional= Nominee::where('nominee_category', '3')->groupBy('artist_name')->groupBy('artist_song')->get();

        return view('Nominee')->with(['getmusicgospel'=>$getmusicgospel,'getmusicmodern'=>$getmusicmodern,'getmusictraditional'=>$getmusictraditional,'listcate'=>$listcate,'getlocation'=>$getlocation,'nominees'=>$nominees,'getmusic'=>$getmusic,'getmusicind'=>$getmusicind,'firstday'=>$newfirst,'lastday'=>$newlast]);

    }
    public function SearchNomineeG(Request $request){
//        $name = "idsvvds";
        $name = $request['searchname'];
        $searchG = Nominee::where('nominee_category', '1')->where('artist_name', 'like', '%' . $name . '%')->get();
//        dd($searchG);
        if (0 == count($searchG)) {
            echo "<div class=\"director-speech clearfix\" id=\"resultsg\" style=\"padding-top: 30px\"> <img src=\"front/images/logo/logojpeg.jpeg\" class=\"d-img\"> <div class=\"bio-block\"> <h6 class=\"name\" style=\"padding: 15px 0;\">Oops no results found</h6> </div></div>";
        }else{
//            echo "not empty";
            foreach ($searchG as $getmusics) {
                echo "<div class=\"director-speech clearfix\" style=\"padding-top: 30px\"> <img src=\"front/images/logo/logojpeg.jpeg\" class=\"d-img\"> <div class=\"bio-block\"> <h6 class=\"name\">$getmusics->artist_name</h6> <span>$getmusics->artist_song</span> </div><h6 class=\"sign\"> Nominee: 1 </h6> </div>";
            }
        }
    }
    public function SearchNomineeM(Request $request){
//        $name = "idsvvds";
        $name = $request['modernSearch'];
        $searchG = Nominee::where('nominee_category', '4')->where('artist_name', 'like', '%' . $name . '%')->get();
//        dd($searchG);
        if (0 == count($searchG)) {
            echo "<div class=\"director-speech clearfix\" id=\"resultsg\" style=\"padding-top: 30px\"> <img src=\"front/images/logo/logojpeg.jpeg\" class=\"d-img\"> <div class=\"bio-block\"> <h6 class=\"name\" style=\"padding: 15px 0;\">Oops no results found</h6> </div></div>";
        }else{
//            echo "not empty";
            foreach ($searchG as $getmusics) {
                echo "<div class=\"director-speech clearfix\" style=\"padding-top: 30px\"> <img src=\"front/images/logo/logojpeg.jpeg\" class=\"d-img\"> <div class=\"bio-block\"> <h6 class=\"name\">$getmusics->artist_name</h6> <span>$getmusics->artist_song</span> </div><h6 class=\"sign\"> Nominee: 1 </h6> </div>";
            }
        }
    }

    public function SearchNomineeT(Request $request){
//        $name = "idsvvds";
        $name = $request['traditionalSearch'];
        $searchG = Nominee::where('nominee_category', '3')->where('artist_name', 'like', '%' . $name . '%')->get();
//        dd($searchG);
        if (0 == count($searchG)) {
            echo "<div class=\"director-speech clearfix\" id=\"resultsg\" style=\"padding-top: 30px\"> <img src=\"front/images/logo/logojpeg.jpeg\" class=\"d-img\"> <div class=\"bio-block\"> <h6 class=\"name\" style=\"padding: 15px 0;\">Oops no results found</h6> </div></div>";
        }else{
//            echo "not empty";
            foreach ($searchG as $getmusics) {
                echo "<div class=\"director-speech clearfix\" style=\"padding-top: 30px\"> <img src=\"front/images/logo/logojpeg.jpeg\" class=\"d-img\"> <div class=\"bio-block\"> <h6 class=\"name\">$getmusics->artist_name</h6> <span>$getmusics->artist_song</span> </div><h6 class=\"sign\"> Nominee: 1 </h6> </div>";
            }
        }
    }

    public function AddNominee(Request $request){
        $all = $request->all();
        $ipdf = request()->ip();
        $arr_ip = geoip()->getLocation($ipdf);
        $getlocation = $arr_ip->country;

        $checkipexist = Nominee::where('nominee_ipaddress',$ipdf)->where('nominee_category',$request['category'])->get();


        if(0 == count($checkipexist)){
            $addnomiee = new Nominee();
            $addnomiee->nominee_category = $request['category'];
            $addnomiee->artist_name = $request['artist_name'];
            $addnomiee->artist_song = $request['artist_song'];
            $addnomiee->nominee_ipaddress = $ipdf;
            $addnomiee->nominee_country = $getlocation;
            $addnomiee->save();

            return back()->with('success','you have successfully nominated your favorite song');

        }else{
            return back()->with('success', 'Wamaze gutanga indirimbo ukunda');
        }

    }

    public function ContactUs(){
        return view('ContactUs');
    }
    public function ThankYou(Request $request){
        $id = $request['id'];
        $firstday = new Carbon('first day of this month');
        $dt = Carbon::now();
        $currentdate = $firstday->subMonth();
        $testindate = $dt->diffForHumans($firstday);

        $lastday = new Carbon('last day of this month');
        $endofmonth = new Carbon('last day of this month');
        $lastday_ = $lastday->subDays(7);

        $testdate = Carbon::parse($firstday);
        $testdateL = Carbon::parse($lastday_);
        $newfirst = $testdate->format('jS F Y');
        $newlast = $testdateL->format('jS F Y');

        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();
        $endmontdate = $endofmonth->toDateString();

//        $getmusicmodern= SongInfo::where('id', $id)->get();
        $getmusicmodern = SongInfo::where('songinfo.id', $id)->where('voter_status','SUCCESSFUL')->leftJoin('votes', 'songinfo.id', '=', 'votes.song_id')
            ->select('songinfo.id','songinfo.created_at','songinfo.song_artist','songinfo.song_name','songinfo.song_youtube_link',
                'songinfo.song_cover_picture','songinfo.playlist_code','songinfo.starting_date',
                'songinfo.ending_date', DB::raw("count(votes.id) as votesNumber"))
//            ->whereBetween(DB::raw('votes.created_at'), [DB::raw($firstdaydate), DB::raw($endmontdate)])
            ->groupBy('votes.song_id')
//            ->orderBy('votes.song_id','DESC')
            ->orderBy('votesNumber','DESC')
//            ->limit(25)
            ->get();

//        $getmusicmodern = SongInfo::where('id', $id)->get();

        foreach ($getmusicmodern as $datas){
            $this->CheckVotesUnlimited($datas);
            $this->CheckVotesMonthlyUnlimited($datas);
            $this->CheckVotesLastMonthUnlimited($datas);
            $datas['votesNumber'] = (double) $this->CheckVotesUnlimited($datas);
            $datas['votesNumberMon'] = (double) $this->CheckVotesMonthlyUnlimited($datas);
            $datas['votesNumberLastMon'] = (double) $this->CheckVotesLastMonthUnlimited($datas);
        }
//        return response()->json($getmusicmodern);
        $check_status = VotingStatus::value('voting_status');
        $link = url()->previous();
        return view('V2.ThankYou')->with(['link'=>$link,'getmusicmodern'=>$getmusicmodern]);
    }
    public function Login(){
        return view('backend.Login');
    }
    public function ArtistRegistration(){
        return view('ArtistRegistration');
    }
    public function ForgetPassword(){
        return view('ForgetPassword');
    }
    public function ForgetPasswordCheck(Request $request){
        $emailreset = $request['email'];
        $checkemail = User::where('email',$emailreset)->get();
        $getnames = ArtistAccounts::where('artist_email',$emailreset)->value('names');
        $name = $getnames;
        $email = "infomni611@gmail.com";
        $toemail = $emailreset;
        $messageMail = "You can reset the password for your control panel account by using the information below:";
        $company_name = "MNI SELECTION";
        $subjectmail = "MNI Password Recovery";

        if(0 == count($checkemail)){
            return back()->with('success', 'Sorry There is no registered user with this email address');
        }else{
            Mail::send('backend.ResetMail',
                array(
                    'namemail'=>$company_name,
                    'name' => $name,
                    'email' => $email,
                    'toEmail' => $toemail,
                    'messageMail' => $messageMail
                ), function($message) use ($email,$messageMail, $name,$subjectmail,$company_name,$toemail)
                {
                    $message->from($email, $company_name);
                    $message->to($toemail, $name)->subject($subjectmail);
                });
            return view('ForgetPasswordEmail');
        }
    }
    public function ForgetPasswordEmail(){
        return view('ForgetPasswordEmail');
    }
    public function ForgetPasswordReset(Request $request){
        $all = $request->all();
        $email = $request['email'];
        return view('ForgetPasswordReset')->with(['email'=>$email]);
    }
    public function NewPassword(Request $request){
        $email = $request['email'];
        $password = bcrypt(($request['artist_password']));
        $updatepass = User::where('email',$email)->update(['password'=>$password]);
        return redirect()->route('ArtistLogin');

    }
    public function StakeHoldersLogin(){
        return view('V2.StakeHoldersLogin');
    }
    public function AddArtistRegistration(Request $request){
        $all = $request->all();
        $checkifemail = User::where('email',$request['artist_email'])->get();

        if(0 == count($checkifemail)){
            $register = new User();
            $register->name = $request['names'];
            $register->email = $request['artist_email'];
            $register->role = "Pending";
            $register->password = bcrypt($request['artist_password']);
            $register->save();

            $addreg = new ArtistAccounts();
            $addreg->names = $request['names'];
            $addreg->artistic_names = $request['artistic_names'];
            $addreg->artist_number = $request['artist_number'];
            $addreg->artist_email = $request['artist_email'];
//            $addreg->artist_ID = $request['artist_ID'];

            $addreg->artist_modeofpayment = $request['artist_modeofpayment'];
            $addreg->artist_mobilemoney_number = $request['artist_mobilemoney_number'];
            $addreg->artist_mobilemoney_names = $request['artist_mobilemoney_names'];

            $addreg->artist_bankaccount_number = $request['artist_bankaccount_number'];
            $addreg->artist_bankaccount_names = $request['artist_bankaccount_names'];
            $addreg->artist_bank_name = $request['artist_bank_name'];

            $addreg->artist_facebook = $request['artist_facebook'];
            $addreg->artist_instagram = $request['artist_instagram'];
            $addreg->artist_twitter = $request['artist_twitter'];
            $addreg->artist_youtube = $request['artist_youtube'];
            $addreg->artist_category = $request['artist_category'];
            $addreg->artist_description = $request['artist_description'];

            $addreg->artist_account_status = "Pending";

            $image = $request->file('picturename');
            $addreg->picturename = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/ArtistPhoto');
            $image->move($destinationPath, $imagename);
            $addreg->save();

            return view('V2.StakeHoldersThankYou');
        }else{
            return back()->with('success', 'Ooops ,email is already registered');
        }

    }
    public function StakeHoldersProfile(Request $request){
        $id = $request['id'];
        $list_account = ArtistAccounts::where('id',$id)->get();
        $user_id = $request['id'];
        $user_email = User::where('id',$user_id)->value('email');
        $list_account = ArtistAccounts::where('id',$id)->get();
//        $list_account = ArtistAccounts::where('artist_email',$user_email)->get();
        $check_status = VotingStatus::value('voting_status');
        $getmusicmodern_promoted = SongInfo::where('user_id',$user_id)->where('archives_status',null)->where('voter_status','SUCCESSFUL')->leftJoin('votes', 'songinfo.id', '=', 'votes.song_id')
            ->select('songinfo.id','songinfo.created_at','songinfo.song_artist','songinfo.song_name','songinfo.song_youtube_link',
                'songinfo.song_cover_picture','songinfo.playlist_code','songinfo.starting_date','songinfo.user_id',
                'songinfo.ending_date', DB::raw("sum(vote) as votesNumber"))
            ->groupBy('votes.song_id')
            ->orderBy('votesNumber','DESC')
            ->limit(10)
            ->get();
//        dd($list_account);
        return view('V2.StakeHoldersProfile')->with(['check_status'=>$check_status,'list_account'=>$list_account,'getmusicmodern_promoted'=>$getmusicmodern_promoted]);
    }
    public function StakeHoldersProfileV2(Request $request){
        $user_id = $request['id'];
        $user_email = User::where('id',$user_id)->value('email');
        $list_account = ArtistAccounts::where('artist_email',$user_email)->get();
        $check_status = VotingStatus::value('voting_status');
        $getmusicmodern_promoted = SongInfo::where('user_id',$user_id)->where('archives_status',null)->where('voter_status','SUCCESSFUL')->leftJoin('votes', 'songinfo.id', '=', 'votes.song_id')
            ->select('songinfo.id','songinfo.created_at','songinfo.song_artist','songinfo.song_name','songinfo.song_youtube_link',
                'songinfo.song_cover_picture','songinfo.playlist_code','songinfo.starting_date','songinfo.user_id',
                'songinfo.ending_date', DB::raw("sum(vote) as votesNumber"))
            ->groupBy('votes.song_id')
            ->orderBy('votesNumber','DESC')
            ->limit(10)
            ->get();


//        dd($getmusicmodern_promoted);
        return view('V2.StakeHoldersProfile')->with(['check_status'=>$check_status,'list_account'=>$list_account,'getmusicmodern_promoted'=>$getmusicmodern_promoted]);
    }
    public function Fundraising(){
        $list = Funds::where('fund_status','Approved')->get();
        $list_slider = FundsSliders::all();
        foreach ($list as $datas){
            $this->CheckRaisedAmount($datas);
//            $datas['votesNumber'] = (double) $this->CheckVotes($datas);
            $datas['CheckRaisedAmount'] = (double) $this->CheckRaisedAmount($datas);
        }
//        dd($list);
        return view('V2.Fundraising')->with(['list'=>$list,'list_slider'=>$list_slider]);
    }

    public function FundraisingMore(Request $request){
        $id = $request['id'];
        $more = Funds::where('funds.id',$id)->join('users', 'users.id', '=', 'funds.fund_user_id')
            ->select('funds.*','users.name')->get();

        foreach ($more as $datas){
            $this->CheckRaisedAmount($datas);
            $this->CheckRaisedPeople($datas);
//            $datas['votesNumber'] = (double) $this->CheckVotes($datas);
            $datas['CheckRaisedAmount'] = (double) $this->CheckRaisedAmount($datas);
            $datas['CheckRaisedPeople'] = $this->CheckRaisedPeople($datas);
        }
//        dd($more);
        return view('V2.FundraisingMore')->with(['more'=>$more]);
    }

    public function Donate(Request $request){
        $id = $request['id'];
        $more = Funds::where('id',$id)->get();

        foreach ($more as $datas){
            $this->CheckRaisedAmount($datas);
            $this->CheckRaisedPeople($datas);
//            $datas['votesNumber'] = (double) $this->CheckVotes($datas);
            $datas['CheckRaisedAmount'] = (double) $this->CheckRaisedAmount($datas);
            $datas['CheckRaisedPeople'] = $this->CheckRaisedPeople($datas);
        }
        return view('V2.Donate')->with(['more'=>$more]);
    }
    public function DonatePayment(Request $request){
        $id = $request['id'];
        $phonenumbers = $request['phonenumber'];
        $unlimitedamount = $request['amount'];
        $names = $request['names'];

        $firstday = new Carbon('first day of last month');
        $lastday = new Carbon('last day of last month');
        $lastday_ = $lastday->subDays(7);
        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();

        $str_number = substr($phonenumbers, 3);
        $phonenumber = $phonenumbers;
//                    $phonenumber = $phoneNumber;
        $amount = $unlimitedamount;
        $idt = mt_rand(10, 99);
        //Generating Payment Gateway
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://akokanya.com/mtn-pay?amount=$amount&phone=$phonenumber&company_name=MNI&payment_code=$idt",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Host: akokanya.com",
                "Postman-Token: caddbf9d-3cf9-4fc3-b051-bc0ee25a2561,de8e362a-f9dd-4a20-a3bf-099dba0f6b26",
                "User-Agent: PostmanRuntime/7.11.0",
                "accept-encoding: gzip, deflate",
                "cache-control: no-cache",
                "content-length: "
            ),
        ));

        $responseapi = curl_exec($curl);
//            dd($response);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            //echo "cURL Error #:" . $err;
            $title="Not Connected";
            $message="Sorry you do not have no internet connection";
            //return redirect()->route('balance')->with($title,$message);
//            echo "$err";
            return response()->json([
                'message' =>$message,
            ]);

        } else {
            //echo $response;

            //Showing Status
            //second part Status checking

            $responseid=json_decode($responseapi);
//                        dd($responseid);

            $curl = curl_init();

            if($responseapi=='{"@attributes":{"errorcode":"TARGET_AUTHORIZATION_ERROR"}}'){
                //checking balance
                $maintitle="Balance";
                $title="Not Enough Balance";
                $response ="Sorry you do not have enough money on your account to make this transaction, please try again after topping up your account";
                //return redirect()->route('balance')->with($title,$message);
                return response()->json([
                    'message' =>$response,
                ]);
//                return view('VotePayError')->with(['response'=>$response]);
            }
            elseif($responseapi=='{"@attributes":{"errorcode":"ACCOUNTHOLDER_WITH_FRI_NOT_FOUND"},"arguments":{"@attributes":{"name":"fri","value":"FRI:25'.$phonenumber.'\/MSISDN"}}}'){
                //User not registered in Momo
                $maintitle="Not Registered";
                $title="Not Registered";
                $response="Sorry you're not registered with MTN Mobile Rwanda";
//                return view('VotePayError')->with(['response'=>$response]);
                return response()->json([
                    'message' =>$response,
                ]);
            }
            elseif($responseapi=='{"error":"the minimum amount is 100"}'){
                //User not registered in Momo
                $maintitle="Minimum amount";
                $title="Minimum amount";
                $response="Sorry the minimum amount to send is 100Frw";
//                return view('VotePayError')->with(['response'=>$response]);
                return response()->json([
                    'message' =>$response,
                ]);
            }
            elseif($responseapi=='{"@attributes":{"errorcode":"AUTHORIZATION_MAXIMUM_AMOUNT_ALLOWED_TO_SEND"}}'){
                //User not registered in Momo
                $maintitle="Maximum amount";
                $title="Maximum amount";
                $response="Sorry the maximum amount to send is 2,000,000Frw";
                //return redirect()->route('balance')->with($title,$message);
//                return view('VotePayError')->with(['response'=>$response]);
                return response()->json([
                    'message' =>$response,
                ]);
            }
            else{
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$responseid->transactionid",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_POSTFIELDS => "",
                    CURLOPT_HTTPHEADER => array(
                        "Accept: */*",
                        "Cache-Control: no-cache",
                        "Connection: keep-alive",
                        "Host: akokanya.com",
                        "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                        "User-Agent: PostmanRuntime/7.11.0",
                        "accept-encoding: gzip, deflate",
                        "cache-control: no-cache",
                        "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                        "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
                    ),
                ));

                $responseapis = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if ($err) {
                    echo "cURL Error #:" . $err;
                } else {
                    //echo $response;

                    $responsedata=json_decode($responseapis);
//                        dd($responsedata);
                    //Save to database Momo payments
                    //dd( $amount);
                    $deletedat=0;
                    $Momopayment = FundTransaction::create([
                        'phonenumber'=> $phonenumber,
                        'fund_id'=> $id,
                        'transactionid'=> $responsedata[0]->external_payment_code,
                        'status'=> $responsedata[0]->payment_status,
                        'assignedid'=> $responsedata[0]->id,
                        'company_name'=> $responsedata[0]->company_name,
                        'code'=> $responsedata[0]->code,
                        'amount'=> $amount,
                        'names'=> $names,
                        'payment_code'=> $responsedata[0]->payment_code,
                        'external_payment_code'=> $responsedata[0]->external_payment_code,
                        'payment_status'=> $responsedata[0]->payment_status,
                        'payment_type'=> $responsedata[0]->payment_type,
                        'callback_url'=> $responsedata[0]->callback_url,
                        'momodeleted_at'=> $deletedat,
                        'momocreated_at'=> $responsedata[0]->created_at,
                        'momoupdated_at'=> $responsedata[0]->updated_at,
                    ]);
                    if($Momopayment){
//                        event(new Event($Momopayment));
//                        $link = "<a href=\"tel:*182*7#\" class=\"phone\" style=\"color: #22706c;font-size: 16px; font-weight: bold;\"><i class=\"fa fa-phone\" ></i> *182*7#</a>";
                        $response="Ntufunge iyi paji izenguruka (Loading). Emeza igikorwa cyo kwishyura wakiriye.";
                        return response()->json([
                            'message' =>$response,
                            'transactionid'=>$responsedata[0]->external_payment_code,
                            'payment_status'=>$responsedata[0]->payment_status
                        ]);
//                        return view('Thankyou')->with(['song_name'=>$song_name,'song_artist'=>$song_artist]);
                    }
                    //end saving to database Momo payments
                }
            }
        }
    }
    public function MoneyCallBack(Request $request){
        $transactionid = "3134589383";
        $status = "SUCCESSFUL";
        $data = \App\MomoTransaction::where('transactionid',"3134589383")->get();
//        $status = $request['payment_status'];
//        $transactionid = $request['external_payment_code'];

        if($status == "SUCCESSFUL"){
            $Update = \App\MomoTransaction::where('transactionid',$transactionid)
                ->update(['payment_status' =>$status,'status' =>$status]);
//            $getVoter_id = MomoTransaction::where('transactionid',$transactionid)->value('voter_id');
//            $updateverstatus = Votes::where('id',$getVoter_id)->update(['voter_status'=> $status]);
            dd($data);
        }else{
            echo "no";
        }

    }


    public function CallBackFundMomo(Request $request){
        $transactionid = $request['transactionid'];
//        $transactionid = "2367841156";
        $getVoter_id = FundTransaction::where('transactionid',$transactionid)->value('fund_id');

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$transactionid",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Host: akokanya.com",
                "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                "User-Agent: PostmanRuntime/7.11.0",
                "accept-encoding: gzip, deflate",
                "cache-control: no-cache",
                "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            //echo $response;

            $responsedata=json_decode($response);
            //Update to database Momo payments
            //echo $responsedata[0]->payment_status;

            $deletedat=0;

            $Momopayment = FundTransaction::where('transactionid',$responsedata[0]->external_payment_code)
                ->update([
                    'transactionid'=> $responsedata[0]->external_payment_code,
                    'status'=> $responsedata[0]->payment_status,
                    'assignedid'=> $responsedata[0]->id,
                    'company_name'=> $responsedata[0]->company_name,
                    'code'=> $responsedata[0]->code,
                    // 'amount'=> $responsedata[0]->amount,
                    'payment_code'=> $responsedata[0]->payment_code,
                    'external_payment_code'=> $responsedata[0]->external_payment_code,
                    'payment_status'=> $responsedata[0]->payment_status,
                    'payment_type'=> $responsedata[0]->payment_type,
                    'callback_url'=> $responsedata[0]->callback_url,
                    'momodeleted_at'=> $deletedat,
                    'momocreated_at'=> $responsedata[0]->created_at,
                    'momoupdated_at'=> $responsedata[0]->updated_at,

                ]);
            if($responsedata[0]->payment_status == "SUCCESSFUL"){
                return response()->json([
                    'message' =>"Transaction successfully processed",
                    'status' =>"SUCCESSFUL",
                ]);
            }else{
                $status = $responsedata[0]->payment_status;
                return response()->json([
                    'message' =>"Transaction is $status ",
                    'status' =>"PENDING",
                ]);
            }

            //end Update to database Momo payments
        }
    }
    public function CheckRaisedAmount($datas){
        $donation = FundTransaction::where('payment_status','SUCCESSFUL')->where('fund_id',$datas->id)->sum('amount');
        return json_encode($donation);
    }

    public function CheckRaisedPeople($datas){
        $donation_people = FundTransaction::where('payment_status','SUCCESSFUL')
            ->where('fund_id',$datas->id)
            ->select('names', 'created_at','amount')
            ->get();
        return $donation_people->toArray();
//        return json_encode($donation_people);
    }

    public function FundsThankYou(){
        return view('V2.FundsThankYou');
    }
    public function Google(){
        return \File::get(pulbic_path() . '/googleede4b21874c85b08.html');
    }
    public function SiteMap(){
        return \File::get(pulbic_path() . '/sitemap.txt');
    }
    public function Godaddy(){

        return \File::get(base_path() . '/.well-known/pki-validation/godaddy.html');
    }
    public function CheckPending()
    {
        $stat = 'PENDING';

        $dt = Carbon::now()->addDay();
        $yesterdaydt = Carbon::now()->subDays(1);
        $tomorrow = $dt->toDateString();
        $yesterday = $yesterdaydt->toDateString();

        $transactions = MomoTransaction::get()->count();
//        $transacs = MomoTransaction::where('status', 'like', '%' . $stat . '%')
//            ->whereBetween('created_at', [$yesterday, $tomorrow])
//            ->count();
//        $votes = MomoTransaction::where('status','PENDING')
//            ->whereBetween('transactionid', ["2351700257", "2353193355"])->count();
        $transacs = MomoTransaction::where('status','PENDING')
            ->whereBetween('transactionid', ["2351698482", "2355178810"])
            ->get();
        dd($transacs);
    }
    public function CheckPendingVotes(){
        $stat='PENDING';

        $dt = Carbon::now()->addDay();
        $yesterdaydt = Carbon::now()->subDays(1);
        $tomorrow = $dt->toDateString();
        $yesterday = $yesterdaydt->toDateString();

        $transactions = MomoTransaction::get()->count();
//        $transacs = MomoTransaction::where('status','like','%'.$stat.'%')
//            ->whereBetween('created_at', [$yesterday,$tomorrow])
//            ->limit(500)->get();
        $transacs = MomoTransaction::where('status','PENDING')
            ->whereBetween('transactionid', ["2351722762", "2351702724"])
            ->get();
        $num = count((array)$transactions);

        $nums = array(count((array)$transactions)=>$num);

        //dd($transacs);
        //return view('backend.dashboard')->with(['num'=>$num]);

        foreach($transacs as $transac){

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$transac->transactionid",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "Accept: */*",
                    "Cache-Control: no-cache",
                    "Connection: keep-alive",
                    "Host: akokanya.com",
                    "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                    "User-Agent: PostmanRuntime/7.11.0",
                    "accept-encoding: gzip, deflate",
                    "cache-control: no-cache",
                    "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                    "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                //echo $response;

                $responsedata=json_decode($response);
                //Update to database Momo payments
                //echo $responsedata[0]->payment_status;

                $deletedat=0;

                $Momopayment = MomoTransaction::where('transactionid',$responsedata[0]->external_payment_code)
                    ->update([
                        'transactionid'=> $responsedata[0]->external_payment_code,
                        'status'=> $responsedata[0]->payment_status,
                        'assignedid'=> $responsedata[0]->id,
                        'company_name'=> $responsedata[0]->company_name,
                        'code'=> $responsedata[0]->code,
                        // 'amount'=> $responsedata[0]->amount,
                        'payment_code'=> $responsedata[0]->payment_code,
                        'external_payment_code'=> $responsedata[0]->external_payment_code,
                        'payment_status'=> $responsedata[0]->payment_status,
                        'payment_type'=> $responsedata[0]->payment_type,
                        'callback_url'=> $responsedata[0]->callback_url,
                        'momodeleted_at'=> $deletedat,
                        'momocreated_at'=> $responsedata[0]->created_at,
                        'momoupdated_at'=> $responsedata[0]->updated_at,

                    ]);
                if($Momopayment){
                    $updateverstatus = Votes::where('id',$transac->voter_id)->update(['voter_status'=> $responsedata[0]->payment_status]);
                    echo "ok";
                }else{
                    echo "Every minute request has been executed but status has not changed";
                }

                //end Update to database Momo payments
            }
            //End Status showing
        }
        echo "Every minute request has been executed";
        //end cron test
    }
    public function Competitions(){
        $list_slider = HomeSlider::all();
        $competition = Competitions::orderBy('id', 'desc')->get();
        return view('V2.Competitions')->with(['competition'=>$competition,'list_slider'=>$list_slider]);
    }
    public function Groups(Request $request){
        $id = $request['id'];
//        $competition_id = Groups::where('id',$request['id'])->value('competition_id');

        $competition_slider = CompetitionSlider::where('competition_sliders.competition_id',$id)
            ->join('competitions', 'competitions.id', '=', 'competition_sliders.competition_id')
            ->select('competition_sliders.*', 'competitions.competition_title')
            ->get();

        $contestant_title = Competitions::where('id',$id)->value('competition_title');
        $competition_Status= Competitions::where('id',$id)->value('voting_status');
        $groups = Groups::where('competition_id',$id)
            ->join('competitions', 'competitions.id', '=', 'groups.competition_id')
            ->select('groups.*', 'competitions.competition_title')
            ->get();

        return view('V2.Groups')->with(['id'=>$id,'competition_Status'=>$competition_Status,'contestant_title'=>$contestant_title,'competition_slider'=>$competition_slider,'groups'=>$groups]);
    }

    public function Contestants(Request $request){

//        $competition_id = Groups::where('id',$request['id'])->value('competition_id');
        $id = $request['id'];

        $competition_slider = CompetitionSlider::where('competition_sliders.competition_id',$request['id'])
            ->join('competitions', 'competitions.id', '=', 'competition_sliders.competition_id')
            ->select('competition_sliders.*', 'competitions.competition_title')
            ->get();

        $contestant_title = Competitions::where('id',$request['id'])->value('competition_title');
        $contestant_description = Competitions::where('id',$request['id'])->value('competition_description');
        $competition_Status= Competitions::where('id',$request['id'])->value('voting_status');
//        dd($contestant_description);
        $groups = Groups::where('competition_id',$id)
            ->where('group_status',"Active")
            ->join('competitions', 'competitions.id', '=', 'groups.competition_id')
            ->select('groups.*', 'competitions.competition_title')
            ->get();

        foreach ($groups as $data){
            $this->ContestantsData($data);
            $data['ContestantsData'] = $this->ContestantsData($data);
        }
//        dd($groups);
        return view('V2.Contestants')->with(['contestant_description'=>$contestant_description,'groups'=>$groups,'competition_Status'=>$competition_Status,'contestant_title'=>$contestant_title,'competition_slider'=>$competition_slider]);
    }

    public function ContestantsData($data){

//        $contestant = DB::table('contestants')
////             ->where('contestants.competition_id',$data->competition_id)
//            ->where('contestants.group_id',$data->id)
////            ->where('archives_status',null)->where('voter_status','SUCCESSFUL')
//            ->leftJoin('competition_transactions', 'contestants.id', '=', 'competition_transactions.contestant_id')
//            ->select('contestants.*','competition_transactions.contestant_id','competition_transactions.vote',DB::raw('coalesce(sum(vote), 0) as votesNumber_'))
////            ->where('payment_status','SUCCESSFUL')
////            ->select('contestants.*',DB::raw('coalesce(sum(vote), 0) as votesNumber_'))
////            ->groupBy('competition_transactions.contestant_id')
//            ->groupBy('contestants.id')
//            ->orderBy('votesNumber_','DESC')
//            ->get();

        $contestant = DB::table('contestants')->where('contestants.group_id',$data->id)
//
            ->leftJoin('comp_votes', 'contestants.id', '=', 'comp_votes.contestant_id')
//            ->select('contestants.*', DB::raw("sum(vote) as votesNumber"))
            ->select('contestants.*','comp_votes.contestant_id','comp_votes.vote',DB::raw('coalesce(sum(vote), 0) as votesNumber_'))
//            ->where('comp_votes.status','SUCCESSFUL')
            ->groupBy('contestants.id')
            ->orderBy('votesNumber_','DESC')
            ->get();
        return $contestant->toArray();
    }

    public function FollowVotes(Request $request){
        $id = $request['id'];
        if($id == "vote"){
            $competition =CompVotes::sum('vote');
            dd($competition);
        }elseif ($id == "card"){
            $competition = CompetitionCardTransactions::where('status','successful')->sum('vote');
            dd($competition);
        }elseif ($id == "diff"){
            $vote = CompVotes::sum('vote');
            $momo = CompetitionTransactions::where('status','successful')->sum('vote');
            $card = CompetitionCardTransactions::where('status','successful')->sum('vote');
            $diff = $vote - ($momo + $card);
            dd($diff);
        }
        elseif ($id == "momo"){
            $momo = CompetitionTransactions::where('status','successful')->sum('vote');
            dd($momo);
        }
    }
    public function StandAloneContestantCard(Request $request){
        $id = $request['id'];
        $competition = CompetitionCardTransactions::where('contestant_id',$id)->where('status','successful')->sum('vote');
        dd($competition);
    }
    public function StandAloneContestant(Request $request){
        $id = $request['id'];
        $competition = CompetitionTransactions::where('contestant_id',$id)->where('payment_status','SUCCESSFUL')->sum('vote');
        dd($competition);
    }
    public function ContestantVote(Request $request){
        $id = $request['id'];
        $country = Country::all();
        $group_id = DB::table('contestants')->where('id',$request['id'])->value('group_id');
        $group_status = Groups::where('id',$group_id)->value('group_status');
        $contestant_name = Contestants::where('id',$request['id'])->value('names');
        $contestant_compt = Contestants::where('id',$request['id'])->value('competition_id');
        $competition_vote_status = Competitions::where('id',$contestant_compt)->value('voting_status');
        $competition_desc = Competitions::where('id',$contestant_compt)->value('competition_description');
        $competition_title = Competitions::where('id',$contestant_compt)->value('competition_title');
//        dd($competition_desc);
        $title = "Gutora $contestant_name muri $competition_title";

//        $contestant = Contestants::where('contestants.id',$request['id'])
//            ->join('competitions', 'competitions.id', '=', 'contestants.competition_id')
//            ->select('contestants.*', 'competitions.competition_title')
////            ->groupBy('contestant_id')
////            ->orderBy('votesNumber_','DESC')
//            ->get();

        $contestant = DB::table('contestants')->where('contestants.id',$request['id'])
//
            ->leftJoin('comp_votes', 'contestants.id', '=', 'comp_votes.contestant_id')
//            ->select('contestants.*', DB::raw("sum(vote) as votesNumber"))
            ->select('contestants.*','comp_votes.contestant_id','comp_votes.vote',DB::raw('coalesce(sum(vote), 0) as votesNumber_'))
//            ->where('comp_votes.status','SUCCESSFUL')
            ->groupBy('contestants.id')
            ->orderBy('votesNumber_','DESC')
            ->get();

//        foreach ($contestant as $datas){
//            $this->CheckCompetitionVotes($datas);
//            $datas['votesNumber_'] = (double) $this->CheckCompetitionVotes($datas);
//        }
        return view('V2.ContestantVote')->with(['competition_title'=>$competition_title,'competition_desc'=>$competition_desc,'competition_vote_status'=>$competition_vote_status,'country'=>$country,'group_status'=>$group_status,'title'=>$title,'contestant'=>$contestant]);
    }

    public function CheckCompetitionVotes($datas){
//        $votes = CompetitionTransactions::where('contestant_id',$datas->id)
//            ->where('payment_status','SUCCESSFUL')
//            ->where('competition_id',$datas->competition_id)
//            ->sum('vote');
        $votes = CompVotes::where('contestant_id',$datas->id)
            ->where('status','SUCCESSFUL')
            ->where('competition_id',$datas->competition_id)
            ->sum('vote');

        return json_encode($votes);
    }
    public function ContestantPayment(Request $request){
        $id = $request['contestant_id'];
        $competition_id = $request['competition_id'];
        $phonenumbers = $request['phonenumber'];
        $unlimitedamount = $request['amount'];
        $names = $request['names'];
        $new_votess = $unlimitedamount / 50 ;

        $firstday = new Carbon('first day of last month');
        $lastday = new Carbon('last day of last month');
        $lastday_ = $lastday->subDays(7);
        $firstdaydate = $firstday->toDateString();
        $lastdaydate = $lastday_->toDateString();

        $str_number = substr($phonenumbers, 3);
        $phonenumber = $phonenumbers;
//                    $phonenumber = $phoneNumber;
        $amount = $unlimitedamount;
        $idt = mt_rand(10, 99);


        //Generating Payment Gateway
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://akokanya.com/mtn-pay?amount=$amount&phone=$phonenumber&company_name=MNI&payment_code=$idt",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Host: akokanya.com",
                "Postman-Token: caddbf9d-3cf9-4fc3-b051-bc0ee25a2561,de8e362a-f9dd-4a20-a3bf-099dba0f6b26",
                "User-Agent: PostmanRuntime/7.11.0",
                "accept-encoding: gzip, deflate",
                "cache-control: no-cache",
                "content-length: "
            ),
        ));

        $responseapi = curl_exec($curl);
//            dd($response);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            //echo "cURL Error #:" . $err;
            $title="Not Connected";
            $message="Sorry you do not have no internet connection";
            //return redirect()->route('balance')->with($title,$message);
//            echo "$err";
            return response()->json([
                'message' =>$message,
            ]);

        } else {
            //echo $response;

            //Showing Status
            //second part Status checking

            $responseid=json_decode($responseapi);
//                        dd($responseid);

            $curl = curl_init();

            if($responseapi=='{"@attributes":{"errorcode":"TARGET_AUTHORIZATION_ERROR"}}'){
                //checking balance
                $maintitle="Balance";
                $title="Not Enough Balance";
                $response ="Sorry you do not have enough money on your account to make this transaction, please try again after topping up your account";
                //return redirect()->route('balance')->with($title,$message);
                return response()->json([
                    'message' =>$response,
                ]);
//                return view('VotePayError')->with(['response'=>$response]);
            }
            elseif($responseapi=='{"@attributes":{"errorcode":"ACCOUNTHOLDER_WITH_FRI_NOT_FOUND"},"arguments":{"@attributes":{"name":"fri","value":"FRI:25'.$phonenumber.'\/MSISDN"}}}'){
                //User not registered in Momo
                $maintitle="Not Registered";
                $title="Not Registered";
                $response="Sorry you're not registered with MTN Mobile Rwanda";
//                return view('VotePayError')->with(['response'=>$response]);
                return response()->json([
                    'message' =>$response,
                ]);
            }
            elseif($responseapi=='{"error":"the minimum amount is 100"}'){
                //User not registered in Momo
                $maintitle="Minimum amount";
                $title="Minimum amount";
                $response="Sorry the minimum amount to send is 100Frw";
//                return view('VotePayError')->with(['response'=>$response]);
                return response()->json([
                    'message' =>$response,
                ]);
            }
            elseif($responseapi=='{"@attributes":{"errorcode":"AUTHORIZATION_MAXIMUM_AMOUNT_ALLOWED_TO_SEND"}}'){
                //User not registered in Momo
                $maintitle="Maximum amount";
                $title="Maximum amount";
                $response="Sorry the maximum amount to send is 2,000,000Frw";
                //return redirect()->route('balance')->with($title,$message);
//                return view('VotePayError')->with(['response'=>$response]);
                return response()->json([
                    'message' =>$response,
                ]);
            }
            else{
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$responseid->transactionid",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_POSTFIELDS => "",
                    CURLOPT_HTTPHEADER => array(
                        "Accept: */*",
                        "Cache-Control: no-cache",
                        "Connection: keep-alive",
                        "Host: akokanya.com",
                        "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                        "User-Agent: PostmanRuntime/7.11.0",
                        "accept-encoding: gzip, deflate",
                        "cache-control: no-cache",
                        "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                        "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
                    ),
                ));

                $responseapis = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if ($err) {
                    echo "cURL Error #:" . $err;
                } else {
                    //echo $response;

                    $responsedata=json_decode($responseapis);
//                        dd($responsedata);
                    //Save to database Momo payments
                    //dd( $amount);
                    $deletedat=0;

                    $Momopayment = CompetitionTransactions::create([
                        'phonenumber'=> $phonenumber,
                        'vote'=> null,
                        'voter_id'=> null,
                        'competition_id'=> $request['competition_id'],
                        'contestant_id'=> $id,
                        'transactionid'=> $responsedata[0]->external_payment_code,
                        'status'=> $responsedata[0]->payment_status,
                        'assignedid'=> $responsedata[0]->id,
                        'company_name'=> $responsedata[0]->company_name,
                        'code'=> $responsedata[0]->code,
                        'amount'=> $amount,
                        'names'=> $names,
                        'payment_code'=> $responsedata[0]->payment_code,
                        'external_payment_code'=> $responsedata[0]->external_payment_code,
                        'payment_status'=> $responsedata[0]->payment_status,
                        'payment_type'=> $responsedata[0]->payment_type,
                        'callback_url'=> $responsedata[0]->callback_url,
                        'momodeleted_at'=> $deletedat,
                        'momocreated_at'=> $responsedata[0]->created_at,
                        'momoupdated_at'=> $responsedata[0]->updated_at,
                    ]);
                    if($Momopayment){
//                        event(new Event($Momopayment));
//                        $link = "<a href=\"tel:*182*7*1#\" class=\"phone\" style=\"color: #22706c;font-size: 16px; font-weight: bold;\"><i class=\"fa fa-phone\" ></i></a>";
                        $response="Ntufunge iyi paji izenguruka (Loading),Hita wemeza igikorwa cyo kwishyura wakiriye (Kubikuza kashi) kuri telephone,tegereza ko ubwirwa ko gutora byaciyemo.";
                        return response()->json([
                            'message' =>$response,
                            'transactionid'=>$responsedata[0]->external_payment_code,
                            'payment_status'=>$responsedata[0]->payment_status
                        ]);
//                        return view('Thankyou')->with(['song_name'=>$song_name,'song_artist'=>$song_artist]);
                    }
                    //end saving to database Momo payments
                }
            }
        }
    }
    public function CompetitionCallBackMomo(Request $request){
        $transactionid = $request['transactionid'];
//        $transactionid = "2367841156";
        $getVoter_id = CompetitionTransactions::where('transactionid',$transactionid)->value('contestant_id');
//        $getVoter_id_ = CompetitionTransactions::where('transactionid',$transactionid)->get();
        $checkpayment = CompetitionTransactions::where('transactionid',$transactionid)->get();

        if(0 == count($checkpayment)){
            return response()->json([
                'message' =>"Sorry, Transaction does not exist",
                'status' =>false,
            ]);
        }else{
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$transactionid",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "Accept: */*",
                    "Cache-Control: no-cache",
                    "Connection: keep-alive",
                    "Host: akokanya.com",
                    "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                    "User-Agent: PostmanRuntime/7.11.0",
                    "accept-encoding: gzip, deflate",
                    "cache-control: no-cache",
                    "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                    "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                //echo $response;

                $responsedata=json_decode($response);

                $deletedat=0;

                $checkpayment_ = CompVotes::where('payment_tx_ref',$transactionid)->get();
                $getVoter_id_ = CompetitionTransactions::where('transactionid',$transactionid)->get();

//                if($getVoter_id_->isEmpty()){
//                    return response()->json([
//                        'message' =>"Financial Transaction does not exist in record",
//                        'status' =>"failed",
//                    ]);
//                }else {
//
//                }

                                if($responsedata[0]->payment_status == "SUCCESSFUL" and $checkpayment_->isEmpty()){
                    $getVoter_id_ = CompetitionTransactions::where('transactionid',$transactionid)->get();
                    foreach ($getVoter_id_ as $voter){
                        $names_ = $voter->names;
                        $competition_id_ = $voter->competition_id;
                        $contestant_id_ = $voter->contestant_id;
                        $tx_ref = $voter->transactionid;
                    }
                    $new_votess = $responsedata[0]->amount / 50 ;
                    $addvote = new CompVotes();
                    $addvote->vote = $new_votess;
                    $addvote->names = $names_;
                    $addvote->competition_id = $competition_id_;
                    $addvote->contestant_id = $contestant_id_;
                    $addvote->payment_from = "MOMO";
                    $addvote->payment_tx_ref = $tx_ref;
                    $addvote->status = "SUCCESSFUL";
                    $addvote->save();
                    $last_id = $addvote->id;

                    $Momopayment = CompetitionTransactions::where('transactionid',$responsedata[0]->external_payment_code)
                        ->update([
                            'transactionid'=> $responsedata[0]->external_payment_code,
                            'status'=> $responsedata[0]->payment_status,
                            'assignedid'=> $responsedata[0]->id,
                            'company_name'=> $responsedata[0]->company_name,
                            'code'=> $responsedata[0]->code,
                            'vote'=> $new_votess,
                            'voter_id'=> $last_id,
                            'payment_code'=> $responsedata[0]->payment_code,
                            'external_payment_code'=> $responsedata[0]->external_payment_code,
                            'payment_status'=> $responsedata[0]->payment_status,
                            'payment_type'=> $responsedata[0]->payment_type,
                            'callback_url'=> $responsedata[0]->callback_url,
                            'momodeleted_at'=> $deletedat,
                            'momocreated_at'=> $responsedata[0]->created_at,
                            'momoupdated_at'=> $responsedata[0]->updated_at,

                        ]);
                    return response()->json([
                        'message' =>"Transaction successfully processed",
                        'status' =>"SUCCESSFUL",
                    ]);

//                    if(0 == count($checkpayment_)){
//
//                    }else{
//                        return response()->json([
//                            'message' =>"Vote has been created",
//                            'status' =>"SUCCESSFUL",
//                        ]);
//                    }
                }else{
                    $status = $responsedata[0]->payment_status;
                    $Momopayment = CompetitionTransactions::where('transactionid',$responsedata[0]->external_payment_code)
                        ->update([
                            'transactionid'=> $responsedata[0]->external_payment_code,
                            'status'=> $responsedata[0]->payment_status,
                            'assignedid'=> $responsedata[0]->id,
                            'company_name'=> $responsedata[0]->company_name,
                            'code'=> $responsedata[0]->code,
//                         'vote'=> $new_votess,
                            'payment_code'=> $responsedata[0]->payment_code,
                            'external_payment_code'=> $responsedata[0]->external_payment_code,
                            'payment_status'=> $responsedata[0]->payment_status,
                            'payment_type'=> $responsedata[0]->payment_type,
                            'callback_url'=> $responsedata[0]->callback_url,
                            'momodeleted_at'=> $deletedat,
                            'momocreated_at'=> $responsedata[0]->created_at,
                            'momoupdated_at'=> $responsedata[0]->updated_at,

                        ]);
                    return response()->json([
                        'message' =>"Transaction is $status ",
                        'status' =>"PENDING",
                        'data' => $checkpayment_,
                    ]);
                }
            }
        }
    }
    public function RefreshVotes(){
        $checkpayment_ = CompVotes::where('payment_tx_ref',"sfdsdf")->get();
        if($checkpayment_->isEmpty()){
            dd('empty');
        }
    }
    public function ImportVote(){
        $compevotes = CompetitionTransactions::all();
        foreach ($compevotes as $votes){
            $add_votes = new CompVotes();
            $add_votes->vote = $votes->vote;
            $add_votes->names = $votes->names;
            $add_votes->competition_id = $votes->competition_id;
            $add_votes->contestant_id = $votes->contestant_id;
            $add_votes->status = $votes->payment_status;
            $add_votes->save();
        }
    }
    public function CompetitionThankYou(Request $request){
        $id = $request['id'];
        $contestant_title = Contestants::where('id',$id)->value('names');
        $contestant = Contestants::where('contestants.id',$request['id'])
//            ->where('payment_status','SUCCESSFUL')->leftJoin('competition_transactions', 'contestants.id', '=', 'competition_transactions.contestant_id')
            ->join('competitions', 'competitions.id', '=', 'contestants.competition_id')
            ->select('contestants.*', 'competitions.competition_title')
//            ->orderBy('votesNumber','DESC')
            ->get();

        foreach ($contestant as $datas){
            $this->CheckCompetitionVotes($datas);
            $datas['votesNumber_'] = (double) $this->CheckCompetitionVotes($datas);
        }
            $check_status = VotingStatus::value('voting_status');
            $link = url()->previous();
            return view('V2.CompetitionThankYou')->with(['contestant_title'=>$contestant_title,'link'=>$link,'contestant'=>$contestant]);
    }
    public function CheckCron(){
        $stat='PENDING';

        $dt = Carbon::now()->addDay();
        $yesterdaydt = Carbon::now()->subDays(1);
//        $yesterdaydt = Carbon::now()->subDays(5);
        $tomorrow = $dt->toDateString();
        $yesterday = $yesterdaydt->toDateString();

        $transacs = MomoTransaction::where('status','like','%'.$stat.'%')
            ->whereBetween('created_at', [$yesterday,$tomorrow])
            ->count();
        dd($transacs);
    }
    public function CheckCronV2(){
        $stat='PENDING';

        $dt = Carbon::now()->addDay();
        $yesterdaydt = Carbon::now()->subDays(1);
//        $yesterdaydt = Carbon::now()->subDays(5);
        $tomorrow = $dt->toDateString();
        $yesterday = $yesterdaydt->toDateString();

        $transactions = MomoTransaction::get()->count();

//        $transacs = MomoTransaction::where('status','like','%'.$stat.'%')
////            ->whereBetween('created_at', [$yesterday,$tomorrow])
//            ->get();

        $transacs = \App\MomoTransaction::select('status','transactionid')
            ->where('status','like','%'.$stat.'%')
//            ->whereBetween('created_at', [$yesterday,$tomorrow])
            ->get();

//        $transacs = MomoTransaction::where('status','PENDING')
//            ->whereBetween('transactionid', ["2351698482", "2355178810"])
//            ->get();
        $num = count((array)$transactions);

        $nums = array(count((array)$transactions)=>$num);

//        dd($transacs);
        //return view('backend.dashboard')->with(['num'=>$num]);

        foreach($transacs as $transac){

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$transac->transactionid",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "Accept: */*",
                    "Cache-Control: no-cache",
                    "Connection: keep-alive",
                    "Host: akokanya.com",
                    "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                    "User-Agent: PostmanRuntime/7.11.0",
                    "accept-encoding: gzip, deflate",
                    "cache-control: no-cache",
                    "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                    "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

//            dd($response);
            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                //echo $response;

                $responsedata=json_decode($response);
                //Update to database Momo payments
                //echo $responsedata[0]->payment_status;d
                $deletedat=0;

                $Momopayment = \App\MomoTransaction::where('transactionid',$responsedata[0]->external_payment_code)
                    ->update([
                        'transactionid'=> $responsedata[0]->external_payment_code,
                        'status'=> $responsedata[0]->payment_status,
                        'assignedid'=> $responsedata[0]->id,
                        'company_name'=> $responsedata[0]->company_name,
                        'code'=> $responsedata[0]->code,
                        // 'amount'=> $responsedata[0]->amount,
                        'payment_code'=> $responsedata[0]->payment_code,
                        'external_payment_code'=> $responsedata[0]->external_payment_code,
                        'payment_status'=> $responsedata[0]->payment_status,
                        'payment_type'=> $responsedata[0]->payment_type,
                        'callback_url'=> $responsedata[0]->callback_url,
                        'momodeleted_at'=> $deletedat,
                        'momocreated_at'=> $responsedata[0]->created_at,
                        'momoupdated_at'=> $responsedata[0]->updated_at,

                    ]);
                if($responsedata[0]->payment_status ==  "SUCCESSFUL"){
                    $updateverstatus = Votes::where('id',$transac->voter_id)->update(['voter_status'=> $responsedata[0]->payment_status]);
                    dd('Your request has been executed and updated votes');
                }else{
                    dd('Your request has not been executed but status has not changed');

                }

                //end Update to database Momo payments
            }
            //End Status showing
        }
    }
    public function SMSRoute(Request $request){
        $all = $request->all();
//        return $all;
        $sender="MUGANGA";
        $phone=$_REQUEST['phoneNumber']??"";
        $message=$_REQUEST['Text']??"";

        if (empty($phone) || empty($message) ) {
            $exception2=["Error"=> "missing_params"];
            echo json_encode( $exception2 );

        }


        if(!empty($phone)&&!empty($message)){
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.mista.io/sms',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array('to' => $phone,'from' => $sender,'unicode' => '0','sms' => $message,'action' => 'send-sms'),
                CURLOPT_HTTPHEADER => array(
                    'x-api-key:OGIwaGh1aThwY3k5OW5wbnN5cGFtYnF3Z29hNm1xem0='
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);

            echo $response;
        }
        else{
            $exception2=["Error"=> "something went wrong"];
            echo json_encode( $exception2 );
        }
    }
    public function SmsManagementApi(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
//            CURLOPT_URL => "https://api.mista.io/sms",
            CURLOPT_URL => "https://api.mista.io/sms",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array('action' => 'get-inbox','response' => 'json'),
            CURLOPT_HTTPHEADER => array(
                "x-api-key: OGIwaGh1aThwY3k5OW5wbnN5cGFtYnF3Z29hNm1xem0="
            ),
        ));
        $response = curl_exec($curl);
        $json_De = json_decode($response);


        curl_setopt_array($curl, array(
//            CURLOPT_URL => "https://api.mista.io/sms",
            CURLOPT_URL => "https://api.mista.io/sms",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array('action' => 'check-balance','response' => 'json'),
            CURLOPT_HTTPHEADER => array(
                "x-api-key: OGIwaGh1aThwY3k5OW5wbnN5cGFtYnF3Z29hNm1xem0="
            ),
        ));

        $response_balance = json_decode(curl_exec($curl));
        curl_close($curl);
        return response()->json([
            'response' => $json_De,
            'response_balance' => $response_balance,
            'response_status' =>true
        ]);
    }
    public function CheckCompetitionVote(){
        return view('V2.CheckCompetitionVote');
    }
    public function ValidateVoteCheck(Request $request){
        $all = $request->all();
        $checkvote = $request['checkvote'];
//        dd($checkvote);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$checkvote",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Host: akokanya.com",
                "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                "User-Agent: PostmanRuntime/7.11.0",
                "accept-encoding: gzip, deflate",
                "cache-control: no-cache",
                "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        $responsedata=json_decode($response);

//        dd($responsedata);

        $deletedat=0;

        $checkpayment_ = CompVotes::where('payment_tx_ref',$checkvote)->get();
//        dd($checkpayment_);

        if(empty($responsedata)){
            return back()->with('success','Iyi nimero ntabwo iboneka muri system. Shyiramo nimero yamaze kwishyurwa cyangwa uhamagare 0780008883 wakire ubufasha');
        }else{
            $getVoter_id_ = CompetitionTransactions::where('transactionid',$checkvote)->get();
            if($getVoter_id_->isEmpty()){
                return back()->with('success','Iyi nimero ntabwo iboneka muri system. Shyiramo nimero yamaze kwishyurwa cyangwa uhamagare 0780008883 wakire ubufasha');
            }else{
                if($responsedata[0]->payment_status == "SUCCESSFUL" and $checkpayment_->isEmpty()){
                    $getVoter_id_ = CompetitionTransactions::where('transactionid',$checkvote)->get();
//                    dd($getVoter_id_);
                    foreach ($getVoter_id_ as $voter){
                        $names_ = $voter->names;
                        $competition_id_ = $voter->competition_id;
                        $contestant_id_ = $voter->contestant_id;
                        $tx_ref = $voter->transactionid;

                        $new_votess = $responsedata[0]->amount / 50 ;
                        $addvote = new CompVotes();
                        $addvote->vote = $new_votess;
                        $addvote->names = $names_;
                        $addvote->competition_id = $competition_id_;
                        $addvote->contestant_id = $contestant_id_;
                        $addvote->payment_from = "MOMO";
                        $addvote->payment_tx_ref = $tx_ref;
                        $addvote->status = "SUCCESSFUL";
                        $addvote->save();
                        $last_id = $addvote->id;
                        $Momopayment = CompetitionTransactions::where('transactionid',$responsedata[0]->external_payment_code)
                            ->update([
                                'transactionid'=> $responsedata[0]->external_payment_code,
                                'status'=> $responsedata[0]->payment_status,
                                'assignedid'=> $responsedata[0]->id,
                                'company_name'=> $responsedata[0]->company_name,
                                'code'=> $responsedata[0]->code,
                                'vote'=> $new_votess,
                                'voter_id'=> $last_id,
                                'payment_code'=> $responsedata[0]->payment_code,
                                'external_payment_code'=> $responsedata[0]->external_payment_code,
                                'payment_status'=> $responsedata[0]->payment_status,
                                'payment_type'=> $responsedata[0]->payment_type,
                                'callback_url'=> $responsedata[0]->callback_url,
                                'momodeleted_at'=> $deletedat,
                                'momocreated_at'=> $responsedata[0]->created_at,
                                'momoupdated_at'=> $responsedata[0]->updated_at,
                            ]);
                    }
                    return back()->with('success','Amajwi yanyu ubu  aremejwe! Jya kurutonde urebe ayiyongeyeho ukomeze no gutora');
                }else{
                    return back()->with('success','Ntabwo wabashije kwishyura! Jya kurutonde urebe ayiyongeyeho ukomeze no gutora');
                }
            }
        }

    }
    public function CBT(Request $request){
        $country_id=$_REQUEST['country_id'];
        $phone_number=$_REQUEST['phone_number'];
        $gender=$_REQUEST['gender'];
        $commodities=$_REQUEST['commodities_names'];
        $message=$_REQUEST['message'];
        $border=$_REQUEST['border_location'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://cbtcomplaints.minicom.gov.rw/api/auth/UssdSubmitComplain',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('country_id' => $country_id,'phone_number' => $phone_number,'gender' => $gender,'commodities_names' => $commodities,'message' => $message,'border_location' => $border),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
//        echo $response;
    }

//    public function handle()
//    {
//        $stat='PENDING';
//
//        $dt = Carbon::now()->addDay();
//        $yesterdaydt = Carbon::now()->subDays(1);
//        $tomorrow = $dt->toDateString();
//        $yesterday = $yesterdaydt->toDateString();
//
//        $transactions = MomoTransaction::get()->count();
//        $transacs = MomoTransaction::where('status','like','%'.$stat.'%')
//            ->whereBetween('created_at', [$yesterday,$tomorrow])
//            ->get();
//        //Updateloop
//        $num = count((array)$transactions);
//
//        $nums = array(count((array)$transactions)=>$num);
//
//        //dd($transacs);
//        //return view('backend.dashboard')->with(['num'=>$num]);
//
//        foreach($transacs as $transac){
//
//            $curl = curl_init();
//
//            curl_setopt_array($curl, array(
//                CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$transac->transactionid",
//                CURLOPT_RETURNTRANSFER => true,
//                CURLOPT_ENCODING => "",
//                CURLOPT_MAXREDIRS => 10,
//                CURLOPT_TIMEOUT => 30,
//                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//                CURLOPT_CUSTOMREQUEST => "GET",
//                CURLOPT_POSTFIELDS => "",
//                CURLOPT_HTTPHEADER => array(
//                    "Accept: */*",
//                    "Cache-Control: no-cache",
//                    "Connection: keep-alive",
//                    "Host: akokanya.com",
//                    "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
//                    "User-Agent: PostmanRuntime/7.11.0",
//                    "accept-encoding: gzip, deflate",
//                    "cache-control: no-cache",
//                    "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
//                    "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
//                ),
//            ));
//
//            $response = curl_exec($curl);
//            $err = curl_error($curl);
//
//            curl_close($curl);
//
//            if ($err) {
//                echo "cURL Error #:" . $err;
//            } else {
//                //echo $response;
//
//                $responsedata=json_decode($response);
//                //Update to database Momo payments
//                //echo $responsedata[0]->payment_status;
//
//                $deletedat=0;
//
//                $Momopayment = MomoTransaction::where('transactionid',$responsedata[0]->external_payment_code)
//                    ->update([
//                        'transactionid'=> $responsedata[0]->external_payment_code,
//                        'status'=> $responsedata[0]->payment_status,
//                        'assignedid'=> $responsedata[0]->id,
//                        'company_name'=> $responsedata[0]->company_name,
//                        'code'=> $responsedata[0]->code,
//                        // 'amount'=> $responsedata[0]->amount,
//                        'payment_code'=> $responsedata[0]->payment_code,
//                        'external_payment_code'=> $responsedata[0]->external_payment_code,
//                        'payment_status'=> $responsedata[0]->payment_status,
//                        'payment_type'=> $responsedata[0]->payment_type,
//                        'callback_url'=> $responsedata[0]->callback_url,
//                        'momodeleted_at'=> $deletedat,
//                        'momocreated_at'=> $responsedata[0]->created_at,
//                        'momoupdated_at'=> $responsedata[0]->updated_at,
//
//                    ]);
//                if($Momopayment){
//                    $updateverstatus = Votes::where('id',$transac->voter_id)->update(['voter_status'=> $responsedata[0]->payment_status]);
//                    $this->info('Every minute request has been executed and updated votes');
//                }else{
//                    $this->info('Every minute request has been executed but status has not changed');
//                }
//
//                //end Update to database Momo payments
//            }
//            //End Status showing
//        }
//        $this->info('Every minute request has been executed');
//        //end cron test
//    }
}
