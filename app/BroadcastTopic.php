<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BroadcastTopic extends Model
{
    protected $table = "broadcast_topics";
    protected $fillable = ['topic_name'];
}
