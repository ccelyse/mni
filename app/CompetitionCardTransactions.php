<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompetitionCardTransactions extends Model
{
    use SoftDeletes;
    protected $table = "competition_card_transactions";
    protected $fillable = ['first_name','last_name','phonenumber','email','city','country','amount','tx_ref','currency','status',
        'payment_options','vote','competition_id','contestant_id','voter_id','transaction_id'];
}
