<?php

namespace App\Listeners;

use App\Events\Event;
use App\MomoTransaction;
use App\Votes;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;


class EventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(Event $event)
    {
        $Momopayment = $event->Momopayment;
        $stat='PENDING';
        $dt = Carbon::now()->addDay();
        $yesterdaydt = Carbon::now()->subDays(1);
        $tomorrow = $dt->toDateString();
        $yesterday = $yesterdaydt->toDateString();

        $transactions = MomoTransaction::get()->count();
        $transacs = MomoTransaction::where('status','like','%'.$stat.'%')
            ->whereBetween('created_at', [$yesterday,$tomorrow])
            ->get();
//        $transacs = MomoTransaction::where('transactionid','2353101016')->get();
        //Updateloop
        $num = count((array)$transactions);

        $nums = array(count((array)$transactions)=>$num);

        //dd($transacs);
        //return view('backend.dashboard')->with(['num'=>$num]);

        foreach($transacs as $transac){

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$transac->transactionid",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "",
                CURLOPT_HTTPHEADER => array(
                    "Accept: */*",
                    "Cache-Control: no-cache",
                    "Connection: keep-alive",
                    "Host: akokanya.com",
                    "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                    "User-Agent: PostmanRuntime/7.11.0",
                    "accept-encoding: gzip, deflate",
                    "cache-control: no-cache",
                    "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                    "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                //echo $response;

                $responsedata=json_decode($response);
                //Update to database Momo payments
                //echo $responsedata[0]->payment_status;

                $deletedat=0;

                $Momopayment = MomoTransaction::where('transactionid',$responsedata[0]->external_payment_code)
                    ->update([
                        'transactionid'=> $responsedata[0]->external_payment_code,
                        'status'=> $responsedata[0]->payment_status,
                        'assignedid'=> $responsedata[0]->id,
                        'company_name'=> $responsedata[0]->company_name,
                        'code'=> $responsedata[0]->code,
                        // 'amount'=> $responsedata[0]->amount,
                        'payment_code'=> $responsedata[0]->payment_code,
                        'external_payment_code'=> $responsedata[0]->external_payment_code,
                        'payment_status'=> $responsedata[0]->payment_status,
                        'payment_type'=> $responsedata[0]->payment_type,
                        'callback_url'=> $responsedata[0]->callback_url,
                        'momodeleted_at'=> $deletedat,
                        'momocreated_at'=> $responsedata[0]->created_at,
                        'momoupdated_at'=> $responsedata[0]->updated_at,

                    ]);
                if($Momopayment){
                    $updateverstatus = Votes::where('id',$transac->voter_id)->update(['voter_status'=> $responsedata[0]->payment_status]);
//                    $this->info('Every minute request has been executed and updated votes');
                }else{
                    $email = "infomni611@gmail.com";
                    $name = "Elysee CONFIANCE";
                    $toemail = "ccelyse1@gmail.com";
                    $messageMail = "Every minute request has been executed but status has not changed";

                    Mail::send('backend.mail',
                        array(
                            'name' => $name,
                            'email' => $email,
                            'messageMail' => $messageMail
                        ), function($message) use ($email,$messageMail, $name,$toemail)
                        {
                            $message->from($email, "MNI");
                            $message->to($toemail, $name)->subject("MNI");
                        });
//                    $this->info('Every minute request has been executed but status has not changed');
                }

                //end Update to database Momo payments
            }
            //End Status showing
        }
//        $this->info('Every minute request has been executed');
        $name = "Elysee CONFIANCE";
        $email = "infomni611@gmail.com";
        $toemail = "ccelyse1@gmail.com";
        $messageMail = "Every minute request has been executed and updated votes";

        MAIL::send('backend.mail',
            array(
                'name' => $name,
                'email' => $email,
                'messageMail' => $messageMail
            ), function($message) use ($email,$messageMail, $name,$toemail)
            {
                $message->from($email, "MNI");
                $message->to($toemail, $name)->subject("MNI");
            });
        //end cron test
    }
}
