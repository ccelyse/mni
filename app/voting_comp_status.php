<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class voting_comp_status extends Model
{
    protected $table = "voting_comp_statuses";
    protected $fillable = ['user_id','voting_status'];

}
