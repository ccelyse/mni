<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contestants extends Model
{
//    use SoftDeletes;
    protected $table = "contestants";
    protected $fillable = ['names','youtube','photo','competition_id','group_id','user_id'];
}
