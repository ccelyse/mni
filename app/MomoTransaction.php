<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MomoTransaction extends Model

{   protected $table = "momotransaction";
    protected $fillable = ['phone','voter_id','transactionid', 'status', 'assignedid', 'company_name', 'code', 'amount','artist_name', 'artist_song', 'payment_code',
        'external_payment_code', 'payment_status', 'payment_type', 'callback_url', 'momodeleted_at', 'momocreated_at', 'momoupdated_at',
    ];

}
