<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpennTransactions extends Model
{
    protected $table = "spenn_transactions";
    protected $fillable = ['phonenumber','email','amount','status','vote','competition_id','contestant_id','voter_id','transaction_id'];
}
