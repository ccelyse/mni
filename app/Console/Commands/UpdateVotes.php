<?php

namespace App\Console\Commands;

use App\CompetitionTransactions;
use App\CompVotes;
use Illuminate\Console\Command;

class UpdateVotes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Update:Votes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update votes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('this request has been executed');
//        $compevotes = CompetitionTransactions::where('payment_status', 'SUCCESSFUL')->get();
//        foreach ($compevotes as $votes) {
//            $add_votes = new CompVotes();
//            $add_votes->vote = $votes->vote;
//            $add_votes->names = $votes->names;
//            $add_votes->competition_id = $votes->competition_id;
//            $add_votes->contestant_id = $votes->contestant_id;
//            $add_votes->status = $votes->payment_status;
//            $add_votes->save();
//            $this->info('this request has been executed');
//        }
    }
}