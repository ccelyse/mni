<?php

namespace App\Console\Commands;

use App\CompetitionTransactions;
use App\CompVotes;
use App\MomoTransaction;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CronCompetition extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Cron:Competition';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Pending Competition and update when is successful';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        $dt = Carbon::now()->addDay();
        $yesterdaydt = Carbon::now()->subDays(1);
        $tomorrow = $dt->toDateString();
        $yesterday = $yesterdaydt->toDateString();
        $transacs = \App\CompetitionTransactions::select('status','transactionid')
            ->where('status','PENDING')
            ->whereBetween('created_at', [$yesterday,$tomorrow])
            ->get();
        foreach($transacs as $transac){
            $transactionid = $transac->transactionid;
            $getVoter_id = CompetitionTransactions::where('transactionid',$transactionid)->value('contestant_id');
            $checkpayment = CompetitionTransactions::where('transactionid',$transactionid)->get();

            if(0 == count($checkpayment)){
                echo "Sorry, Transaction does not exist";
            }else{
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$transactionid",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_POSTFIELDS => "",
                    CURLOPT_HTTPHEADER => array(
                        "Accept: */*",
                        "Cache-Control: no-cache",
                        "Connection: keep-alive",
                        "Host: akokanya.com",
                        "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
                        "User-Agent: PostmanRuntime/7.11.0",
                        "accept-encoding: gzip, deflate",
                        "cache-control: no-cache",
                        "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
                        "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
                    ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                $responsedata=json_decode($response);

                $deletedat=0;

                $checkpayment_ = CompVotes::where('payment_tx_ref',$transactionid)->get();

                if($responsedata[0]->payment_status == "SUCCESSFUL" and $checkpayment_->isEmpty()){
                    $getVoter_id_ = CompetitionTransactions::where('transactionid',$transactionid)->get();

                    foreach ($getVoter_id_ as $voter){
                        $names_ = $voter->names;
                        $competition_id_ = $voter->competition_id;
                        $contestant_id_ = $voter->contestant_id;
                        $tx_ref = $voter->transactionid;
                    }
                    $new_votess = $responsedata[0]->amount / 50 ;
                    $addvote = new CompVotes();
                    $addvote->vote = $new_votess;
                    $addvote->names = $names_;
                    $addvote->competition_id = $competition_id_;
                    $addvote->contestant_id = $contestant_id_;
                    $addvote->payment_from = "MOMO";
                    $addvote->payment_tx_ref = $tx_ref;
                    $addvote->status = "SUCCESSFUL";
                    $addvote->save();
                    $last_id = $addvote->id;
                    $Momopayment = CompetitionTransactions::where('transactionid',$responsedata[0]->external_payment_code)
                        ->update([
                            'transactionid'=> $responsedata[0]->external_payment_code,
                            'status'=> $responsedata[0]->payment_status,
                            'assignedid'=> $responsedata[0]->id,
                            'company_name'=> $responsedata[0]->company_name,
                            'code'=> $responsedata[0]->code,
                            'vote'=> $new_votess,
                            'voter_id'=> $last_id,
                            'payment_code'=> $responsedata[0]->payment_code,
                            'external_payment_code'=> $responsedata[0]->external_payment_code,
                            'payment_status'=> $responsedata[0]->payment_status,
                            'payment_type'=> $responsedata[0]->payment_type,
                            'callback_url'=> $responsedata[0]->callback_url,
                            'momodeleted_at'=> $deletedat,
                            'momocreated_at'=> $responsedata[0]->created_at,
                            'momoupdated_at'=> $responsedata[0]->updated_at,
                        ]);
                    $this->info('Transaction successfully processed');
                }else{
                    $status = $responsedata[0]->payment_status;
                    $Momopayment = CompetitionTransactions::where('transactionid',$responsedata[0]->external_payment_code)
                        ->update([
                            'transactionid'=> $responsedata[0]->external_payment_code,
                            'status'=> $responsedata[0]->payment_status,
                            'assignedid'=> $responsedata[0]->id,
                            'company_name'=> $responsedata[0]->company_name,
                            'code'=> $responsedata[0]->code,
//                         'vote'=> $new_votess,
                            'payment_code'=> $responsedata[0]->payment_code,
                            'external_payment_code'=> $responsedata[0]->external_payment_code,
                            'payment_status'=> $responsedata[0]->payment_status,
                            'payment_type'=> $responsedata[0]->payment_type,
                            'callback_url'=> $responsedata[0]->callback_url,
                            'momodeleted_at'=> $deletedat,
                            'momocreated_at'=> $responsedata[0]->created_at,
                            'momoupdated_at'=> $responsedata[0]->updated_at,

                        ]);
                    $message = "Transaction is $status";
                    $this->info($message);
                }
            }
        }
    }
//    public function handle()
//    {
//        $stat='PENDING';
//
//        $dt = Carbon::now()->addDay();
//        $yesterdaydt = Carbon::now()->subDays(1);
////        $yesterdaydt = Carbon::now()->subDays(5);
//        $tomorrow = $dt->toDateString();
//        $yesterday = $yesterdaydt->toDateString();
//
////        $transactions = MomoTransaction::get()->count();
//
//        $transacs = \App\CompetitionTransactions::select('status','transactionid')
//            ->where('status','like','%'.$stat.'%')
//            ->whereBetween('created_at', [$yesterday,$tomorrow])
//            ->get();
////        foreach($transacs as $transac){
////
////            $curl = curl_init();
////
////            curl_setopt_array($curl, array(
////                CURLOPT_URL => "http://akokanya.com/api/mtn-integration/$transac->transactionid",
////                CURLOPT_RETURNTRANSFER => true,
////                CURLOPT_ENCODING => "",
////                CURLOPT_MAXREDIRS => 10,
////                CURLOPT_TIMEOUT => 30,
////                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
////                CURLOPT_CUSTOMREQUEST => "GET",
////                CURLOPT_POSTFIELDS => "",
////                CURLOPT_HTTPHEADER => array(
////                    "Accept: */*",
////                    "Cache-Control: no-cache",
////                    "Connection: keep-alive",
////                    "Host: akokanya.com",
////                    "Postman-Token: 8ee69e07-b01a-490c-8bb5-249ae3563665,a2e45631-b5fd-4ba1-95e6-0b6c2409059a",
////                    "User-Agent: PostmanRuntime/7.11.0",
////                    "accept-encoding: gzip, deflate",
////                    "cache-control: no-cache",
////                    "cookie: laravel_session=eyJpdiI6IjlYMkl6MSt4amVKdGdaTlREQkVTdVE9PSIsInZhbHVlIjoicHM5QWdlemZ3M0pVNTZWakEzOVozZFdcL0RJKzJ0WnNxV0FBdlZ3T1h0eEVHeUJcL21CTkRTaElnU1NwUGczeFVoNGxnUkRzcXJPb0xLeUg5QlJmZXZwUT09IiwibWFjIjoiYTM5MTA2NzdjYjE5YjFkZjlmNmU3ZmQ4ZjliN2ZiODc4ZjFjMWEzNDMwN2FmNjI1ZjFjNTU2ZjljZTZiNmRiOSJ9",
////                    "token: OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^"
////                ),
////            ));
////
////            $response = curl_exec($curl);
////            $err = curl_error($curl);
////
////            curl_close($curl);
////
////            if ($err) {
////                echo "cURL Error #:" . $err;
////            } else {
////                //echo $response;
////
////                $responsedata=json_decode($response);
//////                dd($responsedata);
////                //Update to database Momo payments
////                //echo $responsedata[0]->payment_status;
////
////                $deletedat=0;
////
////                $checkpayment_ = CompVotes::where('payment_tx_ref',$transac->transactionid)->get();
////
////                if($responsedata[0]->payment_status == "SUCCESSFUL"){
////
////                    $getVoter_id_ = CompetitionTransactions::where('transactionid',$transac->transactionid)->get();
////
////                    if(0 == count($checkpayment_)){
////                        foreach ($getVoter_id_ as $voter){
////                            $names_ = $voter->names;
////                            $competition_id_ = $voter->competition_id;
////                            $contestant_id_ = $voter->contestant_id;
////                            $tx_ref = $voter->transactionid;
////                        }
////                        $new_votess = $responsedata[0]->amount / 50 ;
////                        $addvote = new CompVotes();
////                        $addvote->vote = $new_votess;
////                        $addvote->names = $names_;
////                        $addvote->competition_id = $competition_id_;
////                        $addvote->contestant_id = $contestant_id_;
////                        $addvote->payment_from = "MOMO";
////                        $addvote->payment_tx_ref = $tx_ref;
////                        $addvote->status = "SUCCESSFUL";
////                        $addvote->save();
////                        $last_id = $addvote->id;
////                        $Momopayment = CompetitionTransactions::where('transactionid',$responsedata[0]->external_payment_code)
////                            ->update([
////                                'transactionid'=> $responsedata[0]->external_payment_code,
////                                'status'=> $responsedata[0]->payment_status,
////                                'assignedid'=> $responsedata[0]->id,
////                                'company_name'=> $responsedata[0]->company_name,
////                                'code'=> $responsedata[0]->code,
////                                'vote'=> $new_votess,
////                                'voter_id'=> $last_id,
////                                'payment_code'=> $responsedata[0]->payment_code,
////                                'external_payment_code'=> $responsedata[0]->external_payment_code,
////                                'payment_status'=> $responsedata[0]->payment_status,
////                                'payment_type'=> $responsedata[0]->payment_type,
////                                'callback_url'=> $responsedata[0]->callback_url,
////                                'momodeleted_at'=> $deletedat,
////                                'momocreated_at'=> $responsedata[0]->created_at,
////                                'momoupdated_at'=> $responsedata[0]->updated_at,
////
////                            ]);
////                        $this->info('Transaction successfully processed');
////                    }else{
////                        $this->info('Vote has been created');
////                    }
////                }else{
////                    $status = $responsedata[0]->payment_status;
////                    $Momopayment = CompetitionTransactions::where('transactionid',$responsedata[0]->external_payment_code)
////                        ->update([
////                            'transactionid'=> $responsedata[0]->external_payment_code,
////                            'status'=> $responsedata[0]->payment_status,
////                            'assignedid'=> $responsedata[0]->id,
////                            'company_name'=> $responsedata[0]->company_name,
////                            'code'=> $responsedata[0]->code,
//////                         'vote'=> $new_votess,
////                            'payment_code'=> $responsedata[0]->payment_code,
////                            'external_payment_code'=> $responsedata[0]->external_payment_code,
////                            'payment_status'=> $responsedata[0]->payment_status,
////                            'payment_type'=> $responsedata[0]->payment_type,
////                            'callback_url'=> $responsedata[0]->callback_url,
////                            'momodeleted_at'=> $deletedat,
////                            'momocreated_at'=> $responsedata[0]->created_at,
////                            'momoupdated_at'=> $responsedata[0]->updated_at,
////
////                        ]);
////                    $message = "Transaction is $status";
////                    $this->info($message);
////                }
////
////                //end Update to database Momo payments
////            }
////            //End Status showing
////        }
//        $this->info('all request has been executed');
//    }
}
