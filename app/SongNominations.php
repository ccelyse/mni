<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SongNominations extends Model
{
    protected $table = "songnominations";
    protected $fillable = ['id','user_nominee','user_session','artist','song'];
}
