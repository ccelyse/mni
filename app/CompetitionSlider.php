<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompetitionSlider extends Model
{
    use SoftDeletes;
    protected $table = "competition_sliders";
    protected $fillable = ['slider_title','slider_description','slider_photo','slider_status','competition_id','user_id'];
}
