<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VotingCardTransactions extends Model
{
    use SoftDeletes;
    protected $table = "voting_card_transactions";
    protected $fillable = ['first_name','last_name','phonenumber','email','city','country','amount','tx_ref','currency','status',
        'payment_options','voter_id','song_name','song_artist','transaction_id'];
}
