<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Funds extends Model
{
    protected $table = "funds";
    protected $fillable = ['fund_title','fund_description','fund_image','fund_amount','fund_status','fund_user_id'];

}
