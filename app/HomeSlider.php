<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeSlider extends Model
{
    protected $table = "home_sliders";
    protected $fillable = ['slider_title','slider_description','slider_photo'];
}
