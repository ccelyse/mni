<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompetitionTransactions extends Model
{
    use SoftDeletes;
    protected $table = "competition_transactions";
    protected $fillable = ['vote','phonenumber','names','competition_id','contestant_id','transactionid', 'status', 'assignedid', 'company_name', 'code', 'amount','payment_code',
        'external_payment_code', 'payment_status', 'payment_type', 'callback_url', 'momodeleted_at', 'momocreated_at', 'momoupdated_at','voter_id'];
}
