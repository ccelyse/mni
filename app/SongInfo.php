<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SongInfo extends Model
{
    protected $table = "songinfo";
    protected $fillable = ['id','playlist_id','archives_status','song_artist','song_name','song_youtube_link','song_cover_picture','song_number','created_at','promotion_status','promotion_period'];
}
