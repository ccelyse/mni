<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentStatus extends Model
{
    protected $table = 'comment_statuses';
    protected $fillable = ['comment_status'];
}
