<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompVotes extends Model
{
    use SoftDeletes;
    protected $table = "comp_votes";
    protected $fillable = ['vote','names','competition_id','contestant_id','contestant_id','status','payment_from','payment_tx_ref'];
}
