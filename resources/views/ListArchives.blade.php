@extends('layouts.newmaster')

@section('title', 'Urutonde rw\'Ukwezi|MNI Selection|Indirimbo z\'Ikunzwe mu Gihugu')

@section('content')
<style>
    .spb-asset-content p{
        color:#fff !important;
    }
    .pagination li a{
        color:#e2a34c;
    }
    .title-wrap h3{
        color: #fff !important;
    }
    .chart-page .chart-detail-header__chart-name {
        padding-top: 50px !important;
    }
    .modal-backdrop{
        position: relative !important;
    }
    /*.modal-backdrop.show {*/
        /*opacity: .5;*/
    /*}*/

    .modal-dialog {
        max-width: 800px !important;
        margin: 30px auto;
    }
    iframe{
        width: 100% !important;
    }

    .single-blog-post a,.single-blog-post p{
        color: #fff;
    }
    .bottom-title{
        font-size: 24px;
        line-height: 34px;
        color: #fff;
        padding: 45px 0 35px;
    }
    .title-box{
        display: inline-block;
        line-height: 35px;
        font-size: 20px;
        text-transform: uppercase;
        color: #fff;
        padding: 0 30px;
        border: 1px solid rgba(255,255,255,0.2);
        border-radius: 3px;
        font-weight: bold;
    }
    .contact-us-section{
        background: #1c706f;
    }
    .contact-us-section .contact-info .title {
        font-family: 'CircularStdmed';
        font-size: 42px;
        line-height: 55px;
        color: #fff;
        margin: -8px 0 25px;
        text-align: center;
    }
    .contact-us-section .contact-info ul li a {
        color: #fff;
        width: 100px !important;
    }
    .contact-us-section .contact-info ul li a:hover {
        color: #fff;
    }
    /*.contact-us-section .contact-info ul li a {*/
        /*width: 100px;*/
        /*line-height: 36px;*/
        /*border: 2px solid #1c706f;*/
        /*border-radius: 50%;*/
        /*text-align: center;*/
        /*font-size: 18px;*/
        /*color: #1c706f;*/
        /*margin-right: 5px;*/
        /*padding: 23px;*/
    /*}*/
    #theme-banner-five .main-wrapper .button-group li a{
        padding: 10px;
        border-radius: 30px;
        bottom: 10px;
        background: #266664;
    }
    #theme-banner-five .main-wrapper .button-group li a:hover{
        background: #fff;
        color:#266664 !important;
    }
    .howtovote{
        text-align: center;
        font-size: 35px;
        font-weight: bold;
        color: #1c706e;
        padding: 10px;
    }
    #theme-banner-five {
        /* background: linear-gradient( 145deg, rgb(132,0,252) 0%, rgb(17,222,244) 100%); */
        /* background: linear-gradient( 145deg, rgb(5, 54, 50) 0%, rgb(28, 112, 111) 100%); */
        position: relative;
        z-index: 5;
        background: url(./front/images/audience-band-celebration-1190298-min.jpg) !important;
        background-size: cover !important;
    }
    .muzikahashtag{
        position: relative;
        left: 0;
        right: 0;
        color: #fff;
        font-size: 25px;
        text-align: right;
        padding: 25px;
    }
    .why-choose-us-app .text-wrapper .director-speech .bio-block span {
        font-size: 18px;
        color: #1c706e;
        font-style: normal;
    }
    @media (max-width: 649px){
        #theme-banner-five {
             background-position: center !important;
        }
    }
    .position_song{
        color: #1c706e;
        position: relative;
        font-size: 50px;
        left: 5px;
        top: 10%;
    }
    .songnumber{
        background: #fff;
        margin-bottom: 5px;
        text-align: center;
        /*border: 1px solid #1c706e;*/
    }
</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script
        src="https://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>
@include('layouts.newtopmenu')

<div class="why-choose-us-app">
    <div class="container">
        <div class="pricing-tab-menu mt-30">
            <div class="howtovote">MNI Selection</div>
            {{--<div class="nav-wrapper">--}}
                {{--<ul class="nav nav-tabs justify-content-center">--}}
                    {{--<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#Modern">Modern</a></li>--}}
                    {{--<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Gospel">Gospel</a></li>--}}
                    {{--<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Traditional">Traditional</a></li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        </div> <!-- /.pricing-tab-menu -->
    </div> <!-- /.container -->

    <div class="tab-content" id="ToraIndirimbo" style="padding-top: 30px;">
        <!-- ^^^^^^^^^^^^^^^^^^^^^ Monthly ^^^^^^^^^^^^^^^^^^^^^^^^^^^ -->

        <div id="Modern" class="tab-pane fade show active">
            <div class="row justify-content-md-center">
                <div class="col-lg-6" style="padding-top: 15px">

                        <div class="text-wrapper">
                            <div id="resultsM">

                                    @foreach($all as $index => $getmusicsmodern)

                                    <div class="row" style="border: 1px solid #1c706f;margin-bottom: 5px;">
                                        <a style="display: flex;" href="{{ route('Archives',['id'=> $getmusicsmodern->id])}}">
                                        <div class="col-md-2 songnumber">
                                            <p class="position_song">{{ $index+1 }}</p>
                                        </div>
                                        <div class="col-md-10 songinfo">
                                            <div class="director-speech clearfix">
                                                <img src="SongImages/{{$getmusicsmodern->archive_name}}" alt="" class="d-img">
                                                <div class="bio-block">
                                                    <h6 class="name" >{{$getmusicsmodern->archive_name}}</h6>
                                                    <h6 class="name">
                                                    <span style="font-size: 12px; !important;">
                                                        <?php
                                                        $created_at =  $getmusicsmodern->created_at;
                                                        $lastTimeLoggedOut = \Illuminate\Support\Carbon::parse($created_at)->diffForHumans();
                                                        echo $lastTimeLoggedOut;
                                                        ?>
                                                    </span>
                                                    </h6>

                                                </div> <!-- /.bio-block -->
                                            </div> <!-- /.director-speech -->
                                        </div>
                                        </a>
                                    </div>

                                @endforeach
                            </div>
                            {{--<div class="text-wrapper md-center">--}}
                                {{--<a href="" class="title-box" style="text-align: center;">See More</a>--}}
                            {{--</div> <!-- /.text-wrapper -->--}}
                            <div id="displayModern">
                            </div>
                            {{--{{ $getmusicmodern->links() }}--}}
                        </div> <!-- /.text-wrapper -->
                </div>

            </div>
        </div> <!-- /#modern -->


    </div>

</div> <!-- /.agn-our-pricing -->

<script type="text/javascript">
    $(document).ready(function(){
        $('.modernSearch input[type="text"]').on("keyup input", function() {
            /* Get input value on change */
            var inputValM = $(this).val();
//                alert(inputValM);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "SearchVotingM",
                data: {
                    'modernSearch': inputValM,

                },
                success: function (data) {
                    document.getElementById('resultsM').style.display = 'none';
                    $("#displayModern").html(data);
                }
            });
        });
    });

</script>
@include('layouts.newfooter')
@endsection
