
<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta name="format-detection" content="telephone=no">
    <script src="front/js/3ts2ksMwXvKRuG480KNifJ2_JNM.js"></script>
    <link rel="icon" href="front/images/logo/logo_icon.png" type="image/x-icon">
    <link rel="stylesheet" href="front/css/grid.css">
    <link rel="stylesheet" href="front/css/stylev2.css">
    <link rel="stylesheet" href="front/css/camera.css">
    <link rel="stylesheet" href="front/css/owl-carousel.css">
    <link rel="stylesheet" href="front/css/styleswitcher.css"/>
    <link rel="stylesheet" type="text/css" href="front/css/prettyPhoto.css" />
    <link href="front/css/jquery.bsPhotoGallery.css" rel="stylesheet">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-156034751-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-156034751-1');
    </script>
    <script src="front/js/jquery.js"></script>
    <script src="front/js/jquery-migrate-1.2.1.js"></script><!--[if lt IE 9]>
    <html class="lt-ie9">
    <div style="clear: both; text-align:center; position: relative;">
        <a href="https://windows.microsoft.com/en-US/internet-explorer/..">
            <img src="https://livedemo00.template-help.com/wt_55566/images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today.">
        </a>
    </div>
    </html>
    <script src="front/js/html5shiv.js"></script><![endif]-->
    <script src="front/js/device.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<style>
    #Checked{
        display: none;
    }
</style>
<body>

@yield('content')

<script src="front/js/script.js"></script>
</body>
</html>