<style>
    .footer_social li{
        float: left;
        padding: 10px;
        font-size: 35px;
        margin-top: 0px;
    }
</style>
<footer>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <h3>Our Contact</h3>
                    <ul>
{{--                        <li><a href=#" class="email" style="color: #fff;font-size: 13px;"><i class="fa fa-location-arrow" ></i> Kigali ,Rwanda</a></li>--}}
                        <li><a href="mailto:info@mni.rw" class="email" style="color: #fff;font-size: 13px;"><i class="fa fa-envelope-square" ></i> info@mni.rw</a></li>
                        <li><a href="tel:+250789813910" class="phone" style="color: #fff;font-size: 13px;"><i class="fa fa-phone" ></i> 0789813910</a></li>
                        <li><a href="tel:+250780008883" class="phone" style="color: #fff;font-size: 13px;"><i class="fa fa-phone" ></i> 0780008883</a></li>
                        <li><a href="tel:+250784702692" class="phone" style="color: #fff;font-size: 13px;"><i class="fa fa-phone" ></i> 0784702692</a></li>
                    </ul>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <h3>Important Links</h3>
                    <ul>
                        <li><a href="{{'Faq'}}" style="color: #fff;font-size: 13px;">FAQ</a></li>
                        <li><a href="{{'PrivacyPolicy'}}" style="color: #fff;font-size: 13px;">Privacy policy</a></li>
                        <li><a href="{{'TermsAndConditions'}}" style="color: #fff;font-size: 13px;">Terms & conditions</a></li>
                        <li><a href="{{'TermsAndConditionsStakeHolders'}}" style="color: #fff;font-size: 13px;">TERMS & CONDITIONS FOR MUSIC STAKEHOLDERS</a></li>
                        <li><a href="{{'AboutUs'}}" style="color: #fff;font-size: 13px;">About Us</a></li>
                        <li><a href="{{'SalesAndMarketing'}}" style="color: #fff;font-size: 13px;">Sales & Marketing</a></li>

                    </ul>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <h3>Partners</h3>
                    <img src="front/images/MNI_PARTNERS-min.png" style="width: 200px;" alt="">
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <h3><a href="AboutUs#ourbrands" style="color: white">Our brands</a></h3>
                    <ul>
                        <li><a href="{{'Charts'}}" style="color: #fff;font-size: 13px;">MNI Selection</a></li>
                        <li><a href="{{'Broadcast'}}" style="color: #fff;font-size: 13px;">MNI Broadcast</a></li>
                        <li><a href="{{'Fundraising'}}" style="color: #fff;font-size: 13px;">MNI Fund</a></li>
                        <li><a href="#" style="color: #fff;font-size: 13px;">MNI Selection Award</a></li>
                        <li><a href="#" style="color: #fff;font-size: 13px;">MNI Tarama</a></li>
                    </ul>
                </div>
            </div>
            <a href="{{'/'}}" ><img src="front/images/logo/logo.png" style="width: 100px;" alt=""></a>
            <a href="{{'/'}}" ><h1>Muzika Nyarwanda Ipande</h1></a>
            <div class="copyright">
                <p>© <span id="copyright-year"></span>
                    &nbsp;<a href="{{'PrivacyPolicy'}}">Privacy policy</a>
                </p>
                <p>
                <ul class="footer_social">
                    <li>
                        <a href="https://www.facebook.com/aboutMNI " target="_blank"> <span class="fab fa-facebook"></span></a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/aboutmni/" target="_blank"> <span class="fab fa-instagram"></span></a>
                    </li>
                    <li>
                        <a href="https://twitter.com/aboutMNI" target="_blank"> <span class="fab fa-twitter"></span></a>
                    </li>
                    <li>
                        <a href="https://www.youtube.com/channel/UC2kHMCoGVRYmaTeG5APFBPA?view_as=subscriber" target="_blank"> <span class="fab fa-youtube"></span></a>
                    </li>
                </ul>
                </p>
            </div>
        </div>
    </section>
</footer>