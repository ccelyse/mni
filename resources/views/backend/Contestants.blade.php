@extends('backend.layout.master')

@section('title', 'MNI')

@section('content')
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }
        .btn-primary:hover{
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }

        .btn-secondary{
            color:#fff !important;
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }

        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        iframe{
            width: 100%;
        }

    </style>
    {{--<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">--}}



    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif
            <div class="content-body">
                <div class="content-body">

                    <section id="form-control-repeater">
                        <div class="row">
                            <div class="col-12">
                                <button type="button" class="btn btn-icon btn-primary btn-min-width mr-1 mb-1"
                                        data-toggle="modal"
                                        data-target="#addarchives">Add new contestant
                                </button>
                                <!-- Modal -->
                                <div class="modal fade text-left" id="addarchives" tabindex="-1"
                                     role="dialog" aria-labelledby="myModalLabel1"
                                     aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form class="form-horizontal form-simple" method="POST" action="{{ url('AddContestants') }}" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <div class="row">

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Competition Name</label>
                                                                <div class="input-group">
                                                                    <select class="form-control" name="competition_id">
                                                                        @foreach($competition as $comp)
                                                                        <option value="{{$comp->id}}">{{$comp->competition_title}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Group Name</label>
                                                                <div class="input-group">
                                                                    <select class="form-control" name="group_id">
                                                                        @foreach($group as $groups)
                                                                            <option value="{{$groups->id}}">{{$groups->group_name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Names</label>
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="names" required/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Youtube</label>
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="youtube"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Photo</label>
                                                                <div class="input-group">
                                                                    <input type="file" class="form-control" name="photo" required/>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group" style="margin-top: 20px">
                                                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-12">
                                <div class="card">

                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Transfer</th>
                                                    <th>Votes</th>
                                                    <th>Name</th>
                                                    <th>Competition Name</th>
                                                    <th>Group Name</th>
                                                    <th>Photo</th>
                                                    <th>Youtube</th>
                                                    <th>Date Created</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($contestant as $data)
                                                    <tr>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                                    data-toggle="modal"
                                                                    data-target="#transfer{{$data->id}}">Transfer
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="transfer{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('TransferContestant') }}" enctype="multipart/form-data">
                                                                                {{ csrf_field() }}
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Names</label>
                                                                                            <div class="input-group">
                                                                                                <input type="text" class="form-control" name="names" value="{{$data->names}}"/>
                                                                                                <input type="text" class="form-control" name="id" value="{{$data->id}}" hidden/>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Group Name</label>
                                                                                            <div class="input-group">
                                                                                                <select class="form-control" name="group_id">
                                                                                                    <option value="{{$data->group_id}}">{{$data->group_name}}</option>
                                                                                                    @foreach($group as $groups)
                                                                                                        <option value="{{$groups->id}}">{{$groups->group_name}}</option>
                                                                                                    @endforeach
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group" style="margin-top: 20px">
                                                                                            <button type="submit" class="btn btn-primary" onclick="return confirm('Are you sure you would like to transfer this record?');"> <i class="la la-check-square-o"></i> Transfer</button>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                            </form>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>{{$data->votesNumber}}</td>
                                                        <td>{{$data->names}}</td>
                                                        <td>{{$data->competition_title}}</td>
                                                        <td>{{$data->group_name}}</td>
                                                        <td><img src="Contestant/{{$data->photo}}" style="width:100px"></td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                                    data-toggle="modal"
                                                                    data-target="#youtube{{$data->id}}">Youtube
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="youtube{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <?php
                                                                            $video = $data->youtube;
                                                                            echo $video;
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>{{$data->created_at}}</td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                                    data-toggle="modal"
                                                                    data-target="#archives{{$data->id}}">Edit
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="archives{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('EditContestants') }}" enctype="multipart/form-data">
                                                                                {{ csrf_field() }}
                                                                                <div class="row">

                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Competition Name</label>
                                                                                            <div class="input-group">
                                                                                                <select class="form-control" name="competition_id">
                                                                                                    <option value="{{$data->competition_id}}">{{$data->competition_title}}</option>
                                                                                                    @foreach($competition as $comp)
                                                                                                        <option value="{{$comp->id}}">{{$comp->competition_title}}</option>
                                                                                                    @endforeach
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Group Name</label>
                                                                                            <div class="input-group">
                                                                                                <select class="form-control" name="group_id">
                                                                                                    <option value="{{$data->group_id}}">{{$data->group_name}}</option>
                                                                                                    @foreach($group as $groups)
                                                                                                        <option value="{{$groups->id}}">{{$groups->group_name}}</option>
                                                                                                    @endforeach
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Names</label>
                                                                                            <div class="input-group">
                                                                                                <input type="text" class="form-control" name="names" value="{{$data->names}}"/>
                                                                                                <input type="text" class="form-control" name="id" value="{{$data->id}}" hidden/>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Youtube</label>
                                                                                            <div class="input-group">
                                                                                                <input type="text" class="form-control" name="youtube" value="{{$data->youtube}}"/>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Photo</label>
                                                                                            <div class="input-group">
                                                                                                <input type="file" class="form-control" name="photo"/>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12" style="padding-bottom: 15px">
                                                                                        <img src="Contestant/{{$data->photo}}" style="width:50%">
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group" style="margin-top: 20px">
                                                                                            <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                            </form>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><a href="{{ route('backend.DeleteContestant',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary" onclick="return confirm('Are you sure you would like to delete this this slider?');">Delete</a></td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>--}}
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>--}}
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/jszip.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/pdfmake.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/vfs_fonts.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.html5.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.print.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.colVis.min.js"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables-extensions/datatable-button/datatable-html5.js"></script>
@endsection
