@extends('backend.layout.master')

@section('title', 'MNI')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
<style>
    .height-75 {
        height: 10px!important;
    }
</style>
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">

        </div>

        <div class="content-body">
            <div class="card-header" style="margin-bottom: 30px">
                <h4 class="card-title">Dashboard</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                </div>
            </div>
            <div id="crypto-stats-3" class="row">

                @foreach($getmusicmodern as $data)
                <div class="col-xl-4 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-music font-large-1" title="BTC"></i></h1>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <h4>Song Name</h4>
                                    </div>
                                    <div class="col-10 text-right">
                                        <h4>{{$data->song_name}}</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="btc-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-users warning blue-grey lighten-1 font-large-1" title="ETH"></i></h1>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <h4>Votes</h4>
                                    </div>
                                    <div class="col-10 text-right">
                                        <h4>
                                            <?php
                                            $votesbe = $data->votesnumber;
                                            $newvotes = $votesbe - 1;
                                            echo "Month votes: $newvotes" ?></h4>
                                        <h4>
                                            <?php
                                            $song_id = $data->id;
                                            $votes = \App\Votes::where('voter_status','SUCCESSFUL')->select(DB::raw('count(id) as votes'))
                                                ->where('song_id',$song_id)
                                                ->where('vote','1')
                                                ->value('votes');
                                            $newvotes = $votes - 1;
                                            echo "Total votes: $newvotes";
                                            ?>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="eth-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-money-bill font-large-1" title="BTC"></i></h1>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <h4>Monthly Royalty Payment</h4>
                                    </div>
                                    <div class="col-5 text-right">
                                        <h4><?php $votes = $data->votesnumber; echo ($votes*25)-25;?> FRW</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="btc-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>

            {{--<div class="row">--}}
                {{--<div class="col-12">--}}
                    {{--<div class="card">--}}
                        {{--<div class="card-content">--}}
                            {{--<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            <!-- Active Orders -->
        </div>
    </div>
</div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
<!-- ////////////////////////////////////////////////////////////////////////////-->

@endsection