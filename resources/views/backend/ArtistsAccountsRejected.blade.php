@extends('backend.layout.master')

@section('title', 'MNI')

@section('content')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }
        .btn-primary:hover{
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }

        .btn-secondary{
            color:#fff !important;
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }

        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        iframe{
            width: 100%;
        }

    </style>
    <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            $('.multi-field-wrapper').each(function() {
                var $wrapper = $('.multi-fields', this);
                $(".add-field", $(this)).click(function(e) {
                    $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                });
                $('.multi-field .remove-field', $wrapper).click(function() {
                    if ($('.multi-field', $wrapper).length > 1)
                        $(this).parent('.multi-field').remove();
                });
            });
        });
    </script>
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')


    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <div class="content-body">
                    <div class="card-header" style="margin-bottom: 30px">
                        <h4 class="card-title">Artists rejected account</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Basic Summernote start -->
                    @if (session('success'))
                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                            {{ session('success') }}
                        </div>
                    @endif
                    <section id="form-control-repeater">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Names</th>
                                                    <th>Artistic Names</th>
                                                    <th>Mobile number</th>
                                                    <th>Email</th>
                                                    <th>ID no/Valid passport no</th>
                                                    <th>Mode of Payment</th>

                                                    <th>Account number</th>
                                                    <th>Name on the account</th>
                                                    <th>Bank name</th>

                                                    <th>Number to receive payment</th>
                                                    <th>Name on the number that must receive payment</th>

                                                    <th>Facebook profile link</th>
                                                    <th>Instagram profile link</th>
                                                    <th>Twitter profile link</th>
                                                    <th>Photo</th>
                                                    <th>Date Created</th>
                                                    <th>Approve</th>
                                                    {{--<th>Reject</th>--}}
                                                    <th>Delete</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($listaccounts as $data)
                                                    <tr>
                                                        <td>{{$data->names}}</td>
                                                        <td>{{$data->artistic_names}}</td>
                                                        <td>{{$data->artist_number}}</td>
                                                        <td>{{$data->artist_email}}</td>
                                                        <td>{{$data->artist_ID}}</td>
                                                        <td>{{$data->artist_modeofpayment}}</td>
                                                        <td>{{$data->artist_bankaccount_number}}</td>
                                                        <td>{{$data->artist_bankaccount_names}}</td>
                                                        <td>{{$data->artist_bank_name}}</td>
                                                        <td>{{$data->artist_mobilemoney_number}}</td>
                                                        <td>{{$data->artist_mobilemoney_names}}</td>
                                                        <td><a href="{{$data->artist_facebook}}" class="btn btn-icon btn-outline-primary" target="_blank">Facebook profile link</a></td>
                                                        <td><a href="{{$data->artist_instagram}}" class="btn btn-icon btn-outline-primary" target="_blank">Instagram profile link</a></td>
                                                        <td><a href="{{$data->artist_twitter}}" class="btn btn-icon btn-outline-primary" target="_blank">Twitter profile link</a></td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                                    data-toggle="modal"
                                                                    data-target="#editplaylist{{$data->id}}">Photo
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="editplaylist{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <img src="ArtistPhoto/{{$data->picturename}}" style="width:100%">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>{{$data->created_at}}</td>
                                                        <td><a href="{{ route('backend.ApproveSendSMS',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary" onclick="return confirm('Are you sure you would like to approve this account?');">Approve</a></td>
                                                        {{--<td><a href="{{ route('backend.RejectSendSMS',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Reject</a></td>--}}
                                                        <td><a href="{{ route('backend.DeleteArtistAccount',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary" onclick="return confirm('Are you sure you would like to delete this account?');">Delete</a></td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>--}}
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>--}}
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/jszip.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/pdfmake.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/vfs_fonts.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.html5.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.print.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.colVis.min.js"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables-extensions/datatable-button/datatable-html5.js"></script>
@endsection
