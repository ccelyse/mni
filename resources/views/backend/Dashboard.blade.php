@extends('backend.layout.master')

@section('title', 'MNI')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <style>
        .card-content{
            background: #1c706e;
            border-radius: 5px;
        }
        .card-content h4{
            color: #fff;
            font-weight: bold;
        }
        .font-large-1 {
            color: #fff;
        }
        /*.main-menu.menu-light {*/
        /*    color: #fff !important;*/
        /*    background: #1c706e !important;*/
        /*    border-right: 1px solid #1c706e !important;*/
        /*}*/
        /*.main-menu.menu-light .navigation {*/
        /*    background: #1c706e !important;*/
        /*}*/
        /*.main-menu.menu-light .navigation li a {*/
        /*    color: #fff !important;*/
        /*}*/
        .height-75 {
            height: 10px!important;
        }
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
        .alert-success {
            border-color: #032a29!important;
            background-color: #032a29!important;
            color: #fff !important;
        }
    </style>
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <div class="card-header" style="margin-bottom: 30px">
                <h4 class="card-title">Dashboard</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                    </ul>
                </div>
            </div>
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif
            <div class="alert alert-success text-center" id="voting_noti" style="width:100%;margin-top: 10px;display: none"></div>

            <div id="crypto-stats-3" class="row">

                <div class="col-xl-4 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-10 pl-2">
                                        <h4> Enable / Disable voting</h4>
                                    </div>
                                    <div class="custom-control custom-switch">
                                        <label class="switch">
                                            <input type="checkbox" id="checkbox1" {{$check_status}}>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-10 pl-2">
                                        <h4> Enable / Disable Comments</h4>
                                    </div>
                                    <div class="custom-control custom-switch">
                                        <label class="switch">
                                            <input type="checkbox" id="checkbox2" {{$comment_status}}>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-12" data-toggle="modal" data-target="#refresh_momo">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-10 pl-2">
                                        <h4>Refresh Momo transaction</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="btc-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-music font-large-1" title="BTC"></i></h1>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <h4>Number of Songs</h4>
                                    </div>
                                    <div class="col-5 text-right">
                                        <h4><?php echo $songs;?></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="btc-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="col-xl-4 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-users warning blue-grey lighten-1 font-large-1" title="ETH"></i></h1>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <h4>Pending votes</h4>
                                    </div>
                                    <div class="col-5 text-right">
                                        <h4><?php echo $votespending;?></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="eth-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-users warning blue-grey lighten-1 font-large-1" title="ETH"></i></h1>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <h4>Monthly Successful votes</h4>
                                    </div>
                                    <div class="col-5 text-right">
                                        <h4>{{$getvotesM}}</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="eth-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-users warning blue-grey lighten-1 font-large-1" title="ETH"></i></h1>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <h4>Successful votes</h4>
                                    </div>
                                    <div class="col-5 text-right">
                                        <h4><?php
                                                $newvotes = $votessuccessful - $countSongs;
                                            echo $newvotes;
                                            ?></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="eth-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-users warning blue-grey lighten-1 font-large-1" title="ETH"></i></h1>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <h4>Approved Accounts</h4>
                                    </div>
                                    <div class="col-5 text-right">
                                        <h4><?php echo $approvedaccounts;?></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="eth-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-users warning blue-grey lighten-1 font-large-1" title="ETH"></i></h1>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <h4>Rejected Accounts</h4>
                                    </div>
                                    <div class="col-5 text-right">
                                        <h4><?php echo $rejectaccounts;?></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="eth-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-users warning blue-grey lighten-1 font-large-1" title="ETH"></i></h1>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <h4>Pending Accounts</h4>
                                    </div>
                                    <div class="col-5 text-right">
                                        <h4><?php echo $pendingaccounts;?></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="eth-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-chart-line font-large-1" title="BTC"></i></h1>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <h4>Competition</h4>
                                    </div>
                                    <div class="col-10 text-right">
                                        <h4>{{$Competitions}}</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="btc-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-users font-large-1" title="BTC"></i></h1>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <h4>Contestants</h4>
                                    </div>
                                    <div class="col-10 text-right">
                                        <h4>{{$Contestants}}</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="btc-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-users font-large-1" title="BTC"></i></h1>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <h4>Competition Pending Transaction</h4>
                                    </div>
                                    <div class="col-10 text-right">
                                        <h4>{{$competitionpending}}</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="btc-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-users font-large-1" title="BTC"></i></h1>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <h4>Competition Successful Transaction</h4>
                                    </div>
                                    <div class="col-10 text-right">
                                        <h4>{{$competitionsuccessful}}</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="btc-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-chart-pie font-large-1" title="BTC"></i></h1>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <h4>Monthly Successful Competition Votes</h4>
                                    </div>
                                    <div class="col-10 text-right">
                                        <h4>{{$votes_monthly}}</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="btc-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-12">
                    <div class="card crypto-card-3 pull-up">
                        <div class="card-content">
                            <div class="card-body pb-0">
                                <div class="row">
                                    <div class="col-2">
                                        <h1><i class="fas fa-chart-pie font-large-1" title="BTC"></i></h1>
                                    </div>
                                    <div class="col-10 pl-2">
                                        <h4>Total Successful Competition Votes</h4>
                                    </div>
                                    <div class="col-10 text-right">
                                        <h4>{{$votes}}</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <canvas id="btc-chartjs" class="height-75"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade text-left" id="refresh_momo" tabindex="-1"
                 role="dialog" aria-labelledby="myModalLabel1"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="#" id="ClientRecord">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="projectinput1">Start date</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="ft-calendar"></i></span>
                                                </div>
                                                <input type="date" class="form-control dp-month-year" name="startdate" value="" required/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="projectinput1">End date</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="ft-calendar"></i></span>
                                                </div>
                                                <input type="date" class="form-control dp-month-year" name="enddate" value="" required/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="alert alert-success text-center" id="refresh_status" style="width:100%;margin-top: 10px;display: none"></div>
                                        <img src="front/images/loading-gif-transparent-4.gif" style="max-width: 100px;display:none; margin: 0 auto;" id="loading">
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group" style="margin-top: 20px">
                                            <button type="button" class="btn btn-primary" id="refreshmomo"> <i class="la la-check-square-o"></i>
                                                Refresh</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                </div>
            </div>

            {{--<div class="row">--}}
                {{--<div class="col-12">--}}
                    {{--<div class="card">--}}
                        {{--<div class="card-content">--}}
                            {{--<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            <!-- Active Orders -->
        </div>
    </div>
</div>
    <script type="text/javascript">
        $(document).ready(function() {
            //set initial state.
            $("#checkbox1").change(function(){
                if ($(this).is(':checked')){
                    // alert('Checked!');
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: "POST",
                        url: "api/VotingStatus",
                        data: {
                            'voting_status': 'Checked',

                        },
                        success: function (response) {
                            JSON.stringify(response); //to string
                            console.log(response);
                            jQuery('#voting_noti').show();
                            document.getElementById("voting_noti").style.display = "inherit";
                            jQuery('#voting_noti').append('<p>' + response.response_message + '</p>');
                            setTimeout(function() {
                                window.location.reload()
                            }, 2000);
                        },
                        error: function(xhr, status, error) {
                            console.log(xhr.responseText);
                        }
                    });
                }else{
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: "POST",
                        url: "api/VotingStatus",
                        data: {
                            'voting_status': 'NotChecked',

                        },
                        success: function (response) {
                            JSON.stringify(response); //to string
                            console.log(response);
                            jQuery('#voting_noti').show();
                            document.getElementById("voting_noti").style.display = "inherit";
                            jQuery('#voting_noti').append('<p>' + response.response_message + '</p>');
                            setTimeout(function() {
                                window.location.reload()
                            }, 2000);
                        },
                        error: function(xhr, status, error) {
                            console.log(xhr.responseText);
                        }
                    });
                }

            });
        });

        $(document).ready(function() {
            //set initial state.
            $("#checkbox2").change(function(){
                if ($(this).is(':checked')){
                    // alert('Checked!');
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: "POST",
                        url: "api/CommentStatus",
                        data: {
                            'comment_status': 'Checked',

                        },
                        success: function (response) {
                            JSON.stringify(response); //to string
                            console.log(response);
                            jQuery('#voting_noti').show();
                            document.getElementById("voting_noti").style.display = "inherit";
                            jQuery('#voting_noti').append('<p>' + response.response_message + '</p>');
                            setTimeout(function() {
                                window.location.reload()
                            }, 2000);
                        },
                        error: function(xhr, status, error) {
                            console.log(xhr.responseText);
                        }
                    });
                }else{
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: "POST",
                        url: "api/CommentStatus",
                        data: {
                            'comment_status': 'NotChecked',

                        },
                        success: function (response) {
                            JSON.stringify(response); //to string
                            console.log(response);
                            jQuery('#voting_noti').show();
                            document.getElementById("voting_noti").style.display = "inherit";
                            jQuery('#voting_noti').append('<p>' + response.response_message + '</p>');
                            setTimeout(function() {
                                window.location.reload()
                            }, 2000);
                        },
                        error: function(xhr, status, error) {
                            console.log(xhr.responseText);
                        }
                    });
                }

            });
        });
        $(document).on('click', '#refreshmomo', function () {
            $('#refreshmomo').html('Refreshing Transactions..');
            var formData = $("#ClientRecord").serialize();
            document.getElementById("loading").style.display = "block";
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "api/RefreshMomo",
                data: formData,
                dataType: 'json',

                success: function (response) {
                    document.getElementById("loading").style.display = "none";
                    JSON.stringify(response); //to string
                    console.log(response);

                    document.getElementById("refresh_status").style.display = "inherit";
                    jQuery('#refresh_status').append('<p>' + response.message + '</p>');
                    $('#refreshmomo').html('Refresh');

                }, error: function (xhr, status, error) {
                    console.log(xhr.responseText);
                }
            });

        });

    </script>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
<!-- ////////////////////////////////////////////////////////////////////////////-->

@endsection