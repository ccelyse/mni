@extends('backend.layout.master')

@section('title', 'MNI')

@section('content')
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }
        .btn-primary:hover{
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }

        .btn-secondary{
            color:#fff !important;
            background-color: #032b2a !important;
            border-color:#032b2a !important;
        }

        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        iframe{
            width: 100%;
        }

        .pagination {
            display: inline-block;
        }

        .pagination li {
            color: black;
            float: left;
            padding: 8px 16px;
            text-decoration: none;
        }
        .pagination li.active {
            background-color: #032a29;
            color: white;
        }

        .pagination li:hover:not(.active) {background-color: #ddd;}
        div.dataTables_wrapper div.dataTables_paginate {
            margin: 0;
            white-space: nowrap;
            text-align: right;
            display: none;
        }
        .pagination li {
            border: 1px solid #ddd; /* Gray */
        }
        .pagination li:first-child {
            border-top-left-radius: 5px;
            border-bottom-left-radius: 5px;
        }

        .pagination li:last-child {
            border-top-right-radius: 5px;
            border-bottom-right-radius: 5px;
        }
        .pagination li {
            border-radius: 5px;
            margin: 0 2px; /* 0 is for top and bottom. Feel free to change it */
        }
    </style>
    {{--<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">--}}



    <div class="app-content content">
        <div class="content-wrapper">
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif
            <div class="content-body">
                <div class="content-body">
                    <section id="form-control-repeater">
                        <div class="row">
                            <div class="col-12">
                                <form class="form-horizontal form-simple" method="POST" action="{{ url('CompetitionMobileMoneyFilter') }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1">Competition Name</label>
                                                <div class="input-group">
                                                    <select class="form-control" name="competition_id">
                                                        @foreach($competition as $comp)
                                                             <option value="{{$comp->id}}">{{$comp->competition_title}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1">Start date</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="ft-calendar"></i></span>
                                                    </div>
                                                    <input type="date" class="form-control dp-month-year" name="startdate" value=""/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="projectinput1">End date</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="ft-calendar"></i></span>
                                                    </div>
                                                    <input type="date" class="form-control dp-month-year" name="enddate" value=""/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group" style="margin-top: 20px">
                                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Filter</button>
                                            </div>

                                        </div>
                                    </div>
                                </form>

                                <div class="card">

                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered dataex-html5-export table-responsive" id="MobileMoneyTable" >
                                                <thead>
                                                <tr>
                                                    <th>Competition Name</th>
                                                    <th>Contestant Name</th>
                                                    <th>Phone Number</th>
                                                    <th>Transaction Id</th>
                                                    <th>Payment Status</th>
                                                    <th>Amount</th>
                                                    <th>Payment Code</th>
                                                    <th>Date Created</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($list as $data)
                                                    <tr>
                                                        <td>{{$data->competition_title}}</td>
                                                        <td>{{$data->names}}</td>
                                                        <td>{{$data->phonenumber}}</td>
                                                        <td>{{$data->transactionid}}</td>
                                                        <td>{{$data->status}}</td>
                                                        <td>{{$data->amount}}</td>
                                                        <td>{{$data->payment_code}}</td>
                                                        <td>{{$data->created_at}}</td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>--}}
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>--}}
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/jszip.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/pdfmake.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/vfs_fonts.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.html5.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.print.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.colVis.min.js"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables-extensions/datatable-button/datatable-html5.js"></script>
    <script type="application/javascript">
        // $(document).ready(function() {
        //     $('#MobileMoneyTable').DataTable( {
        //         "paging":   false,
        //         "bDestroy": true
        //         // "ordering": false,
        //     } );
        // } );
    </script>
@endsection
