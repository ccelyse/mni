@extends('backend.layout.master')

@section('title', 'MNI')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <style>
        #payment_,#promotion_period_{
            display: none;
        }
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary:hover{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }

        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #b4753c  !important;
            border-color:#b4753c  !important;
        }
        .btn-primary:hover{
            background-color: #b4753c !important;
            border-color:#b4753c !important;
        }
        .btn-dark {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        .btn-dark:hover {
            color: #000 !important;
            background-color: transparent;
            border-color: transparent;
        }
        iframe{
            width: 100%;
        }

    </style>
    <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            $('.multi-field-wrapper').each(function() {
                var $wrapper = $('.multi-fields', this);
                $(".add-field", $(this)).click(function(e) {
                    $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();
                });
                $('.multi-field .remove-field', $wrapper).click(function() {
                    if ($('.multi-field', $wrapper).length > 1)
                        $(this).parent('.multi-field').remove();
                });
            });
        });
    </script>
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')


    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <div class="content-body">
                    <!-- Basic Summernote start -->
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif

                                    <h4 class="card-title" id="basic-layout-form">Competition Info</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('UploadCompetition') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                               <div class="multi-field-wrapper">
                                                    <div class="multi-fields">
                                                        <div class="row  multi-field">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Competition Title</label>
                                                                    <input type="text" id="projectinput1" class="form-control" value=""
                                                                           name="competition_title" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Competition Youtube Link</label>
                                                                    <input type="text" id="projectinput1" class="form-control" value="{{ old('competition_youtube_link') }}"
                                                                           name="competition_youtube_link" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Competition Cover Picture(<strong>Image should be Png, Jpg or Jpeg</strong>)</label>
                                                                    <input type="file" id="projectinput1" class="form-control"
                                                                           name="competition_image">
                                                                </div>
                                                                @if ($errors->has('song_cover_picture'))
                                                                    <span class="help-block">
                                                                         <strong style="color: red;">{{ $errors->first('song_cover_picture') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Starting Date</label>
                                                                    <input type="date" id="projectinput1" class="form-control"
                                                                           name="starting_date" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">End Date</label>
                                                                    <input type="date" id="projectinput1" class="form-control"
                                                                           name="end_date" required>
                                                                </div>
                                                            </div>
{{--                                                            <div class="col-md-6">--}}
{{--                                                                <div class="form-group">--}}
{{--                                                                    <label for="projectinput1">Voting Price</label>--}}
{{--                                                                    <input type="number" id="projectinput1" class="form-control"--}}
{{--                                                                           name="voting_price" required>--}}
{{--                                                                </div>--}}
{{--                                                            </div>--}}
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="projectinput1">Competition Description</label>
                                                                    <textarea rows="10" id="projectinput1" class="form-control"
                                                                              name="competition_description" required></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                            </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section id="form-control-repeater">
                        <div class="row">

                            <div class="col-12">
                                <div class="card">
                                    <div class="alert alert-success text-center response_error" id="response_error" style="padding-bottom: 15px;display:none"></div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <table class="table table-striped table-bordered zero-configuration table-responsive">
                                                <thead>
                                                <tr>
                                                    <th>Voting Status</th>
                                                    <th>View Groups</th>
                                                    <th>Competition Title</th>
                                                    <th>Competition Code</th>
                                                    <th>Competition Cover Picture</th>
                                                    <th>Starting Date</th>
                                                    <th>Ending Date</th>
                                                    <th>Youtube</th>
                                                    <th>Competition Description</th>
                                                    <th>Created At</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($competition as $data)
                                                    <tr>
                                                        <td>
                                                            <select class="form-control" name="voting_status" id="voting_status" onchange="voting_status(this)">
                                                                <option value="{{$data->voting_status}}" data-id="{{$data->id}}" >{{$data->voting_status}} (Current Status)</option>
                                                                <option value="On" data-id="{{$data->id}}">On</option>
                                                                <option value="Off" data-id="{{$data->id}}">Off</option>
                                                            </select>
                                                        </td>
                                                        <td><a href="{{ route('backend.Groups',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Groups</a></td>
                                                        <td>{{$data->competition_title}}</td>
                                                        <td>{{$data->competition_code}}</td>
                                                        <td><img src="Competition/{{$data->competition_image}}" style="width:100%"></td>
                                                        <td>{{$data->starting_date}}</td>
                                                        <td>{{$data->end_date}}</td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                                    data-toggle="modal"
                                                                    data-target="#video{{$data->id}}">Youtube
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="video{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <?php
                                                                            $video = $data->competition_youtube_link;
                                                                            echo $video;
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                                    data-toggle="modal"
                                                                    data-target="#description{{$data->id}}">Description
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="description{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            {{$data->competition_description}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>{{$data->created_at}}</td>
                                                        <td>
                                                            <button type="button" class="btn btn-icon btn-outline-primary btn-min-width mr-1 mb-1"
                                                                    data-toggle="modal"
                                                                    data-target="#editplaylist{{$data->id}}">Edit Competition
                                                            </button>
                                                            <!-- Modal -->
                                                            <div class="modal fade text-left" id="editplaylist{{$data->id}}" tabindex="-1"
                                                                 role="dialog" aria-labelledby="myModalLabel1"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myModalLabel1">Edit Competition</h4>
                                                                            <button type="button" class="close" data-dismiss="modal"
                                                                                    aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <form class="form-horizontal form-simple" method="POST" action="{{ url('EditCompetition') }}" enctype="multipart/form-data">
                                                                                {{ csrf_field() }}
                                                                                <div class="form-body">

                                                                                    <div class="multi-field-wrapper">
                                                                                        <div class="multi-fields">
                                                                                            <div class="row  multi-field">
                                                                                                <div class="col-md-12">
                                                                                                    <div class="form-group">
                                                                                                        <label for="projectinput1">Competition Title</label>
                                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->competition_title}}"
                                                                                                               name="competition_title" required>
                                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->id}}"
                                                                                                               name="id" hidden>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-12">
                                                                                                    <div class="form-group">
                                                                                                        <label for="projectinput1">Competition Youtube Link</label>
                                                                                                        <input type="text" id="projectinput1" class="form-control" value="{{$data->competition_youtube_link}}"
                                                                                                               name="competition_youtube_link" required>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-12">
                                                                                                    <div class="form-group">
                                                                                                        <label for="projectinput1">Competition Cover Picture(<strong>Image should be Png, Jpg or Jpeg</strong>)</label>
                                                                                                        <input type="file" id="projectinput1" class="form-control"
                                                                                                               name="competition_image">
                                                                                                    </div>
                                                                                                    @if ($errors->has('song_cover_picture'))
                                                                                                        <span class="help-block">
                                                                         <strong style="color: red;">{{ $errors->first('song_cover_picture') }}</strong>
                                                                    </span>
                                                                                                    @endif
                                                                                                </div>
                                                                                                <div class="col-md-6">
                                                                                                    <div class="form-group">
                                                                                                        <label for="projectinput1">Starting Date</label>
                                                                                                        <input type="date" id="projectinput1" class="form-control"
                                                                                                               name="starting_date" value="{{$data->starting_date}}">
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-6">
                                                                                                    <div class="form-group">
                                                                                                        <label for="projectinput1">End Date</label>
                                                                                                        <input type="date" id="projectinput1" class="form-control"
                                                                                                               name="end_date" value="{{$data->end_date}}">
                                                                                                    </div>
                                                                                                </div>
{{--                                                                                                <div class="col-md-6">--}}
{{--                                                                                                    <div class="form-group">--}}
{{--                                                                                                        <label for="projectinput1">Voting Price</label>--}}
{{--                                                                                                        <input type="number" id="projectinput1" class="form-control"--}}
{{--                                                                                                               name="voting_price" value="{{$data->voting_price}}">--}}
{{--                                                                                                    </div>--}}
{{--                                                                                                </div>--}}
                                                                                                <div class="col-md-12">
                                                                                                    <div class="form-group">
                                                                                                        <label for="projectinput1">Competition Description</label>
                                                                                                        <textarea rows="10" id="projectinput1" class="form-control"
                                                                                                                  name="competition_description">{{$data->competition_description}}</textarea>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-6" style="padding-bottom: 15px">
                                                                                                    <img src="Competition/{{$data->competition_image}}" style="width:100%">
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                                                                    </div>
                                                                                </div>
                                                                            </form>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><a href="{{ route('backend.DeleteCompetition',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary" onclick="return confirm('Are you sure you would like to delete this this song?');">Delete</a></td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>


                </div>
            </div>
        </div>
    </div>

    <script type="application/javascript">
        function voting_status(event) {
            var status = document.getElementById("voting_status").value;
            var id = event.selectedOptions[0].getAttribute('data-id');

            if (confirm("Are you sure you want to update status")) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "../api/UpdateVotingStatus",
                    data: {
                        'status': status,
                        'id': id,
                    },
                    dataType: 'json',
                    success: function(response) {
                        JSON.stringify(response);
                        console.log(response);
                        $( ".response_error" ).show();
                        jQuery('.response_error').html('<p>' + response.response_message + '</p>');
                        setTimeout(function() {
                            window.location.reload()
                        }, 4000);

                    },
                    error: function(xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            } else {
                console.log('canceled')
            }
        }
        $(document).on('change', '#voting_status', function() {
            var voting_status =$('#voting_status').val();
        });
        $(document).on('change', '#promotion_status', function() {
            var promotion_status =$('#promotion_status').val();
            if(promotion_status == "Promoted"){
                document.getElementById("payment_").style.display = "block";
                document.getElementById("promotion_period_").style.display = "block";
            }else{
                document.getElementById("payment_").style.display = "none";
                document.getElementById("promotion_period_").style.display = "none";
            }

        });
        $(document).on('click', '#ChangeStatus', function() {
            $('#ChangeStatus').html('Changing Promotion Status..');
            var promotion_status = document.getElementById("promotion_status").value;
            var promotion_period = document.getElementById("promotion_period").value;
            var phonenumber = document.getElementById("phonenumber").value;
            var song_id = document.getElementById("song_id").value;
            // alert(promotion_status);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "../api/PromoteStatus",
                data: {
                    'promotion_status': promotion_status,
                    'promotion_period': promotion_period,
                    'phonenumber': phonenumber,
                    'song_id': song_id,
                    // 'amount': amount,
                },
                dataType: 'json',
                success: function(response) {
                    JSON.stringify(response);
                    // console.log(response);
                    // jQuery('#messagestatus').show();
                    if(response.status_promoted == "promoted"){
                        document.getElementById("Voting_response").style.display = "block";
                        $('#successmodal').html('<p style="text-align:center;">' + response.message + '</p>');

                        $(function(){
                            setInterval(oneSecondFunction, 4000);
                        });
                        function oneSecondFunction() {
                            $.ajax
                            ({
                                type: "POST",
                                url: "../api/CallBackPromotedMomo",
                                data: {
                                    transactionid:response.transactionid,
                                },
                                cache: false,
                                success: function (data) {
                                    $('#messagestatus').html('<p style="text-align:center;">' + data.message + '</p>');
                                    if(data.status == "SUCCESSFUL"){
                                        document.getElementById("loading").style.display = "none";
                                        document.getElementById("successmodal").style.display = "none";
                                        setTimeout(function() { window.location.reload() }, 2000);
                                    }else{
                                        // jQuery('#messagestatus').show();
                                        document.getElementById("messagestatus").style.display = "block";
                                        document.getElementById("loading").style.display = "block";
                                    }

                                },
                                error: function(xhr, status, error) {
                                    console.log(xhr.responseText);
                                }
                            });
                        }
                    }else{
                        console.log(response.message);
                        // document.getElementById("Voting_response").style.display = "block";
                        $('#messagestatus_not_promoted').html('<p style="text-align:center;">' + response.message + '</p>');
                        document.getElementById("ChangeStatus").style.display = "none";
                        setTimeout(function() { window.location.reload() }, 5000);
                    }

                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                }
            });
        });

    </script>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>--}}
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    {{--<script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>--}}
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
@endsection
