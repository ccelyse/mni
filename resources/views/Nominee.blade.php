@extends('layouts.newmaster')

@section('title', 'MNI PLAYLIST')

@section('content')
<style>
    .spb-asset-content p{
        color:#fff !important;
    }
    .title-wrap h3{
        color: #fff !important;
    }
    .chart-page .chart-detail-header__chart-name {
        padding-top: 50px !important;
    }
    .modal-backdrop{
        position: relative !important;
    }
    /*.modal-backdrop.show {*/
        /*opacity: .5;*/
    /*}*/

    .modal-dialog {
        max-width: 800px !important;
        margin: 30px auto;
    }
    iframe{
        width: 100% !important;
    }

    .single-blog-post a,.single-blog-post p{
        color: #fff;
    }
    .bottom-title{
        font-size: 24px;
        line-height: 34px;
        color: #fff;
        padding: 45px 0 35px;
    }
    .title-box{
        display: inline-block;
        line-height: 35px;
        font-size: 13px;
        text-transform: uppercase;
        color: #fff;
        padding: 0 30px;
        border: 1px solid rgba(255,255,255,0.2);
        border-radius: 3px;
    }
    .alert-success {
        color: #155724;
        background-color: #d4edda;
        border-color: #c3e6cb;
        width: 100%;
        text-align: center;
        margin: 15px;
    }
.page-title{
    color: #fff !important;
}
</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script
        src="https://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>


@include('layouts.newtopmenu')

<div class="solid-inner-banner">
    <div class="bg-shape-holder">
        <span class="big-round-one wow fadeInLeft animated" data-wow-duration="3s"></span>
        <span class="big-round-two wow fadeInLeft animated" data-wow-duration="3s"></span>
        <span class="big-round-three wow fadeInLeft animated" data-wow-duration="3s"></span>
        <span class="shape-one"></span>
        <span class="shape-two"></span>
        <img src="front/images/shape/shape-32.svg" alt="" class="shape-three">
        <span class="shape-four"></span>
    </div>
    <h2 class="page-title">Tanga Indirimbo</h2>
</div> <!-- /.solid-inner-banner -->



<!--
    =============================================
        Contact Us
    ==============================================
    -->
@if($getlocation !== "Rwanda")
<div class="contact-us-section" style="padding-bottom: 20px;padding-top: 20px; ">
    <div class="container">
        <div class="row">
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif
            <div class="col-lg-3">
                <div class="contact-form">
                    <form class="form" id="contact-form" action="{{url('AddNominee')}}" method="post">
                        {{ csrf_field() }}
                        <div class="messages"></div>
                        <div class="controls">
                            <div class="form-group">
                                <select class="" name="category">
                                    <option value="Hitamo Ijyana" selected>Hitamo Ijyana</option>
                                    @foreach($listcate as $listcates)
                                    <option value="{{$listcates->id}}">{{$listcates->category}}</option>
                                    @endforeach
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <input id="form_email" type="text" name="artist_name" placeholder="Umuhanzi" required="required">
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <input id="form_sub" type="text" name="artist_song" placeholder="Indirimbo" required="required">
                                <div class="help-block with-errors"></div>
                            </div>

                            <button class="theme-button-two">Tanga Indirimbo</button>
                        </div> <!-- /.controls -->
                    </form>
                </div> <!-- /.contact-form -->
            </div> <!-- /.col- -->

            <div class="col-lg-3">
                <div class="contact-info">
                    {{--<h2 class="title">Don’t Hesitate to contact with us for any kind of information Ntutinde kutwandikira</h2>--}}
                    <p>Muramutse mugize ikibazo mwaduhamagara kuri iyi numero bagufashe</p>
                    <a href="contact-us-standard.html#" class="call">+250 789 813 910</a>
                    <ul>
                        <li><a href="contact-us-standard.html#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="contact-us-standard.html#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="contact-us-standard.html#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    </ul>
                </div> <!-- /.contact-info -->
            </div>
                <div class="why-choose-us-app">
                    <div class="container">
                        <div class="pricing-tab-menu">
                            <div class="nav-wrapper">
                                <ul class="nav nav-tabs justify-content-center">
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#Gospel">Gospel</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Modern">Modern</a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Traditional">Traditional</a></li>
                                </ul>
                            </div>
                        </div> <!-- /.pricing-tab-menu -->
                    </div> <!-- /.container -->

                    <div class="tab-content">
                        <!-- ^^^^^^^^^^^^^^^^^^^^^ Monthly ^^^^^^^^^^^^^^^^^^^^^^^^^^^ -->
                        <div id="Gospel" class="tab-pane fade show active">
                            <div class="row justify-content-md-center">

                                <div class="col-lg-12" style="padding-top: 15px">
                                    @if(0 == count($getmusicgospel))
                                        <div class="text-wrapper md-center">
                                            <div class="title-box">Mutwihanganire urutonde ruraza vuba</div>
                                        </div> <!-- /.text-wrapper -->

                                    @else
                                        <div class="text-wrapper">
                                            <div class="form gospelSearch" id="contact-form">
                                                <div class="controls">
                                                    <div class="form-group">
                                                        <input id="searchname" type="text" name="searchname" placeholder="search">
                                                    </div>
                                                </div> <!-- /.controls -->
                                            </div>
                                            {{--<div class="title-box" style="position: relative;bottom: 10px;"> NOMINEE</div>--}}
                                            <div id="resultsg">
                                                @foreach($getmusicgospel as $getmusics)
                                                    <div class="director-speech clearfix" style="padding-top: 30px">
                                                        <img src="front/images/logo/logojpeg.jpeg" class="d-img">
                                                        <div class="bio-block">
                                                            <h6 class="name">{{$getmusics->artist_name}}</h6>
                                                            <span>{{$getmusics->artist_song}}</span>

                                                        </div> <!-- /.bio-block -->
                                                        <h6 class="sign">
                                                            <?php
                                                            $accountnumber =\App\Nominee::where('artist_name',$getmusics->artist_name)->where('artist_song', $getmusics->artist_song)->select(DB::raw('count(id) as numbers'))->value('numbers');
                                                            echo "Nominee:  $accountnumber";
                                                            ?>
                                                        </h6>
                                                    </div> <!-- /.director-speech -->
                                                @endforeach
                                            </div>

                                            <div id="display">
                                            </div>
                                        </div> <!-- /.text-wrapper -->
                                    @endif
                                </div>
                            </div>
                        </div> <!-- /#gospel -->

                        <!-- ^^^^^^^^^^^^^^^^^^^^^ Yearly ^^^^^^^^^^^^^^^^^^^^^^^^^^^ -->
                        <div id="Modern" class="tab-pane fade">
                            <div class="row justify-content-md-center">
                                <div class="col-lg-12" style="padding-top: 15px">
                                    @if(0 == count($getmusicmodern))
                                        <div class="text-wrapper md-center">
                                            <div class="title-box">Mutwihanganire urutonde ruraza vuba</div>
                                        </div> <!-- /.text-wrapper -->

                                    @else
                                        <div class="text-wrapper">
                                            <div class="form modernSearch" id="contact-form">
                                                <div class="controls">
                                                    <div class="form-group">
                                                        <input id="modernSearch" type="text" name="modernSearch" placeholder="search">
                                                    </div>
                                                </div> <!-- /.controls -->
                                            </div>
                                            {{--<div class="title-box" style="position: relative;bottom: 10px;"> NOMINEE</div>--}}
                                            <div id="resultsM">
                                                @foreach($getmusicmodern as $getmusicsmodern)
                                                    <div class="director-speech clearfix"  style="padding-top: 30px">
                                                        <img src="front/images/logo/logojpeg.jpeg" class="d-img">
                                                        <div class="bio-block">
                                                            <h6 class="name">{{$getmusicsmodern->artist_name}}</h6>
                                                            <span>{{$getmusicsmodern->artist_song}}</span>

                                                        </div> <!-- /.bio-block -->
                                                        <h6 class="sign">
                                                            <?php
                                                            $accountnumber =\App\Nominee::where('artist_name',$getmusicsmodern->artist_name)->where('artist_song', $getmusicsmodern->artist_song)->select(DB::raw('count(id) as numbers'))->value('numbers');
                                                            echo "Nominee:  $accountnumber";
                                                            ?>
                                                        </h6>
                                                    </div> <!-- /.director-speech -->
                                                @endforeach
                                            </div>

                                            <div id="displayModern">
                                            </div>
                                        </div> <!-- /.text-wrapper -->


                                    @endif
                                </div>

                            </div>
                        </div> <!-- /#modern -->

                        <div id="Traditional" class="tab-pane fade">
                            <div class="row justify-content-md-center">
                                <div class="col-lg-12" style="padding-top:15px">
                                    @if(0 == count($getmusictraditional))
                                        <div class="text-wrapper md-center">
                                            <div class="title-box">Mutwihanganire urutonde ruraza vuba</div>
                                        </div>
                                    @else
                                        <div class="text-wrapper">
                                            <div class="form traditionalSearch" id="contact-form">
                                                <div class="controls">
                                                    <div class="form-group">
                                                        <input id="traditionalSearch" type="text" name="traditionalSearch" placeholder="search">
                                                    </div>
                                                </div> <!-- /.controls -->
                                            </div>
                                            {{--<div class="title-box" style="position: relative;bottom: 10px;"> NOMINEE</div>--}}
                                            <div id="resultsT">
                                                @foreach($getmusictraditional as $getmusictraditionals)
                                                    <div class="director-speech clearfix" style="padding-top: 30px">
                                                        <img src="front/images/logo/logojpeg.jpeg" class="d-img">
                                                        <div class="bio-block">
                                                            <h6 class="name">{{$getmusictraditionals->artist_name}}</h6>
                                                            <span>{{$getmusictraditionals->artist_song}}</span>

                                                        </div> <!-- /.bio-block -->
                                                        <h6 class="sign">
                                                            <?php
                                                            $accountnumber =\App\Nominee::where('artist_name',$getmusictraditionals->artist_name)->where('artist_song', $getmusictraditionals->artist_song)->select(DB::raw('count(id) as numbers'))->value('numbers');
                                                            echo "Nominee:  $accountnumber";
                                                            ?>
                                                        </h6>
                                                    </div> <!-- /.director-speech -->
                                                @endforeach
                                            </div>

                                            <div id="displayTraditional">
                                            </div>
                                        </div> <!-- /.text-wrapper -->
                                    @endif
                                </div>

                            </div>
                        </div> <!-- /#traditional -->
                    </div>

                </div> <!-- /.agn-our-pricing -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div> <!-- /.contact-us-section -->
@else
<div class="why-choose-us-app" style="margin-top: 15px">
    <div class="container">
        <div class="pricing-tab-menu">
            <div class="nav-wrapper">
                <ul class="nav nav-tabs justify-content-center">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#Gospel">Gospel</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Modern">Modern</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Traditional">Traditional</a></li>
                </ul>
            </div>
        </div> <!-- /.pricing-tab-menu -->
    </div> <!-- /.container -->

    <div class="tab-content">
        <!-- ^^^^^^^^^^^^^^^^^^^^^ Monthly ^^^^^^^^^^^^^^^^^^^^^^^^^^^ -->
        <div id="Gospel" class="tab-pane fade show active">
            <div class="row justify-content-md-center">

                <div class="col-lg-6" style="padding-top: 15px">
                    @if(0 == count($getmusicgospel))
                        <div class="text-wrapper md-center">
                            <div class="title-box">Mutwihanganire urutonde ruraza vuba</div>
                        </div> <!-- /.text-wrapper -->

                    @else
                        <div class="text-wrapper">
                            <div class="form gospelSearch" id="contact-form">
                                <div class="controls">
                                    <div class="form-group">
                                        <input id="searchgospel" type="text" name="searchgospel" placeholder="search">
                                    </div>
                                </div> <!-- /.controls -->
                            </div>
                            {{--<div class="title-box" style="position: relative;bottom: 10px;"> NOMINEE</div>--}}
                            <div id="resultsg">
                                @foreach($getmusicgospel as $getmusics)
                                    <div class="director-speech clearfix" style="padding-top: 30px">
                                        <img src="front/images/logo/logojpeg.jpeg" class="d-img">
                                        <div class="bio-block">
                                            <h6 class="name">{{$getmusics->artist_name}}</h6>
                                            <span>{{$getmusics->artist_song}}</span>

                                        </div> <!-- /.bio-block -->
                                        <h6 class="sign">
                                            <?php
                                            $accountnumber =\App\Nominee::where('artist_name',$getmusics->artist_name)->where('artist_song', $getmusics->artist_song)->select(DB::raw('count(id) as numbers'))->value('numbers');
                                            echo "Nominee:  $accountnumber";
                                            ?>
                                        </h6>
                                    </div> <!-- /.director-speech -->
                                @endforeach
                            </div>

                            <div id="display">
                            </div>
                        </div> <!-- /.text-wrapper -->
                    @endif
                </div>
            </div>
        </div> <!-- /#gospel -->

        <!-- ^^^^^^^^^^^^^^^^^^^^^ Yearly ^^^^^^^^^^^^^^^^^^^^^^^^^^^ -->
        <div id="Modern" class="tab-pane fade">
            <div class="row justify-content-md-center">
                <div class="col-lg-6" style="padding-top: 15px">
                    @if(0 == count($getmusicmodern))
                        <div class="text-wrapper md-center">
                            <div class="title-box">Mutwihanganire urutonde ruraza vuba</div>
                        </div> <!-- /.text-wrapper -->

                    @else
                        <div class="text-wrapper">
                            <div class="form modernSearch" id="contact-form">
                                <div class="controls">
                                    <div class="form-group">
                                        <input id="modernSearch" type="text" name="modernSearch" placeholder="search">
                                    </div>
                                </div> <!-- /.controls -->
                            </div>
                            {{--<div class="title-box" style="position: relative;bottom: 10px;"> NOMINEE</div>--}}
                            <div id="resultsM">
                                @foreach($getmusicmodern as $getmusicsmodern)
                                    <div class="director-speech clearfix"  style="padding-top: 30px">
                                        <img src="front/images/logo/logojpeg.jpeg" class="d-img">
                                        <div class="bio-block">
                                            <h6 class="name">{{$getmusicsmodern->artist_name}}</h6>
                                            <span>{{$getmusicsmodern->artist_song}}</span>

                                        </div> <!-- /.bio-block -->
                                        <h6 class="sign">
                                            <?php
                                            $accountnumber =\App\Nominee::where('artist_name',$getmusicsmodern->artist_name)->where('artist_song', $getmusicsmodern->artist_song)->select(DB::raw('count(id) as numbers'))->value('numbers');
                                            echo "Nominee:  $accountnumber";
                                            ?>
                                        </h6>
                                    </div> <!-- /.director-speech -->
                                @endforeach
                            </div>

                            <div id="displayModern">
                            </div>
                        </div> <!-- /.text-wrapper -->


                    @endif
                </div>

            </div>
        </div> <!-- /#modern -->

        <div id="Traditional" class="tab-pane fade">
            <div class="row justify-content-md-center">
                <div class="col-lg-6" style="padding-top:15px">
                    @if(0 == count($getmusictraditional))
                        <div class="text-wrapper md-center">
                            <div class="title-box">Mutwihanganire urutonde ruraza vuba</div>
                        </div>
                    @else
                        <div class="text-wrapper">
                            <div class="form traditionalSearch" id="contact-form">
                                <div class="controls">
                                    <div class="form-group">
                                        <input id="traditionalSearch" type="text" name="traditionalSearch" placeholder="search">
                                    </div>
                                </div> <!-- /.controls -->
                            </div>
                            {{--<div class="title-box" style="position: relative;bottom: 10px;"> NOMINEE</div>--}}
                            <div id="resultsT">
                                @foreach($getmusictraditional as $getmusictraditionals)
                                    <div class="director-speech clearfix" style="padding-top: 30px">
                                        <img src="front/images/logo/logojpeg.jpeg" class="d-img">
                                        <div class="bio-block">
                                            <h6 class="name">{{$getmusictraditionals->artist_name}}</h6>
                                            <span>{{$getmusictraditionals->artist_song}}</span>

                                        </div> <!-- /.bio-block -->
                                        <h6 class="sign">
                                            <?php
                                            $accountnumber =\App\Nominee::where('artist_name',$getmusictraditionals->artist_name)->where('artist_song', $getmusictraditionals->artist_song)->select(DB::raw('count(id) as numbers'))->value('numbers');
                                            echo "Nominee:  $accountnumber";
                                            ?>
                                        </h6>
                                    </div> <!-- /.director-speech -->
                                @endforeach
                            </div>

                            <div id="displayTraditional">
                            </div>
                        </div> <!-- /.text-wrapper -->
                    @endif
                </div>

            </div>
        </div> <!-- /#traditional -->
    </div>

</div> <!-- /.agn-our-pricing -->

@endif

<script type="text/javascript">
        $(document).ready(function(){
            $('.gospelSearch input[type="text"]').on("keyup input", function() {
                /* Get input value on change */
                var inputVal = $(this).val();
//                alert(inputVal);
                var resultDropdown = $(this).siblings(".result");

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "SearchNomineeG",
                    data: {
                        'searchname': inputVal,

                    },
                    success: function (data) {
                        document.getElementById('resultsg').style.display = 'none';
                        $("#display").html(data);
                    }
                });
            });
        });

        $(document).ready(function(){
            $('.modernSearch input[type="text"]').on("keyup input", function() {
                /* Get input value on change */
                var inputValM = $(this).val();
//                alert(inputValM);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "SearchNomineeM",
                    data: {
                        'modernSearch': inputValM,

                    },
                    success: function (data) {
                        document.getElementById('resultsM').style.display = 'none';
                        $("#displayModern").html(data);
                    }
                });
            });
        });
        $(document).ready(function(){
            $('.traditionalSearch input[type="text"]').on("keyup input", function() {
                /* Get input value on change */
                var inputValT = $(this).val();
//                alert(inputValM);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "SearchNomineeT",
                    data: {
                        'traditionalSearch': inputValT,

                    },
                    success: function (data) {
                        document.getElementById('resultsT').style.display = 'none';
                        $("#displayTraditional").html(data);
                    }
                });
            });
        });

</script>
@include('layouts.newfooter')
@endsection
