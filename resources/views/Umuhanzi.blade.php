@extends('layouts.newmaster')

@section('title', 'MNI Selection')

@section('content')
<style>
    .spb-asset-content p{
        color:#fff !important;
    }
    .title-wrap h3{
        color: #fff !important;
    }
    .chart-page .chart-detail-header__chart-name {
        padding-top: 50px !important;
    }
    .modal-backdrop{
        position: relative !important;
    }
    /*.modal-backdrop.show {*/
        /*opacity: .5;*/
    /*}*/

    .modal-dialog {
        max-width: 800px !important;
        margin: 30px auto;
    }
    iframe{
        width: 100% !important;
    }

    .single-blog-post a,.single-blog-post p{
        color: #fff;
    }
    .bottom-title{
        font-size: 24px;
        line-height: 34px;
        color: #fff;
        padding: 45px 0 35px;
    }
    .title-box{
        display: inline-block;
        line-height: 35px;
        font-size: 13px;
        text-transform: uppercase;
        color: #fff;
        padding: 0 30px;
        border: 1px solid rgba(255,255,255,0.2);
        border-radius: 3px;
    }
    .alert-success {
        color: #155724;
        background-color: #d4edda;
        border-color: #c3e6cb;
        width: 100%;
        text-align: center;
        margin: 15px;
    }
    .theme-title-three p {
        font-size: 17px !important;
        line-height: 35px !important;
        color: #798598;
        padding-top: 10px !important;
    }
.page-title{
    color: #fff !important;
}
    .solid-inner-banner {
        padding: 100px 0 0 !important;
    }
    .solid-inner-banner .page-title {
        padding-bottom: 100px;
    }
    /*.our-service{*/
        /*background: url('front/images/artists-audience-band-426976-compressed.jpg');*/
        /*background-size: contain;*/
        /*background-position: bottom;*/
    /*}*/
    .service-title{
        color:#fff;
    }
    .theme-list-item li {
        font-family: 'Rubik', sans-serif;
        line-height: 44px;
        color: #fff;
        position: relative;
        padding-left: 22px;
        font-size: 19px;
    }
    .theme-list-item li i, .theme-list-item li span {
        position: absolute;
        line-height: 44px;
        left: 0;
        top: 0;
        color: #fff;
        font-size: 15px;
    }
    .solid-inner-banner .page-title {
        font-size: 35px;
    }
    #theme-banner-five {
        /* background: linear-gradient( 145deg, rgb(132,0,252) 0%, rgb(17,222,244) 100%); */
        /*background: linear-gradient( 145deg, rgb(241, 122, 7) 0%, rgba(250, 128, 2, 0.99) 100%);*/
        background: linear-gradient( 145deg, rgb(5, 54, 50) 0%, rgb(28, 112, 111) 100%);
        position: relative;
        z-index: 5;
        /*background: url(./front/images/artist.jpg) !important;*/
        background-size: cover !important;
    }
    #theme-banner-five .main-wrapper .button-group li a{
        padding: 10px;
        border-radius: 30px;
        bottom: 10px;
        background: #e1a34c;
    }
    #umuhanzibutton {
        padding: 7px;
        border-radius: 30px;
        bottom: 10px;
        background: #e1a34c !important;
    }
    #theme-banner-five .main-wrapper .button-group li a:hover{
        background: #fff;
        color:#266664 !important;
    }
    #theme-banner-five .mobile-screen-one {
        right: 0 !important;
        width: 50%;
        left: 5%;
    }
    #theme-banner-five .main-wrapper {
        position: relative;
        right: 0;
        left: 40%;
    }
    #theme-banner-five .main-wrapper {
        /*padding: 230px 0 215px 10%;*/
        background: rgba(0, 0, 0, 0.19);
    }
    .our-service .our-history .text-wrapper {
    padding: 80px 0 0 80px;
    }
    @media (max-width: 575px) {
        /*#theme-banner-five {*/
            /*!* background: linear-gradient( 145deg, rgb(132,0,252) 0%, rgb(17,222,244) 100%); *!*/
            /*!*background: linear-gradient( 145deg, rgb(241, 122, 7) 0%, rgba(250, 128, 2, 0.99) 100%);*!*/
            /*!*background: linear-gradient( 145deg, rgb(5, 54, 50) 0%, rgb(28, 112, 111) 100%);*!*/
            /*position: relative;*/
            /*z-index: 5;*/
            /*!*background: url(./front/images/artist.png) !important;*!*/
            /*!*background-size: cover !important;*!*/
        /*}*/
        .our-service {
            background: transparent;
        }
        #service-block_money{
            background-position: center !important;
        }
        .mobile_money{
            padding: 10px !important;
        }
    }
    .service-creative .service-block .service-info p {
        padding: 0px 0 8px !important;
    }
</style>
@include('layouts.newArtisttopmenu')

<!--
			=============================================
				Theme Main Banner Five
			==============================================
			-->
<div id="theme-banner-five">

    <div class="mobile-screen-one wow fadeInRight animated" data-wow-duration="1.5s"><img src="front/images/artist.png" alt=""></div>
    <div class="mobile-screen-two wow fadeInRight animated" data-wow-duration="1.5s" data-wow-delay="0.8s">
        {{--<img src="front/images/home/screen8.png" alt="">--}}
        {{--<img src="front/images/home/screen9.png" alt="" class="search-box wow zoomIn animated" data-wow-duration="1.5s" data-wow-delay="1.8s">--}}
    </div>
    <div class="bg-shape-holder">
        <span class="big-round-one wow fadeInLeft animated" data-wow-duration="3s"></span>
        <span class="big-round-two wow fadeInLeft animated" data-wow-duration="3s"></span>
        <span class="big-round-three wow fadeInLeft animated" data-wow-duration="3s"></span>
        <span class="shape-one"></span>
        <span class="shape-two"></span>
        <img src="front/images/shape/shape-32.svg" alt="" class="shape-three">
        <span class="shape-four"></span>
    </div>
    <div class="main-wrapper">
        <h1 class="main-title wow fadeInUp animated" data-wow-delay="0.4s">Igihangano cyawe ni cyiza<br> kandi gifite ugikunda.
        </h1>
        <p class="sub-title wow fadeInUp animated" data-wow-delay="0.9s">Abakunzi ba muzika biteguye kugushyigikira</p>
        <ul class="button-group">
            <li><a href="{{'ArtistRegistration'}}" id="umuhanzibutton" class="wow fadeInLeft animated" data-wow-delay="1.5s"><span>Fungura Konti</span></a></li>
            <li><a href="{{'ArtistLogin'}}" id="umuhanzibutton" class="wow fadeInLeft animated" data-wow-delay="1.5s"><span>Kwinjira</span></a></li>
            {{--<li><a href="#" id="umuhanzibutton" class="wow fadeInRight animated" data-wow-delay="1.5s"><span>Tora kuri *611#</span></a></li>--}}

        </ul>
    </div> <!-- /.main-wrapper -->
    {{--<p class="muzikahashtag">#MuzikaNyarwandaIpande</p>--}}
</div> <!-- /#theme-banner-five -->


<!--
=============================================
    Abahanzi
==============================================
-->
<div class="our-service service-creative">
    <div class="our-history">
        <div class="container" style="max-width: 100%;">
            <div class="row">
                <div class="col-lg-7 offset-md-3" style="background: #fff">
                    <div class="text-wrapper override">
                        <h2 class="service-title" style="color: #266664;font-size: 35px;padding-bottom: 15px;">Ibyiza bya MNI Selection  </h2>
                        <ul class="theme-list-item pb-70">
                            <li style="color: #000;"><i class="fa fa-check-circle" aria-hidden="true" style="color: #266664;"></i> Kwihuza n’abakunzi bamuzika bawe </li>
                            <li style="color: #000;"><i class="fa fa-check-circle" aria-hidden="true" style="color: #266664;"></i> Kubyaza inyungu igihangano cyawe</li>
                            <li style="color: #000;"><i class="fa fa-check-circle" aria-hidden="true" style="color: #266664;"></i> Kugaragara ku igihangano cyawe k'urutonde rw'indirimbo zikunzwe mu igihugu.</li>
                            <li style="color: #000;"><i class="fa fa-check-circle" aria-hidden="true" style="color: #266664;"></i>Kuririmba muri MNI Tarama </li>
                            <li style="color: #000;"><i class="fa fa-check-circle" aria-hidden="true" style="color: #266664;"></i>Guhabwa ibihembo bitangwa na MNI ndetse n’abafatanyabikorwa</li>
                        </ul>

                    </div>
                </div>
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /.our-history -->


    <div class="service-block" style="background: #032a29;">
        <div class="container">
            <div class="demo-container-1100" style="background: rgba(3, 3, 3, 0.5);padding-top: 15px;">
                <div class="row">
                    <div class="col-lg-12" data-aos="fade-left">
                        <div class="service-info">
                            {{--<div class="num">01</div>--}}
                            <h2 class="service-title" style="padding-bottom: 10px;"><a href="#">Uko MNI selection ikora</a></h2>
                            <ul class="theme-list-item pb-70">
                                <li><i class="fa fa-chevron-right" aria-hidden="true"></i>Umuhanzi asabwa gufungura konti kuri MNI</li>
                                <li><i class="fa fa-chevron-right" aria-hidden="true"></i>Kugaragaza igihangano ku rubuga rwa MNI kugirango gitorwe n’abakunzi bamuzika ku mafaranga 50 RWF cyangwa ukanda *611#</li>
                                <li><i class="fa fa-chevron-right" aria-hidden="true"></i>Umuhanzi ahabwa 25 RWF kuri buri jwi. </li>
                                <li><i class="fa fa-chevron-right" aria-hidden="true"></i>Uko indirimbo itorwa cyane igenda igira umwanya mwiza k'urutonde rw'indirimbo zikunzwe mu igihugu muri uko kwezi </li>
                            </ul>
                            <a href="#" class="read-more"><i class="flaticon-next-1"></i></a>
                        </div> <!-- /.service-info -->
                    </div> <!-- /.col- -->
                </div> <!-- /.row -->
            </div> <!-- /.demo-container-1100 -->
        </div> <!-- /.container -->
    </div> <!-- /.service-block -->

    <div class="service-block" id="service-block_money" style="background:#266664;">
        <div class="container">
            <div class="demo-container-1100" style="background: rgba(3, 3, 3, 0.28); padding-top: 15px;">
                <div class="row">
                    <div class="col-lg-12" data-aos="fade-right">
                        <div class="service-info mobile_money" style="padding-left: 60px; padding-bottom: 15px">
                            {{--<div class="num">02</div>--}}
                            <h2 class="service-title" style="padding-bottom: 10px;"><a href="#" style="color: #fff">Ibihembo bitangwa na MNI </a></h2>
                            <p>MNI itanga ibihembo buri gihembwe cy’umwaka kingana n’amezi atatu ku indirimbo zahize izindi.</p>
                            <p>Ibihembo bitangwa mu buryo bukurikira;</p>
                            <ul class="theme-list-item pb-70">
                                <li style="color: #fff"><i class="fa fa-chevron-right" aria-hidden="true" style="color: #fff"></i>Indirimbo ya mbere: Miliyoni ebyiri (2,000,000 RWF)</li>
                                <li style="color: #fff"><i class="fa fa-chevron-right" aria-hidden="true" style="color: #fff"></i>Indirimbo ya kabiri: Ibihumbi magana rindwi (700,000 RWF)</li>
                                <li style="color: #fff"><i class="fa fa-chevron-right" aria-hidden="true" style="color: #fff"></i>Indirimbo ya gatatu: Ibihumbi magana tatu (300,000 RWF)</li>
                            </ul>
                            <p style="color: #ff6363 !important;">N.B: Amafaranga yagaragajwe aba abariyemo umusoro wa Withholding bivuze ko uyahabwa uwo musoro wabanje gukurwamo ukishyurwa RRA.</p>

                            {{--<img src="front/images/money security-min.png" style="width: 150px;float: right;">--}}
                            {{--<a href="{{'ArtistRegistration'}}" class="wow fadeInLeft animated" data-wow-delay="1.5s"><span>Fungura Konti</span></a>--}}
                            <a href="{{'ArtistRegistration'}}"  id="umuhanzibutton" class="read-more" style="padding: 15px !important;"><i class="flaticon-next-1"> Fungura konti </i></a>
                        </div> <!-- /.service-info -->
                    </div> <!-- /.col- -->
                </div> <!-- /.row -->
            </div> <!-- /.demo-container-1100 -->
        </div> <!-- /.container -->
    </div> <!-- /.service-block -->

    <div class="service-block" id="service-block_money" style="background:#fff;">
        <div class="container">
            <div class="demo-container-1100" style=" padding-top: 15px;">
                <div class="row">
                    <div class="col-lg-12" data-aos="fade-right">
                        <div class="service-info mobile_money" style="padding-left: 60px; padding-bottom: 15px">
                            {{--<div class="num">02</div>--}}
                            <h2 class="service-title" style="padding-bottom: 10px;"><a href="#" style="color: #032a29">Umutekano w’amafaranga, uburyo n’igihe cyo kwishyurwa.</a></h2>
                            <ul class="theme-list-item pb-70">
                            <li style="color: #032a29"><i class="fa fa-chevron-right" aria-hidden="true" style="color: #032a29"></i>Amafaranga yishyurwa n’abakunzi bamuzika yakirwa kuri konti ya banki MNI ihuriyeho na Rwanda Music Federation (RMF) nk’urwego rurengera inyungu z’abahanzi. Amafaranga akurwaho gusa iyo byagizwemo uruhare n’impande zombi akoherezwe kuri ba nyirayo.</li>
                            <li style="color: #032a29"><i class="fa fa-chevron-right" aria-hidden="true" style="color: #032a29"></i>Nyuma y’ukwezi indirimbo imaze muri MNI Selection, nyir’igihangano yishyurwa bitarenze ibyumweru bibiri. Iyo hagize imbogamizi ituma icyo gihe kitubahirizwa, nyir’igihangano amenyeshwa impamvu mu ibaruwa yanditse n’igihe azishyurwa.</li>
                            </ul>
                            <img src="front/images/money security-min.png" style="width: 150px;float: right;">
                            {{--<a href="{{'ArtistRegistration'}}" class="wow fadeInLeft animated" data-wow-delay="1.5s"><span>Fungura Konti</span></a>--}}
                            <a href="{{'ArtistRegistration'}}"  id="umuhanzibutton" class="read-more" style="padding: 15px !important;"><i class="flaticon-next-1"> Fungura konti </i></a>
                        </div> <!-- /.service-info -->
                    </div> <!-- /.col- -->
                </div> <!-- /.row -->
            </div> <!-- /.demo-container-1100 -->
        </div> <!-- /.container -->
    </div> <!-- /.service-block -->



</div> <!-- /.our-service -->






@include('layouts.newArtistfooter')
@endsection
