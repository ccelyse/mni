@extends('layouts.newmaster')

@section('title', 'MNI-Kwiyandikisha')

@section('content')
    <style>
        .spb-asset-content p{
            color:#fff !important;
        }
        .title-wrap h3{
            color: #fff !important;
        }
        .chart-page .chart-detail-header__chart-name {
            padding-top: 50px !important;
        }
        .modal-backdrop{
            position: relative !important;
        }
        /*.modal-backdrop.show {*/
        /*opacity: .5;*/
        /*}*/

        .modal-dialog {
            max-width: 800px !important;
            margin: 30px auto;
        }
        iframe{
            width: 100% !important;
        }

        .single-blog-post a,.single-blog-post p{
            color: #fff;
        }
        .bottom-title{
            font-size: 24px;
            line-height: 34px;
            color: #fff;
            padding: 45px 0 35px;
        }
        .title-box{
            display: inline-block;
            line-height: 35px;
            font-size: 13px;
            text-transform: uppercase;
            color: #fff;
            padding: 0 30px;
            border: 1px solid rgba(255,255,255,0.2);
            border-radius: 3px;
        }
        .alert-success {
            color: #155724;
            background-color: #d4edda;
            border-color: #c3e6cb;
            width: 100%;
            text-align: center;
            margin: 15px;
        }
        .theme-title-three p {
            font-size: 17px !important;
            line-height: 35px !important;
            color: #000;
            padding-top: 10px !important;
        }
        .page-title{
            color: #fff !important;
        }
        .our-service .our-history .text-wrapper {
            padding: 80px 0 80px 0px !important;
        }
        .theme-list-item li {
            color: #fff;
        }
        .theme-list-item li i, .theme-list-item li span {
            color: #fff;
        }
        .our-service .our-history .text-wrapper p {
            color: #000 !important;
        }
        #theme-banner-five {
            /* background: linear-gradient( 145deg, rgb(132,0,252) 0%, rgb(17,222,244) 100%); */
            /* background: linear-gradient( 145deg, rgb(5, 54, 50) 0%, rgb(28, 112, 111) 100%); */
            position: relative;
            z-index: 5;
            background: url(./front/images/audience-band-celebration-2240771-compressed.jpg) !important;
            background-size: cover !important;
        }
        .signUp-page .agreement-checkbox input[type="checkbox"] {
             display: inline-block;
        }
        .signUp-page .agreement-checkbox label:before {
            display: none;
        }
    </style>
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <script type="application/javascript">
        function ModeOfPayment(that) {
            if (that.value == "Bank account") {
                document.getElementById("Bank").style.display = "flex";
                document.getElementById("MobileMoney").style.display = "none";
            } else {
                document.getElementById("MobileMoney").style.display = "flex";
                document.getElementById("Bank").style.display = "none";
            }
        }
        $(function(){
            //on keypress
            $('#confirm_artist_password').keyup(function(e){
                //get values
                var artist_password = $('#artist_password').val();
                var confirm_artist_password = $(this).val();

                //check the strings
                if(artist_password == confirm_artist_password){
                    //if both are same remove the error and allow to submit
                    $('.error').text('Password are matching');
                }else{
                    //if not matching show error and not allow to submit
                    $('.error').text('Password not matching');
                }
            });

            //jquery form submit
        });
    </script>
    @include('layouts.newtopmenu')

    <div class="signUp-page signUp-minimal pt-50 pb-100">
        <div class="shape-wrapper">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div> <!-- /.shape-wrapper -->
        <div class="sign-up-form-wrapper">
            <div class="title-area text-center">
                <h3>Fungura Konti Ubungubu.</h3>
            </div> <!-- /.title-area -->
            @if (session('success'))
                <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                    {{ session('success') }}
                </div>
            @endif
            <p class="or-text"><span></span></p>
            <form id="signUp-form" method="POST" action="{{ url('AddArtistRegistration') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <h5 style="padding-bottom: 10px;">Personal Information</h5>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" name="names" value="{{ old('names') }}" required>
                            <label>Names</label>
                        </div> <!-- /.input-group -->
                    </div> <!-- /.col- -->
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" name="artistic_names" value="{{ old('artistic_names') }}" required>
                            <label>Artistic Names</label>
                        </div> <!-- /.input-group -->
                    </div> <!-- /.col- -->
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="number" name="artist_number" value="{{ old('artist_number') }}" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" pattern="[+]{1}[0-9]{12}" maxlength="10" required>
                            <label>Mobile number (078xxxxxx)</label>
                        </div> <!-- /.input-group -->
                    </div> <!-- /.col- -->
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="email" name="artist_email" value="{{ old('artist_email') }}" required>
                            <label>Email</label>
                        </div> <!-- /.input-group -->
                    </div> <!-- /.col- -->
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="password" name="artist_password" id="artist_password" value="{{ old('artist_password') }}" required>
                            <label>Password</label>
                        </div> <!-- /.input-group -->
                    </div> <!-- /.col- -->
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="password" name="confirm_artist_password" id="confirm_artist_password" value="{{ old('confirm_artist_password') }}" required>
                            <label>Confirm Password</label>
                        </div> <!-- /.input-group -->
                        <div class="form-group">
                            <span class="error" style="color:red"></span><br />
                        </div>
                    </div> <!-- /.col- -->
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" name="artist_ID" value="{{ old('artist_ID') }}" required>
                            <label>ID no/Valid passport no</label>
                        </div> <!-- /.input-group -->
                    </div> <!-- /.col- -->
                    <div class="col-md-12">
                        <h5 style="padding-bottom: 10px;">Mode of payment</h5>
                    </div>
                    <div class="col-md-12">
                        <div class="input-group">
                            <select name="artist_modeofpayment" onchange="ModeOfPayment(this);" required>
                                {{--<option value="{{ old('artist_modeofpayment') }}">{{ old('artist_modeofpayment') }}</option>--}}
                                <option value="Select mode of payment" selected>Select mode of payment</option>
                                <option value="Mobile Money">Mobile Money</option>
                                <option value="Bank account">Bank account</option>
                            </select>
                        </div> <!-- /.input-group -->
                    </div> <!-- /.col- -->
                </div>
                <div class="row" id="MobileMoney">

                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="number" name="artist_mobilemoney_number" value="{{ old('artist_mobilemoney_number') }}" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" pattern="[+]{1}[0-9]{12}" maxlength="10">
                            <label>Number to receive payment (078xxxxxx)</label>
                        </div> <!-- /.input-group -->
                    </div><!-- /.col- -->
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" name="artist_mobilemoney_names" value="{{ old('artist_mobilemoney_names') }}">
                            <label>Name on mobile money</label>
                        </div> <!-- /.input-group -->
                    </div> <!-- /.col- -->
                </div> <!-- /.row -->
                <div class="row" id="Bank" style="display: none">
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="number" name="artist_bankaccount_number" value="{{ old('artist_bankaccount_number') }}" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" pattern="[+]{1}[0-9]{12}" maxlength="40">
                            <label>Account number(write correctly)</label>
                        </div> <!-- /.input-group -->
                    </div> <!-- /.col- -->
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" name="artist_bankaccount_names" value="{{ old('artist_bankaccount_names') }}">
                            <label>Name on the account</label>
                        </div> <!-- /.input-group -->
                    </div> <!-- /.col- -->
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="text" name="artist_bank_name" value="{{ old('artist_bank_name') }}">
                            <label>Bank name</label>
                        </div> <!-- /.input-group -->
                    </div> <!-- /.col- -->
                </div> <!-- /.row -->

                <div class="row">
                    <div class="col-md-12">
                        <h5 style="padding-bottom: 10px;">Social Media</h5>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="url" name="artist_facebook" value="{{ old('artist_facebook') }}">
                            <label>Facebook profile link</label>
                        </div> <!-- /.input-group -->
                    </div> <!-- /.col- -->
                    <div class="col-md-6">
                        <div class="input-group">
                            <input type="url" name="artist_instagram" value="{{ old('artist_instagram') }}">
                            <label>Instagram profile link</label>
                        </div> <!-- /.input-group -->
                    </div> <!-- /.col- -->
                    <div class="col-md-6">
                        <label>.</label>
                        <div class="input-group">
                            <input type="url" name="artist_twitter" value="{{ old('artist_twitter') }}">
                            <label>Twitter profile link</label>
                        </div> <!-- /.input-group -->
                    </div> <!-- /.col- -->
                    <div class="col-md-6">
                        <label>Profile Photo</label>
                        <div class="input-group">

                            <input type="file" name="picturename" required>

                        </div> <!-- /.input-group -->
                    </div> <!-- /.col- -->
                </div> <!-- /.row -->
                <div class="agreement-checkbox">
                    <input type="checkbox" id="agreement" id="confirm" required>
                    <label for="agreement">
                        <a href="{{'AbahanziTermsandConditions'}}" target="_blank" id="reset" >Accept our terms and conditions then press submit
                        </a>
                    </label>
                </div>
                <button class="line-button-one">Register</button>
            </form>
        </div> <!-- /.sign-up-form-wrapper -->
    </div> <!-- /.signUp-page -->
    <script type="application/javascript">
        $('#reset').click(function () {
            if (!$('#confirm').is(':checked')) {
                alert('not checked');
                return false;
            }
        });
    </script>
    @include('layouts.newfooter')
@endsection
