@extends('layouts.newmaster')

@section('title', 'MN-Muzika Nyarwanda Ipande| Urutonde rw\'Ukwezi|MNI Selection')

@section('content')
<style>
    .spb-asset-content p{
        color:#fff !important;
    }
    .title-wrap h3{
        color: #fff !important;
    }
    .chart-page .chart-detail-header__chart-name {
        padding-top: 50px !important;
    }
    .modal-backdrop{
        position: relative !important;
    }
    /*.modal-backdrop.show {*/
        /*opacity: .5;*/
    /*}*/

    .modal-dialog {
        max-width: 800px !important;
        margin: 30px auto;
    }
    iframe{
        width: 100% !important;
    }

    .single-blog-post a,.single-blog-post p{
        color: #fff;
    }
    .bottom-title{
        font-size: 24px;
        line-height: 34px;
        color: #fff;
        padding: 45px 0 35px;
    }
    .title-box{
        display: inline-block;
        line-height: 35px;
        font-size: 20px;
        text-transform: uppercase;
        color: #fff;
        padding: 0 30px;
        border: 1px solid rgba(255,255,255,0.2);
        border-radius: 3px;
        font-weight: bold;
    }
    .contact-us-section{
        background: #1c706f;
    }
    .contact-us-section .contact-info .title {
        font-family: 'CircularStdmed';
        font-size: 42px;
        line-height: 55px;
        color: #fff;
        margin: -8px 0 25px;
        text-align: center;
    }
    .contact-us-section .contact-info ul li a {
        color: #fff;
        width: 100px !important;
    }
    .contact-us-section .contact-info ul li a:hover {
        color: #fff;
    }
    /*.contact-us-section .contact-info ul li a {*/
        /*width: 100px;*/
        /*line-height: 36px;*/
        /*border: 2px solid #1c706f;*/
        /*border-radius: 50%;*/
        /*text-align: center;*/
        /*font-size: 18px;*/
        /*color: #1c706f;*/
        /*margin-right: 5px;*/
        /*padding: 23px;*/
    /*}*/
    #theme-banner-five .main-wrapper .button-group li a{
        padding: 10px;
        border-radius: 30px;
        bottom: 10px;
        background: #e2a34c;
    }
    #theme-banner-five .main-wrapper .button-group li a:hover{
        background: #fff;
        color:#266664 !important;
    }
    .howtovote{
        text-align: center;
        font-size: 35px;
        font-weight: bold;
        color: #1c706e;
        padding: 10px;
    }
    #theme-banner-five {
        /* background: linear-gradient( 145deg, rgb(132,0,252) 0%, rgb(17,222,244) 100%); */
        /* background: linear-gradient( 145deg, rgb(5, 54, 50) 0%, rgb(28, 112, 111) 100%); */
        position: relative;
        z-index: 5;
        background: url(./front/images/newslider.jpeg) !important;
        background-size: cover !important;
    }
    .muzikahashtag{
        position: relative;
        left: 0;
        right: 0;
        color: #fff;
        font-size: 25px;
        text-align: right;
        padding: 25px;
    }
    .why-choose-us-app .text-wrapper .director-speech .bio-block span {
        font-size: 18px;
        color: #1c706e;
        font-style: normal;
    }
    @media (max-width: 649px){
        #theme-banner-five {
             background-position: center !important;
        }
    }
    .position_song{
        color: #1c706e;
        position: relative;
        font-size: 50px;
        left: 5px;
        top: 10%;
    }
    .songnumber{
        background: #fff;
        margin-bottom: 5px;
        text-align: center;
        /*border: 1px solid #1c706e;*/
    }
    .navigation li{
        float: left;
        padding: 10px;
    }
</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script
        src="https://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>
@include('layouts.newtopmenu')

<!--
			=============================================
				Theme Main Banner Five
			==============================================
			-->
<div id="theme-banner-five">

    {{--<div class="mobile-screen-one wow fadeInRight animated" data-wow-duration="1.5s"><img src="front/images/Picture1.png" alt=""></div>--}}
    <div class="mobile-screen-two wow fadeInRight animated" data-wow-duration="1.5s" data-wow-delay="0.8s">
        {{--<img src="front/images/home/screen8.png" alt="">--}}
        {{--<img src="front/images/home/screen9.png" alt="" class="search-box wow zoomIn animated" data-wow-duration="1.5s" data-wow-delay="1.8s">--}}
    </div>
    <div class="bg-shape-holder">
        <span class="big-round-one wow fadeInLeft animated" data-wow-duration="3s"></span>
        <span class="big-round-two wow fadeInLeft animated" data-wow-duration="3s"></span>
        <span class="big-round-three wow fadeInLeft animated" data-wow-duration="3s"></span>
        <span class="shape-one"></span>
        <span class="shape-two"></span>
        <img src="front/images/shape/shape-32.svg" alt="" class="shape-three">
        <span class="shape-four"></span>
    </div>
    <div class="main-wrapper">
        <h1 class="main-title wow fadeInUp animated" data-wow-delay="0.4s">Umuziki Nyarwanda ni kimwe<br> mubigize ubuzima bwacu.</h1>
        <p class="sub-title wow fadeInUp animated" data-wow-delay="0.9s">Shyigikira indirimbo ukunda.</p>
        <ul class="button-group">
            <li><a href="#ToraIndirimbo" id="umuhanzibutton" class="wow fadeInLeft animated" data-wow-delay="1.5s"><span>Tora Indirimbo</span></a></li>
            <li><a href="tel:*611##" id="umuhanzibutton" class="wow fadeInRight animated" data-wow-delay="1.5s"><span>Tora kuri *611#(Iratangira vuba)</span></a></li>

        </ul>
    </div> <!-- /.main-wrapper -->
    <p class="muzikahashtag">#MuzikaNyarwandaIpande</p>
</div> <!-- /#theme-banner-five -->




<!--
=============================================
    Why Choose Us
==============================================
-->
{{--<div class="why-choose-us-app">--}}
    {{--<div class="container">--}}
        {{--<div class="text-wrapper md-center">--}}
            {{--<div class="howtovote">Dore Uko Watora</div>--}}
        {{--</div> <!-- /.text-wrapper -->--}}
       {{--<img src="front/images/STEP.png" style="width: 100%">--}}
    {{--</div> <!-- /.container -->--}}



{{--</div> <!-- /.agn-our-pricing -->--}}

<div class="why-choose-us-app">
    <div class="container">
        <div class="pricing-tab-menu mt-30">
            <div class="howtovote">MNI Selection</div>

            {{--<div class="nav-wrapper">--}}
                {{--<ul class="nav nav-tabs justify-content-center">--}}
                    {{--<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#Modern">Modern</a></li>--}}
                    {{--<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Gospel">Gospel</a></li>--}}
                    {{--<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Traditional">Traditional</a></li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        </div> <!-- /.pricing-tab-menu -->
    </div> <!-- /.container -->

    <div class="tab-content" id="ToraIndirimbo">
        <!-- ^^^^^^^^^^^^^^^^^^^^^ Monthly ^^^^^^^^^^^^^^^^^^^^^^^^^^^ -->

        <div id="Modern" class="tab-pane fade show active">
            <div class="row justify-content-md-center">
                <div class="col-lg-6" style="padding-top: 15px">
                    @if(0 == count($getmusicmodern))
                        <div class="text-wrapper md-center">
                            <div class="title-box">Urutonde rutangira kugaragara vuba</div>
                        </div> <!-- /.text-wrapper -->

                    @else

                        <div class="text-wrapper">
                            <div class="title-box text-center">Urutonde rw'ukwezi</div>
                            <div class="form modernSearch" id="contact-form" style="margin-top: 10px">
                                <div class="controls">
                                    <div class="form-group">
                                        <input id="modernSearch" type="text" name="modernSearch" placeholder="search">
                                    </div>
                                </div> <!-- /.controls -->
                            </div>
                            <div id="resultsM">

                                    @foreach($getmusicmodern as $index => $getmusicsmodern)

                                    <div class="row" style="border: 1px solid #1c706f;margin-bottom: 5px;">
                                        <div class="col-md-2 songnumber">
                                            <p class="position_song">{{ $index+1 }}</p>
                                        </div>
                                        <div class="col-md-10 songinfo">
                                            <div class="director-speech clearfix">
                                                <img src="SongImages/{{$getmusicsmodern->song_cover_picture}}" alt="" class="d-img">
                                                <div class="bio-block">
                                                    <h6 class="name" >{{$getmusicsmodern->song_name}}</h6>
                                                    <span style="color: #848484 !important;">{{$getmusicsmodern->song_artist}}</span><br>
                                                    <span style="color: #848484 !important;"> <p>Cecile Kayirebwa -<iframe src="https://audiomack.com/embed/song/ishimwe-13/rwanda??background=white&color=1c7070" scrolling="no" width="73" height="30" scrollbars="no" frameborder="0"></iframe></p>
   </span><br>
                                                    <span><a href="{{$getmusicsmodern->song_youtube_link}}" target="_blank"><i class="fa fa-youtube" aria-hidden="true" style="font-size: 30px;color:#1c706d;"></i></a></span>
                                                    <h6 class="name">
                                                    <span>
                                                    <?php
                                                        $votesbe = $getmusicsmodern->votesNumberMon;
                                                        $newvotes = $votesbe - 1;
                                                        if($newvotes < 0){
                                                            echo "Amajwi y'ukwezi: 0";
                                                        }else{
                                                            echo "Amajwi y'ukwezi: $newvotes";
                                                        }
                                                        ?>
                                                    </span><br>
                                                    <span style="font-size: 12px; !important;">
                                                        <?php
                                                        $created_at =  $getmusicsmodern->created_at;
                                                        $lastTimeLoggedOut = \Illuminate\Support\Carbon::parse($created_at)->diffForHumans();
                                                        echo $lastTimeLoggedOut;
                                                        ?>
                                                    </span>
                                                    </h6>

                                                </div> <!-- /.bio-block -->

                                                <div class="bio-block" id="votesmobile" style="position: absolute; right: 20%; float: none; top: 14%;">

                                                </div> <!-- /.bio-block -->

                                                <h6 class="sign" id="{{$check_status}}">
                                                    <a class="votebutton" href="{{ route('VoteNow',['id'=> $getmusicsmodern->id])}}">Tora</a><br>
                                                    <span style="top: 5px;position: relative;">
                                                    <?php
//                                                        $song_id = $getmusicsmodern->id;
//                                                        $votes = \App\Votes::where('voter_status','SUCCESSFUL')->select(DB::raw('count(id) as votes'))
//                                                            ->where('song_id',$song_id)
//                                                            ->where('vote','1')
//                                                            ->value('votes');
//                                                        $newvotes = $votes - 1;
                                                        $votesbe = $getmusicsmodern->votesNumber;
                                                        $newvotes = $votesbe - 1;
                                                        if($newvotes < 0){
                                                            echo "Total: 0";
                                                        }else{
                                                            echo "Total: $newvotes";
                                                        }
                                                        ?>
                                                    </span>
                                                </h6>
                                                <div id="{{$getmusicsmodern->id}}m" class="modal" data-backdrop="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">{{$getmusicsmodern->song_name}} Music Video</h5>
                                                            </div>
                                                            <div class="modal-body text-center p-lg">
                                                                <?php
                                                                $video = $getmusicsmodern->song_youtube_link;
                                                                echo $video;
                                                                ?>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div>
                                                </div>
                                            </div> <!-- /.director-speech -->
                                        </div>

                                    </div>
                                    @endforeach
                            </div>

                            <div id="displayModern">
                            </div>
                            <div class="text-wrapper md-center">
                                <a href="{{'Urutonde'}}" class="title-box " id="umuhanzibutton" style="text-align: center;color: #fff;">Reba urutonde rwose</a>
                            </div> <!-- /.text-wrapper -->
                        </div> <!-- /.text-wrapper -->
                    @endif
                </div>

            </div>
        </div> <!-- /#modern -->
    </div>

</div> <!-- /.agn-our-pricing -->

<script type="text/javascript">
    $(document).ready(function(){
        $('.modernSearch input[type="text"]').on("keyup input", function() {
            /* Get input value on change */
            var inputValM = $(this).val();
//                alert(inputValM);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "SearchVotingM",
                data: {
                    'modernSearch': inputValM,

                },
                success: function (data) {
                    document.getElementById('resultsM').style.display = 'none';
                    $("#displayModern").html(data);
                }
            });
        });
    });

</script>
@include('layouts.newfooter')
@endsection
