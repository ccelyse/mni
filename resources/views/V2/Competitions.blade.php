@extends('layoutsv2.master')

@section('title', 'Competition')

@section('content')
<style>
    .embed__am-link{
        display: none !important;
    }
    .music-detail{
        position: relative;
        left: 30px;
    }
    h3 {
        font-size: 18px !important;
        line-height: 35px;
        letter-spacing: 3px;
        text-transform: uppercase;
        padding: 18px !important;
        display: table-caption !important;
        text-align: justify !important;
    }
    .custom_wr .wr {
        background: #222431;
         padding: 0px !important;
        -moz-transition: 0.5s ease all;
        -o-transition: 0.5s ease all;
        -webkit-transition: 0.5s ease all;
        transition: 0.5s ease all;
    }
    .custom_wr .wr * + .btn {
        text-decoration: none;
        margin-top: 15px;
        color: #21706c;
        /* padding-bottom: 15px; */
        background: #ffffff;
        margin: 10px;
        padding: 5px;
        border-radius: 5px;
    }
    .custom_wr .wr p, .custom_wr .wr .terms-list dd, .terms-list .custom_wr .wr dd {
        color: #fff;
        font-size: 1.4em;
        font-weight: bold;
        line-height: 1.5;
        font-family: "Roboto Slab", serif;
        padding: 15px;
    }
    .custom_wr:hover .wr {
        background: #232531 !important;
    }
</style>
<div class="page">
    <!--
    ========================================================
                            HEADER
    ========================================================

    -->
    <header>
        @include('layoutsv2.upmenu')
{{--        <div class="camera_container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-lg-6">--}}
{{--                    <div id="camera" class="camera_wrap">--}}
{{--                        @foreach($list_slider as $sliders)--}}
{{--                            <div data-src="HomeSlider/{{$sliders->slider_photo}}">--}}
{{--                                <div class="camera_caption fadeIn">--}}
{{--                                    <h3>“{{$sliders->slider_title}}”</h3>--}}
{{--                                    <p>{{$sliders->slider_description}}</p>--}}
{{--                                    <div class="wr">--}}
{{--                                        <div class="link_wr">--}}
{{--                                            <a href="{{'StakeHoldersLogin'}}" class="btn btn_mod1">Sign In</a>--}}
{{--                                        </div>--}}
{{--                                        <a href="{{'StakeHoldersRegister'}}" class="btn">SIgn Up</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        @endforeach--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </header>

    <!--
    ========================================================
                             CONTENT
    ========================================================
    -->
    <main>
{{--        <section class="well3 well3__ins1" style="">--}}
{{--            <div class="container">--}}
{{--                <div class="col-md-12">--}}
{{--                    <h3 class="center">Available Competitions</h3>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </section>--}}
        <section class="well" style="padding-bottom: 15px !important;">
            <div class="container center">
                <h3 style="text-align: center !important;font-size: 40px !important;display: block !important;">Available Competitions</h3>
                <div class="row">
                    <div class="owl-stake">
                        <div class="item">
                            @foreach($competition as $list)
                                <div class="col-xs-12 col-sm-6 col-md-6">
                                    <div class="custom_wr">
                                        <div class="col-md-6">
                                            <img src="Competition/{{$list->competition_image}}" alt="" style="width: 300px;height: 300px;">
                                        </div>

                                        <div class="wr col-md-6">
                                            <p>“{{$list->competition_title}}”</p>
                                            <hr><a href="{{ route('V2.Contestants',['id'=> $list->id])}}" class="btn">VOTE NOW</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    @include('layoutsv2.footer')
</div>
@endsection
