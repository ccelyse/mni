@extends('layoutsv2.master')

@section('title', 'BroadCast')

@section('content')
    <script src="https://semantic-ui.com/dist/semantic.min.js"></script>

    <link rel="stylesheet" type="text/css" class="ui" href="https://semantic-ui.com/dist/semantic.min.css">
    <style>
        .embed__am-link{
            display: none !important;
        }
        .music-detail{
            position: relative;
            left: 30px;
        }
        .spb-asset-content p{
            color:#fff !important;
        }
        .title-wrap h3{
            color: #fff !important;
        }
        .chart-page .chart-detail-header__chart-name {
            padding-top: 50px !important;
        }
        .modal-backdrop{
            position: relative !important;
        }
        /*.modal-backdrop.show {*/
        /*opacity: .5;*/
        /*}*/

        .modal-dialog {
            max-width: 800px !important;
            margin: 30px auto;
        }
        iframe{
            width: 100% !important;
        }

        .single-blog-post a,.single-blog-post p{
            color: #fff;
        }
        .bottom-title{
            font-size: 24px;
            line-height: 34px;
            color: #fff;
            padding: 45px 0 35px;
        }
        .title-box{
            display: inline-block;
            line-height: 35px;
            font-size: 13px;
            text-transform: uppercase;
            color: #fff;
            padding: 0 30px;
            border: 1px solid rgba(255,255,255,0.2);
            border-radius: 3px;
        }
        .alert-success {
            color: #155724;
            background-color: #d4edda;
            border-color: #c3e6cb;
            width: 100%;
            text-align: center;
            margin: 15px;
        }
        .theme-title-three p {
            font-size: 17px !important;
            line-height: 35px !important;
            color: #000;
            padding-top: 10px !important;
        }
        .page-title{
            color: #fff !important;
        }
        .our-service .our-history .text-wrapper {
            padding: 80px 0 80px 0px !important;
        }
        .theme-list-item li {
            color: #fff;
        }
        .theme-list-item li i, .theme-list-item li span {
            color: #fff;
        }
        .our-service .our-history .text-wrapper p {
            color: #000 !important;
        }
        #theme-banner-five {
            /* background: linear-gradient( 145deg, rgb(132,0,252) 0%, rgb(17,222,244) 100%); */
            /* background: linear-gradient( 145deg, rgb(5, 54, 50) 0%, rgb(28, 112, 111) 100%); */
            position: relative;
            z-index: 5;
            background: url(./front/images/audience-band-celebration-2240771-compressed.jpg) !important;
            background-size: cover !important;
        }
        .signUp-page .agreement-checkbox input[type="checkbox"] {
            display: inline-block;
        }
        .signUp-page .agreement-checkbox label:before {
            /*display: none;*/
        }
        .signUp-page form ::placeholder {color: #bcbcbc;opacity: 1; /* Firefox */}
        .signUp-page form :-ms-input-placeholder {color: #bcbcbc;}
        .signUp-page form ::-ms-input-placeholder {color: #bcbcbc;}
        .signUp-page form .input-group {position: relative;z-index: 1; margin-bottom: 35px;}
        .signUp-page form .input-group input, .input-group textarea {
            width: 100%;
            border: 1px solid #e0e0e0;
            border-radius: 5px;
            background: transparent;
            height: 60px;
            font-size: 18px;
            color: #454545;
            position: relative;
            padding: 0 20px;
        }
        .signUp-page form .input-group select {
            width: 100%;
            border: 1px solid #e0e0e0;
            border-radius: 5px;
            background: transparent;
            height: 60px;
            font-size: 18px;
            color: #454545;
            position: relative;
            padding: 0 20px;
        }
        .signUp-page form .input-group label {
            font-weight: normal;
            font-size: 15px;
            color: #bcbcbc;
            line-height: 60px;
            position: absolute;
            left:20px;
            top:0;
            z-index: -1;
            margin: 0;
            transition: all 0.3s ease-in-out;
        }
        .signUp-page form .input-group input:focus ~ label,
        .signUp-page form .input-group input:valid ~ label {top: -12px;}
        .signUp-page form .input-group input:focus {border-color: #393939;}
        .signUp-page form .input-group input:focus,
        .signUp-page form .input-group input:valid {padding-top: 18px;}
        .signUp-page .selectize-control {width: 100%;}
        .signUp-page .input-group .selectize-input {
            width: 100%;
            line-height: 60px;
            height: 60px;
            border-radius: 5px;
            border: none;
            border: 1px solid #e0e0e0;
            box-shadow: none;
            outline: none;
            padding: 0 20px;
            background: transparent;
        }
        .signUp-page .input-group .selectize-input input {font-size: 15px;}
        .signUp-page .selectize-input .item {font-size: 18px;color: #454545;}
        .signUp-page .selectize-dropdown {
            background: #fff;
            border: 1px solid #e9e9e9;
            border-top: none;
            box-shadow: 0px 50px 100px 0px rgba(229, 232, 235, 0.2);
            cursor: pointer;
        }
        .signUp-page .selectize-dropdown .option {font-size: 14px;color: #1a1a1a;line-height: 22px;}
        .signUp-page .selectize-dropdown .option:hover,
        .signUp-page .selectize-dropdown .active {color: #fff; background:#1a1a1a; }
        .signUp-page .selectize-control.single .selectize-input:after {right: 8px;}
        .signUp-page .acType-content h4 {font-size: 28px;}
        .signUp-page .acType-content p {padding: 8px 0 25px;}
        .signUp-page .acType-content .acType-list {margin: 0 -22px;}
        .signUp-page .acType-content .acType-list li {float: left;padding: 0 22px;}
        .signUp-page .acType-content .acType-list li>div {position: relative;}
        .signUp-page .acType-content .acType-list li>div input {
            position: absolute;
            opacity: 0;
            z-index: 1;
            width: 100%;
            height: 100%;
            cursor: pointer;
        }
        .signUp-page .acType-content .acType-list li>div label {
            position: relative;
            font-size: 16px;
            line-height: 15px;
            color: rgba(47,52,62,0.8);
            cursor: pointer;
            padding-left: 25px;
            margin: 0 0 15px;
            transition: all 0.1s ease-in-out;
        }
        .signUp-page .acType-content .acType-list li>div label:before {
            content: '';
            width: 14px;
            height: 14px;
            border-radius: 50%;
            border: 2px solid #d6d6d6;
            position: absolute;
            left:0;
            top:0;
            transition: all 0.1s ease-in-out;
        }
        .signUp-page .acType-content .acType-list li>div input:checked + label:before {border-color: #FF3A46;}
        .signUp-page .acType-content .acType-list li>div input:checked + label {color:rgba(47,52,62,1); }
        .signUp-page .acType-content {border-bottom: 1px solid #e0e0e0;padding-bottom: 65px;}
        .signUp-page .agreement-checkbox label {
            position: relative;
            font-size: 16px;
            color: rgba(47,52,62,0.6);
            cursor: pointer;
            padding-left: 22px;
            margin: 30px 0 35px;
            transition: all 0.1s ease-in-out;
        }
        .signUp-page .agreement-checkbox input[type="checkbox"] {display: none;}
        .signUp-page .agreement-checkbox label:before {
            content: '';
            width: 12px;
            height: 12px;
            line-height: 10px;
            border-radius: 2px;
            border: 1px solid #d5d5d5;
            font-size: 8px;
            text-align: center;
            position: absolute;
            left:0;
            top:6px;
            transition: all 0.1s ease-in-out;
        }
        .signUp-page .agreement-checkbox input[type="checkbox"]:checked + label:before {
            content: "";
            font-family: 'font-awesome';
            background: #373737;
            color: #fff;
            border-color:  #373737;
        }
        .signUp-page .agreement-checkbox input[type="checkbox"]:checked + label {color:#373737; }
        .signUp-page.signUp-minimal .agreement-checkbox label {margin-top: 0;}
        .signUp-page.signUp-minimal form .line-button-one {width: 100%;border-radius: 5px;}
        .signin-form-wrapper .title-area {margin-bottom: 50px;}
        .signUp-standard .signin-form-wrapper #login-form .input-group input {
            border: none;
            border-radius: 0;
            border-bottom: 1px solid #e0e0e0;
            padding-left: 0;
        }
        .signUp-standard .signin-form-wrapper #login-form .input-group label {left: 0;}
        .signUp-standard .signin-form-wrapper #login-form {padding-right: 170px;}
        #login-form .agreement-checkbox label {margin: 0;}
        #login-form .agreement-checkbox a {font-size: 16px;color: rgba(47,52,62,0.6);}
        #login-form .agreement-checkbox a:hover {color: #212121;}
        #login-form .agreement-checkbox  {margin-bottom: 55px;}
        .signin-form-wrapper .signUp-text {font-size: 20px;padding-top: 40px;}
        #login-form button {text-transform: uppercase;}
        .signUp-minimal .signin-form-wrapper {
            max-width: 520px;
            margin: 0 auto;
            border: 1px solid #e0e0e0;
            padding: 70px 65px 35px;
            position: relative;
        }
        .signUp-minimal .signin-form-wrapper .signUp-text {font-size: 17px;padding-top: 35px;}
        /*.textareaElement{*/
        /*    height: 200px !important;*/
        /*}*/
        .line-button-one {
            text-transform: capitalize;
            line-height: 48px;
            border-style: solid;
            border-width: 2px;
            border-radius: 30px;
            text-align: center;
            padding: 0 35px;
            min-width: 180px;
            border-color: #1c706e;
            margin-top: 15px;
        }
        .ui.dividing.header {
            font-family: "Roboto Slab", serif !important;
        }
        .ui.comments .comment .text {
            font-family: "Roboto Slab", serif !important;
        }
        .ui.comments .comment a.author {
            font-family: "Roboto Slab", serif !important;
        }
        h1, h2, h3, h4, h5 {
            font-family: "Roboto Slab", serif !important;
        }
        p{
            font-family: "Roboto Slab", serif !important;
        }
        @media (max-width: 479px){
            #mobile_first_Add {
                top: 247px;
                position: relative;
                padding: 160px 0px !important;
                margin-top: 0px !important;
            }
        }

    </style>
<div class="page">
    <!--
    ========================================================
                            HEADER
    ========================================================

    -->
    <header>
        @include('layoutsv2.upmenu')
        <div class="camera_container">
            <div class="row">
                <div class="col-lg-6">
                    <div id="camera" class="camera_wrap">
                        @foreach($list_slider as $sliders)
                        <div data-src="HomeSlider/{{$sliders->slider_photo}}">
                            <div class="camera_caption fadeIn">
                                <h3>“{{$sliders->slider_title}}”</h3>
                                <p>{{$sliders->slider_description}}</p>
                                <div class="wr">
                                    <div class="link_wr">
                                        <a href="https://www.youtube.com/channel/UC2kHMCoGVRYmaTeG5APFBPA?view_as=subscriber" class="btn">
                                            <i class="fa fa-youtube" aria-hidden="true" style="font-size: 25px;">

                                            </i>Listen Previous Playlist</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!--
    ========================================================
                             CONTENT
    ========================================================
    -->
    <main>

        <section class="well3 well3__ins1" style="margin-top: 60px;" id="mobile_first_Add">
            <div class="container">
                <script src="https://apps.elfsight.com/p/platform.js" defer></script>
                <div class="elfsight-app-dfa841c7-4929-4e41-a054-52dcd149c1ff"></div>
            </div>
        </section>
        <section class="well" style="padding-bottom: 15px; padding-top: 0px !important;">
            <div class="container">
                <h1 style="color: #21706d">
                    Today's topic
                </h1>
                <p style="font-size: 18px;">{{$last_id->topic_name}}?</p>
                <div class="row" id="{{$comment_status}}">
                    <div class="col-md-8">
                        <div class="signUp-page signUp-minimal pt-50 pb-100">
                            <div class="shape-wrapper">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div> <!-- /.shape-wrapper -->
                            <div class="sign-up-form-wrapper">
                                @if (session('success'))
                                    <div class="alert alert-success" id="success_messages" style="margin-top: 10px;padding: 10px;">
                                        {{ session('success') }}
                                    </div>
                                @endif
                                <div class="title-area text-center">
                                    <h3>Make Comment</h3>
                                </div> <!-- /.title-area -->

                                <p class="or-text"><span></span></p>
                                <form id="signUp-form" method="POST" action="{{ url('AddBroadcastComment') }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5 style="padding-bottom: 10px;">Personal Information</h5>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <input type="text" name="names" value="{{ old('names') }}" required>
                                                {{--                                                @foreach($last_id as $id)--}}
                                                <input type="text" name="broadcast_id" value="{{$last_id->id}}" hidden>
                                                {{--                                                @endforeach--}}
                                                <label>Names</label>
                                            </div> <!-- /.input-group -->
                                        </div> <!-- /.col- -->
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <input type="number" name="email" required>
                                                <label>Phone Number</label>
                                            </div> <!-- /.input-group -->
                                        </div> <!-- /.col- -->
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                {{--                                                <input type="email" name="artistic_names" value="{{ old('artistic_names') }}" required>--}}
                                                <textarea rows="4" name="comment" placeholder="comment"></textarea>
                                            </div> <!-- /.input-group -->
                                        </div> <!-- /.col- -->
                                    </div>

                                    <button class="line-button-one">Send</button>
                                </form>
                            </div> <!-- /.sign-up-form-wrapper -->
                        </div> <!-- /.signUp-page -->
                    </div>


                </div>
            </div>
        </section>
        <section class="well3 well3__ins1" style="margin-top: 60px;">
            <div class="container">
                {{--                <h2>Music StakeHolders</h2>--}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="ui comments">
                            <h3 class="ui dividing header">Comments</h3>
                            @foreach($list_comment as $comment)
                                <div class="comment">
                                    <a class="avatar">
                                        <img src="https://semantic-ui.com/images/avatar/small/elliot.jpg">
                                    </a>
                                    <div class="content">
                                        <a class="author">{{$comment->names}}</a>
                                        <div class="metadata">
                                        <span class="date">
                                            <?php
                                            $created_at =  $comment->created_at;
                                            $lastTimeLoggedOut = \Illuminate\Support\Carbon::parse($created_at)->diffForHumans();
                                            echo $lastTimeLoggedOut;
                                            ?>
                                        </span>
                                        </div>
                                        <div class="text">
                                            {{$comment->comment}}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </main>
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    @include('layoutsv2.footer')
</div>
@endsection
