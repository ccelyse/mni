@extends('layoutsv2.master')

@section('title', 'MN-Muzika Nyarwanda Ipande| Urutonde rw\'Ukwezi|MNI Selection')

@section('content')
<style>
    .embed__am-link{
        display: none !important;
    }
    .music-detail{
        position: relative;
        left: 30px;
    }
</style>
<div class="page">
    <!--
    ========================================================
                            HEADER
    ========================================================

    -->
    <header>
        @include('layoutsv2.upmenu')
    </header>

    <!--
    ========================================================
                             CONTENT
    ========================================================
    -->
    <main>
        <section class="well" style="padding-bottom: 100px;">
            <div class="container center">
                <div class="row">
                    <h1>Thank you for Donating <p style="font-size: 100px;margin-top: 40px;">&#x1F64F;</p></h1>
                </div>
            </div>
        </section>
    </main>
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    @include('layoutsv2.footer')
</div>
@endsection
