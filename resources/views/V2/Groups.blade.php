@extends('layoutsv2.master')

@section('title', 'Competition Group')

@section('content')
<style>
    .embed__am-link{
        display: none !important;
    }
    .music-detail{
        position: relative;
        left: 30px;
    }
    @media (max-width: 479px){
        #mobile_first_Add {
            top: 302px !important;
            position: relative;
            padding: 130px 0px !important;
            margin-top: 0px !important;
        }
    }
    #Off{
        display: none;
    }

</style>
<div class="page">
    <!--
    ========================================================
                            HEADER
    ========================================================

    -->
    <header>
        @include('layoutsv2.upmenu')
        <div class="camera_container">
            <div class="row">
                <div class="col-lg-6">
                    <div id="camera" class="camera_wrap">
                        @foreach($competition_slider as $sliders)
                        <div data-src="HomeSlider/{{$sliders->slider_photo}}">
                            <div class="camera_caption fadeIn">
                                <h3>“{{$sliders->slider_title}}”</h3>
                                <p>{{$sliders->slider_description}}</p>
{{--                                <div class="wr">--}}
{{--                                    <div class="link_wr"><a href="#mni_selection" class="btn btn_mod1">Vote</a>--}}
{{--                                        <a href="{{'Discover'}}" class="btn btn_mod2">Discover</a>--}}
{{--                                        <a href="{{'StakeHoldersLogin'}}" class="btn">Sign In</a>--}}
{{--                                    </div>--}}
{{--                                    <a href="{{'BroadcastV2'}}" class="btn">Listen Music</a>--}}
{{--                                    <a href="{{'StakeHoldersRegister'}}" class="btn">SIgn Up</a>--}}
{{--                                </div>--}}
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!--
    ========================================================
                             CONTENT
    ========================================================
    -->
    <main>
{{--        <section class="well3 well3__ins1" style="margin-top: 60px;" id="mobile_first_Add">--}}
{{--            <div class="container">--}}
{{--                <div class="col-md-12">--}}
{{--                    <a href="{{'SalesAndMarketing'}}">--}}
{{--                        <img src="front/images/MNIAdvertbannersize-.jpg" style="width: 100%">--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </section>--}}
        <section class="well3 well3__ins1" style="" id="promoted_song">
            <div class="container">
                <h3 class="center" style="margin-top: 50px;">{{$contestant_title}}</h3>
                    <ul data-player-id="player" class="music-list music-list_mod1 border">
                    <li data-src="audio/sound_1.mp3" class="stopped"></li>
                    <li data-src="audio/sound_1.mp3" class="mod1">
                        <table>
                            <thead>
                            <tr>
                                <th class="position">Pos</th>
                                {{--                                <th class="primary-color lw">LW</th>--}}
                                <th class="artist" style="text-align: left !important;">Group Name</th>
                                <th class="label" style="text-align: right !important;">Time Added</th>
{{--                                <th class="secondary-color" style="text-align: right !important;">Votes</th>--}}
                                <th class="secondary-color" style="text-align: right !important;">Action</th>
                                <th></th>
                            </tr>
                            </thead>
                        </table>
                    </li>
                    @foreach($groups as $index => $getmusicsmodern)
                        <li data-src="audio/sound_1.mp3">
                            <table>
                                <tbody>
                                <tr>
                                    <td class="secondary-color numbering">
                                        {{ $index+1 }}
                                    </td>
                                    {{--                                    <td class="lw">--}}
                                    {{--                                        <p class="fa-long-arrow-up secondary-color">3</p>--}}
                                    {{--                                    </td>--}}
                                    <td class="artist">
{{--                                        <a href="#" style="text-decoration: none;">--}}
{{--                                            <img src="Contestant/{{$getmusicsmodern->photo}}" style="width: 100px;" alt="" id="promoted_song_picture">--}}
{{--                                        </a>--}}
                                        <p class="secondary-color"><a href="#" style="text-decoration: none;">{{$getmusicsmodern->group_name}}</a></p>
                                    </td>

                                    <td class="secondary-color" style="color: #f18d04 !important;text-align: center;" id="promoted_song_picture">
                                        <?php
                                        $created_at =  $getmusicsmodern->created_at;
                                        $lastTimeLoggedOut = \Illuminate\Support\Carbon::parse($created_at)->diffForHumans();
                                        echo $lastTimeLoggedOut;
                                        ?>
                                    </td>
                                    <td class="secondary-color" style="color: #000 !important;">
                                       {{$getmusicsmodern->votesNumber_}}
                                    </td>
                                    <td>
                                        <a href="{{ route('V2.Contestants',['id'=> $getmusicsmodern->id])}}" id="{{$competition_Status}}" class="dlw-btn">View Contestant</a>
{{--                                        <div style="margin-right: 10px;">--}}
{{--                                            <span>--}}
{{--                                                <a href="{{$getmusicsmodern->youtube}}" target="_blank">--}}
{{--                                                    <i class="fa fa-youtube" aria-hidden="true" style="font-size: 30px;color:#1c706d;" id="promoted_song_icon"></i>--}}
{{--                                                </a>--}}
{{--                                            </span>--}}
{{--                                        </div>--}}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </li>
                    @endforeach

                </ul>

            </div>
        </section>
    </main>
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    @include('layoutsv2.footer')
</div>
@endsection
