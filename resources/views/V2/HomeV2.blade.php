@extends('layoutsv2.master')

@section('title', 'Home')

@section('content')
<style>
    .embed__am-link{
        display: none !important;
    }
    .music-detail{
        position: relative;
        left: 30px;
    }
    @media (max-width: 479px){
        /*#mobile_first_Add {*/
        /*    top: 160px !important;*/
        /*    position: relative;*/
        /*    padding: 135px 0px !important;*/
        /*    margin-top: 0px !important;*/
        /*}*/
        #mobile_first_Add {
            top: 0px !important;
            position: relative;
            padding: 0px 0px !important;
            margin-top: 295px !important;
        }
    }

</style>
<div class="page">
    <!--
    ========================================================
                            HEADER
    ========================================================

    -->
    <header>
        @include('layoutsv2.upmenu')
        <div class="camera_container">
            <div class="row">
                <div class="col-lg-6">
                    <div id="camera" class="camera_wrap">
                        @foreach($list_slider as $sliders)
                        <div data-src="HomeSlider/{{$sliders->slider_photo}}">
                            <div class="camera_caption fadeIn">
                                <h3>“{{$sliders->slider_title}}”</h3>
                                <p>{{$sliders->slider_description}}</p>
                                <div class="wr">
                                    <div class="link_wr"><a href="#mni_selection" class="btn btn_mod1">Vote</a>
{{--                                        <a href="{{'Discover'}}" class="btn btn_mod2">Discover</a>--}}
                                        <a href="{{'StakeHoldersLogin'}}" class="btn">Sign In</a>
                                    </div>
{{--                                    <a href="{{'BroadcastV2'}}" class="btn">Listen Music</a>--}}
                                    <a href="{{'StakeHoldersRegister'}}" class="btn">SIgn Up</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!--
    ========================================================
                             CONTENT
    ========================================================
    -->
    <main>
        <section class="well3 well3__ins1" style="margin-top: 60px;" id="mobile_first_Add">
            <div class="container">
                <div class="col-md-12">
                    <a href="https://mni.rw/Competition_Contestants?id=27">
{{--                        <img src="front/images/MNIAdvertbannersize-.jpg" style="width: 100%">--}}
                        <img src="frontend/assets/images/surpa 2nd.gif" style="width: 100%">
                    </a>
                </div>
            </div>
        </section>

        <section class="well3 well3__ins1" style="" id="mni_selection">
            <div class="container">
                <h3 class="center">MNI Selection Chart of  <?php echo $newfirstYear; ?></h3>
                <ul data-player-id="player" class="music-list music-list_mod1 border">
                    <li data-src="audio/sound_1.mp3" class="stopped"></li>
                    <li data-src="audio/sound_1.mp3" class="mod1">
                        <table>
                            <thead>
                            <tr>
                                <th class="position">Pos</th>
{{--                                <th class="primary-color lw">LW</th>--}}
                                <th class="artist">Artist</th>
                                <th class="title">Title</th>
                                <th class="label">Duration</th>
                                <th class="secondary-color">Votes</th>
                                <th class="secondary-color">Action</th>
                                <th></th>
                            </tr>
                            </thead>
{{--                            <tbody>--}}
{{--                            <tr>--}}
{{--                                <td class="secondary-color position"></td>--}}
{{--                                <td class="lw">--}}
{{--                                    <p class="fa secondary-color">1</p>--}}
{{--                                </td>--}}
{{--                                <td class="artist"><img src="https://livedemo00.template-help.com/wt_55566/images/page-3_img01.jpg" alt="">--}}
{{--                                    <p class="secondary-color">Justin Bieber</p>--}}
{{--                                </td>--}}
{{--                                <td class="title"><span class="name primary-color">What Do You Mean</span></td>--}}
{{--                                <td class="primary-color label">1232</td>--}}
{{--                                <td class="primary-color label">1232</td>--}}
{{--                                <td>--}}
{{--                                    <a href="#" class="dlw-btn">Vote Now</a>--}}
{{--                                    <div style="margin-right: 10px;">--}}
{{--                                        <iframe src="https://audiomack.com/embed/song/ishimwe-13/rwanda??background=white&color=1c7070" scrolling="no" width="73" height="30" scrollbars="no" frameborder="0">--}}
{{--                                        </iframe>--}}
{{--                                    </div>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            </tbody>--}}
                        </table>
                    </li>
                    @foreach($getmusicmodern as $index => $getmusicsmodern)
                        <li data-src="audio/sound_1.mp3">
                            <table>
                                <tbody>
                                <tr>
                                    <td class="secondary-color numbering">
                                        {{ $index+1 }} <span id="promoted_song_picture">/</span>
                                    </td>
{{--                                    <td class="lw">--}}
{{--                                        <p class="fa-long-arrow-up secondary-color">3</p>--}}
{{--                                    </td>--}}
                                    <td class="artist">
                                        <a href="{{ route('StakeHoldersProfileV2',['id'=> $getmusicsmodern->user_id])}}" style="text-decoration: none;">
                                            <img src="SongImages/{{$getmusicsmodern->song_cover_picture}}" style="width: 100px;" alt="" id="promoted_song_picture">
                                        </a>
                                        <a href="{{ route('StakeHoldersProfileV2',['id'=> $getmusicsmodern->user_id])}}" style="text-decoration: none;">
                                            <p class="secondary-color">{{$getmusicsmodern->song_artist}}</p>
                                        </a>
                                    </td>
                                    <td class="title">
                                        <span class="name secondary-color" style="color: #f18d04 !important;"><a href="{{$getmusicsmodern->song_youtube_link}}">{{$getmusicsmodern->song_name}}</a></span>
                                    </td>
                                    <td class="secondary-color" style="color: #f18d04 !important;" id="promoted_song_picture">
                                        <?php
                                        $created_at =  $getmusicsmodern->created_at;
                                        $lastTimeLoggedOut = \Illuminate\Support\Carbon::parse($created_at)->diffForHumans();
                                        echo $lastTimeLoggedOut;
                                        ?>
                                    </td>
                                    <td class="secondary-color" style="color: #000 !important;">
                                        <?php
                                        //                                                        $song_id = $getmusicsmodern->id;
                                        //                                                        $votes = \App\Votes::where('voter_status','SUCCESSFUL')->select(DB::raw('count(id) as votes'))
                                        //                                                            ->where('song_id',$song_id)
                                        //                                                            ->where('vote','1')
                                        //                                                            ->value('votes');
                                        //                                                        $newvotes = $votes - 1;
                                        $votesbe = $getmusicsmodern->votesNumber;
                                        $newvotes = $votesbe - 1;
                                        if($newvotes < 0){
                                            echo "0";
                                        }else{
                                            echo "$newvotes";
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <a href="{{ route('VoteNowV2',['id'=> $getmusicsmodern->id])}}" id="{{$check_status}}" class="dlw-btn">Vote Now</a>
                                        <div style="margin-right: 10px;">
{{--                                            <span><a href="{{$getmusicsmodern->song_youtube_link}}" target="_blank">--}}
{{--                                                    <i class="fa fa-youtube" aria-hidden="true" style="font-size: 30px;color:#1c706d;" id="promoted_song_icon"></i>--}}
{{--                                                </a>--}}
{{--                                            </span>--}}
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </li>
                    @endforeach

                </ul><a href="{{'Charts'}}" class="btn2">view all</a>

            </div>
        </section>
        <section class="well3 well3__ins1" style="">
            <div class="container">
                <div class="col-md-12">
                    <a href="{{'SalesAndMarketing'}}">
                        <img src="front/images/MNIAdvertbannersize-.jpg" style="width: 100%">
                    </a>
                </div>
            </div>
        </section>
        <section class="well" style="padding-bottom: 15px;padding-top: 0px !important;">
            <div class="container center">
                <h2>Music StakeHolders</h2>
                <div class="row">
                    <div class="owl-stake">
                        <div class="item">
                            @foreach($list_stake as $list)
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="custom_wr"><img src="HomeSlider/{{$list->slider_photo}}" alt="">
                                    <div class="wr">
{{--                                        <h3><span>Diana Krall</span></h3>--}}
                                        <h3>“{{$list->slider_title}}”</h3>
                                        <h6>{{$list->slider_moto}}</h6>
                                        <p>{{$list->slider_moto}}</p>
                                        <hr>
                                        <a href="{{ route('StakeHolders',['id'=> $list->id])}}" class="btn">read more</a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="well3 well3__ins1" style="">
            <div class="container">
                <div class="col-md-12">
                    <a href="{{'SalesAndMarketing'}}">
                        <img src="front/images/MNIAdvertbannersize-.jpg" style="width: 100%">
                    </a>
                </div>
            </div>
        </section>
        <section class="well3 bg-primary" style="padding-top: 100px; !important;">
            <div class="container center">
                <div class="row">
                    <div data-wow-delay="0.1s" class="col-xs-4 col-sm-4 wr wow fadeInUp">
{{--                        <img src="https://livedemo00.template-help.com/wt_55566/images/page-1_img07.png" alt="">--}}
                        <h2>{{$music_Stake}}</h2>
                        <h3>
                            <span>Music StakeHolders</span>
                        </h3>
                        <hr>
                    </div>

                    <div data-wow-delay="0.3s" class="col-xs-4 col-sm-4 wr wow fadeInUp">
{{--                        <img src="https://livedemo00.template-help.com/wt_55566/images/page-1_img08.png" alt="">--}}
                        <h2>{{$songs_}}</h2>
                        <h3><span>Song Displayed</span></h3>
                        <hr>
                    </div>
                    <div data-wow-delay="0.3s" class="col-xs-4 col-sm-4 wr wow fadeInUp">
{{--                        <i class="fa fa-money-bill-alt"></i>--}}
                        <h2>{{$funds}} Rwf</h2>
                        <h3><span>Raised Fund</span></h3>
                        <hr>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    @include('layoutsv2.footer')
</div>
@endsection
