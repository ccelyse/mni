@extends('layoutsv2.master')

@section('title', 'MN-Muzika Nyarwanda Ipande| Urutonde rw\'Ukwezi|MNI Selection')

@section('content')
<style>
    .embed__am-link{
        display: none !important;
    }
    .music-detail{
        position: relative;
        left: 30px;
    }
     .spb-asset-content p{
         color:#fff !important;
     }
    .title-wrap h3{
        color: #fff !important;
    }
    .chart-page .chart-detail-header__chart-name {
        padding-top: 50px !important;
    }
    .modal-backdrop{
        position: relative !important;
    }
    /*.modal-backdrop.show {*/
    /*opacity: .5;*/
    /*}*/

    .modal-dialog {
        max-width: 800px !important;
        margin: 30px auto;
    }
    iframe{
        width: 100% !important;
    }

    .single-blog-post a,.single-blog-post p{
        color: #fff;
    }
    .bottom-title{
        font-size: 24px;
        line-height: 34px;
        color: #fff;
        padding: 45px 0 35px;
    }
    .title-box{
        display: inline-block;
        line-height: 35px;
        font-size: 13px;
        text-transform: uppercase;
        color: #fff;
        padding: 0 30px;
        border: 1px solid rgba(255,255,255,0.2);
        border-radius: 3px;
    }
    .alert-success {
        color: #155724;
        background-color: #d4edda;
        border-color: #c3e6cb;
        width: 100%;
        text-align: center;
        margin: 15px;
    }
    .theme-title-three p {
        font-size: 17px !important;
        line-height: 35px !important;
        color: #000;
        padding-top: 10px !important;
    }
    .page-title{
        color: #fff !important;
    }
    .our-service .our-history .text-wrapper {
        padding: 80px 0 80px 0px !important;
    }
    .theme-list-item li {
        color: #fff;
    }
    .theme-list-item li i, .theme-list-item li span {
        color: #fff;
    }
    .our-service .our-history .text-wrapper p {
        color: #000 !important;
    }
    #theme-banner-five {
        /* background: linear-gradient( 145deg, rgb(132,0,252) 0%, rgb(17,222,244) 100%); */
        /* background: linear-gradient( 145deg, rgb(5, 54, 50) 0%, rgb(28, 112, 111) 100%); */
        position: relative;
        z-index: 5;
        background: url(./front/images/audience-band-celebration-2240771-compressed.jpg) !important;
        background-size: cover !important;
    }
    .signUp-page .agreement-checkbox input[type="checkbox"] {
        display: inline-block;
    }
    .signUp-page .agreement-checkbox label:before {
        /*display: none;*/
    }
    .signUp-page form ::placeholder {color: #bcbcbc;opacity: 1; /* Firefox */}
    .signUp-page form :-ms-input-placeholder {color: #bcbcbc;}
    .signUp-page form ::-ms-input-placeholder {color: #bcbcbc;}
    .signUp-page form .input-group {position: relative;z-index: 1; margin-bottom: 35px;}
    .signUp-page form .input-group input, .input-group textarea {
        width: 100%;
        border: 1px solid #e0e0e0;
        border-radius: 5px;
        background: transparent;
        height: 60px;
        font-size: 18px;
        color: #454545;
        position: relative;
        padding: 0 20px;
    }
    .signUp-page form .input-group select {
        width: 100%;
        border: 1px solid #e0e0e0;
        border-radius: 5px;
        background: transparent;
        height: 60px;
        font-size: 18px;
        color: #454545;
        position: relative;
        padding: 0 20px;
    }
    .signUp-page form .input-group label {
        font-weight: normal;
        font-size: 15px;
        color: #bcbcbc;
        line-height: 60px;
        position: absolute;
        left:20px;
        top:0;
        z-index: -1;
        margin: 0;
        transition: all 0.3s ease-in-out;
    }
    .signUp-page form .input-group input:focus ~ label,
    .signUp-page form .input-group input:valid ~ label {top: -12px;}
    .signUp-page form .input-group input:focus {border-color: #393939;}
    .signUp-page form .input-group input:focus,
    .signUp-page form .input-group input:valid {padding-top: 18px;}
    .signUp-page .selectize-control {width: 100%;}
    .signUp-page .input-group .selectize-input {
        width: 100%;
        line-height: 60px;
        height: 60px;
        border-radius: 5px;
        border: none;
        border: 1px solid #e0e0e0;
        box-shadow: none;
        outline: none;
        padding: 0 20px;
        background: transparent;
    }
    .signUp-page .input-group .selectize-input input {font-size: 15px;}
    .signUp-page .selectize-input .item {font-size: 18px;color: #454545;}
    .signUp-page .selectize-dropdown {
        background: #fff;
        border: 1px solid #e9e9e9;
        border-top: none;
        box-shadow: 0px 50px 100px 0px rgba(229, 232, 235, 0.2);
        cursor: pointer;
    }
    .signUp-page .selectize-dropdown .option {font-size: 14px;color: #1a1a1a;line-height: 22px;}
    .signUp-page .selectize-dropdown .option:hover,
    .signUp-page .selectize-dropdown .active {color: #fff; background:#1a1a1a; }
    .signUp-page .selectize-control.single .selectize-input:after {right: 8px;}
    .signUp-page .acType-content h4 {font-size: 28px;}
    .signUp-page .acType-content p {padding: 8px 0 25px;}
    .signUp-page .acType-content .acType-list {margin: 0 -22px;}
    .signUp-page .acType-content .acType-list li {float: left;padding: 0 22px;}
    .signUp-page .acType-content .acType-list li>div {position: relative;}
    .signUp-page .acType-content .acType-list li>div input {
        position: absolute;
        opacity: 0;
        z-index: 1;
        width: 100%;
        height: 100%;
        cursor: pointer;
    }
    .signUp-page .acType-content .acType-list li>div label {
        position: relative;
        font-size: 16px;
        line-height: 15px;
        color: rgba(47,52,62,0.8);
        cursor: pointer;
        padding-left: 25px;
        margin: 0 0 15px;
        transition: all 0.1s ease-in-out;
    }
    .signUp-page .acType-content .acType-list li>div label:before {
        content: '';
        width: 14px;
        height: 14px;
        border-radius: 50%;
        border: 2px solid #d6d6d6;
        position: absolute;
        left:0;
        top:0;
        transition: all 0.1s ease-in-out;
    }
    .signUp-page .acType-content .acType-list li>div input:checked + label:before {border-color: #FF3A46;}
    .signUp-page .acType-content .acType-list li>div input:checked + label {color:rgba(47,52,62,1); }
    .signUp-page .acType-content {border-bottom: 1px solid #e0e0e0;padding-bottom: 65px;}
    .signUp-page .agreement-checkbox label {
        position: relative;
        font-size: 16px;
        color: rgba(47,52,62,0.6);
        cursor: pointer;
        padding-left: 22px;
        margin: 30px 0 35px;
        transition: all 0.1s ease-in-out;
    }
    .signUp-page .agreement-checkbox input[type="checkbox"] {display: none;}
    .signUp-page .agreement-checkbox label:before {
        content: '';
        width: 12px;
        height: 12px;
        line-height: 10px;
        border-radius: 2px;
        border: 1px solid #d5d5d5;
        font-size: 8px;
        text-align: center;
        position: absolute;
        left:0;
        top:6px;
        transition: all 0.1s ease-in-out;
    }
    .signUp-page .agreement-checkbox input[type="checkbox"]:checked + label:before {
        content: "";
        font-family: 'font-awesome';
        background: #373737;
        color: #fff;
        border-color:  #373737;
    }
    .signUp-page .agreement-checkbox input[type="checkbox"]:checked + label {color:#373737; }
    .signUp-page.signUp-minimal .agreement-checkbox label {margin-top: 0;}
    .signUp-page.signUp-minimal form .line-button-one {width: 100%;border-radius: 5px;}
    .signin-form-wrapper .title-area {margin-bottom: 50px;}
    .signUp-standard .signin-form-wrapper #login-form .input-group input {
        border: none;
        border-radius: 0;
        border-bottom: 1px solid #e0e0e0;
        padding-left: 0;
    }
    .signUp-standard .signin-form-wrapper #login-form .input-group label {left: 0;}
    .signUp-standard .signin-form-wrapper #login-form {padding-right: 170px;}
    #login-form .agreement-checkbox label {margin: 0;}
    #login-form .agreement-checkbox a {font-size: 16px;color: rgba(47,52,62,0.6);}
    #login-form .agreement-checkbox a:hover {color: #212121;}
    #login-form .agreement-checkbox  {margin-bottom: 55px;}
    .signin-form-wrapper .signUp-text {font-size: 20px;padding-top: 40px;}
    #login-form button {text-transform: uppercase;}
    .signUp-minimal .signin-form-wrapper {
        max-width: 520px;
        margin: 0 auto;
        border: 1px solid #e0e0e0;
        padding: 70px 65px 35px;
        position: relative;
    }
    .signUp-minimal .signin-form-wrapper .signUp-text {font-size: 17px;padding-top: 35px;}
    /*.textareaElement{*/
    /*    height: 200px !important;*/
    /*}*/
   .line-button-one {
        text-transform: capitalize;
        line-height: 48px;
        border-style: solid;
        border-width: 2px;
        border-radius: 30px;
        text-align: center;
        padding: 0 35px;
        min-width: 180px;
       border-color: #1c706e;
       margin-top: 15px;
    }
</style>
<script
        src="https://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>
<script type="application/javascript">
    function ModeOfPayment(that) {
        if (that.value == "Bank account") {
            document.getElementById("Bank").style.display = "block";
            document.getElementById("MobileMoney").style.display = "none";
        } else {
            document.getElementById("MobileMoney").style.display = "block";
            document.getElementById("Bank").style.display = "none";
        }
    }
    $(function(){
        //on keypress
        $('#confirm_artist_password').keyup(function(e){
            //get values
            var artist_password = $('#artist_password').val();
            var confirm_artist_password = $(this).val();

            //check the strings
            if(artist_password == confirm_artist_password){
                //if both are same remove the error and allow to submit
                $('.error').text('Password are matching');
            }else{
                //if not matching show error and not allow to submit
                $('.error').text('Password not matching');
            }
        });

        //jquery form submit
    });
</script>
<div class="page">
    <!--
    ========================================================
                            HEADER
    ========================================================

    -->
    <header>
        @include('layoutsv2.upmenu')
    </header>

    <!--
    ========================================================
                             CONTENT
    ========================================================
    -->
    <main>
        <section class="well" style="padding-bottom: 100px;padding-top: 20px !important;">
            <div class="container center">
{{--                <h2>Music StakeHolders</h2>--}}
                <div class="row">
                    <div class="signUp-page signUp-minimal pt-50 pb-100" style="padding-top: 120px;">
                        <div class="shape-wrapper">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div> <!-- /.shape-wrapper -->
                        <div class="signin-form-wrapper">
                            <div class="title-area text-center">
                                <h3>Kwinjira.</h3>
                            </div> <!-- /.title-area -->

                            <form class="form-horizontal form-simple"  id="login-form" method="POST" action="{{ route('SignIn_') }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <div class="col-12">
                                        <div class="input-group">
                                            <input type="email" value="{{ old('email') }}" name="email" required>
                                            <label>Email</label>
                                        </div> <!-- /.input-group -->
                                    </div> <!-- /.col- -->
                                    <div class="col-12">
                                        <div class="input-group">
                                            <input type="password" name="password" required>
                                            <label>Password</label>
                                        </div> <!-- /.input-group -->
                                    </div> <!-- /.col- -->
                                </div> <!-- /.row -->
                                {{--<div class="agreement-checkbox d-flex justify-content-between align-items-center">--}}
                                {{--<div>--}}
                                {{--<input type="checkbox" id="remember">--}}
                                {{--<label for="remember">Remember Me</label>--}}
                                {{--</div>--}}
                                {{--<a href="#">Forget Password?</a>--}}
                                {{--</div>--}}
                                <button class="line-button-one">Kwinjira</button>
                            </form>
                            <p class="signUp-text text-center"><a href="{{'ForgetPassword'}}" style="color: #007bff;">Forgot Password?</a></p>
                            <p class="signUp-text text-center">Nta Konti ufite? <a href="{{'StakeHoldersRegister'}}" style="color: #007bff;">Fungura Konti</a> Ubungubu.</p>
                        </div> <!-- /.sign-up-form-wrapper -->
                    </div> <!-- /.signUp-page -->
                </div>
            </div>
        </section>
    </main>
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    @include('layoutsv2.footer')
    <script type="application/javascript">
        $('#reset').click(function () {
            if (!$('#confirm').is(':checked')) {
                alert('not checked');
                return false;
            }
        });
        $(document).on('change', '.artist_category', function() {
            var artist_category =$('.artist_category').val();
            if(artist_category == "Musicians"){
                document.getElementById("mode_of_payment_website").style.display = "block";
            }else{
                document.getElementById("mode_of_payment_website").style.display = "none";
            }

        });
        $('textarea').on('paste input', function () {
            if ($(this).outerHeight() > this.scrollHeight){
                $(this).height(1)
            }
            while ($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))){
                $(this).height($(this).height() + 1)
            }
        });
    </script>
</div>
@endsection
