@extends('layoutsv2.master')

@section('title', "Check competition vote")

@section('content')
    <link rel="stylesheet" id="compiled.css-css" href="https://z9t4u9f6.stackpathcdn.com/wp-content/themes/mdbootstrap4/css/compiled-4.15.0.min.css?ver=4.15.0" type="text/css" media="all">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" type="text/css" media="all">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <style>

    .embed__am-link{
        display: none !important;
    }
    .music-detail{
        position: relative;
        left: 30px;
    }
    .spb-asset-content p{
        color:#fff !important;
    }
    .title-wrap h3{
        color: #fff !important;
    }
    .chart-page .chart-detail-header__chart-name {
        padding-top: 50px !important;
    }
    .modal-backdrop{
        position: relative !important;
    }
    /*.modal-backdrop.show {*/
    /*opacity: .5;*/
    /*}*/

    .modal-dialog {
        max-width: 800px !important;
        margin: 30px auto;
    }
    iframe{
        width: 100% !important;
    }
    #contact-form button {
        /*margin-top: 35px;*/
        width: 100%;
    }
    .social-icon li a i {
        bottom: 0px;
        position: relative;
    }
    #contact-form .form-group input, #contact-form .form-group textarea, #contact-form .form-group select {
        border: 1px solid #fff !important;
        color: #fff !important;
    }
    .single-blog-post a,.single-blog-post p{
        color: #fff;
    }
    .bottom-title{
        font-size: 24px;
        line-height: 34px;
        color: #fff;
        padding: 45px 0 35px;
    }
    .title-box{
        display: inline-block;
        line-height: 35px;
        font-size: 13px;
        text-transform: uppercase;
        color: #fff;
        padding: 0 30px;
        border: 1px solid rgba(255,255,255,0.2);
        border-radius: 3px;
    }
    .alert-success {
        color: #155724;
        background-color: #d4edda;
        border-color: #c3e6cb;
        width: 100%;
        text-align: center;
        margin: 15px;
    }
    .solid-inner-banner .page-title {
        font-size: 22px !important;
        padding-bottom: 0px !important;
        text-align: center;
    }
    .voteshare li a {
        width: 40px;
        line-height: 36px;
        border: 2px solid #fff !important;
        border-radius: 50%;
        text-align: center;
        font-size: 18px;
        color: #fff !important;
        margin-right: 5px;
    }
    .contact-us-section .contact-info ul li a {
        height: 40px;
    }
    #contact-form .form-group input, #contact-form .form-group textarea, #contact-form .form-group select {
        background: #15605d !important;
    }
    .page-title{
        color: #1c706e !important;
    }
    .card {
        background-color: transparent !important;
        /*background-color: #0f423f !important;*/
    }
    .md-tabs {
        position: relative;
        z-index: 1;
        padding: .7rem;
        margin-right: 1rem;
        margin-bottom: -20px;
        margin-left: 1rem;
        background-color: #1b706f;
        border: 0;
        border-radius: .25rem;
        -webkit-box-shadow: 0 5px 11px 0 rgba(0,0,0,0.18), 0 4px 15px 0 rgba(0,0,0,0.15);
        box-shadow: 0 5px 11px 0 rgba(0,0,0,0.18), 0 4px 15px 0 rgba(0,0,0,0.15);
        margin-top: 25px;
    }
    .md-tabs .nav-link.active, .md-tabs .nav-item.open .nav-link {
        color: #fff;
        background-color: rgba(0,0,0,0.2);
        border-radius: .25rem;
        -webkit-transition: all 1s;
        transition: all 1s;
    }
    .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
        /*color: #495057;*/
        /*background-color: #fff;*/
        border-color: #1b706f;
    }
    .nav-tabs .nav-item {
        margin-bottom: -1px;
        margin: 0 auto;
    }
    /*#Checked{*/
    /*    display: none;*/
    /*}*/

    /*------------------- Contact us -------------------*/
    #contact-form .form-group {position: relative;margin-bottom: 22px;padding: 10px}
    #contact-form .form-group input,#contact-form .form-group textarea,#contact-form .form-group select {
        border: 1px solid #ebebeb;
        width: 100%;
        max-width: 100%;
        color: #989ca2;
        background: transparent;
    }
    #contact-form .form-group input:focus,#contact-form .form-group select:focus,#contact-form .form-group textarea:focus {border-color: #545454;}
    #contact-form .form-group ::placeholder {color: #989ca2;;opacity: 1;}
    #contact-form .form-group :-ms-input-placeholder {color: #989ca2;;}
    #contact-form .form-group ::-ms-input-placeholder {color: #989ca2;;}
    #contact-form .form-group input,#contact-form .form-group select {height: 60px;padding: 0 25px;}
    #contact-form .form-group textarea {
        height: 190px;
        max-height: 190px;
        resize:none;
        padding: 20px 25px;
    }
    #contact-form .form-group .help-block {
        position: absolute;
        left: 0;
        bottom: -12px;
        font-size: 15px;
        line-height: 20px;
        color: #fff;
        padding: 0 15px;
        border-radius: 3px;
        box-shadow: 0px 10px 25px 0px rgba(123,147,171,0.15);
    }
    #contact-form .form-group .help-block li {position: relative;}

    #contact-form .form-group .help-block li:before {
        content: '';
        font-family: 'font-awesome';
        position: absolute;
        top:-12px;
        left:0;
    }
    #contact-form button {
        margin-top: 35px;
        background: #1c706f;
        text-transform: capitalize;
        text-align: center;
        font-size: 18px;
        color: #fff;
        line-height: 50px;
        padding: 0 40px;
        position: relative;
        z-index: 1;
    }
    /*.contact-us-section .contact-info {padding-left: 100px;}*/
    .contact-us-section .contact-info .title {
        font-family: 'CircularStdmed';
        font-size: 42px;
        line-height: 55px;
        color: #3e3e3e;
        margin: -8px 0 25px;
    }
    .contact-us-section .contact-info p {font-size: 20px;color: #798598;}
    .contact-us-section .contact-info .call {
        font-size: 27px;
        color: #3e3e3e;
        margin: 25px 0 40px;
    }
    .contact-us-section .contact-info .call:hover {text-decoration: underline;}
    .contact-us-section .contact-info ul li {display: inline-block;}
    .contact-us-section .contact-info ul li a {
        width: 40px;
        line-height: 36px;
        border: 2px solid #1c706f;
        border-radius: 50%;
        text-align: center;
        font-size: 18px;
        color: #1c706f;
        margin-right: 5px;
    }
    .contact-us-section .contact-info ul li a:hover {color: #fff;}
    #google-map {height: 700px;}
    #google-map-two {margin: 170px 70px 0;height: 625px;}
    #google-map-three {height: 100%;}
    .map-canvas {height: 100%;}
    #contact-form.form-style-two .form-group input,
    #contact-form.form-style-two .form-group textarea {
        border: none;
        border-bottom: 1px solid #ebebeb;
        padding-left: 0;
        padding-right: 0;
    }
    #contact-form.form-style-two .form-group input:focus,#contact-form.form-style-two .form-group textarea:focus {border-bottom-color: #545454;}
    .contact-address-two {
        text-align: center;
        padding: 250px 0 150px;
    }
    .contact-address-two .theme-title-one .upper-title {color: rgba(147,155,169,0.5);}
    .contact-address-two .theme-title-one {padding-bottom: 50px;}
    .contact-address-two .address-block {padding-top: 40px;}
    .contact-address-two .address-block .icon-box {display: inline-block;height: 70px;}
    .contact-address-two .address-block h5 {font-size: 24px;padding: 18px 0 20px;}
    .contact-address-two .address-block p,.contact-address-two .address-block p a {color: #939ba9;}
    .contact-address-two .address-block ul li {display: inline-block;margin: 10px 8px 0;}
    .contact-address-two .address-block ul li a {font-size: 20px;color: #d3d3d3;}
    .contact-minimal .inner-wrapper {background: #2f2f2f;padding: 60px 15px 80px 100px;}
    .contact-minimal .inner-wrapper .contact-form {max-width: 585px;}
    #contact-form.form-style-three .form-group ::placeholder {color: #fff;;opacity: 1;}
    #contact-form.form-style-three .form-group :-ms-input-placeholder {color: #fff;;}
    #contact-form.form-style-three .form-group ::-ms-input-placeholder {color: #fff;;}
    #contact-form.form-style-three .form-group input,#contact-form.form-style-three .form-group textarea {
        border: none;
        border-bottom: 2px solid #ffffff;
        padding-left: 0;
        padding-right: 0;
        color: #fff;
    }
    .form-style-three .send-button {
        font-family: 'CircularStdmed';
        width: 193px;
        line-height: 51px;
        border: 2px solid #fff;
        font-size: 17px;
        color: #fff;
        background: transparent;
    }
    body .theme-button-two:before {
        content: '';
        position: absolute;
        top: 4px;
        right: 4px;
        bottom: 4px;
        left: 4px;
        border: 1px solid #fff;
        opacity: 0;
    }
    .md-tabs {
        position: relative;
        z-index: 1;
        padding: .7rem;
        margin-right: 1rem;
        margin-bottom: -20px;
        margin-left: 1rem;
        background-color: #1b706f;
        border: 0;
        border-radius: .25rem;
        -webkit-box-shadow: 0 5px 11px 0 rgba(0,0,0,0.18), 0 4px 15px 0 rgba(0,0,0,0.15);
        box-shadow: 0 5px 11px 0 rgba(0,0,0,0.18), 0 4px 15px 0 rgba(0,0,0,0.15);
        margin-top: 25px;
    }
    .md-tabs .nav-link.active, .md-tabs .nav-item.open .nav-link {
        color: #fff;
        background-color: rgba(0,0,0,0.2);
        border-radius: .25rem;
        -webkit-transition: all 1s;
        transition: all 1s;
    }
    .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
        /*color: #495057;*/
        /*background-color: #fff;*/
        border-color: #1b706f;
    }
    .nav-tabs .nav-item {
        margin-bottom: -1px;
        margin: 0 auto;
    }
    @media (max-width: 479px){
        .well3__ins1 {
            padding-top: 0px !important;
        }
    }
</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="page">
    <!--
    ========================================================
                            HEADER
    ========================================================

    -->
    @include('layoutsv2.upmenu')
    <!--
    ========================================================
                             CONTENT
    ========================================================
    -->
    <main>
        <section class="well3 well3__ins1" style="margin-top: 60px;">
            <div class="container">
                @if (session('success'))
                    <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                        {{ session('success') }}
                    </div>
                @endif
                    <div class="solid-inner-banner">
                        <div class="bg-shape-holder">
                            <span class="big-round-one wow fadeInLeft animated" data-wow-duration="3s"></span>
                            <span class="big-round-two wow fadeInLeft animated" data-wow-duration="3s"></span>
                            <span class="big-round-three wow fadeInLeft animated" data-wow-duration="3s"></span>
                            <span class="shape-one"></span>
                            <span class="shape-two"></span>
                            <img src="front/images/shape/shape-32.svg" alt="" class="shape-three">
                            <span class="shape-four"></span>
                        </div>
                        <h2 class="page-title" id="page-titles"><strong>REBA UBUTUMWA BWA MOBILE MONEY UTANGE FINANCIAL TRANSACTION</strong></h2><br><br>
                        <div class="col-lg-12" >
                            <div class="contact-form" style="padding-bottom: 15px">
                                <form class="form-horizontal form-simple" id="contact-form" method="POST" action="{{ url('ValidateVoteCheck') }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div id="messages_1" style="display: none"></div>
                                    <div class="controls">
                                        <div class="form-group">
                                            <input type="text" name="checkvote" placeholder="34899625669" required>
                                        </div>
                                        <div class="form-group">
                                            <button type='submit' class="theme-button-two">Check Vote</button>
                                        </div>
                                    </div> <!-- /.controls -->
                                </form>
                            </div>
                        </div> <!-- /.contact-us-section -->
                    </div> <!-- /.contact-form -->
            </div> <!-- /.col- -->
        </section>
    </main>
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    @include('layoutsv2.footer')
{{--    <script src="front/vendor/jquery.2.2.3.min.js"></script>--}}

    <script>
        $(document).on('click', '#VoteNowUser_Contestant', function() {
            $('#VoteNowUser_Contestant').html('Voting Now..');

            var phonenumber = document.getElementById("phonenumber_1").value;
            var names = document.getElementById("names").value;
            var competition_id = document.getElementById("competition_id").value;
            var contestant_id = document.getElementById("contestant_id").value;
            var amount = document.getElementById("amount_contestant").value;

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "../api/ContestantPayment",
                data: {
                    'contestant_id': contestant_id,
                    'competition_id': competition_id,
                    'names': names,
                    'phonenumber': phonenumber,
                    'amount': amount,
                },
                dataType: 'json',
                success: function(response) {
                    $('#VoteNowUser_Contestant').html('Vote now');
                    JSON.stringify(response);
                    console.log(response);

                    jQuery('#messagestatus').show();
                    document.getElementById("Voting_response").style.display = "block";
                    $('#successmodal').html('<p style="text-align:center;">' + response.message + '</p>');
                    $('#successmessage').modal('show');
                    // document.getElementById("page-titles").style.display = "none";

                    $(function(){
                        setInterval(oneSecondFunction, 4000);
                    });
                    function oneSecondFunction() {
                        var url_string = location.href;
                        var urlink = new URL(url_string);
                        var id = urlink.searchParams.get("id");
                        $.ajax
                        ({
                            type: "POST",
                            url: "../api/CompetitionCallBackMomo",
                            data: {
                                transactionid:response.transactionid,
                            },
                            cache: false,
                            success: function (data) {
                                $('#messagestatus').html('<p style="text-align:center;">' + data.message + '</p>');
                                if(data.status == "SUCCESSFUL"){
                                    console.log(data);
                                    document.getElementById("loading").style.display = "none";
                                    document.getElementById("successmodal").style.display = "none";
                                    setTimeout(function() {
                                        window.location = 'CompetitionThankYou?id='+ id;
                                    }, 4000);

                                }else{
                                    console.log(data);
                                    // jQuery('#messagestatus').show();
                                    document.getElementById("messagestatus").style.display = "block";
                                    document.getElementById("loading").style.display = "block";
                                }

                            },
                            error: function(xhr, status, error) {
                                console.log(xhr.responseText);
                            }
                        });
                    }
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                }
            });
        });

        $(document).on('click', '#VoteNowUser_Card', function() {
            $('#VoteNowUser_Card').html('Voting with card..');

            var song_artist_card = document.getElementById("song_artist_card").value;
            var id_card = document.getElementById("id_").value;
            var song_name_card = document.getElementById("song_name_card").value;

            var amount_card = document.getElementById("amount_card").value;
            var first_name = document.getElementById("first_name").value;
            var last_name = document.getElementById("last_name").value;
            var email = document.getElementById("email").value;
            var country = document.getElementById("country").value;
            var city = document.getElementById("city").value;
            var phonenumber_card = document.getElementById("phonenumber_card").value;

            // alert(phonenumber);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "../api/VoteNowCard",
                data: {
                    'song_artist': song_artist_card,
                    'id': id_card,
                    'song_name': song_name_card,
                    'phonenumber': phonenumber_card,
                    'first_name': first_name,
                    'last_name': last_name,
                    'email': email,
                    'amount': amount_card,
                    'country': country,
                    'city': city,
                },
                dataType: 'json',
                success: function(response) {
                    // $('#VoteNowUser').html('Vote now');
                    JSON.stringify(response);
                    console.log(response);
                    setTimeout(function() {
                        window.location = response.message;
                    }, 2000);


                    // if(response.message == "We are sorry , you can not vote twice in less than 30 minutes"){
                    //     jQuery('#messagestatus').show();
                    //     document.getElementById("Voting_response").style.display = "block";
                    //     document.getElementById("messagestatus").style.display = "block";
                    //     $('#successmodal').html('<p style="text-align:center;">' + response.message + '</p>');
                    //     $('#successmessage').modal('show');
                    // }else{
                    //     jQuery('#messagestatus').show();
                    //     document.getElementById("Voting_response").style.display = "block";
                    //     $('#successmodal').html('<p style="text-align:center;">' + response.message + '</p>');
                    //     $('#successmessage').modal('show');
                    //
                    //     document.getElementById("page-titles").style.display = "none";
                    //     $(function(){
                    //         setInterval(oneSecondFunction, 4000);
                    //     });
                    //     function oneSecondFunction() {
                    //         var url_string = location.href;
                    //         var urlink = new URL(url_string);
                    //         var id = urlink.searchParams.get("id");
                    //         $.ajax
                    //         ({
                    //             type: "POST",
                    //             url: "../api/CallBackMomo",
                    //             data: {
                    //                 transactionid:response.transactionid,
                    //             },
                    //             cache: false,
                    //             success: function (data) {
                    //                 $('#messagestatus').html('<p style="text-align:center;">' + data.message + '</p>');
                    //                 if(data.status == "SUCCESSFUL"){
                    //                     document.getElementById("loading").style.display = "none";
                    //                     document.getElementById("successmodal").style.display = "none";
                    //                     var title = 'ThankYou';
                    //                     setTimeout(function() {
                    //                         window.location = 'ThankYou?id='+ id ;
                    //                     }, 2000);
                    //
                    //                 }else{
                    //                     // jQuery('#messagestatus').show();
                    //                     document.getElementById("messagestatus").style.display = "block";
                    //                     document.getElementById("loading").style.display = "block";
                    //                 }
                    //
                    //             },
                    //             error: function(xhr, status, error) {
                    //                 console.log(xhr.responseText);
                    //             }
                    //         });
                    //     }
                    // }
                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                }
            });
        });
    </script>
</div>
@endsection
