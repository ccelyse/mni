@extends('layoutsv2.master')

@section('title', $contestant_title)

@section('content')
<style>
    .embed__am-link{
        display: none !important;
    }
    .music-detail{
        position: relative;
        left: 30px;
    }
    @media (max-width: 479px){
        /*#mobile_first_Add {*/
        /*    top: 160px !important;*/
        /*    position: relative;*/
        /*    padding: 135px 0px !important;*/
        /*    margin-top: 0px !important;*/
        /*}*/
        .well {
            padding-top: 100px !important;
        }
    }
</style>
<div class="page">
    <!--
    ========================================================
                            HEADER
    ========================================================

    -->
    <header>
        @include('layoutsv2.upmenu')
    </header>

    <!--
    ========================================================
                             CONTENT
    ========================================================
    -->
    <main>
        <section class="well" style="padding-bottom: 100px;">
            <div class="container center">
                <div class="row">
                    @foreach($contestant as $data)
                    <h3>Murakoze gutora <strong>{{$data->names}} agize amajwi: {{$data->votesNumber_}} </strong></h3>
                    <h4><a href="{{$link}}" style="color:#000;    padding: 55px;" class="btn">Kanda hano wongere umutore</a></h4>
                    <h4><a href="{{ route('V2.Contestants',['id'=> $data->competition_id])}}" style="color:#000;">Reba uko ahagaze ku rutonde</a></h4>
                    @endforeach
                    <h4 style="padding-top: 22px;">Ugize ikibazo mu gutora tuvugishe kuri <a href="https://wa.me/250780008883" style="color: #3cbea5">+ 250780008883</a> (WhatsApp) cyangwa utwandikire kuri <a href="mailto:info@mni.rw">email:info@mni.rw</a></h4>
                </div>
            </div>
        </section>
    </main>
    <!--
    ========================================================
                                FOOTER
    ========================================================
    -->
    @include('layoutsv2.footer')
</div>
@endsection
