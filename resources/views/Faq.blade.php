@extends('layouts.newmaster')

@section('title', 'MNI-Ibibazo bikunzwe kubazwa')

@section('content')
    <style>
        .spb-asset-content p{
            color:#fff !important;
        }
        .title-wrap h3{
            color: #fff !important;
        }
        .chart-page .chart-detail-header__chart-name {
            padding-top: 50px !important;
        }
        .modal-backdrop{
            position: relative !important;
        }
        /*.modal-backdrop.show {*/
        /*opacity: .5;*/
        /*}*/

        .modal-dialog {
            max-width: 800px !important;
            margin: 30px auto;
        }
        iframe{
            width: 100% !important;
        }

        .single-blog-post a,.single-blog-post p{
            color: #fff;
        }
        .bottom-title{
            font-size: 24px;
            line-height: 34px;
            color: #fff;
            padding: 45px 0 35px;
        }
        .title-box{
            display: inline-block;
            line-height: 35px;
            font-size: 13px;
            text-transform: uppercase;
            color: #fff;
            padding: 0 30px;
            border: 1px solid rgba(255,255,255,0.2);
            border-radius: 3px;
        }
        .alert-success {
            color: #155724;
            background-color: #d4edda;
            border-color: #c3e6cb;
            width: 100%;
            text-align: center;
            margin: 15px;
        }
        .theme-title-three p {
            font-size: 17px !important;
            line-height: 35px !important;
            color: #000;
            padding-top: 10px !important;
        }
        .theme-title-three .title {
            font-size: 30px !important;
            font-weight: bold;
        }
        .theme-list-item li {
            color: #000;
        }
        .page-title{
            color: #fff !important;
        }
    </style>
    @include('layouts.newArtisttopmenu')

    <div class="solid-inner-banner">
        <div class="bg-shape-holder">
            <span class="big-round-one wow fadeInLeft animated" data-wow-duration="3s"></span>
            <span class="big-round-two wow fadeInLeft animated" data-wow-duration="3s"></span>
            <span class="big-round-three wow fadeInLeft animated" data-wow-duration="3s"></span>
            <span class="shape-one"></span>
            <span class="shape-two"></span>
            <img src="front/images/shape/shape-32.svg" alt="" class="shape-three">
            <span class="shape-four"></span>
        </div>
        <h2 class="page-title">Ibibazo bikunze kubazwa(F.A.Q)</h2>
    </div> <!-- /.solid-inner-banner -->


    <!--
        =============================================
            About Us Standard
        ==============================================
        -->
    <div class="about-us-standard">
        <div class="container">
            <div class="theme-title-three mb-170">
                <h2 class="title">1. Indiririmbo igira amajwi menshi iyo bigenze gute?</h2>
                <p>Indirimbo igira amajwi menshi ni iyagize amajwi menshi mu ndirimbo zagaragajwe. </p>
                <h2 class="title">2. Ukwezi kwa MNI kubarwa gute?</h2>
                <p>Ukwezi kwa MNI kubarwa kuva ku itariki ya mbere y’ukwezi gusanzwe kugezi ku itariki ya nyuma y’ukwezi gusanzwe.</p>

                <h2 class="title">3. Umunsi wo gutanga amafanga ku bahanzi?</h2>
                <p>Amafaranga ya MNI atangwa mu mpera z’icyumweru cya kabiri cy’ukwezi gukurikira.</p>

                <h2 class="title">4. Ni gute batora?</h2>
                <p>Uburyo bwa mbere: Kujya kurubuga rwa <a href="{{'/'}}">www.mni.rw</a> </p>

                <p><strong>Uburyo bwa mbere: Kujya kurubuga rwa <a href="{{'/'}}">www.mni.rw</a></strong></p>
                <ul class="theme-list-item pb-70">
                    <li><i class="fa fa-chevron-right" aria-hidden="true"></i>1.Jya muri browser wandike ahabugenewe www.mni.rw</li>
                    {{--<li><i class="fa fa-chevron-right" aria-hidden="true"></i>2.kanda ahanditse gutora ubundi uhitemo icyiciro (Gospel, Modern cg Gakondo), uhitemo indirimbo ushaka gushyigikira iri kurutonde</li>--}}
                    <li><i class="fa fa-chevron-right" aria-hidden="true"></i>2.Kanda ahanditse “Tora”</li>
                    <li><i class="fa fa-chevron-right" aria-hidden="true"></i>3.Niba ubona amahitamo yawe ariyo shyiramo nimero ya mobile money cyangwa ikarita ya banki ubundi wemeze</li>
                    <li><i class="fa fa-chevron-right" aria-hidden="true"></i>4.Niba wakoresheje mobile money, urahita ubona ubutumwa bugusaba kubikuza amafaranga 50 Rwf, wemeze ushyiremo na PIN ubwo uraba utoye iyo ndirimbo cyangwa ukurikize amabwiriza agenga kwishyura ukoresheje ikarita.</li>
                    <li><i class="fa fa-chevron-right" aria-hidden="true"></i>5.Kanda ahabanza urebe amajwi indirimbo imaze kugira</li>
                    <li><i class="fa fa-chevron-right" aria-hidden="true"></i>6.Niba wifuza gukomeza gushyigikira, subirmo inzira twavuze kuva 1-6</li>
                </ul>

                <p>N.B: ubu buryo busaba kuba uri kuri murandasi gusa butanga amakuru menshi arimo no kuba wakumva/kureba igihangano ugiye gutora mbere.</p>

                <p><strong>Uburyo bwa kabiri: USSD *611# </strong></p>

                <ul class="theme-list-item pb-70">
                    <li><i class="fa fa-chevron-right" aria-hidden="true"></i>1.Jya aho bahamagarira ukande *611#</li>
                    <li><i class="fa fa-chevron-right" aria-hidden="true"></i>2.  Niba ufite code y’igihangano hitamo ahanditse gutora ukoresheje code, niba utayizi hitamo ahanditse kureba code cyangwa niba wifuza kureba amajwi indirimbo igejeje hitamo kureba amajwi ubundi ushyiremo code y’indirimbo wifuza kureba.</li>
                    <li><i class="fa fa-chevron-right" aria-hidden="true"></i>3.Niba wahisemo gutora, andikamo code y’indirimbo</li>
                    <li><i class="fa fa-chevron-right" aria-hidden="true"></i>4.Emeza niba utibeshye</li>
                    <li><i class="fa fa-chevron-right" aria-hidden="true"></i>5.Urabona ubutumwa bugusaba kubikuza amafaranga 50 RWF</li>
                    <li><i class="fa fa-chevron-right" aria-hidden="true"></i>6.Shyiramo PIN yawe ubundi wemeze</li>
                    <li><i class="fa fa-chevron-right" aria-hidden="true"></i>7.Niba wifuza gukomeza gushyigikira, subirmo inzira twavuze kuva 1-6</li>
                </ul>

                <p>NB: Ubu buryo ntibukenera murandasi gusa butanga amakuru make.</p>

                <h2 class="title">5. Umuntu yemerewe gutora inshuro zingahe?</h2>
                <p>Wemere gutora inshoro zose wifuza mugihe igihangano wifuza gutora kikiri kurutonde.</p>


                <h2 class="title">6. Namenya gute ibihangano bishobora gutorwa?</h2>
                <p>Umenya ko igihangano  gishobora gutorwa usuye urubuga rwa www.mni.rw ukareba niba kiri kurutonde, cyangwa ugakanda *611# ubundi ukareba kurutonde niba kihaboneka. Ushobora kutuvugisha muburyo ubwo aribwo bwose bwakorohera tukagufasha kumenya niba gihari cyangwa ukavugisha nyir’igihangano ubwe.</p>
                <h2 class="title">7. Ese ibihanagano byose byemewe kujya kuri MNI Selection?</h2>
                <p>Uretse ibihangano byaciwe n’inzego zibishinzwe, ibindi byose byahanzwe n’abahanzi nyarwanda byemewe gushyirwa kuri MNI Selection.</p>


                <h2 class="title">8. Ninde wemerewe gutora</h2>
                <p>Umuntu wese ushobora kubona uburyo bwo gutora twavuze arabyemerewe.</p>

                <h2 class="title">9. Bishyura amafaranga angahe mu gutora</h2>
                <p>Buri tora ritwara amafaranga mirongo itanu ava kuri mobile money cyangwa kuri konti ya banki mugihe hifashishijwe ikarita.</p>


                <h2 class="title">10. Ninde utanga indirimbo kuri MNI Selection?</h2>
                <p>Ni umuhanzi wafunguye konti kuri MNI kandi akaba yarabyemerewe ndetse akaba afite uburenganzira bwo kubyaza inyungu icyo gihangano.</p>

                {{--<h2 class="title">11. Indirimbo imara igihe kingana gite kuri MNI?</h2>--}}
                {{--<p>Indirimbo imara ukwezi itorwa, ikaba ishobora kongera gusubiraho rimwe gusa mugihe nyir’igihangano abyifuje.</p>--}}

                {{--<h2 class="title">12. Ni indirimbo zingahe z’umuhanzi umwe zemerewe kujya kurutonde?</h2>--}}
                {{--<p>Umuhanzi umwe yemerewe gushyiraho igihangano kimwe gusa mugihe cy’ukwezi  akaba ashobora kongera kugisubizaho indi inshuro cyangwa agashyiraho ikindi mugihe abyifuje nabwo bikagenda uko.</p>--}}

                {{--<h2 class="title">13. Indirimbo ishyirwaho n’umuhanzi ryari?</h2>--}}
                {{--<p>Indirimbo ishyirwaho itariki iyo riyo yose y’ukwezi, ikahava uko kwezi kurangiye</p>--}}
                <p>N.B: Ibyo n’ibibazo twakusanyije ariko mugihe waba ufite ikindi kibazo kitagaragajwe haruguru watwandikira tukagufasha gusobanukirwa birushijeho.</p>
                <p>Email: info@mni.rw<br>Tel: +250 780 008 883<br>Social Network: @MNISelection
                </p>
            </div> <!-- /.theme-title-three -->
        </div> <!-- /.container -->
    </div> <!-- /.about-us-standard -->



    @include('layouts.newArtistfooter')
@endsection
