<style>
    .extensionlogo{
        float: right;
        color: #fff;
        font-weight: bold;
        font-size: 25px;
        position: relative;
        bottom: 55px;
        left: 42px;
    }
    #umuhanzibutton{
        padding: 7px;
        border-radius: 30px;
        bottom: 10px;
        background: #e1a34c;
    }
    #umuhanzibutton:hover{
        background: #fff;
        color:#266664 !important;
    }
</style>
<div class="theme-main-menu theme-menu-three">
    <div class="logo"><a href="{{'/'}}"><img src="front/images/logo/logo.png" alt="" style="width: 100px;"><span class="extensionlogo">MNI</span></a></div>
    <nav id="mega-menu-holder" class="navbar navbar-expand-lg">
        <div  class="position-relative ml-auto nav-container">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="flaticon-setup"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item active position-relative">
                        <a href="{{'/'}}" class="nav-link">Ahabanza</a>
                    </li>
                    {{--<li class="nav-item position-relative">--}}
                        {{--<a href="#" class="nav-link">Urutonde</a>--}}
                    {{--</li>--}}
                    <li class="nav-item position-relative">
                        <a href="{{'AboutUs'}}" class="nav-link">Abo turibo</a>
                    </li>
                    <li class="nav-item position-relative">
                        <a href="{{'Urutonde'}}" class="nav-link">Urutonde</a>
                    </li>
                    <li class="nav-item position-relative">
                        <a href="{{'ListArchives'}}" class="nav-link">Intonde zabanje</a>
                    </li>
                    <li class="nav-item position-relative" id="umuhanzibutton">
                        <a href="{{'Umuhanzi'}}" class="nav-link">Umuhanzi</a>
                    </li>
                    {{--<li class="nav-item position-relative">--}}
                        {{--<a href="{{'ContactUs'}}" class="nav-link">Twandikire</a>--}}
                    {{--</li>--}}
                    {{--<li class="login-button-two"><a href="{{'Nominee'}}">Tanga Indirimbo</a></li>--}}
                </ul>
            </div>
        </div> <!-- /.container -->
    </nav> <!-- /#mega-menu-holder -->
    <div class="header-right-widget">
        <ul class="social-icon" style="margin-top: 15px;">
            <li><a href="https://www.facebook.com/MNISelection/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="https://twitter.com/MNISelection"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <li><a href="https://www.instagram.com/MNISelection/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            <li><a href="https://www.youtube.com/channel/UC2kHMCoGVRYmaTeG5APFBPA?view_as=subscriber"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
        </ul>
    </div> <!-- /.header-right-widget -->
</div> <!-- /.theme-main-menu -->


