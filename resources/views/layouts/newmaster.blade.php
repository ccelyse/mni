<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="keywords" content="">
    <meta name="author" content="Elysee CONFIANCE">
    <meta name="description" content="">
    <meta name='og:image' content='front/images/logo/logo.png'>
    <!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- For Resposive Device -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- For Window Tab Color -->
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#233D63">
    <meta name="google-site-verification" content="ml9ZIe9TIeLmooQXFtKneyGc9FtbaQ1Gwl3CDThjN7E" />
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#233D63">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#233D63">
    <title>@yield('title')</title>
    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="56x56" href="front/images/logo/logo_icon.png">
    <!-- Main style sheet -->
    <link rel="stylesheet" type="text/css" href="front/css/style.css">
    <!-- responsive style sheet -->
    <link rel="stylesheet" type="text/css" href="front/css/responsive.css">



    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120820678-4"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-120820678-4');
    </script>

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5d4ec77077aa790be32e507a/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
    <![endif]-->
</head>

<body>
<div class="main-page-wrapper">
{{--    <script src="https://apps.elfsight.com/p/platform.js" defer></script>--}}
{{--    <div class="elfsight-app-dfa841c7-4929-4e41-a054-52dcd149c1ff"></div>--}}
@yield('content')