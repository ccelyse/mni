<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comp_votes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vote');
            $table->string('names');
            $table->string('competition_id');
            $table->string('contestant_id');
            $table->string('status');
            $table->string('payment_from');
            $table->string('payment_tx_ref');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comp_votes');
    }
}
