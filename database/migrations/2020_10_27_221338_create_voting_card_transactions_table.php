<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotingCardTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voting_card_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('phonenumber');
            $table->string('email');
            $table->string('city');
            $table->string('country');
            $table->string('amount');
            $table->string('tx_ref');
            $table->string('currency');
            $table->string('status');
            $table->string('payment_options');
            $table->string('voter_id');
            $table->string('song_name');
            $table->string('song_artist');
            $table->string('transaction_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voting_card_transactions');
    }
}
