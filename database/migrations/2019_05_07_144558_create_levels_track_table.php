<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLevelsTrackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('levels_track', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phonenumber');
            $table->string('user_level');
            $table->string('user_session');
            $table->string('user_choice');
            $table->string('category_track')->nullable();
            $table->string('song_nominated')->nullable();
            $table->string('artist_nominated')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('levels_track');
    }
}
