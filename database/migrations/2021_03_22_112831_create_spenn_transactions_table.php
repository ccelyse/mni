<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpennTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spenn_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phonenumber');
            $table->string('amount');
            $table->string('status');
            $table->string('vote')->nullable();
            $table->string('competition_id');
            $table->string('contestant_id');
            $table->string('voter_id');
            $table->string('transaction_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spenn_transactions');
    }
}
